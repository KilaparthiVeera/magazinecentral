package com.mirabel.magazinecentral.adapters;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCTextView;

import java.util.List;

/**
 * Created by venkat on 09/29/17.
 */
public class CustomDialogListAdapter extends ArrayAdapter {

    private final Context context;
    private LayoutInflater layoutInflater;
    private final List<String> values;
    private Constants.SelectionType selectionType;

    public CustomDialogListAdapter(Context ctx, int resource, List<String> objects, Constants.SelectionType selectionType) {
        super(ctx, resource, objects);

        this.context = ctx;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.values = objects;
        this.selectionType = selectionType;
    }

    static class ViewHolder {
        MCTextView textView;
    }

    @Override
    public int getCount() {
        if (values.size() <= 0)
            return 1;
        else
            return values.size();
    }

    @Override
    public String getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        try {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.custom_dialog_list_item, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.textView = (MCTextView) convertView.findViewById(R.id.textView);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if (values.size() <= 0) {
                viewHolder.textView.setText("No Data Found.");
                viewHolder.textView.setGravity(Gravity.CENTER);
            } else {
                viewHolder.textView.setText(getItem(position));

                /*if ((selectionType == Constants.SelectionType.PINTEREST_BOARD && position == PostMessageActivity.selectedBoardIndex)) {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.selected_board_color));
                    viewHolder.textView.setTypeface(null, Typeface.BOLD);
                } else {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.unselected_board_color));
                    viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                }*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
