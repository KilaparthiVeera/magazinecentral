package com.mirabel.magazinecentral.activities.viewissue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Interactivity;
import com.mirabel.magazinecentral.beans.viewissue.InteractivityItem;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCTextView;

import java.io.File;
import java.util.ArrayList;

public class MultimediaListActivity extends Activity {
    MultimediaAdapter listAdapter = null;
    ListView listView = null;
    public static ArrayList<Page> _multimediaPages = new ArrayList<>();
    private ArrayList<Page> downloadedItems = new ArrayList<>();
    Content content = null;

    public MultimediaListActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multimedia_list_activity);
        getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        try {
            content = (Content) getIntent().getSerializableExtra("content");
            listView = (ListView) findViewById(R.id.multimediaListView);

            if (!(_multimediaPages.size() > 0)) {
                TextView alertText = (TextView) findViewById(R.id.alertText);
                alertText.setTextColor(Color.BLACK);
                alertText.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                return;
            }

            if (GlobalContent.getInstance().getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateDownloading) {
                for (Page p : _multimediaPages) {
                    InteractivityItem interactivityItem = p.getActivities().get(0).items.get(0);
                    String vSource = interactivityItem.vSource;
                    if (vSource == null || vSource.isEmpty()) {
                        String itemPath = interactivityItem.source;
                        File f = new File(itemPath);
                        if (f.exists()) {
                            downloadedItems.add(p);
                        }
                    } else {
                        downloadedItems.add(p);
                    }
                }

                listAdapter = new MultimediaAdapter(this, downloadedItems);
            } else {
                listAdapter = new MultimediaAdapter(this, _multimediaPages);
            }

            listView.setAdapter(listAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void close(View v) {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down_out);
    }

    public void closeWithResult(Page p) {
        _multimediaPages.clear();
        Intent inten = new Intent();
        inten.putExtra("isDone", false);
        inten.putExtra("index", p.index);
        setResult(500, inten);
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_down_out);
    }

    public class MultimediaAdapter extends ArrayAdapter<Object> {

        LayoutInflater inflater = null;

        ArrayList<Page> pages = new ArrayList<>();

        public MultimediaAdapter(Context context, ArrayList<Page> pages) {
            super(context, R.layout.multimedia_list_item);
            inflater = LayoutInflater.from(context);
            this.pages.addAll(pages);
        }

        @Override
        public int getCount() {
            if (pages.size() == 0) {
                return 1;
            }
            return pages.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (pages.size() == 0) {
                convertView = inflater.inflate(R.layout.empty_multimedia_item, null);
                MCTextView tv = (MCTextView) convertView.findViewById(R.id.multimediaListItemText);
                tv.setText("Please wait until all the multimedia items downloads");
                tv.setTextColor(Color.BLACK);
                return convertView;
            }

            final Page _p = pages.get(position);
            final Interactivity interactivity = _p.getActivities().get(0);
            final int activityType = interactivity.type;
            final String activityTitle = interactivity.title;
            final InteractivityItem item = interactivity.items.get(0);
            final String videoPath = item.source;

            convertView = inflater.inflate(R.layout.multimedia_list_item, null);

            // we are displaying preview or thumbnail view only for local videos which are downloaded to device
            if ((activityType == 3 || activityType == 5) && (item.vSource == null || item.vSource.isEmpty())) {
                try {
                    ImageView videoPreviewImageView = (ImageView) convertView.findViewById(R.id.videoPreviewImageView);
                    Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
                    videoPreviewImageView.setImageBitmap(bmThumbnail);
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }

            ImageButton img = (ImageButton) convertView.findViewById(R.id.multimediaPlayButton);
            img.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String label;
                    if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                        label = String.format("/%s/%s/%s/android/%d/p/page%s", content.getPublisherId(), content.getProductName(), content.getName(), Constants.APP_TYPE, _p.imageName);
                    } else {
                        label = String.format("/%s/%s/%s/android/%d/l/page%s", content.getPublisherId(), content.getProductName(), content.getName(), Constants.APP_TYPE, _p.imageName);
                    }

                    String title = null;
                    if (item.text.length() > 0) {
                        title = item.text;
                    } else {
                        if (activityType == 4)
                            title = String.format("Audio on Page : %s", _p.imageName);
                        else
                            title = String.format("Video on Page : %s", _p.imageName);
                    }

                    ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", GlobalContent.getInstance().primaryEmail);
                    ((MyApplication) getApplication()).trackEvent("Android-Multimedia", title, label);

                    if (item.vSource == null || item.vSource.isEmpty()) {
                        Intent intent = new Intent(MultimediaListActivity.this, VideoPlayerActivity.class);
                        intent.putExtra("videoUrl", videoPath);
                        MultimediaListActivity.this.startActivity(intent);
                    } else {
                        if (item.vSource.equalsIgnoreCase("E")) {
                            Intent playVideo = new Intent("PlayExternalVideo");
                            playVideo.putExtra("source", item.source);
                            getContext().sendBroadcast(playVideo);
                        } else if (item.vSource.equalsIgnoreCase("S")) {

                            String videoUrl = item.source;

                            if (videoUrl.contains("youtube")) { // To play video from you tube
                                Intent playVideo = new Intent("PlayYoutubeVideo");
                                playVideo.putExtra("source", item.source);
                                getContext().sendBroadcast(playVideo);
                            } else { // To play video from other streaming sites
                                Intent playVideo = new Intent("PlayVideoInWebView");
                                playVideo.putExtra("source", item.source);
                                getContext().sendBroadcast(playVideo);
                            }

                        } else {
                            Intent intent = new Intent(MultimediaListActivity.this, VideoPlayerActivity.class);
                            intent.putExtra("videoUrl", videoPath);
                            MultimediaListActivity.this.startActivity(intent);
                        }
                    }
                }
            });

            Button playIconButtonText = (Button) convertView.findViewById(R.id.playIconButtonText);
            MCTextView tv = (MCTextView) convertView.findViewById(R.id.multimediaListItemText);

            String pageTitle = null;
            if (activityType == 4) {
                if (activityTitle.isEmpty())
                    pageTitle = String.format("Audio on Page : %s", _p.imageName);
                else
                    pageTitle = activityTitle;
                playIconButtonText.setText("Audio");
            } else {
                if (activityTitle.isEmpty())
                    pageTitle = String.format("Video on Page : %s", _p.imageName);
                else
                    pageTitle = activityTitle;
                playIconButtonText.setText("Video");
            }

            tv.setText(pageTitle);
            tv.setTextColor(Color.BLACK);
            tv.setTag(position);
            tv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Page p = _multimediaPages.get((Integer) v.getTag());
                    MultimediaListActivity.this.closeWithResult(p);
                }
            });
            return convertView;
        }
    }
}
