package com.mirabel.magazinecentral.activities.viewissue;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.util.Utility;

public class WebViewActivity extends Activity {
    WebView webView = null;
    boolean isFullScreen = false;
    MCTextView titleText = null;
    String eventLabel = null;
    boolean isTrackingDone = false;
    ImageButton previous, next, resize, reloadWebView;
    ProgressBar progressBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        try {
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            Intent intent = getIntent();

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                GlobalContent.orientation = Constants.OrientationType.portrait;
            } else {
                GlobalContent.orientation = Constants.OrientationType.landscape;
            }

            if (intent.hasExtra("eventLabel")) {
                eventLabel = intent.getStringExtra("eventLabel");
                isTrackingDone = intent.getBooleanExtra("isTrackingDone", false);
            } else {
                eventLabel = "";
                isTrackingDone = true;
            }

            titleText = (MCTextView) findViewById(R.id.title);
            previous = (ImageButton) findViewById(R.id.previousButton);
            next = (ImageButton) findViewById(R.id.nextButton);
            resize = (ImageButton) findViewById(R.id.resizeButton);
            reloadWebView = (ImageButton) findViewById(R.id.refreshButton);

            disable(previous);
            disable(next);

            webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.setWebViewClient(new CustomWebClient());
            webView.loadUrl(intent.getStringExtra("url"));
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);

            isFullScreen = true;
            adjustSize(null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disable(View v) {
        v.setAlpha(0.3f);
        v.setEnabled(false);
    }

    public void enable(View v) {
        v.setAlpha(1.0f);
        v.setEnabled(true);
    }

    public void previous(View v) {
        webView.goBack();

        if (!webView.canGoBack()) {
            disable(previous);
        }
    }

    public void next(View v) {
        webView.goForward();

        if (!webView.canGoForward()) {
            disable(next);
        }
    }

    public void reloadWebView(View v) {
        if (webView != null) {
            webView.reload();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            GlobalContent.orientation = Constants.OrientationType.portrait;
        } else {
            GlobalContent.orientation = Constants.OrientationType.landscape;
        }

        if (!isFullScreen) {
            isFullScreen = true;
            adjustSize(null);
        }
    }

    public void adjustSize(View v) {
        if (!Utility.isTabletDevice(WebViewActivity.this)) {
            getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            resize.setImageResource(R.drawable.toolbar_icon_minimize);
            isFullScreen = true;
        } else {
            if (isFullScreen) {
                isFullScreen = false;

                WindowManager wm = (WindowManager) MyApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);

                int width = size.x;
                int height = size.y;

                getWindow().setGravity(Gravity.CENTER);

                if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                    getWindow().setLayout((int) (width * 0.7), (int) (height * 0.6));
                } else {
                    getWindow().setLayout((int) (width * 0.7), (int) (height * 0.9));
                }

                resize.setImageResource(R.drawable.toolbar_icon_maximize);
            } else {
                getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                resize.setImageResource(R.drawable.toolbar_icon_minimize);
                isFullScreen = true;
            }
        }
    }

    public void onDone(View v) {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public class CustomWebClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            try {
                showProgressBar();

                if (url.startsWith("mailto:")) {
                    Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("message/rfc822");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.support_email).trim()});
                    startActivity(Intent.createChooser(emailIntent, "Send s_email_icon..."));
                    return true;
                } else {
                    view.loadUrl(url);
                    return true;
                }

            } catch (Exception e) {
                return true;
            }
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();

            try {
                showProgressBar();

                if (url.startsWith("mailto:")) {
                    Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("message/rfc822");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.support_email).trim()});
                    startActivity(Intent.createChooser(emailIntent, "Send s_email_icon..."));
                    return true;
                } else {
                    view.loadUrl(url);
                    return true;
                }

            } catch (Exception e) {
                return true;
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            if (webView.canGoBack()) {
                enable(previous);
            } else {
                disable(previous);
            }

            if (webView.canGoForward()) {
                enable(next);
            } else {
                disable(next);
            }

            titleText.setText("Loading...");
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            hideProgressBar();

            titleText.setText(view.getTitle());

            if (!isTrackingDone) {
                isTrackingDone = true;
                ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", GlobalContent.getInstance().primaryEmail);
                ((MyApplication) getApplication()).trackEvent("Android-Hotspot", url, eventLabel);
            }
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            // Handle the error
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            // Redirect to deprecated method, so you can use it in all SDK versions
            onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }
    }

    public void showProgressBar() {
        if (progressBar != null) {
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.animate();
            }
        }
    }

    public void hideProgressBar() {
        if (progressBar != null) {
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.clearAnimation();
            }
        }
    }
}
