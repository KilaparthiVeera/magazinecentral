package com.mirabel.magazinecentral.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.mirabel.magazinecentral.beans.viewissue.Bookmark;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.util.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by venkat on 7/26/17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = DBHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "MIRABEL";

    private final String TABLE_CONTENTS = "CONTENT";
    private final String TABLE_BOOKMARKS = "BOOKMARK";

    // Column Names for Content Table
    private final String KEY_CONTENT_ID = "id";
    private final String KEY_CONTENT_NAME = "name";
    private final String KEY_CONTENT_PUBLISHER_ID = "publisherID";
    private final String KEY_CONTENT_PUBLISHER_NAME = "publisherName";
    private final String KEY_CONTENT_CATEGORY = "category";
    private final String KEY_CONTENT_TYPE = "type";
    private final String KEY_CONTENT_STATE = "state";
    private final String KEY_CONTENT_START_PAGE_INDEX = "startPageIndex";
    private final String KEY_CONTENT_ISSUED_ON = "issuedOn";
    private final String KEY_CONTENT_PUBLISHED_ON = "publishedOn";
    private final String KEY_CONTENT_WEB_URL = "webUrl";
    private final String KEY_CONTENT_DESCRIPTION = "description";
    private final String KEY_CONTENT_PRODUCT_NAME = "productName";

    // Column Names for Bookmarks Table
    private final String KEY_BOOKMARK_CONTENT_ID = "id";
    private final String KEY_BOOKMARK_PAGE_NAME = "pageName";
    private final String KEY_BOOKMARK_PAGE_INDEX = "pageIndex";
    private final String KEY_BOOKMARK_TYPE = "type";
    private final String KEY_BOOKMARK_CREATED_ON = "createdOn";

    // Content Table Create Statement
    private final String CREATE_STATEMENT_FOR_TABLE_CONTENTS = "CREATE TABLE IF NOT EXISTS " + TABLE_CONTENTS + " ("
            + KEY_CONTENT_ID + " TEXT PRIMARY KEY,"
            + KEY_CONTENT_NAME + " TEXT,"
            + KEY_CONTENT_PUBLISHER_ID + " TEXT,"
            + KEY_CONTENT_PUBLISHER_NAME + " TEXT,"
            + KEY_CONTENT_CATEGORY + " TEXT,"
            + KEY_CONTENT_TYPE + " INTEGER,"
            + KEY_CONTENT_STATE + " INTEGER,"
            + KEY_CONTENT_START_PAGE_INDEX + " INTEGER,"
            + KEY_CONTENT_ISSUED_ON + " TEXT,"
            + KEY_CONTENT_PUBLISHED_ON + " TEXT,"
            + KEY_CONTENT_WEB_URL + " TEXT,"
            + KEY_CONTENT_DESCRIPTION + " TEXT,"
            + KEY_CONTENT_PRODUCT_NAME + " TEXT)";

    // Bookmarks Table Create Statement
    private final String CREATE_STATEMENT_FOR_TABLE_BOOKMARKS = "CREATE TABLE IF NOT EXISTS " + TABLE_BOOKMARKS + " ("
            + KEY_BOOKMARK_CONTENT_ID + " TEXT,"
            + KEY_BOOKMARK_PAGE_NAME + " TEXT,"
            + KEY_BOOKMARK_PAGE_INDEX + " INTEGER,"
            + KEY_BOOKMARK_TYPE + " INTEGER,"
            + KEY_BOOKMARK_CREATED_ON + " TEXT,"
            + " UNIQUE(" + KEY_BOOKMARK_CONTENT_ID + ", " + KEY_BOOKMARK_PAGE_INDEX + "))";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(CREATE_STATEMENT_FOR_TABLE_CONTENTS);
            sqLiteDatabase.execSQL(CREATE_STATEMENT_FOR_TABLE_BOOKMARKS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        try {
            onCreate(sqLiteDatabase);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertContent(Content content) {
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_CONTENT_ID, content.getId());
            values.put(KEY_CONTENT_NAME, content.getName());
            values.put(KEY_CONTENT_PUBLISHER_ID, content.getPublisherId());
            values.put(KEY_CONTENT_PUBLISHER_NAME, content.getPublisherName());
            values.put(KEY_CONTENT_CATEGORY, content.getCategory());
            values.put(KEY_CONTENT_DESCRIPTION, content.getDescription().replace("'", "\\'"));
            values.put(KEY_CONTENT_TYPE, content.getDownloadType().ordinal());
            values.put(KEY_CONTENT_STATE, content.getContentState().ordinal());
            values.put(KEY_CONTENT_START_PAGE_INDEX, -1);
            values.put(KEY_CONTENT_WEB_URL, content.getWebUrl());
            values.put(KEY_CONTENT_ISSUED_ON, content.getIssuedOn());
            values.put(KEY_CONTENT_PUBLISHED_ON, content.getPublishedOn());
            values.put(KEY_CONTENT_PRODUCT_NAME, content.getProductName());

            boolean isAlreadyInserted = checkIssueExistsInContentTable(content.getId());

            SQLiteDatabase db = this.getWritableDatabase();

            if (!isAlreadyInserted) {
                long status = db.insert(TABLE_CONTENTS, null, values);
            }

            values.clear();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkIssueExistsInContentTable(String contentId) {
        boolean isExisted = false;

        try {
            String selectQuery = "SELECT " + KEY_CONTENT_ID + " FROM " + TABLE_CONTENTS + " WHERE " + KEY_CONTENT_ID + "='" + contentId + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int noOfRecords = cursor.getCount();

                if (noOfRecords > 0)
                    isExisted = true;

                cursor.close();
            }

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isExisted;
    }

    public ArrayList<Content> getPendingDownloads() {
        ArrayList<Content> pendingDownloads = new ArrayList<>();

        try {
            String query = String.format("SELECT " + KEY_CONTENT_ID + ", " + KEY_CONTENT_NAME + ", " + KEY_CONTENT_PUBLISHER_ID + ", " + KEY_CONTENT_PRODUCT_NAME + ", " + KEY_CONTENT_STATE + ", " + KEY_CONTENT_TYPE + ", " + KEY_CONTENT_WEB_URL + " FROM " + TABLE_CONTENTS + " WHERE " + KEY_CONTENT_STATE + " != %d", Constants.ContentState.ContentStateDownloaded.ordinal());
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Content content = new Content();
                    content.setId(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_ID)));
                    content.setName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_NAME)));
                    content.setPublisherId(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHER_ID)));
                    content.setProductName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PRODUCT_NAME)));
                    content.setContentState(Constants.ContentState.values()[cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_STATE))]);
                    content.setDownloadType(Constants.DOWNLOAD_TYPE.values()[cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_TYPE))]);
                    content.setWebUrl(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_WEB_URL)));

                    pendingDownloads.add(content);

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pendingDownloads;
    }

    public void updateContentDownloadStatus(String contentId, Constants.ContentState state) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put(KEY_CONTENT_STATE, state.ordinal());

            int status = db.update(TABLE_CONTENTS, values, KEY_CONTENT_ID + " = ?", new String[]{contentId});

            values.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();
    }

    public List<String> getDownloadCompletedContentIds() {
        List<String> contentIds = new ArrayList<>();

        try {
            String query = String.format("SELECT " + KEY_CONTENT_ID + " FROM " + TABLE_CONTENTS + " WHERE " + KEY_CONTENT_STATE + " = %d", Constants.ContentState.ContentStateDownloaded.ordinal());
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    contentIds.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return contentIds;
    }

    public void deleteContent(String contentId) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            int noOfRecordsDeleted = db.delete(TABLE_CONTENTS, KEY_CONTENT_ID + " = ?", new String[]{contentId});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //GlobalContent.getInstance().updateDownloadedContents();
    }

    public ArrayList<Content> getLibrary() {
        ArrayList<Content> libraryContents = new ArrayList<Content>();

        try {
            String query = String.format("SELECT * FROM " + TABLE_CONTENTS + " WHERE " + KEY_CONTENT_STATE + " != %d", Constants.ContentState.ContentStateNone.ordinal());
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Content content = new Content();
                    content.setId(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_ID)));
                    content.setName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_NAME)));
                    content.setPublisherId(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHER_ID)));
                    content.setPublisherName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHER_NAME)));
                    content.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_CATEGORY)));
                    content.setDownloadType(Constants.DOWNLOAD_TYPE.values()[cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_TYPE))]);
                    content.setContentState(Constants.ContentState.values()[cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_STATE))]);
                    content.setWebUrl(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_WEB_URL)));
                    content.setStartPageIndex(cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_START_PAGE_INDEX)));
                    content.setIssuedOn(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_ISSUED_ON)));
                    content.setPublishedOn(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHED_ON)));
                    content.setDescription(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_DESCRIPTION)).replace("\\'", "'"));
                    content.setProductName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PRODUCT_NAME)));

                    libraryContents.add(content);

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

            Collections.sort(libraryContents, new Comparator<Content>() {
                @Override
                public int compare(Content lhs, Content rhs) {
                    return rhs.getPublishedOn().compareTo(lhs.getPublishedOn());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return libraryContents;
    }

    public void updateStartPageIndex(String contentId, int currentPage) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put(KEY_CONTENT_START_PAGE_INDEX, currentPage);

            int status = db.update(TABLE_CONTENTS, values, KEY_CONTENT_ID + " = ?", new String[]{contentId});

            values.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }

        db.close();
    }

    public int getStatePageIndex(String contentId) {
        int startPageIndex = 0;

        try {
            String query = String.format("SELECT " + KEY_CONTENT_START_PAGE_INDEX + " FROM " + TABLE_CONTENTS + " WHERE " + KEY_CONTENT_ID + " = '%s'", contentId);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    startPageIndex = cursor.getInt(0);
                    break;
                }
            }

            cursor.close();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return startPageIndex;
    }

    public Content hasContentDownloaded(String contentId) {
        Content content = null;

        try {
            String query = String.format("SELECT * FROM " + TABLE_CONTENTS + " WHERE " + KEY_CONTENT_ID + " = '%s' AND (" + KEY_CONTENT_TYPE + " = %d OR " + KEY_CONTENT_STATE + " = %d)", contentId, Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD.ordinal(), Constants.ContentState.ContentStateDownloaded.ordinal());
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                content = new Content();
                content.setId(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_ID)));
                content.setName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_NAME)));
                content.setPublisherId(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHER_ID)));
                content.setPublisherName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHER_NAME)));
                content.setCategory(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_CATEGORY)));
                content.setDownloadType(Constants.DOWNLOAD_TYPE.values()[cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_TYPE))]);
                content.setContentState(Constants.ContentState.values()[cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_STATE))]);
                content.setWebUrl(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_WEB_URL)));
                content.setStartPageIndex(cursor.getInt(cursor.getColumnIndex(KEY_CONTENT_START_PAGE_INDEX)));
                content.setIssuedOn(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_ISSUED_ON)));
                content.setPublishedOn(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PUBLISHED_ON)));
                content.setDescription(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_DESCRIPTION)).replace("\\'", "'"));
                content.setProductName(cursor.getString(cursor.getColumnIndex(KEY_CONTENT_PRODUCT_NAME)));
            } else {
                content = null;
            }

            cursor.close();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return content;
    }

    public boolean addBookmark(Bookmark bookmark) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean isBookmarkAdded = false;

        try {
            String createdOn = Utility.formatDate(new Date(), "MM/dd/yyyy");

            ContentValues values = new ContentValues();
            values.put(KEY_BOOKMARK_CONTENT_ID, bookmark.getContentId());
            values.put(KEY_BOOKMARK_PAGE_NAME, bookmark.getPageName());
            values.put(KEY_BOOKMARK_PAGE_INDEX, bookmark.getPageIndex());
            values.put(KEY_BOOKMARK_TYPE, 1);
            values.put(KEY_BOOKMARK_CREATED_ON, createdOn);

            long insertedRowId = db.insert(TABLE_BOOKMARKS, null, values);

            if (insertedRowId == -1) {
                isBookmarkAdded = false;
            } else {
                isBookmarkAdded = true;
            }

            values.clear();

        } catch (Exception e) {
            e.printStackTrace();
            isBookmarkAdded = false;
        }

        db.close();

        return isBookmarkAdded;
    }

    public void deleteBookmark(Bookmark bookmark) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_BOOKMARKS, KEY_BOOKMARK_CONTENT_ID + " = ? AND " + KEY_BOOKMARK_PAGE_INDEX + " = ?", new String[]{bookmark.getContentId(), String.valueOf(bookmark.getPageIndex())});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Bookmark> getAllBookmarks(String contentId) {
        ArrayList<Bookmark> bookmarks = new ArrayList<>();

        try {
            String query = String.format("SELECT " + KEY_BOOKMARK_PAGE_NAME + ", " + KEY_BOOKMARK_PAGE_INDEX + " FROM " + TABLE_BOOKMARKS + " WHERE " + KEY_BOOKMARK_CONTENT_ID + " = '%s' order by ROWID DESC", contentId);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Bookmark bookmark = new Bookmark();
                    bookmark.setContentId(contentId);
                    bookmark.setPageName(cursor.getString(cursor.getColumnIndex(KEY_BOOKMARK_PAGE_NAME)));
                    bookmark.setPageIndex(cursor.getInt(cursor.getColumnIndex(KEY_BOOKMARK_PAGE_INDEX)));

                    bookmarks.add(bookmark);

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookmarks;
    }

    /* To take Local SQLite DB Back Up Based on App Package Name */
    public void takeDBBackUp(String packageName) {
        try {
            File dbFile = new File(Environment.getDataDirectory() + "/data/" + packageName + "/databases/" + DATABASE_NAME);

            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Environment.getExternalStorageDirectory() + "/dbcopy_" + packageName + ".db";

            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);

            // Transfer bytes from the input file to the output file
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            // Close the streams
            output.flush();
            output.close();
            fis.close();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
