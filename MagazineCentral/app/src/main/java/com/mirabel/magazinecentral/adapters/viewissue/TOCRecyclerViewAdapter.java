package com.mirabel.magazinecentral.adapters.viewissue;

import android.graphics.BitmapFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.IndexItem;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

import java.util.ArrayList;
import java.util.List;

public class TOCRecyclerViewAdapter extends RecyclerView.Adapter<TOCRecyclerViewAdapter.TOCViewHolder> {
    private ViewPagerInterface viewPagerListener;
    private String contentID = null;
    private List<IndexItem> tocContents = new ArrayList<IndexItem>();
    private ArrayList<String> multimediaPageIndexes;
    private String filePath = null;
    private View selectedTOC = null;

    public TOCRecyclerViewAdapter(ViewPagerInterface listener, String contentID) {
        this.viewPagerListener = listener;
        this.contentID = contentID;
        this.filePath = GlobalContent.issueFolderPath + contentID;
    }

    public TOCRecyclerViewAdapter(ViewPagerInterface listener, String contentID, ArrayList<IndexItem> contents, ArrayList<String> _multimediaPageIndexes) {
        this.viewPagerListener = listener;
        this.contentID = contentID;
        this.tocContents = contents;
        this.multimediaPageIndexes = _multimediaPageIndexes;
        this.filePath = GlobalContent.issueFolderPath + contentID;
    }

    public void setTocContents(List<IndexItem> tocContents) {
        this.tocContents = tocContents;
    }

    public void setMultimediaPageIndexes(ArrayList<String> multimediaPageIndexes) {
        this.multimediaPageIndexes = multimediaPageIndexes;
    }

    @Override
    public TOCRecyclerViewAdapter.TOCViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.toc_unit, parent, false);

        TOCViewHolder viewHolder = new TOCViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TOCRecyclerViewAdapter.TOCViewHolder holder, final int position) {
        try {
            IndexItem item = tocContents.get(position);

            holder.thumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + item.imageName + "_toc.jpeg"));
            holder.pageName.setText(item.text);

            if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(item.imageName)) {
                holder.pageVideoIcon.setVisibility(View.VISIBLE);
            } else {
                holder.pageVideoIcon.setVisibility(View.GONE);
            }

            if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                if ((ViewIssueActivity.selectedPageIndex + 1) == item.pageNumber) {
                    holder.thumbImageRelativeLayout.setBackgroundResource(R.drawable.border_selected);
                    selectedTOC = holder.view;
                } else {
                    holder.thumbImageRelativeLayout.setBackgroundResource(R.drawable.border);
                }
            } else {
                if (ViewIssueActivity.selectedPageIndex == item.pageNumber || (ViewIssueActivity.selectedPageIndex + 1) == item.pageNumber) {
                    holder.thumbImageRelativeLayout.setBackgroundResource(R.drawable.border_selected);
                    selectedTOC = holder.view;
                } else {
                    holder.thumbImageRelativeLayout.setBackgroundResource(R.drawable.border);
                }
            }

            holder.view.setTag(item.pageNumber - 1);

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedTOC != null) {
                        ((LinearLayout) selectedTOC).getChildAt(0).setBackgroundResource(R.drawable.border);
                    }

                    ((LinearLayout) v).getChildAt(0).setBackgroundResource(R.drawable.border_selected);
                    selectedTOC = v;

                    if (viewPagerListener != null)
                        viewPagerListener.thumbTapped(Integer.parseInt(selectedTOC.getTag().toString()));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return tocContents.size();
    }

    public class TOCViewHolder extends RecyclerView.ViewHolder {
        View view;
        RelativeLayout thumbImageRelativeLayout;
        ImageView thumbImage, pageVideoIcon;
        MCTextView pageName;

        public TOCViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.thumbImageRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.tocInnerRelativeLayout);
            this.thumbImage = (ImageView) itemView.findViewById(R.id.tocImage);
            this.pageVideoIcon = (ImageView) itemView.findViewById(R.id.pageVideoIcon);
            this.pageName = (MCTextView) itemView.findViewById(R.id.tocText);
        }
    }

    public void refreshTOCs() {
        // To clear previously highlighted TOCs
        notifyItemRangeChanged(0, tocContents.size());

        /*if (position >= 0)
            notifyItemChanged(position);*/
    }
}
