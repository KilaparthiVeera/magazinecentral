package com.mirabel.magazinecentral.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.adapters.CategoryIssuesGridViewAdapter;
import com.mirabel.magazinecentral.adapters.LibrarySearchPredictionsAdapter;
import com.mirabel.magazinecentral.adapters.SpinnerAdapter;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.SearchCriteria;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.customviews.DelayAutoCompleteTextView;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;
import com.mirabel.magazinecentral.services.DownloadService;
import com.mirabel.magazinecentral.util.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class LibrarySearchActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AlertDialogSelectionListener {
    public static final String TAG = LibrarySearchActivity.class.getSimpleName();

    private Context applicationContext, activityContext;
    private DelayAutoCompleteTextView search_auto_complete_text_view;
    private Spinner publishers_spinner, years_spinner, months_spinner;
    private MCTextView search_results_count;
    private GridView search_result_issues_grid_view;

    private GlobalContent globalContent;
    private Utility utilityInstance;
    private Map<String, String> publisherList;
    private ArrayList<String> publishersNamesList, yearsList, monthsList;
    private LibrarySearchPredictionsAdapter searchPredictionsAdapter;
    private SpinnerAdapter publishersAdapter, yearsAdapter, monthsAdapter;
    private SearchCriteria searchCriteria;
    private boolean isLoading = false, isAutoCompleteTextViewReset = false, isSpinnersReset = false, canWeSendRequest = false, isDeviceOrientationChanged = false;
    private int totalNoOfRecords;
    private CategoryIssuesGridViewAdapter gridViewAdapter;
    public static String searchKeyword;
    ArrayList<Content> filteredList = new ArrayList<>();

    public static final int SHOW_SELECT_INDEX = 0;
    public static final int SHOW_ALL_INDEX = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_search);

        // In order to hide keyboard when this screen appears because of AutoCompleteTextView.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        try {
            applicationContext = getApplicationContext();
            activityContext = LibrarySearchActivity.this;

            search_auto_complete_text_view = (DelayAutoCompleteTextView) findViewById(R.id.library_search_auto_complete_text_view);
            publishers_spinner = (Spinner) findViewById(R.id.library_search_publishers_spinner);
            years_spinner = (Spinner) findViewById(R.id.library_search_years_spinner);
            months_spinner = (Spinner) findViewById(R.id.library_search_months_spinner);
            search_results_count = (MCTextView) findViewById(R.id.library_search_results_count);
            search_result_issues_grid_view = (GridView) findViewById(R.id.library_search_result_issues_grid_view);

            globalContent = GlobalContent.getInstance();
            utilityInstance = Utility.getInstance();
            searchCriteria = new SearchCriteria();

            yearsList = utilityInstance.getFilterYears();
            monthsList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.months_list)));
            publisherList = globalContent.getPublisherListForLibrary();
            publishersNamesList = new ArrayList<>(publisherList.keySet());

            searchPredictionsAdapter = new LibrarySearchPredictionsAdapter(activityContext);
            search_auto_complete_text_view.setThreshold(1); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            search_auto_complete_text_view.setAdapter(searchPredictionsAdapter);
            search_auto_complete_text_view.setLoadingIndicator((ProgressBar) findViewById(R.id.library_search_pb_loading_indicator));

            search_auto_complete_text_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    String selectedProductName = adapterView.getItemAtPosition(position).toString();
                    search_auto_complete_text_view.setAdapter(null); // to stop filtering after selecting row from drop down
                    search_auto_complete_text_view.setText(selectedProductName);
                    search_auto_complete_text_view.setAdapter(searchPredictionsAdapter);

                    searchCriteria.setSearchKeyword(LibrarySearchActivity.searchKeyword);
                    searchCriteria.setSelectedProductName(selectedProductName);

                    if (!isSpinnersReset)
                        resetSpinners(SHOW_SELECT_INDEX);

                    hideKeyboard();
                    resetValues();
                    displayIssuesForSelectedValues();
                }
            });

            publishersAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, publishersNamesList);
            publishersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            publishersAdapter.setObjects(publishersNamesList); // showing Select Publisher initially in select publisher spinner.
            publishers_spinner.setAdapter(publishersAdapter);
            publishers_spinner.setSelection(SHOW_SELECT_INDEX, false); // here animate is false in order to prevent execution of onItemSelected() on initial loading.
            //setSpinnerSelectionWithoutCallingListener(publishers_spinner, 0);
            publishers_spinner.setOnItemSelectedListener(this);

            yearsAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, yearsList);
            yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            years_spinner.setAdapter(yearsAdapter);
            years_spinner.setSelection(SHOW_SELECT_INDEX, false);
            years_spinner.setOnItemSelectedListener(this);

            /*monthsAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, monthsList);
            monthsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            months_spinner.setAdapter(monthsAdapter);*/
            months_spinner.setSelection(SHOW_SELECT_INDEX, false);
            months_spinner.setOnItemSelectedListener(this);

            isLoading = true;

            // Binding adapter to grid view first time.
            gridViewAdapter = new CategoryIssuesGridViewAdapter(activityContext, this, Constants.RequestFrom.LIBRARY_SEARCH_PAGE);
            search_result_issues_grid_view.setAdapter(gridViewAdapter);

            if (savedInstanceState != null) {
                searchCriteria = (SearchCriteria) savedInstanceState.get("SearchCriteria");
                isDeviceOrientationChanged = true;

                if (searchCriteria != null) {
                    if (searchCriteria.isSearchPerformed()) {
                        ArrayList<Content> previouslyLoadedIssues = globalContent.getLibrarySearchResultIssuesList();
                        totalNoOfRecords = savedInstanceState.getInt("totalNoOfRecords");

                        if (!searchCriteria.getSelectedProductName().isEmpty()) {
                            search_auto_complete_text_view.setAdapter(null); // to stop filtering after populating auto complete text view with previously selected value.
                            search_auto_complete_text_view.post(new Runnable() {
                                @Override
                                public void run() {
                                    search_auto_complete_text_view.setText(searchCriteria.getSelectedProductName());
                                    search_auto_complete_text_view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            search_auto_complete_text_view.setAdapter(searchPredictionsAdapter);
                                        }
                                    });
                                }
                            });

                            if (!isSpinnersReset)
                                resetSpinners(SHOW_SELECT_INDEX);

                            hideKeyboard();

                            if (previouslyLoadedIssues.isEmpty()) {
                                resetValues();
                                displayIssuesForSelectedValues();
                            } else {
                                updatePreviouslyLoadedIssuesList();
                            }
                        } else {
                            int selectedPublisherIndex = publishersNamesList.indexOf(searchCriteria.getSelectedPublisherName());
                            int selectedYearIndex = yearsList.indexOf(searchCriteria.getYear());
                            int selectedMonthIndex = monthsList.indexOf(searchCriteria.getMonthName());

                            setSpinnerSelectionWithoutCallingListener(publishers_spinner, selectedPublisherIndex);
                            setSpinnerSelectionWithoutCallingListener(years_spinner, selectedYearIndex);
                            setSpinnerSelectionWithoutCallingListener(months_spinner, selectedMonthIndex);

                            // Checking weather user have selected any publisher, year or month before device orientation change.
                            if (selectedPublisherIndex == 0 && selectedYearIndex == 0 && selectedMonthIndex == 0)
                                canWeSendRequest = false;
                            else
                                canWeSendRequest = true;

                            if (canWeSendRequest) {
                                if (previouslyLoadedIssues.isEmpty()) {
                                    canWeSendRequest = false;
                                    resetValues();
                                    displayIssuesForSelectedValues();
                                } else {
                                    updatePreviouslyLoadedIssuesList();
                                }
                            }
                        }
                    } else {
                        isDeviceOrientationChanged = false;
                    }
                }
            } else {
                resetValues();
            }

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(downloadManagerBroadcastReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        try {
            outState.putSerializable("SearchCriteria", searchCriteria);
            outState.putInt("totalNoOfRecords", totalNoOfRecords);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        Intent showHomePageIntent = new Intent(activityContext, MainActivity.class);
        showHomePageIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(showHomePageIntent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(search_auto_complete_text_view.getWindowToken(), 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        try {
            int spinnersTag = Integer.parseInt(adapterView.getTag().toString());

            if (!isDeviceOrientationChanged) {

                if (!isAutoCompleteTextViewReset)
                    resetAutoCompleteTextView();

                if (spinnersTag == 2) {
                    if (position > 0) {
                        String publisherName = publishersNamesList.get(position);

                        searchCriteria.setSelectedPublisherId(publisherList.get(publisherName));
                        searchCriteria.setSelectedPublisherName(publisherName);
                        searchCriteria.setYear("All");
                        searchCriteria.setSelectedYear(-1);
                        searchCriteria.setMonthName("All");
                        searchCriteria.setSelectedMonth(-1);

                        setSpinnerSelectionWithoutCallingListener(years_spinner, SHOW_ALL_INDEX);
                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_ALL_INDEX);

                        canWeSendRequest = true;
                    } else {
                        searchCriteria.setSelectedPublisherId("-1");
                        searchCriteria.setSelectedPublisherName("Select Publisher");

                        setSpinnerSelectionWithoutCallingListener(years_spinner, SHOW_SELECT_INDEX);
                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_SELECT_INDEX);

                        resetValues();
                    }
                } else if (spinnersTag == 3) {
                    if (position > 0) {
                        String selectedYear = yearsList.get(position);
                        searchCriteria.setYear(selectedYear);

                        if (position == 1) {
                            searchCriteria.setSelectedYear(-1);
                        } else {
                            searchCriteria.setSelectedYear(Integer.parseInt(selectedYear));
                        }

                        searchCriteria.setMonthName("All");
                        searchCriteria.setSelectedMonth(-1);

                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_ALL_INDEX);

                        canWeSendRequest = true;
                    } else {
                        searchCriteria.setYear("Select Year");
                        searchCriteria.setSelectedYear(-1);

                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_SELECT_INDEX);

                        if (searchCriteria.getSelectedPublisherName().equals("Select Publisher") && searchCriteria.getMonthName().equals("Select Month"))
                            resetValues();
                        else
                            canWeSendRequest = true;
                    }
                } else if (spinnersTag == 4) {
                    if (position > 0) {
                        if (!isDeviceOrientationChanged) {
                            String selectedMonth = monthsList.get(position);
                            searchCriteria.setMonthName(selectedMonth);

                            if (position == 1) {
                                searchCriteria.setSelectedMonth(-1);
                            } else {
                                searchCriteria.setSelectedMonth(position - 1); // Months names index starting from 2 so we are subtracting 1 from selected index
                            }
                        }

                        canWeSendRequest = true;
                    } else {
                        searchCriteria.setMonthName("Select Month");
                        searchCriteria.setSelectedMonth(-1);

                        if (searchCriteria.getSelectedPublisherName().equals("Select Publisher") && searchCriteria.getYear().equals("Select Year"))
                            resetValues();
                        else
                            canWeSendRequest = true;
                    }
                }

                if (canWeSendRequest) {
                    resetValues();
                    canWeSendRequest = false;
                    displayIssuesForSelectedValues();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);
        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(selection);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(listener);
                    }
                });
            }
        });
    }

    public void resetSpinners(int selectedIndex) {
        isAutoCompleteTextViewReset = false;
        isSpinnersReset = true;

        setSpinnerSelectionWithoutCallingListener(publishers_spinner, selectedIndex);
        setSpinnerSelectionWithoutCallingListener(years_spinner, selectedIndex);
        setSpinnerSelectionWithoutCallingListener(months_spinner, selectedIndex);

        if (selectedIndex == 0) {
            searchCriteria.setSelectedPublisherName("Select Publisher");
            searchCriteria.setYear("Select Year");
            searchCriteria.setMonthName("Select Month");
        } else {
            searchCriteria.setSelectedPublisherName("All");
            searchCriteria.setYear("All");
            searchCriteria.setMonthName("All");
        }

        searchCriteria.setSelectedPublisherId("-1");
        searchCriteria.setSelectedYear(-1);
        searchCriteria.setSelectedMonth(-1);
    }

    public void resetAutoCompleteTextView() {
        isAutoCompleteTextViewReset = true;
        isSpinnersReset = false;

        search_auto_complete_text_view.setText("");

        searchCriteria.setSearchKeyword("");
        searchCriteria.setSelectedProductName("");
    }

    public void resetValues() {
        filteredList.clear();
        totalNoOfRecords = 0;
        isLoading = true;
        //canWeSendRequest = false;

        // clearing previous search results saved in global content
        globalContent.setLibrarySearchResultIssuesList(null);
        gridViewAdapter.notifyDataSetChanged();

        search_results_count.setVisibility(View.INVISIBLE);
    }

    public void displayIssuesForSelectedValues() {
        try {
            if (!MCProgressDialog.isProgressDialogShown)
                MCProgressDialog.showProgressDialog(activityContext, Constants.LOADING_MESSAGE);

            filteredList.clear();
            isLoading = true;
            isDeviceOrientationChanged = false;

            if (searchCriteria.getSearchKeyword() != "") {
                for (Content content : globalContent.getLibraryIssuesList()) {
                    if (content.getProductName().equalsIgnoreCase(searchCriteria.getSelectedProductName())) {
                        filteredList.add(content);
                    }
                }
            } else {

                filteredList.clear();

                if (!searchCriteria.getSelectedPublisherId().equals("-1")) {
                    for (Content content : globalContent.getLibraryIssuesList()) {
                        if (content.getPublisherId().equalsIgnoreCase(searchCriteria.getSelectedPublisherId())) {
                            filteredList.add(content);
                        }
                    }
                } else {
                    filteredList.addAll(globalContent.getLibraryIssuesList());
                }

                ArrayList<Content> tempList = new ArrayList<>();

                if (searchCriteria.getSelectedYear() != -1) {
                    for (Content content : filteredList) {
                        if (content.getIssuedOn().substring(6, 10).equalsIgnoreCase(String.valueOf(searchCriteria.getSelectedYear()))) {
                            tempList.add(content);
                        }
                    }

                    filteredList.clear();
                    filteredList.addAll(tempList);
                    tempList.clear();
                }

                if (searchCriteria.getSelectedMonth() != -1) {

                    String selectedMonth = (String.valueOf(searchCriteria.getSelectedMonth()).length() == 1) ? ("0" + searchCriteria.getSelectedMonth()) : ("" + searchCriteria.getSelectedMonth());

                    for (Content content : filteredList) {
                        if (content.getIssuedOn().substring(0, 2).equalsIgnoreCase(selectedMonth)) {
                            tempList.add(content);
                        }
                    }

                    filteredList.clear();
                    filteredList.addAll(tempList);
                    tempList.clear();
                }
            }

            if (filteredList.size() > 0) {
                totalNoOfRecords = filteredList.size();

                search_results_count.setVisibility(View.VISIBLE);
                search_results_count.setText(String.format("%,d magazine(s) found.", totalNoOfRecords));

                globalContent.setLibrarySearchResultIssuesList(filteredList);
                gridViewAdapter.notifyDataSetChanged();
            } else {
                search_results_count.setVisibility(View.VISIBLE);
                search_results_count.setText("0 magazine(s) found.");
            }

            isLoading = false;
            searchCriteria.setSearchPerformed(true);
            hideKeyboard();

        } catch (Exception e) {
            e.printStackTrace();
        }

        MCProgressDialog.hideProgressDialog();
    }

    public void clearLibrarySearchValues(View view) {
        resetAutoCompleteTextView();
        resetSpinners(SHOW_SELECT_INDEX);
        resetValues();
        isDeviceOrientationChanged = false;

        searchCriteria = new SearchCriteria();
    }

    public void updatePreviouslyLoadedIssuesList() {
        try {
            search_results_count.setText(String.format("%,d magazine(s) found.", totalNoOfRecords));
            search_results_count.setVisibility(View.VISIBLE);

            isLoading = false;
            searchCriteria.setSearchPerformed(true);
            isDeviceOrientationChanged = false;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayToast(String message) {
        Utility.displayToast(this, message, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startIssueDownload(GlobalContent.currentDownloadingIssue);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    MCAlertDialog.listener = this;
                    MCAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.need_storage_permission_title), getResources().getString(R.string.need_storage_permission));
                } else {
                    displayToast(getResources().getString(R.string.storage_permission_not_given));
                }
            }
        }
    }

    @Override
    public void alertDialogCallback() {
        ActivityCompat.requestPermissions(LibrarySearchActivity.this, Constants.STORAGE_PERMISSIONS, Constants.REQUEST_EXTERNAL_STORAGE);
    }

    public void startIssueDownload(Content content) {
        if (content != null) {
            content.setDownloadType(Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD);
            content.setContentState(Constants.ContentState.ContentStateDownloading);
            Intent downloadServiceIntent = new Intent(activityContext, DownloadService.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
            downloadServiceIntent.putExtras(bundle);
            startService(downloadServiceIntent);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver downloadManagerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (intent.getAction().equals(Constants.INTENT_ACTION_REFRESH_VIEW)) {
                    gridViewAdapter.notifyDataSetChanged();
                } else {
                    if (search_result_issues_grid_view != null) {
                        for (int i = 0; i < search_result_issues_grid_view.getChildCount(); i++) {
                            View magazineView = search_result_issues_grid_view.getChildAt(i);
                            CategoryIssuesGridViewAdapter.MagazineIssueViewViewHolder viewHolder = (CategoryIssuesGridViewAdapter.MagazineIssueViewViewHolder) magazineView.getTag();

                            if (viewHolder.getContentId().equalsIgnoreCase(contentId)) {
                                ImageView downloadIcon = viewHolder.download_status_icon;
                                ProgressBar currentProgressBar = viewHolder.download_progress_bar;

                                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                                    int progress = (int) ((downloaded / total) * 100);

                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    currentProgressBar.setProgress(progress);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_preview);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    currentProgressBar.setProgress(0);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_download);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
