package com.mirabel.magazinecentral.interfaces;

import com.mirabel.magazinecentral.constants.Constants;

/**
 * Created by venkat on 20/07/17.
 * <p>
 * Interface - all activities which are using this MCAsyncRequest class should implement this interface & override onResponseReceived method
 */
public interface OnAsyncRequestCompleteListener {
    void onResponseReceived(String response);

    void onResponseReceived(String response, Constants.RequestCode requestCode);
}
