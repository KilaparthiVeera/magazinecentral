package com.mirabel.magazinecentral.interfaces;

import android.view.View;

/**
 * Created by venkat on 29/06/16.
 */
public interface FragmentDrawerListener {
    void onDrawerItemSelected(View view, int position);
}
