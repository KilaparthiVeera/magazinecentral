package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

public class CustomViewPager extends ViewPager implements ViewPagerInterface {
    private boolean enabled = false;
    private float zoomValue = 1.0f;
    public boolean userInteraction = true;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        try {
            if (zoomValue > 1.0f) {
                return false;
            }

            return super.onInterceptTouchEvent(arg0);

        } catch (Exception e) {
            e.printStackTrace();

            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent arg0) {
        try {
            if (!userInteraction) {
                return false;
            }
            return super.onTouchEvent(arg0);
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    @Override
    public void enable() {
        if (!enabled) {
            requestDisallowInterceptTouchEvent(false);
            enabled = true;
        }
    }

    @Override
    public void disable() {
        if (enabled) {
            requestDisallowInterceptTouchEvent(true);
            enabled = false;
        }
    }

    @Override
    public void onSingleTapConfirmed() {
        // TODO Auto-generated method stub

    }

    @Override
    public void thumbTapped(int tappedIndex) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateZoomValue(float zoomValue) {
        this.zoomValue = zoomValue;
        // TODO Auto-generated method stub

    }

    @Override
    public void enablePinch(View view) {
        // TODO Auto-generated method stub

    }
}