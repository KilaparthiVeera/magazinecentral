package com.mirabel.magazinecentral.interfaces;

import com.mirabel.magazinecentral.beans.viewissue.Hotspot;

public interface ViewIssueEventListener {
    void hotspotTapped(String url);

    void jumpToPageIndex(int pageIndex);

    void jumpToPage(String pageName);

    void mailTo(Hotspot _data);
}
