package com.mirabel.magazinecentral.interfaces;

/**
 * Created by venkat on 7/31/17.
 */

public interface ConnectivityListener {
    void networkStateChanged(boolean isConnected);
}
