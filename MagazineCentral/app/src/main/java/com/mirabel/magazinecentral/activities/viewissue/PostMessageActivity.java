package com.mirabel.magazinecentral.activities.viewissue;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer.Result;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.github.scribejava.apis.PinterestApi;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.adapters.CustomDialogListAdapter;
import com.mirabel.magazinecentral.asynctasks.SocialClass;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCDialogWithCustomView;
import com.mirabel.magazinecentral.customviews.MCEditText;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.DialogSelectionListener;
import com.mirabel.magazinecentral.interfaces.SocialInterface;
import com.mirabel.magazinecentral.util.Utility;
import com.pinterest.android.pdk.PDKBoard;
import com.pinterest.android.pdk.PDKCallback;
import com.pinterest.android.pdk.PDKClient;
import com.pinterest.android.pdk.PDKException;
import com.pinterest.android.pdk.PDKResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import me.piruin.quickaction.ActionItem;
import me.piruin.quickaction.QuickAction;

public class PostMessageActivity extends Activity implements SocialInterface, DialogSelectionListener {
    Context applicationContext, activityContext;
    ImageView postImage = null;
    MCTextView postLink = null;
    MCEditText postMessage = null;
    ContentValues contentValues = null;
    SocialClass social = null;
    Constants.SocialType socialType = null;
    Constants.ShareType shareType = null;
    ImageButton shareButton = null;
    ProgressBar progressBar = null;
    Button postButton = null;
    PDKClient pdkClient;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_message);

        try {
            applicationContext = getApplicationContext();
            activityContext = PostMessageActivity.this;

            shareButton = (ImageButton) findViewById(R.id.shareDropDown);
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            postButton = (Button) findViewById(R.id.postButton);
            postImage = (ImageView) findViewById(R.id.postImage);
            postLink = (MCTextView) findViewById(R.id.link);
            postMessage = (MCEditText) findViewById(R.id.message);

            contentValues = getIntent().getParcelableExtra("contentValues");

            if (contentValues == null && savedInstanceState != null) {
                contentValues = savedInstanceState.getParcelable("contentValues");
            }

            socialType = Constants.SocialType.values()[contentValues.getAsInteger("socialType")];
            shareType = Constants.ShareType.values()[contentValues.getAsInteger("shareType")];
            postMessage.setText(contentValues.getAsString("postMessage"));

            if (shareType == Constants.ShareType.link) {
                postLink.setText(contentValues.getAsString("name"));
            }

            postImage.setImageBitmap(BitmapFactory.decodeFile(contentValues.getAsString("imagePath")));

            if (socialType != null) {
                setShareImage(socialType);
            }

            //Instantiating Pinterest Client
            pdkClient = PDKClient.configureInstance(this, Constants.PINTEREST_APP_ID);
            pdkClient.onConnect(this);
            pdkClient.setDebugMode(true);

            WindowManager wm = (WindowManager) MyApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            getWindow().setGravity(Gravity.CENTER);

            if (Utility.isTabletDevice(PostMessageActivity.this)) {
                if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                    getWindow().setLayout((int) (width * 0.7), FrameLayout.LayoutParams.WRAP_CONTENT);
                } else {
                    getWindow().setLayout((int) (width * 0.45), FrameLayout.LayoutParams.WRAP_CONTENT);
                }
            } else {
                getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            }

            setFinishOnTouchOutside(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("contentValues", contentValues);

        super.onSaveInstanceState(outState);
    }

    public void shareDropDownAction(View v) {
        ActionItem item0 = new ActionItem(Constants.SocialType.facebook.ordinal(), "", R.drawable.s_facebook_icon);
        ActionItem item1 = new ActionItem(Constants.SocialType.twitter.ordinal(), "", R.drawable.s_twitter_icon);
        ActionItem item2 = new ActionItem(Constants.SocialType.tumblr.ordinal(), "", R.drawable.s_tumblr_icon);
        ActionItem item3 = new ActionItem(Constants.SocialType.linkedin.ordinal(), "", R.drawable.s_linkedin_icon);
        ActionItem item4 = new ActionItem(Constants.SocialType.pinterest.ordinal(), "", R.drawable.s_pinterest_icon);

        QuickAction shareDropDown = new QuickAction(this);
        shareDropDown.addActionItem(item0);
        shareDropDown.addActionItem(item1);
        shareDropDown.addActionItem(item2);
        shareDropDown.addActionItem(item3);
        shareDropDown.addActionItem(item4);

        shareDropDown.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(ActionItem item) {

                socialType = Constants.SocialType.values()[item.getActionId()];
                ViewIssueActivity.socialType = socialType;
                setShareImage(socialType);
            }
        });

        shareDropDown.show(shareButton);
    }

    public void setShareImage(Constants.SocialType type) {
        switch (type) {
            case facebook:
                shareButton.setImageResource(R.drawable.s_facebook_icon);
                break;

            case twitter:
                shareButton.setImageResource(R.drawable.s_twitter_icon);
                break;

            case pinterest:
                shareButton.setImageResource(R.drawable.s_pinterest_icon);
                break;

            case tumblr:
                shareButton.setImageResource(R.drawable.s_tumblr_icon);
                break;

            case linkedin:
                shareButton.setImageResource(R.drawable.s_linkedin_icon);
                break;

            default:
                shareButton.setImageResource(R.drawable.s_facebook_icon);
                break;
        }
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.animate();

    }

    public void hideProgressBar() {
        progressBar.clearAnimation();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(postMessage.getWindowToken(), 0);
    }

    public void close(View v) {
        setResult(RESULT_FIRST_USER);
        finish();
    }

    @Override
    public void updateStatus(boolean status) {
        hideProgressBar();

        if (status) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }

        finish();
    }

    public void post(View v) {
        hideKeyboard();
        showProgressBar();

        contentValues.put("postMessage", postMessage.getText().toString());

        if (socialType == Constants.SocialType.facebook) {
            this.postToFacebook(contentValues);
        } else if (socialType == Constants.SocialType.pinterest) {
            this.postToPinterest(contentValues);
        } else {
            social = new SocialClass(socialType, contentValues, shareType, this);
            social.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        }
    }

    @Override
    public void login(Intent intent) {
        startActivityForResult(intent, 200);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (socialType == Constants.SocialType.pinterest) {
            hideProgressBar();
            PDKClient.getInstance().onOauthResponse(requestCode, resultCode, data);
        } else {
            if ((requestCode == 200 && resultCode == RESULT_OK) || socialType == Constants.SocialType.facebook) {
                switch (socialType) {
                    case facebook: {
                        super.onActivityResult(requestCode, resultCode, data);
                        callbackManager.onActivityResult(requestCode, resultCode, data);
                    }
                    break;

                    case twitter: {
                        social.postToTwitter();
                    }
                    break;

                    case tumblr: {
                        social.postToTumblr();
                    }
                    break;

                    case linkedin: {
                        social.postToLinkedin();
                    }
                    break;

                    default:
                        break;
                }

            } else {
                hideProgressBar();
            }
        }
    }

    public void postToFacebook(final ContentValues contentValues) {
        try {
            callbackManager = CallbackManager.Factory.create();
            shareDialog = new ShareDialog(this);

            String clippedImageSharingChoice = "Photo"; // Link or Photo

            if (shareType == Constants.ShareType.image) {

                File imageFile = new File(contentValues.getAsString("imagePath"));

                if (clippedImageSharingChoice == "Photo") {

                    if (ShareDialog.canShow(SharePhotoContent.class)) {

                        if (imageFile.exists()) {
                            try {
                                Bitmap image = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

                                SharePhoto photo = new SharePhoto.Builder()
                                        .setBitmap(image)
                                        .setCaption(contentValues.getAsString("postMessage") + "\n For Digital Edition, Click " + contentValues.getAsString("shortUrl")).build();

                                SharePhotoContent content = new SharePhotoContent.Builder()
                                        .addPhoto(photo).build();

                                shareDialog.show(content);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to show share dialogue..!", Toast.LENGTH_LONG).show();
                    }
                } else {

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        try {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    .setContentDescription(contentValues.getAsString("postMessage") + "\n For Digital Edition, Click " + contentValues.getAsString("shortUrl"))
                                    .setContentUrl(Uri.fromFile(imageFile))
                                    .build();

                            shareDialog.show(linkContent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to show share dialogue..!", Toast.LENGTH_LONG).show();
                    }
                }

            } else {

                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    try {
                        //final String description = (contentValues.getAsString("description").length() != 0) ? contentValues.getAsString("description") : contentValues.getAsString("postMessage");

                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(contentValues.getAsString("longUrl"))).build();
                        //.setContentTitle(contentValues.getAsString("name"))
                        //.setContentDescription(description)
                        //.setImageUrl(Uri.parse(contentValues.getAsString("picture")))

                        shareDialog.show(linkContent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to show share dialogue..!", Toast.LENGTH_LONG).show();
                }
            }

            shareDialog.registerCallback(callbackManager, new FacebookCallback<Result>() {

                @Override
                public void onSuccess(Result result) {
                    updateStatus(true);
                }

                @Override
                public void onCancel() {
                    updateStatus(false);
                }

                @Override
                public void onError(FacebookException error) {
                    updateStatus(false);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /********************* Pinterest Sharing Functionality Starts Here **********************/

    public void postToPinterest(final ContentValues contentValues) {
        /*SharedPreferences sharedPreferences = getSharedPreferences(PinterestApi.class.getName(), 0);
        if (sharedPreferences.getBoolean("isLogged", false)) {
            createNewPinInPinterest("488148115799841012");
        } else {
            logInIntoPinterest();
        }*/

        logInIntoPinterest();
    }

    public void logInIntoPinterest() {
        try {
            showProgressBar();

            SharedPreferences sharedPreferences = getSharedPreferences(PinterestApi.class.getName(), 0);
            final SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

            List<String> scopes = new ArrayList<>();
            scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PUBLIC);
            scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PUBLIC);

            PDKClient.getInstance().login(this, scopes, new PDKCallback() {
                @Override
                public void onSuccess(PDKResponse response) {
                    sharedPreferencesEditor.putBoolean("isLogged", true);
                    sharedPreferencesEditor.commit();

                    fetchAndDisplayPinterestBoards();
                }

                @Override
                public void onFailure(PDKException exception) {
                    sharedPreferencesEditor.putBoolean("isLogged", false);
                    sharedPreferencesEditor.commit();
                    Log.e(getClass().getName(), exception.getDetailMessage());

                    updateStatus(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchAndDisplayPinterestBoards() {
        String BOARD_FIELDS = "id,name,description,creator,image,counts,created_at";

        PDKClient.getInstance().getMyBoards(BOARD_FIELDS, new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {

                if (response.getBoardList().size() > 0) {
                    final List<PDKBoard> allBoards = response.getBoardList();
                    List<String> allBoardsNames = new ArrayList<>();

                    for (PDKBoard board : allBoards) {
                        allBoardsNames.add(board.getName());
                    }

                    View contentView = getLayoutInflater().inflate(R.layout.custom_dialog_with_list_view, null);
                    ListView customListView = (ListView) contentView.findViewById(R.id.custom_list_view);
                    final CustomDialogListAdapter customListAdapter = new CustomDialogListAdapter(activityContext, R.layout.custom_dialog_list_item, allBoardsNames, Constants.SelectionType.PINTEREST_BOARD);
                    customListView.setAdapter(customListAdapter);

                    final MCDialogWithCustomView dialogWithListView = new MCDialogWithCustomView(activityContext, PostMessageActivity.this, Constants.SelectionType.PINTEREST_BOARD, "Choose Board", "Ok", contentView);

                    customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            dialogWithListView.dismiss();

                            createNewPinInPinterest(allBoards.get(position).getUid());
                        }
                    });

                    dialogWithListView.show();

                } else {
                    hideProgressBar();
                    MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), "No Pinterest Board(s) Found.");
                }
            }

            @Override
            public void onFailure(PDKException exception) {
                Log.e(getClass().getName(), exception.getDetailMessage());
            }
        });
    }

    public void createNewPinInPinterest(String boardId) {
        try {
            String url = contentValues.getAsString("pinItUrl");

            /*if (shareType == Constants.ShareType.image) {
                File imageFile = new File(contentValues.getAsString("imagePath"));
                //byte[] imageData = readLocalFile(imageFile);
                //url = Base64.encodeToString(imageData, Base64.NO_WRAP);
                url = imageFile.toString();
            }*/

            PDKClient.getInstance().createPin(contentValues.getAsString("postMessage"), boardId, contentValues.getAsString("picture"), url, new PDKCallback() {
                @Override
                public void onSuccess(PDKResponse response) {
                    Log.d(getClass().getName(), response.getData().toString());
                    updateStatus(true);
                }

                @Override
                public void onFailure(PDKException exception) {
                    Log.e(getClass().getName(), exception.getDetailMessage());
                    updateStatus(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dialogSelectionCallback(Constants.SelectionType selectionType) {
        // You can write any code to execute after selecting any row & click on dialog Ok button.
    }

    public byte[] readLocalFile(File file) throws IOException, FileNotFoundException {
        ByteArrayOutputStream ous = null;
        InputStream ios = null;

        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        } finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }

        return ous.toByteArray();
    }

    /********************* Pinterest Sharing Functionality Ends Here **********************/
}