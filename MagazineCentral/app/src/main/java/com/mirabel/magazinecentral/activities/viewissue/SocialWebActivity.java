package com.mirabel.magazinecentral.activities.viewissue;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.github.scribejava.apis.LinkedInApi;
import com.github.scribejava.apis.TumblrApi;
import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.constants.Constants;

import org.json.JSONObject;

public class SocialWebActivity extends Activity {
    WebView webView = null;
    OAuth10aService service = null;
    OAuth1RequestToken requestToken = null;
    public String apiKey = null;
    public String apiSecret = null;
    public String sharedPreferencesKey = null;
    Constants.SocialType socialType = null;
    ProgressBar progressBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_web);

        try {
            WindowManager wm = (WindowManager) MyApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            getWindow().setGravity(Gravity.CENTER);

            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            Intent intent = getIntent();
            apiKey = intent.getStringExtra("apiKey");
            apiSecret = intent.getStringExtra("apiSecret");
            Constants.SocialType type = Constants.SocialType.values()[intent.getIntExtra("socialType", 0)];
            String authorizationUrl = null;

            switch (type) {
                case twitter: {
                    service = new ServiceBuilder()
                            .apiKey(Constants.TWITTER_API_KEY)
                            .apiSecret(Constants.TWITTER_API_SECRET)
                            .callback("twitter://open")
                            .build(TwitterApi.instance());

                    requestToken = service.getRequestToken();
                    authorizationUrl = service.getAuthorizationUrl(requestToken);

                    sharedPreferencesKey = TwitterApi.class.getName();
                    socialType = Constants.SocialType.twitter;
                }
                break;

                case tumblr: {
                    service = new ServiceBuilder()
                            .apiKey(Constants.TUMBLR_API_KEY)
                            .apiSecret(Constants.TUMBLR_API_SECRET)
                            .callback("tumblr:open")
                            .build(TumblrApi.instance());

                    requestToken = service.getRequestToken();
                    authorizationUrl = service.getAuthorizationUrl(requestToken);

                    sharedPreferencesKey = TumblrApi.class.getName();
                    socialType = Constants.SocialType.tumblr;
                }
                break;

                case linkedin: {
                    try {
                        service = new ServiceBuilder()
                                .scope("r_basicprofile w_share")
                                .apiKey(Constants.LINKED_IN_API_KEY)
                                .apiSecret(Constants.LINKED_IN_API_SECRET)
                                .callback("https://localhost/auth/linkedin/callback")
                                .build(LinkedInApi.instance());

                        requestToken = service.getRequestToken();
                        authorizationUrl = service.getAuthorizationUrl(requestToken);

                        sharedPreferencesKey = LinkedInApi.class.getName();
                        socialType = Constants.SocialType.linkedin;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

                default:
                    break;
            }

            SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);

            if (prefs.getBoolean("isLogged", false)) {
                // if isLogged is true we don't come to this activity & we are posting image directly to the selected social site in Social class itself.
            } else {
                progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.animate();
                webView = (WebView) findViewById(R.id.webView);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setUseWideViewPort(true);
                webView.setWebViewClient(new CustomWebClient());
                webView.loadUrl(authorizationUrl);
                webView.getSettings().setSupportZoom(true);
                webView.getSettings().setBuiltInZoomControls(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CustomWebClient extends WebViewClient {

        @TargetApi(android.os.Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            return shouldInterceptRequest(view, request.getUrl().toString());
        }

        @SuppressWarnings("deprecation")
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            return super.shouldInterceptRequest(view, url);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();

            return shouldOverrideUrlLoading(view, url);
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url) {
            if (!progressBar.isShown()) {
                progressBar.animate();
                progressBar.setVisibility(View.VISIBLE);
            }

            //check for our custom callback protocol otherwise use default behavior
            if (socialType == Constants.SocialType.twitter && url.startsWith("twitter")) {
                try {
                    Uri uri = Uri.parse(url);
                    String oauth_token = uri.getQueryParameter("oauth_token");
                    String oauth_verifier = uri.getQueryParameter("oauth_verifier");

                    final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, oauth_verifier);

                    // Now let's go and ask for a protected resource! Below request will get logged in user profile details.
                    /*final OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/account/verify_credentials.json");
                    service.signRequest(accessToken, request);
                    final Response response = service.execute(request);*/

                    //host twitter detected from callback twitter://open
                    if (uri.getHost().equals("open")) {
                        SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                        SharedPreferences.Editor prefsEditor = prefs.edit();
                        prefsEditor.putString("ACCESS_TOKEN", accessToken.getToken());
                        prefsEditor.putString("ACCESS_SECRET", accessToken.getTokenSecret());
                        prefsEditor.putBoolean("isLogged", true);
                        prefsEditor.commit();
                        setResult(RESULT_OK);
                        finish();
                        return false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;

            } else if (socialType == Constants.SocialType.tumblr && url.startsWith("tumblr")) {
                try {
                    Uri uri = Uri.parse(url);
                    String oauth_token = uri.getQueryParameter("oauth_token");
                    String oauth_verifier = uri.getQueryParameter("oauth_verifier");

                    final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, oauth_verifier);

                    SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putString("ACCESS_TOKEN", accessToken.getToken());
                    prefsEditor.putString("ACCESS_SECRET", accessToken.getTokenSecret());
                    OAuthRequest userInfoReq = new OAuthRequest(Verb.GET, "http://api.tumblr.com/v2/user/info");
                    service.signRequest(accessToken, userInfoReq);
                    final Response response = service.execute(userInfoReq);

                    JSONObject jObj = new JSONObject(response.getBody());
                    String blogName = jObj.getJSONObject("response").getJSONObject("user").getString("name");
                    prefsEditor.putString("blogName", blogName + ".tumblr.com");
                    prefsEditor.putBoolean("isLogged", true);
                    prefsEditor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                setResult(RESULT_OK);
                finish();

            } else if (socialType == Constants.SocialType.linkedin && url.contains("linkedin") && url.contains("oauth_verifier")) {
                try {
                    Uri uri = Uri.parse(url);
                    String oauth_token = uri.getQueryParameter("oauth_token");
                    String oauth_verifier = uri.getQueryParameter("oauth_verifier");

                    final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, oauth_verifier);

                    SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putString("ACCESS_TOKEN", accessToken.getToken());
                    prefsEditor.putString("ACCESS_SECRET", accessToken.getTokenSecret());
                    prefsEditor.putBoolean("isLogged", true);
                    prefsEditor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                setResult(RESULT_OK);
                finish();

            }

            return super.shouldOverrideUrlLoading(webview, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url.contains("verifier")) {
                Log.d(SocialWebActivity.class.getSimpleName(), url);
            }

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (progressBar.isShown()) {
                progressBar.clearAnimation();
                progressBar.setVisibility(View.INVISIBLE);
            }

            if (url.contains("verifier")) {
                Log.d(SocialWebActivity.class.getSimpleName(), url);
            }

            super.onPageFinished(view, url);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }
    }

    public void onDone(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }
}