package com.mirabel.magazinecentral.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.IssueDetails;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCButton;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;
import com.mirabel.magazinecentral.models.CatalogDownloaderModel;
import com.mirabel.magazinecentral.services.DownloadService;
import com.mirabel.magazinecentral.util.NetworkConnectionDetector;
import com.mirabel.magazinecentral.util.Utility;

import java.io.File;
import java.util.List;

public class IssueDetailsActivity extends AppCompatActivity implements AlertDialogSelectionListener {
    public static final String TAG = IssueDetailsActivity.class.getSimpleName();

    private Context applicationContext, activityContext;
    private LayoutInflater mLayoutInflater;
    private Toolbar mToolbar;
    private ImageView idp_cover_page;
    private MCTextView header_title, idp_issue_name, idp_issue_category, idp_issue_size, idp_issue_description, idp_download_progress_text, no_recent_issues_found_message, no_similar_issues_found_message;
    private MCButton idp_download_button;
    private ProgressBar idp_download_progress_bar;
    private LinearLayout idp_recent_issues_gallery, idp_similar_issues_gallery;

    private Content currentIssueContent;
    private String activityTitle;
    private CatalogDownloaderModel catalogDownloaderModel;
    private IssueDetails issueDetails;
    private GlobalContent globalContent;
    private ProgressBar currentlyDownloadingProgressBar = null;
    private ImageView currentlyDownloadingStatusIcon = null;
    private boolean isCurrentlyDownloadingMagazineViewRefsInstantiated = false, isCurrentIssueDownloading = false;
    private int recentIssuesSize = 0, similarIssuesSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_details);

        try {
            applicationContext = getApplicationContext();
            activityContext = IssueDetailsActivity.this;
            mLayoutInflater = LayoutInflater.from(activityContext);

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                currentIssueContent = (Content) bundle.getSerializable(Constants.BUNDLE_SELECTED_ISSUE_CONTENT);
                activityTitle = currentIssueContent.getProductName();
            }

            /*mToolbar = (Toolbar) findViewById(R.id.issue_detail_page_toolbar);
            mToolbar.setNavigationIcon(R.drawable.icon_tollbar_home);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(activityTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

            header_title = (MCTextView) findViewById(R.id.header_title);
            header_title.setText(activityTitle);

            idp_cover_page = (ImageView) findViewById(R.id.idp_cover_page);
            idp_issue_name = (MCTextView) findViewById(R.id.idp_issue_name);
            idp_issue_category = (MCTextView) findViewById(R.id.idp_issue_category);
            idp_issue_size = (MCTextView) findViewById(R.id.idp_issue_size);
            idp_issue_description = (MCTextView) findViewById(R.id.idp_issue_description);
            idp_download_progress_text = (MCTextView) findViewById(R.id.idp_download_progress_text);
            no_recent_issues_found_message = (MCTextView) findViewById(R.id.no_recent_issues_found_message);
            no_similar_issues_found_message = (MCTextView) findViewById(R.id.no_similar_issues_found_message);
            idp_download_button = (MCButton) findViewById(R.id.idp_download_button);
            idp_download_progress_bar = (ProgressBar) findViewById(R.id.idp_download_progress_bar);
            idp_recent_issues_gallery = (LinearLayout) findViewById(R.id.idp_recent_issues_gallery);
            idp_similar_issues_gallery = (LinearLayout) findViewById(R.id.idp_similar_issues_gallery);

            catalogDownloaderModel = new CatalogDownloaderModel(activityContext, this);
            globalContent = GlobalContent.getInstance();

            idp_issue_name.setText(currentIssueContent.getName());
            idp_issue_category.setText(String.format("CATEGORY: %s", currentIssueContent.getCategory()));

            if (currentIssueContent.getDescription() != null) {
                idp_issue_description.setText(currentIssueContent.getDescription());
                idp_issue_description.setMovementMethod(new ScrollingMovementMethod());
            }

            final Constants.ContentState currentStateOfMagazine = globalContent.getCurrentStateOfMagazine(currentIssueContent);

            if (currentStateOfMagazine == Constants.ContentState.ContentStateNone) {
                idp_download_button.setText(getResources().getString(R.string.download));
                idp_download_progress_bar.setVisibility(View.INVISIBLE);
            } else if (currentStateOfMagazine == Constants.ContentState.ContentStateDownloaded) {
                idp_download_button.setText(getResources().getString(R.string.read_magazine));
                idp_download_progress_bar.setVisibility(View.INVISIBLE);
            } else {
                idp_download_button.setText(getResources().getString(R.string.read_magazine));
                idp_download_progress_bar.setVisibility(View.VISIBLE);
                idp_download_progress_bar.setProgress(0);
            }

            refreshData();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(downloadManagerBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        Intent showHomePageIntent = new Intent(activityContext, MainActivity.class);
        showHomePageIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(showHomePageIntent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void onVolleyErrorResponse(VolleyError error) {
        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), getResources().getString(R.string.error_failure));
    }

    public void refreshData() {
        if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
            if (!MCProgressDialog.isProgressDialogShown)
                MCProgressDialog.showProgressDialog(activityContext, Constants.LOADING_MESSAGE);

            catalogDownloaderModel.getIssueDetailsForProductNameAndIssueName(currentIssueContent.getProductName(), currentIssueContent.getName());
        } else {
            NetworkConnectionDetector.displayNoNetworkError(activityContext);
        }
    }

    public void updateIssueDetails(IssueDetails issueDetailsObj) {
        try {
            if (issueDetailsObj != null) {

                this.issueDetails = issueDetailsObj;

                //Updating Issue Cover Page using Glide library
                String coverPageImageUrl = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, currentIssueContent.getPublisherId(), currentIssueContent.getId());

                RequestOptions glideReqOptions = new RequestOptions();
                glideReqOptions.placeholder(R.drawable.cover_page);
                Glide.with(this).load(coverPageImageUrl).apply(glideReqOptions).into(idp_cover_page);

                idp_issue_name.setText(currentIssueContent.getName());

                if (issueDetailsObj.getIssueInfo().getCategory() != null)
                    idp_issue_category.setText(String.format("CATEGORY: %s", issueDetailsObj.getIssueInfo().getCategory()));
                else
                    idp_issue_category.setText(String.format("CATEGORY: "));

                if (issueDetailsObj.getIssueInfo().getContentSize() != null)
                    idp_issue_size.setText(String.format("Size : %.2f MB", (float) issueDetailsObj.getIssueInfo().getContentSize() / (1024 * 1024)));
                else
                    idp_issue_size.setVisibility(View.GONE);

                if (issueDetailsObj.getIssueInfo().getDescription() != null)
                    idp_issue_description.setText(issueDetailsObj.getIssueInfo().getDescription());

                List<Content> recentIssues = issueDetailsObj.getRecentIssues();
                List<Content> similarIssues = issueDetailsObj.getSimilarIssues();

                recentIssuesSize = recentIssues.size();
                similarIssuesSize = similarIssues.size();

                if (recentIssuesSize > 0) {
                    updateRecentIssues(recentIssues);
                } else {
                    idp_recent_issues_gallery.setVisibility(View.GONE);
                    no_recent_issues_found_message.setVisibility(View.VISIBLE);
                }

                if (similarIssuesSize > 0) {
                    updateSimilarIssues(similarIssues);
                } else {
                    idp_similar_issues_gallery.setVisibility(View.GONE);
                    no_similar_issues_found_message.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MCProgressDialog.hideProgressDialog();
    }

    public void updateRecentIssues(List<Content> recentIssues) {
        try {
            for (Content content : recentIssues) {
                View view = getMagazineView(content);

                if (view != null)
                    idp_recent_issues_gallery.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSimilarIssues(List<Content> popularIssues) {
        try {
            for (Content content : popularIssues) {
                View view = getMagazineView(content);

                if (view != null)
                    idp_similar_issues_gallery.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View getMagazineView(final Content content) {
        View magazineView = null;

        try {
            magazineView = mLayoutInflater.inflate(R.layout.magazine_issue, idp_recent_issues_gallery, false);
            magazineView.setTag(content.getId());

            ImageView cover_page = (ImageView) magazineView.findViewById(R.id.cover_page);
            MCTextView product_name = (MCTextView) magazineView.findViewById(R.id.product_name);
            MCTextView issue_name = (MCTextView) magazineView.findViewById(R.id.issue_name);
            final ImageView download_status_icon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
            final ProgressBar download_progress_bar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

            product_name.setText(content.getProductName());
            issue_name.setText(content.getName());

            //Updating Issue Cover Page using Glide library
            String coverPageImageUrl = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());

            RequestOptions glideReqOptions = new RequestOptions();
            glideReqOptions.placeholder(R.drawable.cover_page);
            Glide.with(this).load(coverPageImageUrl).apply(glideReqOptions).into(cover_page);

            final Constants.ContentState currentStateOfMagazine = globalContent.getCurrentStateOfMagazine(content);

            if (currentStateOfMagazine == Constants.ContentState.ContentStateNone) {
                download_status_icon.setImageResource(R.drawable.icon_download);
                download_progress_bar.setVisibility(View.INVISIBLE);
            } else if (currentStateOfMagazine == Constants.ContentState.ContentStateDownloaded) {
                download_status_icon.setImageResource(R.drawable.icon_preview);
                download_progress_bar.setVisibility(View.INVISIBLE);
            } else {
                download_status_icon.setImageResource(R.drawable.icon_downloading);
                download_progress_bar.setVisibility(View.VISIBLE);
                download_progress_bar.setProgress(0);
            }

            cover_page.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
                        openIssueDetailPage(content);
                    } else {
                        // Read Magazine
                        readMagazine(content);
                    }
                }
            });

            download_status_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
                        downloadIssue(content, download_status_icon);
                    } else {
                        // Open Issue Detail Page
                        openIssueDetailPage(content);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return magazineView;
    }

    public void downloadIssue(View view) {
        if (currentIssueContent != null) {
            if (globalContent.getCurrentStateOfMagazine(currentIssueContent) == Constants.ContentState.ContentStateNone) {
                if (Utility.getInstance().verifyStoragePermissions(this)) {
                    idp_download_button.setText(getResources().getString(R.string.read_magazine));
                    idp_download_progress_bar.setVisibility(View.VISIBLE);
                    idp_download_progress_bar.setProgress(0);

                    startIssueDownload(currentIssueContent);
                } else {
                    GlobalContent.currentDownloadingIssue = currentIssueContent;
                }
            } else {
                readMagazine(currentIssueContent);
            }
        }
    }

    public void downloadIssue(Content content, ImageView downloadIcon) {
        if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
            if (Utility.getInstance().verifyStoragePermissions(this)) {
                downloadIcon.setImageResource(R.drawable.icon_downloading);
                startIssueDownload(content);
            } else {
                GlobalContent.currentDownloadingIssue = content;
            }
        }
    }

    public void openIssueDetailPage(Content content) {
        Intent openIssuesIntent = new Intent(activityContext, IssueDetailsActivity.class);
        openIssuesIntent.putExtra(Constants.BUNDLE_SELECTED_ISSUE_CONTENT, content);
        startActivity(openIssuesIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);

        /*Intent intent = new Intent(Constants.INTENT_ACTION_OPEN_ISSUE);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_CONTENT, currentIssueContent);
        intent.putExtras(bundle);

        // Broadcasting Selected Issue Content to Main Activity
        LocalBroadcastManager.getInstance(activityContext).sendBroadcast(intent);

        // Closing Activity
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);*/
    }

    public void startIssueDownload(Content content) {
        if (content != null) {
            content.setDownloadType(Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD);
            content.setContentState(Constants.ContentState.ContentStateDownloading);
            Intent downloadServiceIntent = new Intent(activityContext, DownloadService.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
            downloadServiceIntent.putExtras(bundle);
            startService(downloadServiceIntent);
        }
    }

    public void readMagazine(Content content) {
        File file = new File(GlobalContent.issueFolderPath + content.getId() + "/issue.xml");

        if (file.exists()) {
            Intent viewIssueActivity = new Intent(activityContext, ViewIssueActivity.class);
            viewIssueActivity.putExtra(Constants.BUNDLE_CONTENT, content);
            startActivity(viewIssueActivity);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateDownloading) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.download_progress), getResources().getString(R.string.download_wait_message));
        }
    }

    public void displayToast(String message) {
        Utility.displayToast(this, message, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startIssueDownload(GlobalContent.currentDownloadingIssue);

                if (GlobalContent.currentDownloadingIssue.equals(currentIssueContent))
                    idp_download_button.setText(getResources().getString(R.string.read_magazine));
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    MCAlertDialog.listener = this;
                    MCAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.need_storage_permission_title), getResources().getString(R.string.need_storage_permission));
                } else {
                    displayToast(getResources().getString(R.string.storage_permission_not_given));
                }
            }
        }
    }

    @Override
    public void alertDialogCallback() {
        ActivityCompat.requestPermissions(IssueDetailsActivity.this, Constants.STORAGE_PERMISSIONS, Constants.REQUEST_EXTERNAL_STORAGE);
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver downloadManagerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (!isCurrentlyDownloadingMagazineViewRefsInstantiated)
                    instantiateCurrentDownloadingMagazineViewRef(contentId);

                if (currentlyDownloadingProgressBar == null)
                    instantiateCurrentDownloadingMagazineViewRef(contentId);

                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                    int progress = (int) ((downloaded / total) * 100);

                    if (isCurrentIssueDownloading) {
                        idp_download_progress_text.setVisibility(View.VISIBLE);

                        if (contentId == GlobalContent.currentDownloadingIssue.getId()) {
                            idp_download_progress_text.setText(String.format("%.2f MB of %.2f MB", downloaded, total));
                        }
                    }

                    if (currentlyDownloadingProgressBar != null) {
                        currentlyDownloadingProgressBar.setVisibility(View.VISIBLE);

                        if (contentId == GlobalContent.currentDownloadingIssue.getId()) {
                            currentlyDownloadingProgressBar.setProgress(progress);
                        }
                    }

                    if (!isCurrentIssueDownloading && currentlyDownloadingStatusIcon != null) {
                        currentlyDownloadingStatusIcon.setImageResource(R.drawable.icon_downloading);
                    }

                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                    if (isCurrentIssueDownloading) {
                        idp_download_progress_text.setVisibility(View.INVISIBLE);
                        isCurrentIssueDownloading = false;
                    }

                    if (currentlyDownloadingProgressBar != null) {
                        currentlyDownloadingProgressBar.setVisibility(View.INVISIBLE);
                        currentlyDownloadingProgressBar = null;
                    }

                    if (!isCurrentIssueDownloading && currentlyDownloadingStatusIcon != null) {
                        currentlyDownloadingStatusIcon.setImageResource(R.drawable.icon_preview);
                        currentlyDownloadingStatusIcon = null;
                    }

                    isCurrentlyDownloadingMagazineViewRefsInstantiated = false;

                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                    if (isCurrentIssueDownloading) {
                        idp_download_progress_text.setVisibility(View.INVISIBLE);
                    }

                    if (currentlyDownloadingProgressBar != null) {
                        currentlyDownloadingProgressBar.setVisibility(View.INVISIBLE);
                        currentlyDownloadingProgressBar.setProgress(0);
                    }

                    if (!isCurrentIssueDownloading && currentlyDownloadingStatusIcon != null) {
                        currentlyDownloadingStatusIcon.setImageResource(R.drawable.icon_downloading);
                    }
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                    if (isCurrentIssueDownloading) {
                        idp_download_progress_text.setVisibility(View.VISIBLE);
                    }

                    if (currentlyDownloadingProgressBar != null) {
                        currentlyDownloadingProgressBar.setVisibility(View.VISIBLE);
                    }

                    if (!isCurrentIssueDownloading && currentlyDownloadingStatusIcon != null) {
                        currentlyDownloadingStatusIcon.setImageResource(R.drawable.icon_downloading);
                    }
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD) || intent.getAction().equals(Constants.INTENT_ACTION_REFRESH_VIEW)) {
                    if (isCurrentIssueDownloading) {
                        idp_download_progress_text.setVisibility(View.INVISIBLE);
                    }

                    if (currentlyDownloadingProgressBar != null) {
                        currentlyDownloadingProgressBar.setVisibility(View.INVISIBLE);
                    }

                    if (!isCurrentIssueDownloading && currentlyDownloadingStatusIcon != null) {
                        currentlyDownloadingStatusIcon.setImageResource(R.drawable.icon_download);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void instantiateCurrentDownloadingMagazineViewRef(String contentId) {
        if (currentIssueContent.getId().equalsIgnoreCase(contentId)) {
            currentlyDownloadingProgressBar = idp_download_progress_bar;
            currentlyDownloadingStatusIcon = null;
            isCurrentIssueDownloading = true;
        } else {
            if (recentIssuesSize > 0) {
                for (int i = 0; i < idp_recent_issues_gallery.getChildCount(); i++) {
                    View magazineView = idp_recent_issues_gallery.getChildAt(i);

                    if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                        ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                        ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                        currentlyDownloadingProgressBar = progressBar;
                        currentlyDownloadingStatusIcon = downloadStatusIcon;
                        isCurrentIssueDownloading = false;
                    }
                }
            }

            if (similarIssuesSize > 0) {
                for (int i = 0; i < idp_similar_issues_gallery.getChildCount(); i++) {
                    View magazineView = idp_similar_issues_gallery.getChildAt(i);

                    if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                        ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                        ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                        currentlyDownloadingProgressBar = progressBar;
                        currentlyDownloadingStatusIcon = downloadStatusIcon;
                        isCurrentIssueDownloading = false;
                    }
                }
            }
        }

        isCurrentlyDownloadingMagazineViewRefsInstantiated = true;
    }
}
