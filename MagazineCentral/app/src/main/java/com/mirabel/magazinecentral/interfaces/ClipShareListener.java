package com.mirabel.magazinecentral.interfaces;

import com.mirabel.magazinecentral.constants.Constants;

public interface ClipShareListener {
    void dismissClipShare();

    void share(Constants.ShareType shareType);

    void clipImage(Constants.SocialType socialType);
}