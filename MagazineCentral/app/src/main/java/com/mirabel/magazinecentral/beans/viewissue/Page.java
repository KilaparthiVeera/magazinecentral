package com.mirabel.magazinecentral.beans.viewissue;

import java.util.ArrayList;

public class Page {
    public String imageName;
    public String imageType;
    public ArrayList<Hotspot> hotspots;
    public ArrayList<Interactivity> activities;
    public String text;
    public int index = 0;

    public Page(Page p) {
        this.imageName = p.imageName;
        this.imageType = p.imageType;
        if (p.hotspots != null) {
            this.hotspots = new ArrayList<>(p.hotspots);
        }

        if (p.activities != null) {
            this.activities = new ArrayList<>(p.activities);
        }

        this.text = p.text;
        this.index = p.index;
    }

    @Override
    public boolean equals(Object pageName) {
        Page p = (Page) pageName;

        if (this.imageName.equalsIgnoreCase(p.imageName)) {
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {

        return this.index;
    }

    public Page() {

    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public ArrayList<Hotspot> getHotspots() {
        return hotspots;
    }

    public void setHotspots(ArrayList<Hotspot> hotspots) {
        this.hotspots = hotspots;
    }

    public ArrayList<Interactivity> getActivities() {
        return activities;
    }

    public void setActivities(ArrayList<Interactivity> activities) {
        this.activities = activities;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
