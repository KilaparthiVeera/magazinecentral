package com.mirabel.magazinecentral.beans;

import android.content.Context;

import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.database.DBHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by venkat on 7/7/17.
 */

public class GlobalContent {
    private static GlobalContent instance;
    public static Context context = null;
    public static String issueFolderPath = "";
    private String magazinesCount;
    private HomePageCatalog homePageCatalog;
    private ArrayList<Category> categoriesList = new ArrayList<>();
    private ArrayList<Content> categoryIssuesList = new ArrayList<>();
    private ArrayList<Publisher> publisherList = new ArrayList<>();
    private ArrayList<Content> searchResultIssuesList = new ArrayList<>();
    private ArrayList<Content> librarySearchResultIssuesList = new ArrayList<>();
    private List<Content> downloadingContents = new ArrayList<>();
    private List<Content> runningContents = new ArrayList<>();
    //private ArrayList<Content> completeIssuesList = new ArrayList<>(); // this contains all available issues list in the current application.
    private List<Content> libraryIssuesList = new ArrayList<>();
    private List<String> downloadedIssuesList = new ArrayList<>();
    public static Content currentDownloadingIssue = null;
    public static int width;
    public static int height;
    public static int screenWidth;
    public static int screenHeight;
    public static Constants.OrientationType orientation;
    public String primaryEmail = "";

    /**
     * This variable - "isDownloadRunning" is to check if any download is in progress so that,
     * appropriate <i>play/pause</i> can be set to the Issue when download is started
     */
    public static boolean isDownloadRunning = false;

    public static GlobalContent getInstance() {
        if (instance == null)
            instance = new GlobalContent();

        return instance;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        GlobalContent.context = context;
    }

    public static String getIssueFolderPath() {
        return issueFolderPath;
    }

    public static void setIssueFolderPath(String issueFolderPath) {
        GlobalContent.issueFolderPath = issueFolderPath;
    }

    public String getMagazinesCount() {
        return magazinesCount;
    }

    public void setMagazinesCount(String magazinesCount) {
        this.magazinesCount = magazinesCount;
    }

    public HomePageCatalog getHomePageCatalog() {
        return homePageCatalog;
    }

    public void setHomePageCatalog(HomePageCatalog homePageCatalog) {
        this.homePageCatalog = homePageCatalog;
    }

    public ArrayList<Category> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(ArrayList<Category> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public ArrayList<Content> getCategoryIssuesList() {
        return categoryIssuesList;
    }

    public void setCategoryIssuesList(ArrayList<Content> categoriesIssuesArrayList) {
        if (categoriesIssuesArrayList == null) {
            if (this.categoryIssuesList.size() > 0) {
                this.categoryIssuesList.clear();
                this.categoryIssuesList = null;
            }
            this.categoryIssuesList = new ArrayList<>();
        } else {
            for (Content content : categoriesIssuesArrayList) {
                if (!this.categoryIssuesList.contains(content)) {
                    this.categoryIssuesList.add(content);
                }
            }
        }
    }

    public ArrayList<Publisher> getPublisherList() {
        return publisherList;
    }

    public void setPublisherList(ArrayList<Publisher> publisherList) {
        this.publisherList = publisherList;
    }

    public ArrayList<String> getPublishersNamesList() {
        ArrayList<String> publishersNamesList = new ArrayList<>();

        for (Publisher publisher : publisherList) {
            publishersNamesList.add(publisher.getPublisherName());
        }

        return publishersNamesList;
    }

    public ArrayList<Content> getSearchResultIssuesList() {
        return searchResultIssuesList;
    }

    public void setSearchResultIssuesList(ArrayList<Content> searchIssuesList) {
        if (searchIssuesList == null) {
            if (this.searchResultIssuesList.size() > 0) {
                this.searchResultIssuesList.clear();
                this.searchResultIssuesList = null;
            }
            this.searchResultIssuesList = new ArrayList<>();
        } else {
            for (Content content : searchIssuesList) {
                if (!this.searchResultIssuesList.contains(content)) {
                    this.searchResultIssuesList.add(content);
                }
            }
        }
    }

    public List<Content> getDownloadingContents() {
        return downloadingContents;
    }

    public void setDownloadingContents(List<Content> downloadingContents) {
        this.downloadingContents = downloadingContents;
    }

    public List<Content> getRunningContents() {
        return runningContents;
    }

    public void setRunningContents(List<Content> runningContents) {
        this.runningContents = runningContents;
    }

    public List<Content> getLibraryIssuesList() {
        return libraryIssuesList;
    }

    public void setLibraryIssuesList(List<Content> libraryIssuesList) {
        this.libraryIssuesList = libraryIssuesList;
    }

    public List<String> getDownloadedIssuesList() {
        return downloadedIssuesList;
    }

    public void setDownloadedIssuesList(List<String> downloadedIssuesList) {
        this.downloadedIssuesList = downloadedIssuesList;
    }

    public void updateDownloadedContents() {
        downloadedIssuesList.clear();

        DBHelper dbHelper = new DBHelper(context);
        ArrayList<String> downloadCompletedContentIds = (ArrayList<String>) dbHelper.getDownloadCompletedContentIds();
        downloadedIssuesList.addAll(downloadCompletedContentIds);

        updateLibrary();
    }

    public void updatePendingDownloadContents() {
        /*for (int i = 0; i < completeIssuesList.size(); i++) {
            Content content = completeIssuesList.get(i);
            for (int j = 0; j < downloadingContents.size(); j++) {
                Content temp = downloadingContents.get(j);
                if (content.equals(temp)) {
                    content.setContentState(Constants.ContentState.ContentStateDownloading);
                    content.setDownloadType(temp.getDownloadType());
                }
            }
        }*/

        updateLibrary();
    }

    /**
     * To update the library with Download/View based on the download/archive i.e., when ever an issue is download/archived,
     * then updateLibrary() method should be invoked to update the issues in library.
     */
    public void updateLibrary() {
        if (libraryIssuesList != null) {
            libraryIssuesList.clear();
        }

        DBHelper dbHelper = new DBHelper(context);
        ArrayList<Content> libraryContents = dbHelper.getLibrary();
        libraryIssuesList.addAll(libraryContents);

        for (Content content : libraryIssuesList) {
            if (!runningContents.contains(content) && content.getContentState() != Constants.ContentState.ContentStateDownloaded) {
                content.setContentState(Constants.ContentState.ContentStatePaused);
            }
        }
    }

    public Constants.ContentState getCurrentStateOfMagazine(Content content) {
        if (downloadedIssuesList.contains(content.getId())) {
            return Constants.ContentState.ContentStateDownloaded;
        } else if (downloadingContents.contains(content)) {
            return Constants.ContentState.ContentStateDownloading;
        } else {
            return Constants.ContentState.ContentStateNone;
        }
    }

    public boolean isIssueCurrentlyDownloading(String contentId) {
        if (runningContents.isEmpty()) {
            return false;
        } else {
            Content currentRunningIssue = runningContents.get(0);

            if (currentRunningIssue != null && currentRunningIssue.getId().equals(contentId))
                return true;
            else
                return false;
        }
    }

    public ArrayList<String> getProductNames() {
        Set<String> distinctProductNames = new HashSet<>();

        for (Content content : libraryIssuesList) {
            distinctProductNames.add(content.getProductName());
        }

        return new ArrayList<>(distinctProductNames);
    }

    public Map<String, String> getPublisherListForLibrary() {
        // We are sorting publishers list by publisher name so we are saving in TreeMap bcoz it will sort by keys by default.
        Map<String, String> publishersList = new TreeMap<>();

        for (Content content : libraryIssuesList) {
            publishersList.put(content.getPublisherName(), content.getPublisherId());
        }

        Map<String, String> publishersHashMap = new LinkedHashMap<>();
        publishersHashMap.put("Select Publisher", "0");
        publishersHashMap.put("All", "-1");
        publishersHashMap.putAll(publishersList);

        return publishersHashMap;
    }

    public ArrayList<Content> getLibrarySearchResultIssuesList() {
        return librarySearchResultIssuesList;
    }

    public void setLibrarySearchResultIssuesList(ArrayList<Content> searchResultIssuesList) {
        if (searchResultIssuesList == null) {
            if (this.librarySearchResultIssuesList.size() > 0) {
                this.librarySearchResultIssuesList.clear();
                this.librarySearchResultIssuesList = null;
            }
            this.librarySearchResultIssuesList = new ArrayList<>();
        } else {
            for (Content content : searchResultIssuesList) {
                if (!this.librarySearchResultIssuesList.contains(content)) {
                    this.librarySearchResultIssuesList.add(content);
                }
            }
        }
    }
}
