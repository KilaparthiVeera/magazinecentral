package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.capricorn.ArcMenu;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.interfaces.ClipShareListener;

/**
 * Created by venkat on 9/18/17.
 */

public class ClipShare extends FrameLayout {
    FrameLayout thisLayout = null;
    Paint p = new Paint();
    GestureDetector mDetector = null;
    public View changeView = null;
    boolean left, right, top, bottom;
    ArcMenu arcMenu = null;
    private static final int[] ITEM_DRAWABLES = {R.drawable.c_email, R.drawable.c_facebook, R.drawable.c_twitter,R.drawable.c_tumblr, R.drawable.c_linkedin,R.drawable.c_pinterest};
    boolean isShareOptionsShown = false;
    ClipShareListener clipShareListener = null;
    int clipPadding = 0, minClipSize = 100, touchThreshold = 50;

    public ClipShare(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ClipShare(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClipShare(Context context) {
        super(context);

        try {
            clipShareListener = (ClipShareListener) context;
            mDetector = new GestureDetector(context, new GestureListener());
            arcMenu = new ArcMenu(context);

            final int itemCount = ITEM_DRAWABLES.length;
            final float scale = getResources().getDisplayMetrics().density;
            clipPadding = getResources().getInteger(R.integer.clipPadding);
            minClipSize = getResources().getInteger(R.integer.minClipSize);
            touchThreshold = getResources().getInteger(R.integer.thresholdTouch);

            for (int i = 0; i < itemCount; i++) {
                ImageView item = new ImageView(context);
                item.setLayoutParams(new RelativeLayout.LayoutParams((int) (100 * scale + 0.5f), (int) (100 * scale + 0.5f)));
                item.setScaleType(ImageView.ScaleType.FIT_XY);
                item.setImageResource(ITEM_DRAWABLES[i]);

                final int position = i;
                arcMenu.addItem(item, new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        new Runnable() {
                            @Override
                            public void run() {
                                arcMenu.setVisibility(View.INVISIBLE);
                                clipShareListener.clipImage(Constants.SocialType.values()[position]);
                                arcMenu.setVisibility(View.VISIBLE);
                            }
                        }.run();
                    }
                });// Add a menu item*/
            }

            FrameLayout.LayoutParams arcMenuParams = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            arcMenu.setLayoutParams(arcMenuParams);
            arcMenu.setGravity(Gravity.TOP | Gravity.LEFT);
            arcMenu.setX(-getResources().getInteger(R.integer.radius));
            arcMenu.setY(-getResources().getInteger(R.integer.radius));
            arcMenu.setClickable(true);

            thisLayout = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.clip_share_layout, null);
            changeView = new View(context);
            changeView.setBackgroundResource(R.drawable.dashed_border);

            FrameLayout.LayoutParams changeViewParam = new FrameLayout.LayoutParams(minClipSize * 2, minClipSize * 2);
            changeView.setLayoutParams(changeViewParam);
            changeView.setX((GlobalContent.screenWidth / 2) - minClipSize);
            changeView.setY((GlobalContent.screenHeight / 2) - minClipSize);

            addView(thisLayout);
            setWillNotDraw(false);
            p.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.CLEAR));
            thisLayout.addView(arcMenu);
            thisLayout.addView(changeView);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setAlpha(180);

            float _x1 = changeView.getX() + 15;
            float _y1 = changeView.getY() + 15;
            float _x2 = _x1 + changeView.getWidth() - 30;
            float _y2 = _y1 + changeView.getHeight() - 30;

            RectF[] dark = new RectF[]{
                    new RectF(0, 0, _x1, GlobalContent.screenHeight), // Left Side
                    new RectF(_x1, 0, _x2, _y1), // Top Side
                    new RectF(_x2, 0, GlobalContent.screenWidth, GlobalContent.screenHeight), // Right Side
                    new RectF(_x1, _y2, _x2, GlobalContent.screenHeight) // Bottom Side
            };

            for (RectF rectF : dark) {
                canvas.drawRect(rectF, paint);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    private int _xDelta;
    private int _yDelta;
    boolean shouldMove = false;
    boolean isDraggable = false;
    private float x1, y1, x2, y2;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        mDetector.onTouchEvent(event);

        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                arcMenu.setVisibility(GONE);
                if (isDraggable(changeView, X, Y)) {
                    isDraggable = true;
                    x1 = changeView.getX();
                    x2 = changeView.getX() + changeView.getWidth();
                    y1 = changeView.getY();
                    y2 = changeView.getY() + changeView.getHeight();
                    _xDelta = (int) (X - x1);
                    _yDelta = (int) (Y - y1);
                }

                if (isViewContains(changeView, X, Y)) {
                    shouldMove = true;
                    _xDelta = X - (int) changeView.getX();
                    _yDelta = Y - (int) changeView.getY();
                    break;
                }

            case MotionEvent.ACTION_UP:
                arcMenu.setVisibility(VISIBLE);
                shouldMove = false;
                isDraggable = false;
                left = false;
                right = false;
                top = false;
                bottom = false;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_CANCEL:
                arcMenu.setVisibility(VISIBLE);
                break;
            case MotionEvent.ACTION_MOVE:
                if (isDraggable) {
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) changeView.getLayoutParams();

                    if (left) {
                        layoutParams.width = (int) Math.min(GlobalContent.screenWidth - minClipSize / 2, Math.max(minClipSize, x2 - X));
                        changeView.setLayoutParams(layoutParams);
                        changeView.setX(x2 - layoutParams.width);
                    }

                    if (right) {
                        layoutParams.width = (int) Math.min(GlobalContent.screenWidth - minClipSize / 2, Math.max(minClipSize, X - x1));
                        changeView.setLayoutParams(layoutParams);
                        changeView.setX(x1);
                    }

                    if (top) {
                        layoutParams.height = (int) Math.min(Math.max(minClipSize, y2 - Y), GlobalContent.screenHeight - minClipSize / 2);
                        changeView.setLayoutParams(layoutParams);
                        changeView.setY(y2 - layoutParams.height);
                    }

                    if (bottom) {
                        layoutParams.height = (int) Math.min(Math.max(minClipSize, Y - y1), GlobalContent.screenHeight - minClipSize / 2);
                        changeView.setLayoutParams(layoutParams);
                        changeView.setY(y1);
                    }

                } else if (shouldMove) {
                    float _x = Math.max(0, X - _xDelta);
                    if (_x + changeView.getWidth() > GlobalContent.screenWidth) {
                        _x = GlobalContent.screenWidth - changeView.getWidth();
                    }

                    changeView.setX(_x);

                    float _y = Math.max(0, Y - _yDelta);
                    if (_y + changeView.getHeight() > GlobalContent.screenHeight) {
                        _y = GlobalContent.screenHeight - changeView.getHeight();
                    }
                    changeView.setY(_y);
                    requestLayout();
                }
                break;
        }

        return true;
    }

    private boolean isViewContains(View view, int rx, int ry) {
        int[] l = new int[2];
        view.getLocationOnScreen(l);
        int x = l[0];
        int y = l[1];
        int w = view.getWidth();
        int h = view.getHeight();

        if (rx < x || rx > x + w || ry < y || ry > y + h) {
            return false;
        }

        return true;
    }

    private boolean isDraggable(View view, int rx, int ry) {
        int[] l = new int[2];
        view.getLocationOnScreen(l);
        int x = l[0];
        int y = l[1];
        int w = view.getWidth();
        int h = view.getHeight();

        if (Math.abs(rx - x) < touchThreshold) {
            left = true;
            x2 = changeView.getX() + changeView.getWidth();
        }

        if (Math.abs(rx - (x + w)) < touchThreshold) {
            right = true;
            x1 = changeView.getX();
        }

        if (Math.abs(ry - y) < touchThreshold) {
            top = true;
            y2 = changeView.getY() + changeView.getWidth();
        }

        if (Math.abs(ry - (y + h)) < touchThreshold) {
            bottom = true;
            y1 = changeView.getY();
        }

        if (left || right || top || bottom) {
            isDraggable = true;
            return true;
        }

        isDraggable = false;
        return false;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (!isViewContains(changeView, (int) e.getRawX(), (int) e.getRawY())) {
                clipShareListener.dismissClipShare();
            }

            if (isViewContains(arcMenu, (int) e.getRawX(), (int) e.getRawY())) {
                arcMenu.showOrHide();
                isShareOptionsShown = !isShareOptionsShown;
            } else {
                //Toast.makeText(MyApplication.getAppContext(), "Dismissed Clip 'n' Share", Toast.LENGTH_LONG).show();
            }
            return super.onSingleTapConfirmed(e);
        }
    }
}
