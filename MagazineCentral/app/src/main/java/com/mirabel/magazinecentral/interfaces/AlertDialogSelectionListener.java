package com.mirabel.magazinecentral.interfaces;

/**
 * Created by venkat on 01/07/16.
 */
public interface AlertDialogSelectionListener {
    void alertDialogCallback();
}
