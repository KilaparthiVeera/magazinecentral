package com.mirabel.magazinecentral.util;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.Toast;

import com.mirabel.magazinecentral.constants.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by venkat.
 *
 * @author venkat
 */
public class Utility {

    private static Utility instance;

    public static Utility getInstance() {
        if (instance == null)
            instance = new Utility();
        return instance;
    }

    public static int getDeviceOrientation(Context context) {
        return context.getResources().getConfiguration().orientation;
    }

    public static boolean isLandscapeOrientation(Context context) {
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return true;
        else
            return false;
    }

    public static int getDeviceWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static float getDeviceDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static void displayToast(Context context, String message, int length) {
        Toast.makeText(context, message, length).show();
    }

    public static void logResult(String tag, String message) {
        Log.d(tag, message);
    }

    //Method to convert UTC time to Local time
    public static String formatTime(long timeInMillSecs, String format) {
        Date utcTime = new Date(timeInMillSecs);
        DateFormat df = new SimpleDateFormat(format);
        return df.format(utcTime);
    }

    public static String formatDate(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static Date convertStringToDate(String stringDate, String format) {
        Date date = null;
        try {
            DateFormat df = new SimpleDateFormat(format);
            date = df.parse(stringDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String getHexaStringForColor(int colorCode) {
        return String.format("#%06X", (0xFFFFFF & colorCode));
    }

    /**
     * @param values
     * @param delimiter
     * @return
     */
    public static String convertArrayListToString(ArrayList values, String delimiter) {
        StringBuilder sb = new StringBuilder();

        for (Object obj : values) {
            sb.append(obj);

            if (values.size() > 1)
                sb.append(delimiter);
        }

        if (values.size() > 1)
            sb.setLength(sb.length() - 1);

        return sb.toString();
    }

    /**
     * Checks if the device is a tablet or a phone
     * <p/>
     * Note:
     * 320dp : a typical phone screen (240x320 ldpi, 320x480 mdpi, 480x800 hdpi, etc).
     * 480dp : a tweener tablet like the Streak (480x800 mdpi).
     * 600dp : a 7” tablet (600x1024 mdpi).
     * 720dp : a 10” tablet (720x1280 mdpi, 800x1280 mdpi, etc).
     *
     * @param context The context.
     * @return Returns true if the device is a Tablet
     */
    public static boolean isTabletDevice(Context context) {
        boolean isTablet = false;

        try {
            float smallestWidth = context.getResources().getConfiguration().smallestScreenWidthDp;

            // if smallestWidth >= 600 - Device is a 7" tablet & if smallestWidth >= 720 - Device is a 10" tablet
            if (smallestWidth >= 600) {
                isTablet = true;
            }

            //isTablet = (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isTablet;
    }

    public static boolean isLargeScreen(Context context) {
        if (context.getResources().getDisplayMetrics().widthPixels > Constants.DEVICE_MIN_WIDTH)
            return true;
        else
            return false;
    }

    public static Spanned getColouredText(String key, String value) {
        return fromHtml(String.format("%s : <font color='#005d94'> %s </font>", key, value));
    }

    public static Spanned getColouredText(String value) {
        return fromHtml(String.format("<font color='#005d94'> %s </font>", value));
    }

    public static Spanned getColouredBoldText(String value) {
        return fromHtml(String.format("<b><font color='#005d94'> %s </font></b>", value));
    }

    public static Spanned getColouredBoldText(String key, String value) {
        return fromHtml(String.format("%s : <b><font color='#005d94'> %s </font></b>", key, value));
    }

    public static Spanned getColouredNumber(String key, int value) {
        return fromHtml(String.format("%s : <font color='#005d94'> %,d </font>", key, value));
    }

    public static Spanned getColouredTextWithUnderscore(String value) {
        return fromHtml(String.format("<u><font color='#005d94'>%s</font></u>", value));
    }

    public static Spanned getColouredTextWithUnderscore(String key, String value) {
        return fromHtml(String.format("%s : <br/><u><font color='#005d94'>%s</font></u>", key, value));
    }

    public static Spanned getColouredNumberWithUnderscore(String key, int value) {
        return fromHtml(String.format("%s : <u><font color='#005d94'>%s</font></u>", key, value));
    }

    public static int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    public ArrayList<String> getFilterYears() {
        ArrayList<String> years = new ArrayList<>();

        int initialYear = 2010;
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int totalYears = (currentYear - initialYear) + 1; //To include initial year i.e. 2010
        int j = totalYears;

        years.add("Select Year");
        years.add("All");
        for (int i = 1; i <= totalYears; i++) {
            years.add(String.valueOf(initialYear));
            initialYear++;
            j--;
        }

        return years;
    }

    public int getDrawableByResourcesName(Context context, String resourceName) {
        return context.getResources().getIdentifier(resourceName, "drawable", context.getPackageName());
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    @TargetApi(23)
    public static boolean verifyStoragePermissions(Activity activity) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            // Check if we have write permission
            int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(activity, Constants.STORAGE_PERMISSIONS, Constants.REQUEST_EXTERNAL_STORAGE);
            } else
                return true;

        } else {
            return true;
        }

        return false;
    }

    @TargetApi(23)
    public static boolean verifyGetAccountsPermissions(Activity activity) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            // Check if we have write permission
            int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.GET_ACCOUNTS);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(activity, Constants.GET_ACCOUNTS_PERMISSIONS, Constants.REQUEST_GET_ACCOUNTS);
            } else
                return true;

        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }

        return result;
    }
}
