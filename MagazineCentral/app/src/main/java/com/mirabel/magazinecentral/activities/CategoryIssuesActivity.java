package com.mirabel.magazinecentral.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.adapters.CategoryIssuesGridViewAdapter;
import com.mirabel.magazinecentral.beans.CatalogIssues;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;
import com.mirabel.magazinecentral.models.CatalogDownloaderModel;
import com.mirabel.magazinecentral.services.DownloadService;
import com.mirabel.magazinecentral.util.NetworkConnectionDetector;
import com.mirabel.magazinecentral.util.Utility;

import java.util.ArrayList;

public class CategoryIssuesActivity extends AppCompatActivity implements AlertDialogSelectionListener {
    public static final String TAG = CategoryIssuesActivity.class.getSimpleName();

    private Context applicationContext, activityContext;
    private Toolbar mToolbar;
    private GridView category_issues_grid_view;
    private MCTextView header_title, no_issues_found_message;

    private CategoryIssuesGridViewAdapter gridViewAdapter;
    private CatalogDownloaderModel catalogDownloaderModel;
    private String selectedCategoryName;
    protected int currentPageNumber, totalNoOfPages, totalNoOfRecords, pageSize;
    private boolean isLoading = false, isInitialRequest = false, isAllRecordsLoaded = false;
    private GlobalContent globalContent;

    private String CURRENT_PAGE_NUMBER = "currentPageNumber";
    private String TOTAL_NO_OF_PAGES = "totalNoOfPages";
    private String TOTAL_NO_OF_RECORDS = "totalNoOfRecords";
    private String IS_ALL_RECORDS_LOADED = "isAllRecordsLoaded";
    private String IS_INITIAL_REQUEST = "isInitialRequest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_issues);

        try {
            applicationContext = getApplicationContext();
            activityContext = CategoryIssuesActivity.this;

            /*mToolbar = (Toolbar) findViewById(R.id.category_issues_toolbar);
            mToolbar.setNavigationIcon(R.drawable.icon_tollbar_home);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

            header_title = (MCTextView) findViewById(R.id.header_title);
            category_issues_grid_view = (GridView) findViewById(R.id.category_issues_grid_view);
            no_issues_found_message = (MCTextView) findViewById(R.id.no_issues_found_message);

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCategoryName = bundle.getString(Constants.BUNDLE_SELECTED_CATEGORY);
            }

            if (selectedCategoryName.equalsIgnoreCase("all")) {
                header_title.setText(getResources().getString(R.string.app_name));
            } else {
                header_title.setText(selectedCategoryName);
            }

            catalogDownloaderModel = new CatalogDownloaderModel(activityContext, this);
            globalContent = GlobalContent.getInstance();
            //isLoading = true; // in order to restrict initial loading due to grid view scroll listener.

            if (Utility.isLargeScreen(activityContext) || Utility.isTabletDevice(activityContext))
                pageSize = Constants.PAGE_SIZE_FOR_TABLETS;
            else
                pageSize = Constants.PAGE_SIZE;

            // Binding adapter to grid view first time.
            gridViewAdapter = new CategoryIssuesGridViewAdapter(activityContext, this, Constants.RequestFrom.CATEGORY);
            category_issues_grid_view.setAdapter(gridViewAdapter);

            if (savedInstanceState != null) {
                currentPageNumber = savedInstanceState.getInt(CURRENT_PAGE_NUMBER);
                totalNoOfPages = savedInstanceState.getInt(TOTAL_NO_OF_PAGES);
                totalNoOfRecords = savedInstanceState.getInt(TOTAL_NO_OF_RECORDS);
                isAllRecordsLoaded = savedInstanceState.getBoolean(IS_ALL_RECORDS_LOADED);
                isInitialRequest = savedInstanceState.getBoolean(IS_INITIAL_REQUEST);
                ArrayList<Content> previouslyLoadedIssues = globalContent.getCategoryIssuesList();

                if (previouslyLoadedIssues.size() < 0) {
                    category_issues_grid_view.setVisibility(View.GONE);
                    no_issues_found_message.setVisibility(View.VISIBLE);
                }
            } else {
                resetValues();
            }

            category_issues_grid_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int visibleThreshold = 2;

                    /**
                     * we can use this condition also (category_issues_grid_view.getLastVisiblePosition() + 1 == totalItemCount && !isLoading) to achieve below thing.
                     */
                    if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                        currentPageNumber++;
                        getIssuesForSelectedCategory(currentPageNumber);
                    }
                }
            });

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(downloadManagerBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        try {
            outState.putInt(CURRENT_PAGE_NUMBER, currentPageNumber);
            outState.putInt(TOTAL_NO_OF_PAGES, totalNoOfPages);
            outState.putInt(TOTAL_NO_OF_RECORDS, totalNoOfRecords);
            outState.putBoolean(IS_ALL_RECORDS_LOADED, isAllRecordsLoaded);
            outState.putBoolean(IS_INITIAL_REQUEST, isInitialRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        onBackPressed();
    }

    public void onVolleyErrorResponse(VolleyError error) {
        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), getResources().getString(R.string.error_failure));
    }

    public void resetValues() {
        currentPageNumber = 1;
        totalNoOfPages = -1;
        totalNoOfRecords = 0;
        isLoading = true;// assigned to true in order to restrict initial loading due to grid view scroll listener.
        isInitialRequest = true;
        isAllRecordsLoaded = false;

        // initially resetting category issues list which are saved in global content
        globalContent.setCategoryIssuesList(null);
        gridViewAdapter.notifyDataSetChanged();

        getIssuesForSelectedCategory(currentPageNumber);
    }

    public void getIssuesForSelectedCategory(int pageNumber) {
        if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
            currentPageNumber = pageNumber;

            if (selectedCategoryName.contains("&"))
                selectedCategoryName = selectedCategoryName.replace("&", "-");

            if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
                if (!MCProgressDialog.isProgressDialogShown)
                    MCProgressDialog.showProgressDialog(activityContext, Constants.LOADING_MESSAGE);

                catalogDownloaderModel.getIssuesForCategory(selectedCategoryName, currentPageNumber, pageSize);
                isLoading = true;
            }
        } else {
            NetworkConnectionDetector.displayNoNetworkError(activityContext);
        }
    }

    public void updateCatalogIssues(CatalogIssues catalogIssues) {
        try {
            // if no magazines found, we are hiding grid view
            if (currentPageNumber == 1 && catalogIssues.getCatalogIssues().size() <= 0) {
                category_issues_grid_view.setVisibility(View.GONE);
                no_issues_found_message.setVisibility(View.VISIBLE);

                isLoading = false;
            } else {
                if (currentPageNumber == 1) {
                    totalNoOfPages = catalogIssues.getPageCount();
                    totalNoOfRecords = catalogIssues.getTotalNoOfRecords();
                }

                globalContent.setCategoryIssuesList((ArrayList<Content>) catalogIssues.getCatalogIssues());

                // informing adapter to update grid view with latest issues
                gridViewAdapter.notifyDataSetChanged();

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;

                isLoading = false;
            }

            isInitialRequest = false;

        } catch (Exception e) {
            e.printStackTrace();
        }

        MCProgressDialog.hideProgressDialog();
    }

    public void displayToast(String message) {
        Utility.displayToast(this, message, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startIssueDownload(GlobalContent.currentDownloadingIssue);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    MCAlertDialog.listener = this;
                    MCAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.need_storage_permission_title), getResources().getString(R.string.need_storage_permission));
                } else {
                    displayToast(getResources().getString(R.string.storage_permission_not_given));
                }
            }
        }
    }

    @Override
    public void alertDialogCallback() {
        ActivityCompat.requestPermissions(CategoryIssuesActivity.this, Constants.STORAGE_PERMISSIONS, Constants.REQUEST_EXTERNAL_STORAGE);
    }

    public void startIssueDownload(Content content) {
        if (content != null) {
            content.setDownloadType(Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD);
            content.setContentState(Constants.ContentState.ContentStateDownloading);
            Intent downloadServiceIntent = new Intent(activityContext, DownloadService.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
            downloadServiceIntent.putExtras(bundle);
            startService(downloadServiceIntent);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver downloadManagerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (intent.getAction().equals(Constants.INTENT_ACTION_REFRESH_VIEW)) {
                    gridViewAdapter.notifyDataSetChanged();
                } else {
                    if (category_issues_grid_view != null) {
                        for (int i = 0; i < category_issues_grid_view.getChildCount(); i++) {
                            View magazineView = category_issues_grid_view.getChildAt(i);
                            CategoryIssuesGridViewAdapter.MagazineIssueViewViewHolder viewHolder = (CategoryIssuesGridViewAdapter.MagazineIssueViewViewHolder) magazineView.getTag();

                            if (viewHolder.getContentId().equalsIgnoreCase(contentId)) {
                                ImageView downloadIcon = viewHolder.download_status_icon;
                                ProgressBar currentProgressBar = viewHolder.download_progress_bar;

                                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                                    int progress = (int) ((downloaded / total) * 100);

                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    currentProgressBar.setProgress(progress);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_preview);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    currentProgressBar.setProgress(0);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_download);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
