package com.mirabel.magazinecentral.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.IssueDetailsActivity;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.services.DownloadService;
import com.mirabel.magazinecentral.util.Utility;

import java.io.File;

/**
 * Created by venkat on 6/30/17.
 */

public class CategoryIssuesGridViewAdapter extends BaseAdapter {
    private Context context;
    private Activity activity;
    private LayoutInflater layoutInflater;
    private Constants.RequestFrom requestFrom;
    private GlobalContent globalContent;

    public CategoryIssuesGridViewAdapter(Context context, Activity activity, Constants.RequestFrom reqFrom) {
        this.context = context;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.requestFrom = reqFrom;
        this.globalContent = GlobalContent.getInstance();
    }

    static public class MagazineIssueViewViewHolder {
        public ImageView cover_page, download_status_icon;
        public MCTextView product_name, issue_name;
        public ProgressBar download_progress_bar;
        public String contentId;

        public String getContentId() {
            return contentId;
        }

        public void setContentId(String contentId) {
            this.contentId = contentId;
        }
    }

    @Override
    public int getCount() {
        if (requestFrom == Constants.RequestFrom.CATEGORY)
            return globalContent.getCategoryIssuesList().size();
        else if (requestFrom == Constants.RequestFrom.SEARCH_PAGE)
            return globalContent.getSearchResultIssuesList().size();
        else
            return globalContent.getLibrarySearchResultIssuesList().size();
    }

    @Override
    public Content getItem(int position) {
        if (requestFrom == Constants.RequestFrom.CATEGORY)
            return globalContent.getCategoryIssuesList().get(position);
        else if (requestFrom == Constants.RequestFrom.SEARCH_PAGE)
            return globalContent.getSearchResultIssuesList().get(position);
        else
            return globalContent.getLibrarySearchResultIssuesList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final MagazineIssueViewViewHolder magazineIssueViewViewHolder;

        try {
            if (convertView == null) {
                if (requestFrom == Constants.RequestFrom.CATEGORY)
                    convertView = layoutInflater.inflate(R.layout.magazine_issue_for_category, viewGroup, false);
                else
                    convertView = layoutInflater.inflate(R.layout.magazine_issue_for_search_results, viewGroup, false);

                magazineIssueViewViewHolder = new MagazineIssueViewViewHolder();
                magazineIssueViewViewHolder.cover_page = (ImageView) convertView.findViewById(R.id.cover_page);
                magazineIssueViewViewHolder.product_name = (MCTextView) convertView.findViewById(R.id.product_name);
                magazineIssueViewViewHolder.issue_name = (MCTextView) convertView.findViewById(R.id.issue_name);
                magazineIssueViewViewHolder.download_status_icon = (ImageView) convertView.findViewById(R.id.download_status_icon);
                magazineIssueViewViewHolder.download_progress_bar = (ProgressBar) convertView.findViewById(R.id.download_progress_bar);

                convertView.setTag(magazineIssueViewViewHolder);
            } else {
                magazineIssueViewViewHolder = (MagazineIssueViewViewHolder) convertView.getTag();
            }

            final Content content = getItem(position);

            magazineIssueViewViewHolder.setContentId(content.getId());
            magazineIssueViewViewHolder.product_name.setText(content.getProductName());
            magazineIssueViewViewHolder.issue_name.setText(content.getName());

            //Updating Issue Cover Page using Glide library
            String coverPageImageUrl = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());

            RequestOptions glideReqOptions = new RequestOptions();
            glideReqOptions.placeholder(R.drawable.cover_page);
            //glideReqOptions.format(DecodeFormat.PREFER_ARGB_8888);
            Glide.with(activity).load(coverPageImageUrl).apply(glideReqOptions).into(magazineIssueViewViewHolder.cover_page);

            final Constants.ContentState currentStateOfMagazine = globalContent.getCurrentStateOfMagazine(content);

            if (currentStateOfMagazine == Constants.ContentState.ContentStateNone) {
                magazineIssueViewViewHolder.download_status_icon.setImageResource(R.drawable.icon_download);
                magazineIssueViewViewHolder.download_progress_bar.setVisibility(View.INVISIBLE);
            } else if (currentStateOfMagazine == Constants.ContentState.ContentStateDownloaded) {
                magazineIssueViewViewHolder.download_status_icon.setImageResource(R.drawable.icon_preview);
                magazineIssueViewViewHolder.download_progress_bar.setVisibility(View.INVISIBLE);
            } else {
                magazineIssueViewViewHolder.download_status_icon.setImageResource(R.drawable.icon_downloading);
                magazineIssueViewViewHolder.download_progress_bar.setVisibility(View.VISIBLE);
                magazineIssueViewViewHolder.download_progress_bar.setProgress(0);
            }

            magazineIssueViewViewHolder.cover_page.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
                        openIssueDetailPage(content);
                    } else {
                        // Read Magazine
                        readMagazine(content);
                    }
                }
            });

            magazineIssueViewViewHolder.download_status_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
                        downloadIssue(content, magazineIssueViewViewHolder.download_status_icon, magazineIssueViewViewHolder.download_progress_bar);
                    } else {
                        // Open Issue Detail Page
                        openIssueDetailPage(content);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void openIssueDetailPage(Content content) {
        Intent openIssuesIntent = new Intent(context, IssueDetailsActivity.class);
        openIssuesIntent.putExtra(Constants.BUNDLE_SELECTED_ISSUE_CONTENT, content);
        openIssuesIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(openIssuesIntent);
        activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);

        /*Intent intent = new Intent(Constants.INTENT_ACTION_OPEN_ISSUE);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
        intent.putExtras(bundle);

        // Broadcasting Selected Issue Content to Main Activity
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        // Closing Activity
        activity.finish();
        activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);*/
    }

    public void downloadIssue(Content content, ImageView downloadIcon, ProgressBar progressBar) {
        if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
            if (Utility.getInstance().verifyStoragePermissions(activity)) {
                downloadIcon.setImageResource(R.drawable.icon_downloading);
                downloadIcon.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);

                startIssueDownload(content);
            } else {
                GlobalContent.currentDownloadingIssue = content;
            }
        }
    }

    public void startIssueDownload(Content content) {
        content.setDownloadType(Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD);
        content.setContentState(Constants.ContentState.ContentStateDownloading);
        Intent downloadServiceIntent = new Intent(context, DownloadService.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
        downloadServiceIntent.putExtras(bundle);
        context.startService(downloadServiceIntent);
    }

    public void readMagazine(Content content) {
        File file = new File(GlobalContent.issueFolderPath + content.getId() + "/issue.xml");

        if (file.exists()) {
            Intent viewIssueActivity = new Intent(context, ViewIssueActivity.class);
            viewIssueActivity.putExtra(Constants.BUNDLE_CONTENT, content);
            context.startActivity(viewIssueActivity);
            activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateDownloading) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.download_progress), context.getResources().getString(R.string.download_wait_message));
        }
    }
}
