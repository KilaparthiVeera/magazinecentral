package com.mirabel.magazinecentral.beans.viewissue;

import java.io.Serializable;

public class IndexItem implements Serializable {
    public int pageNumber;
    public String imageName;
    public String text;

    @Override
    public String toString() {
        return "IndexItem{" +
                "pageNumber=" + pageNumber +
                ", imageName='" + imageName + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
