package com.mirabel.magazinecentral.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.IssueDetailsActivity;
import com.mirabel.magazinecentral.activities.LibraryActivity;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;

import java.io.File;

/**
 * Created by venkat on 6/30/17.
 */

public class LibraryIssuesGridViewAdapter extends BaseAdapter {
    private Context context;
    private LibraryActivity activity;
    private LayoutInflater layoutInflater;
    private GlobalContent globalContent;

    public LibraryIssuesGridViewAdapter(Context context, LibraryActivity activity) {
        this.context = context;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.globalContent = GlobalContent.getInstance();
    }

    static public class LibraryIssueViewViewHolder {
        public ImageView cover_page, download_status_icon, delete_magazine_icon;
        public MCTextView product_name, issue_name;
        public ProgressBar download_progress_bar;
        public String contentId;

        public String getContentId() {
            return contentId;
        }

        public void setContentId(String contentId) {
            this.contentId = contentId;
        }
    }

    @Override
    public int getCount() {
        return globalContent.getLibraryIssuesList().size();
    }

    @Override
    public Content getItem(int position) {
        return globalContent.getLibraryIssuesList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final LibraryIssueViewViewHolder libraryIssueViewViewHolder;

        try {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.magazine_issue_for_library, viewGroup, false);

                libraryIssueViewViewHolder = new LibraryIssueViewViewHolder();
                libraryIssueViewViewHolder.cover_page = (ImageView) convertView.findViewById(R.id.cover_page);
                libraryIssueViewViewHolder.product_name = (MCTextView) convertView.findViewById(R.id.product_name);
                libraryIssueViewViewHolder.issue_name = (MCTextView) convertView.findViewById(R.id.issue_name);
                libraryIssueViewViewHolder.download_status_icon = (ImageView) convertView.findViewById(R.id.download_status_icon);
                libraryIssueViewViewHolder.delete_magazine_icon = (ImageView) convertView.findViewById(R.id.delete_magazine_icon);
                libraryIssueViewViewHolder.download_progress_bar = (ProgressBar) convertView.findViewById(R.id.download_progress_bar);

                convertView.setTag(libraryIssueViewViewHolder);
            } else {
                libraryIssueViewViewHolder = (LibraryIssueViewViewHolder) convertView.getTag();
            }

            final Content content = getItem(position);

            libraryIssueViewViewHolder.setContentId(content.getId());
            libraryIssueViewViewHolder.product_name.setText(content.getProductName());
            libraryIssueViewViewHolder.issue_name.setText(content.getName());

            File file_370 = new File(GlobalContent.context.getFilesDir() + "/Catalog/" + content.getId() + "_370.jpeg");
            if (file_370.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file_370.getAbsolutePath());
                libraryIssueViewViewHolder.cover_page.setImageBitmap(bitmap);
            } else {
                //Bitmap cover_page = BitmapFactory.decodeResource(context.getResources(), R.drawable.cover_page);
                //libraryIssueViewViewHolder.cover_page.setImageBitmap(cover_page);

                //Updating Issue Cover Page using Glide library
                String coverPageImageUrl = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());
                RequestOptions glideReqOptions = new RequestOptions();
                glideReqOptions.placeholder(R.drawable.cover_page);
                Glide.with(activity).load(coverPageImageUrl).apply(glideReqOptions).into(libraryIssueViewViewHolder.cover_page);
            }

            if (globalContent.getDownloadedIssuesList().contains(content.getId())) {
                libraryIssueViewViewHolder.download_status_icon.setImageResource(R.drawable.icon_preview);
                libraryIssueViewViewHolder.delete_magazine_icon.setVisibility(View.VISIBLE);
                libraryIssueViewViewHolder.download_progress_bar.setVisibility(View.INVISIBLE);
            } else {
                libraryIssueViewViewHolder.download_status_icon.setImageResource(R.drawable.icon_downloading);
                libraryIssueViewViewHolder.delete_magazine_icon.setVisibility(View.GONE);
                libraryIssueViewViewHolder.download_progress_bar.setVisibility(View.VISIBLE);
                libraryIssueViewViewHolder.download_progress_bar.setProgress(0);
            }

            libraryIssueViewViewHolder.cover_page.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Read Magazine
                    readMagazine(content);
                }
            });

            libraryIssueViewViewHolder.download_status_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Open Issue Detail Page
                    openIssueDetailPage(content);
                }
            });

            libraryIssueViewViewHolder.delete_magazine_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Delete Magazine
                    deleteMagazine(content);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void readMagazine(Content content) {
        File file = new File(GlobalContent.issueFolderPath + content.getId() + "/issue.xml");

        if (file.exists()) {
            Intent viewIssueActivity = new Intent(context, ViewIssueActivity.class);
            viewIssueActivity.putExtra(Constants.BUNDLE_CONTENT, content);
            context.startActivity(viewIssueActivity);
            activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateDownloading) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.download_progress), context.getResources().getString(R.string.download_wait_message));
        }
    }

    public void openIssueDetailPage(Content content) {
        Intent openIssueDetailIntent = new Intent(context, IssueDetailsActivity.class);
        openIssueDetailIntent.putExtra(Constants.BUNDLE_SELECTED_ISSUE_CONTENT, content);
        openIssueDetailIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(openIssueDetailIntent);
        activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void deleteMagazine(Content content) {
        Intent deleteMagazineIntent = new Intent(Constants.INTENT_ACTION_ARCHIVE_ISSUE);
        deleteMagazineIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
        LocalBroadcastManager.getInstance(context).sendBroadcast(deleteMagazineIntent);
    }
}
