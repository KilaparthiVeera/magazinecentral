package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Interactivity;
import com.mirabel.magazinecentral.beans.viewissue.InteractivityItem;
import com.mirabel.magazinecentral.constants.Constants;

public class AudioInteractivityView extends RelativeLayout {
    Interactivity interActivity = null;
    InteractivityItem interactivityItem = null;
    Content content = null;
    String pageName = null;
    RelativeLayout layout;
    public static ImageButton playAudioButton = null;
    public static boolean isPlaying = false;
    String audioFilePath = null;
    boolean isTracked = false;

    public AudioInteractivityView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    public AudioInteractivityView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public AudioInteractivityView(Context context, Interactivity interActivity, final Content content, final String pageName) {
        super(context);
        this.interActivity = interActivity;
        this.content = content;
        this.pageName = pageName;
        this.interactivityItem = interActivity.items.get(0);
        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (RelativeLayout) inflater.inflate(R.layout.audio_interactive_view, null);
        layout.setBackgroundColor(Color.TRANSPARENT);
        playAudioButton = (ImageButton) inflater.inflate(R.layout.mcbutton, null);
        playAudioButton.setImageDrawable(ContextCompat.getDrawable(MyApplication.getAppContext(), R.drawable.play_audio));
        playAudioButton.setScaleType(ScaleType.FIT_XY);
        playAudioButton.setAdjustViewBounds(true);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        playAudioButton.setLayoutParams(layoutParams);
        addView(playAudioButton);
        addView(layout);

        try {
            isPlaying = false;
            audioFilePath = interactivityItem.source;

            playAudioButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        if (ViewIssueActivity.mediaPlayer == null) {
                            ViewIssueActivity.mediaPlayer = new MediaPlayer();
                            ViewIssueActivity.mediaPlayer.setDataSource(audioFilePath);
                            ViewIssueActivity.mediaPlayer.prepare();
                        }

                        if (isPlaying) {
                            ViewIssueActivity.mediaPlayer.pause();
                            playAudioButton.setImageDrawable(ContextCompat.getDrawable(MyApplication.getAppContext(), R.drawable.play_audio));
                            isPlaying = false;
                        } else {
                            ViewIssueActivity.mediaPlayer.start();
                            playAudioButton.setImageDrawable(ContextCompat.getDrawable(MyApplication.getAppContext(), R.drawable.pause_audio));
                            isPlaying = true;

                            if (!isTracked) {
                                String label;

                                if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                                    label = String.format("/%s/%s/%s/android/%d/p/page%s", content.getPublisherId(), content.getProductName(), content.getName(), Constants.APP_TYPE, pageName);
                                } else {
                                    label = String.format("/%s/%s/%s/android/%d/l/page%s", content.getPublisherId(), content.getProductName(), content.getName(), Constants.APP_TYPE, pageName);
                                }

                                String title = null;
                                if (interactivityItem.text.length() > 0) {
                                    title = interactivityItem.text;
                                } else {
                                    title = String.format("Audio on Page : %s", pageName);
                                }

                                MyApplication.setCustomVar(1, "primaryEmail", GlobalContent.getInstance().primaryEmail);
                                MyApplication.trackEvent("Android-Multimedia", title, label);
                                isTracked = true;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setIconForPlayAudioButton() {
        if (playAudioButton != null)
            playAudioButton.setImageDrawable(ContextCompat.getDrawable(MyApplication.getAppContext(), R.drawable.play_audio));
    }
}