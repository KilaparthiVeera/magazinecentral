package com.mirabel.magazinecentral.fragments.viewissue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.viewissue.AudioInteractivityView;
import com.mirabel.magazinecentral.customviews.viewissue.PageView;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

import java.lang.ref.WeakReference;


public class ViewIssueFragment extends Fragment {
    public String issueFolderPath = "";
    Content content;
    Context context;
    Page page;
    ViewPagerInterface viewPagerInterface;
    public PageView pageView = null;
    Handler handler = new Handler();

    public static Fragment newInstance(Context context, Content content, Page page, ViewPagerInterface viewPagerInterface) {
        ViewIssueFragment sf = new ViewIssueFragment();
        sf.context = context;
        sf.content = content;
        sf.page = new Page(page);
        sf.viewPagerInterface = viewPagerInterface;
        sf.setUserVisibleHint(true);
        return sf;
    }

    // Zero argument constructor
    public ViewIssueFragment() {

    }

    Runnable triggerVideo = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            pageView.triggerVideo();
        }
    };

    Runnable hideHotspotsBackground = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            pageView.hideHotspotsBackground();
        }
    };

    /*
     * This method is called as part of Fragment life cycle & when page swipes.(non-Javadoc)
     * This method is called twice and initially menuVisible is false and then it is true.
     * @see android.support.v4.app.Fragment#setMenuVisibility(boolean)
     */
    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (GlobalContent.orientation == Constants.OrientationType.landscape)
            return;

        if (menuVisible) {

            if (page.activities != null && page.activities.size() > 0) { //For auto play video's
                if (handler == null) {
                    handler = new Handler();
                }
                handler.postDelayed(triggerVideo, 3000);
            }

            if (page.hotspots != null && page.hotspots.size() > 0) { // To hide hotspot(s) background
                if (handler == null) {
                    handler = new Handler();
                }
                handler.postDelayed(hideHotspotsBackground, 3000);
            }
        } else {

            if (handler != null) {
                handler.removeCallbacks(triggerVideo);
                handler.removeCallbacks(hideHotspotsBackground);
            }

            //To stop video playing when we swipe
            if (pageView != null && pageView.iv != null) {

                if (pageView.iv.videoView != null && pageView.iv.videoView.isPlaying()) {
                    pageView.iv.videoView.pause();
                    //InteractivityView.stopPosition = pageView.iv.videoView.getCurrentPosition();
                    //InteractivityView.isPaused = true;
                }
            }

            //To stop audio playing when we swipe
            if (ViewIssueActivity.mediaPlayer != null && ViewIssueActivity.mediaPlayer.isPlaying()) {
                ViewIssueActivity.mediaPlayer.stop();
                ViewIssueActivity.mediaPlayer.release();
                ViewIssueActivity.mediaPlayer = null;
                AudioInteractivityView.isPlaying = false;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        try {
            pageView = new PageView(context, page, content, viewPagerInterface);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            pageView.setLayoutParams(params);
            pageView.requestDisallowInterceptTouchEvent(true);
            return pageView;
        } catch (Exception e) {
            e.printStackTrace();
            getActivity().finish();
            return pageView;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        pageView.hideInteractivities();

        //To pause video playing when we lock the device
        if (pageView != null && pageView.iv != null) {

            if (pageView.iv.videoView != null && pageView.iv.videoView.isPlaying()) {
                pageView.iv.videoView.pause();
                //InteractivityView.stopPosition = pageView.iv.videoView.getCurrentPosition();
                //InteractivityView.isPaused = true;
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onActivityCreated(savedInstanceState);
    }
}
