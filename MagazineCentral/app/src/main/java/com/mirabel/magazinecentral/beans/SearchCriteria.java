package com.mirabel.magazinecentral.beans;

import java.io.Serializable;

/**
 * Created by venkat on 7/20/17.
 */

public class SearchCriteria implements Serializable {
    private String searchKeyword;
    private String selectedProductName;
    private String selectedPublisherId;
    private String selectedPublisherName;
    private String year;
    private int selectedYear;
    private String monthName;
    private int selectedMonth;
    private boolean isSearchPerformed;

    public SearchCriteria() {
        searchKeyword = "";
        selectedProductName = "";
        selectedPublisherId = "-1";
        selectedPublisherName = "Select Publisher";
        year = "Select Year";
        selectedYear = -1;
        monthName = "Select Month";
        selectedMonth = -1;
        isSearchPerformed = false;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSelectedProductName() {
        return selectedProductName;
    }

    public void setSelectedProductName(String selectedProductName) {
        this.selectedProductName = selectedProductName;
    }

    public String getSelectedPublisherId() {
        return selectedPublisherId;
    }

    public void setSelectedPublisherId(String selectedPublisherId) {
        this.selectedPublisherId = selectedPublisherId;
    }

    public String getSelectedPublisherName() {
        return selectedPublisherName;
    }

    public void setSelectedPublisherName(String selectedPublisherName) {
        this.selectedPublisherName = selectedPublisherName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(int selectedYear) {
        this.selectedYear = selectedYear;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public int getSelectedMonth() {
        return selectedMonth;
    }

    public void setSelectedMonth(int selectedMonth) {
        this.selectedMonth = selectedMonth;
    }

    public boolean isSearchPerformed() {
        return isSearchPerformed;
    }

    public void setSearchPerformed(boolean searchPerformed) {
        isSearchPerformed = searchPerformed;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "searchKeyword='" + searchKeyword + '\'' +
                ", selectedProductName='" + selectedProductName + '\'' +
                ", selectedPublisherId='" + selectedPublisherId + '\'' +
                ", selectedPublisherName='" + selectedPublisherName + '\'' +
                ", year='" + year + '\'' +
                ", selectedYear=" + selectedYear +
                ", monthName='" + monthName + '\'' +
                ", selectedMonth=" + selectedMonth +
                ", isSearchPerformed=" + isSearchPerformed +
                '}';
    }
}
