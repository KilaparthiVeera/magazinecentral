package com.mirabel.magazinecentral.asynctasks;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.github.scribejava.apis.LinkedInApi;
import com.github.scribejava.apis.TumblrApi;
import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.activities.viewissue.SocialWebActivity;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.interfaces.SocialInterface;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class SocialClass extends AsyncTask {
    SharedPreferences sharedPreferences = null;
    Constants.SocialType socialType = null;
    Constants.ShareType shareType; // 0 = LinkShare & 1 = ImageShare
    SocialInterface socialInterface = null;
    public ContentValues contentValues = null;
    boolean status = false;
    OAuth10aService service = null;
    OAuth1AccessToken accessToken = null;

    public SocialClass(Constants.SocialType socialType, ContentValues contentValues, Constants.ShareType shareType, SocialInterface socialInterface) {
        this.socialType = socialType;
        this.contentValues = contentValues;
        this.shareType = shareType;
        this.socialInterface = socialInterface;
    }

    @Override
    protected Object doInBackground(Object... params) {
        switch (socialType) {
            case twitter: {
                service = new ServiceBuilder()
                        .apiKey(Constants.TWITTER_API_KEY)
                        .apiSecret(Constants.TWITTER_API_SECRET)
                        .callback("")
                        .build(TwitterApi.instance());

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TwitterApi.class.getName(), 0);

                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToTwitter();
                } else {
                    login(Constants.TWITTER_API_KEY, Constants.TWITTER_API_SECRET);
                }
            }
            break;

            case tumblr: {
                service = new ServiceBuilder()
                        .apiKey(Constants.TUMBLR_API_KEY)
                        .apiSecret(Constants.TUMBLR_API_SECRET)
                        .callback("")
                        .build(TumblrApi.instance());

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TumblrApi.class.getName(), 0);
                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToTumblr();
                } else {
                    login(Constants.TUMBLR_API_KEY, Constants.TUMBLR_API_SECRET);
                }
            }
            break;

            case linkedin: {
                service = new ServiceBuilder()
                        .scope("r_basicprofile w_share")
                        .apiKey(Constants.LINKED_IN_API_KEY)
                        .apiSecret(Constants.LINKED_IN_API_SECRET)
                        .callback("")
                        .build(LinkedInApi.instance());

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(LinkedInApi.class.getName(), 0);
                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToLinkedin();
                } else {
                    login(Constants.LINKED_IN_API_KEY, Constants.LINKED_IN_API_SECRET);
                }
            }
            break;

            default:
                break;
        }

        return status;
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        socialInterface.updateStatus((Boolean) values[0]);
    }

    public void login(String apiKey, String apiSecret) {
        Intent i = new Intent(MyApplication.getAppContext(), SocialWebActivity.class);
        i.putExtra("socialType", socialType.ordinal());
        i.putExtra("apiKey", apiKey);
        i.putExtra("apiSecret", apiSecret);
        socialInterface.login(i);
    }

    public void postToTwitter() {
        try {
            service = new ServiceBuilder()
                    .apiKey(Constants.TWITTER_API_KEY)
                    .apiSecret(Constants.TWITTER_API_SECRET)
                    .callback("")
                    .build(TwitterApi.instance());

            sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TwitterApi.class.getName(), 0);
            accessToken = new OAuth1AccessToken(sharedPreferences.getString("ACCESS_TOKEN", ""), sharedPreferences.getString("ACCESS_SECRET", ""));
            OAuthRequest request = new OAuthRequest(Verb.POST, "https://api.twitter.com/1.1/statuses/update_with_media.json");
            //service.signRequest(accessToken, request);
            //final Response response = service.execute(request); // if you want to authorize every uncomment this line.

            try {
                MultipartEntity entity = new MultipartEntity();

                // Twitter allows its message length up to 160 characters only.
                if (shareType == Constants.ShareType.image) {
                    String twitterMessage = "For Digital Edition,\nClick " + contentValues.getAsString("longUrl");
                    entity.addPart("status", new StringBody(twitterMessage));
                    entity.addPart("media", new FileBody(new File(contentValues.getAsString("imagePath").replace("_t", ""))));  // THIS IS THE PHOTO TO UPLOAD
                } else {
                    entity.addPart("status", new StringBody(contentValues.getAsString("postMessage")));
                    entity.addPart("media", new FileBody(new File(contentValues.getAsString("imagePath").replace("_t", ""))));  // THIS IS THE PHOTO TO UPLOAD
                }

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                entity.writeTo(out);
                request.setPayload(out.toByteArray());
                request.addHeader(entity.getContentType().getName(), entity.getContentType().getValue());

                service.signRequest(accessToken, request);
                Response response = service.execute(request);

                JSONObject responseJSONObject = new JSONObject(response.getBody());

                if (responseJSONObject.has("text")) {
                    status = true;
                    publishProgress(status);
                } else {
                    status = false;
                    publishProgress(status);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postToTumblr() {
        try {
            service = new ServiceBuilder()
                    .apiKey(Constants.TUMBLR_API_KEY)
                    .apiSecret(Constants.TUMBLR_API_SECRET)
                    .callback("")
                    .build(TumblrApi.instance());

            sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TumblrApi.class.getName(), 0);
            String url = String.format("http://api.tumblr.com/v2/blog/%s/post", sharedPreferences.getString("blogName", ""));
            accessToken = new OAuth1AccessToken(sharedPreferences.getString("ACCESS_TOKEN", ""), sharedPreferences.getString("ACCESS_SECRET", ""));
            OAuthRequest postRequest = new OAuthRequest(Verb.POST, url);

            if (shareType == Constants.ShareType.link) {
                postRequest.addBodyParameter("link", contentValues.getAsString("shortUrl"));
                postRequest.addBodyParameter("type", "photo");
                postRequest.addBodyParameter("tags", "Magazine Central,Mirabel");
                postRequest.addBodyParameter("source", contentValues.getAsString("picture"));
                postRequest.addBodyParameter("caption", contentValues.getAsString("postMessage"));

                service.signRequest(accessToken, postRequest);
                Response response = service.execute(postRequest);

                try {
                    JSONObject responseJSONObject = new JSONObject(response.getBody());

                    if (responseJSONObject.has("response")) {
                        status = true;
                        publishProgress(status);
                    } else {
                        status = false;
                        publishProgress(status);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                postRequest.addBodyParameter("type", "photo");
                postRequest.addBodyParameter("link", contentValues.getAsString("shortUrl"));
                postRequest.addBodyParameter("caption", contentValues.getAsString("postMessage"));

                try {
                    MultipartEntity entity = new MultipartEntity();
                    entity.addPart("link", new StringBody(contentValues.getAsString("shortUrl")));
                    entity.addPart("type", new StringBody("photo"));
                    entity.addPart("caption", new StringBody(contentValues.getAsString("postMessage")));
                    ContentBody cb = new FileBody(new File(contentValues.getAsString("imagePath")), "image/jpeg");
                    entity.addPart("data", cb);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    entity.writeTo(out);
                    postRequest.setPayload(out.toByteArray());
                    postRequest.addHeader(entity.getContentType().getName(), entity.getContentType().getValue());

                    service.signRequest(accessToken, postRequest);
                    Response response = service.execute(postRequest);

                    JSONObject responseJSONObject = new JSONObject(response.getBody());

                    if (responseJSONObject.has("response")) {
                        status = true;
                        publishProgress(status);
                    } else {
                        status = false;
                        publishProgress(status);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postToLinkedin() {
        try {
            service = new ServiceBuilder()
                    .scope("r_basicprofile w_share")
                    .apiKey(Constants.LINKED_IN_API_KEY)
                    .apiSecret(Constants.LINKED_IN_API_SECRET)
                    .callback("")
                    .build(LinkedInApi.instance());

            sharedPreferences = MyApplication.getAppContext().getSharedPreferences(LinkedInApi.class.getName(), 0);

            accessToken = new OAuth1AccessToken(sharedPreferences.getString("ACCESS_TOKEN", ""), sharedPreferences.getString("ACCESS_SECRET", ""));
            OAuthRequest postRequest = new OAuthRequest(Verb.POST, "http://api.linkedin.com/v1/people/~/shares");

            //set the headers to the server knows what we are sending
            postRequest.addHeader("Content-Type", "application/json");
            postRequest.addHeader("x-li-format", "json");

            try {
                JSONObject jsonMap = new JSONObject();
                jsonMap.put("comment", contentValues.getAsString("postMessage"));

                JSONObject contentObject = new JSONObject();
                contentObject.put("title", contentValues.get("name"));
                contentObject.put("submitted-url", contentValues.getAsString("longUrl"));
                contentObject.put("submitted-image-url", contentValues.getAsString("picture"));
                jsonMap.put("content", contentObject);

                JSONObject visibilityObject = new JSONObject();
                visibilityObject.put("code", "anyone");
                jsonMap.put("visibility", visibilityObject);

                postRequest.setPayload(jsonMap.toString());

                service.signRequest(accessToken, postRequest);
                Response response = service.execute(postRequest);

                JSONObject responseJSONObject = new JSONObject(response.getBody());

                if (responseJSONObject.has("updateKey")) {
                    status = true;
                    publishProgress(true);
                } else {
                    status = false;
                    publishProgress(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}