package com.mirabel.magazinecentral.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.mirabel.magazinecentral.interfaces.ConnectivityListener;

public class ConnectivityStatusReceiver extends BroadcastReceiver {
    public static ConnectivityListener listener;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving

        String action = intent.getAction();
        if (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            return;
        }

        boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

        if (listener != null) {
            listener.networkStateChanged(!noConnectivity);
        }
    }
}
