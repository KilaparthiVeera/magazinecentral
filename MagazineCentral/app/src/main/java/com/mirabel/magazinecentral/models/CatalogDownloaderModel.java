package com.mirabel.magazinecentral.models;

import android.app.Activity;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabel.magazinecentral.activities.CategoryIssuesActivity;
import com.mirabel.magazinecentral.activities.IssueDetailsActivity;
import com.mirabel.magazinecentral.activities.MainActivity;
import com.mirabel.magazinecentral.activities.SearchActivity;
import com.mirabel.magazinecentral.beans.CatalogIssues;
import com.mirabel.magazinecentral.beans.Category;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.HomePageCatalog;
import com.mirabel.magazinecentral.beans.IssueDetails;
import com.mirabel.magazinecentral.beans.Publisher;
import com.mirabel.magazinecentral.beans.SearchCriteria;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.util.EncryptDecryptStringWithDES;
import com.mirabel.magazinecentral.util.NetworkConnectionDetector;
import com.mirabel.magazinecentral.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by venkat on 6/9/17.
 */

public class CatalogDownloaderModel {
    private String TAG;

    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private NetworkConnectionDetector networkConnectionDetector;
    private EncryptDecryptStringWithDES des;

    public CatalogDownloaderModel(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.TAG = activity.getClass().getSimpleName();
        this.networkConnectionDetector = new NetworkConnectionDetector(context);
        this.des = new EncryptDecryptStringWithDES();
    }

    public CatalogDownloaderModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
        this.networkConnectionDetector = new NetworkConnectionDetector(context);
        this.des = new EncryptDecryptStringWithDES();
    }

    public String getDTTicks() {
        return des.getEncryptedString(String.valueOf(System.currentTimeMillis()));
    }

    public void getMagazinesCountForHomePage() {
        try {
            String getMagazinesCountForHomePageReqURL = String.format("%s/MagazineCount?dtticks=%s", Constants.MAIN_URL, getDTTicks());

            JsonObjectRequest getMagazinesCountForHomePageReq = new JsonObjectRequest(Request.Method.GET, getMagazinesCountForHomePageReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                ((MainActivity) activity).updateMagazinesCountInHeader(response.getString("MagazineCount"), true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((MainActivity) activity).onVolleyErrorResponse(error);
                        }
                    }
            );

            AppController.getInstance(context).addToRequestQueue(getMagazinesCountForHomePageReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCategoriesList() {
        try {
            String getCategoriesListReqURL = String.format("%s/Category?dtticks=%s&publisherid=-1", Constants.MAIN_URL, getDTTicks());

            JsonObjectRequest getCategoriesListReq = new JsonObjectRequest(Request.Method.GET, getCategoriesListReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray categories = response.getJSONArray("Categories");

                                Type listType = new TypeToken<ArrayList<Category>>() {
                                }.getType();

                                ArrayList<Category> categoriesArrayList = new Gson().fromJson(categories.toString(), listType);
                                ((MainActivity) activity).updateCategories(categoriesArrayList, true);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((MainActivity) activity).onVolleyErrorResponse(error);
                        }
                    }
            );

            AppController.getInstance(context).addToRequestQueue(getCategoriesListReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getHomePageCatalog() {
        try {
            String getHomePageCatalogReqURL = String.format("%s/Home?dtticks=%s&PageSize=%d", Constants.MAIN_URL, getDTTicks(), Constants.HOME_SCREEN_PAGE_SIZE);

            JsonObjectRequest getHomePageCatalogReq = new JsonObjectRequest(Request.Method.GET, getHomePageCatalogReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Type homePageCatalogType = new TypeToken<HomePageCatalog>() {
                                }.getType();

                                HomePageCatalog homePageCatalog = new Gson().fromJson(response.toString(), homePageCatalogType);
                                ((MainActivity) activity).updateHomePageCatalog(homePageCatalog, true);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((MainActivity) activity).onVolleyErrorResponse(error);
                        }
                    }
            );

            AppController.getInstance(context).addToRequestQueue(getHomePageCatalogReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
            ((MainActivity) activity).onVolleyErrorResponse(null);
        }
    }

    public void getIssuesForCategory(String categoryName, int pageNo, int pageSize) {
        try {
            String getIssuesForCategoryReqURL = String.format("%s/Catalog?dtticks=%s&PublisherId=-1&CategoryId=%s&ProductName=&ProductId=0&IssueYear=-1&IssueMonth=-1&PageIndex=%d&pageSize=%d",
                    Constants.MAIN_URL, getDTTicks(), URLEncoder.encode(categoryName, Constants.CHARSET_NAME), pageNo, pageSize);

            JsonObjectRequest getIssuesForCategoryReq = new JsonObjectRequest(Request.Method.GET, getIssuesForCategoryReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Type catalogIssuesType = new TypeToken<CatalogIssues>() {
                            }.getType();

                            CatalogIssues catalogIssues = new Gson().fromJson(response.toString(), catalogIssuesType);
                            ((CategoryIssuesActivity) activity).updateCatalogIssues(catalogIssues);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((CategoryIssuesActivity) activity).onVolleyErrorResponse(error);
                        }
                    });

            AppController.getInstance(context).addToRequestQueue(getIssuesForCategoryReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getIssueDetailsForProductNameAndIssueName(String productName, String issueName) {
        try {
            String getIssueDetailsReqURL = String.format("%s/IssueDetails?dtticks=%s&productName=%s&issueName=%s", Constants.MAIN_URL, getDTTicks(), URLEncoder.encode(productName, Constants.CHARSET_NAME), URLEncoder.encode(issueName, Constants.CHARSET_NAME));

            JsonObjectRequest getIssueDetailsReq = new JsonObjectRequest(Request.Method.GET, getIssueDetailsReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Type issueDetailsType = new TypeToken<IssueDetails>() {
                            }.getType();

                            IssueDetails issueDetails = new Gson().fromJson(response.toString(), issueDetailsType);
                            ((IssueDetailsActivity) activity).updateIssueDetails(issueDetails);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((IssueDetailsActivity) activity).onVolleyErrorResponse(error);
                        }
                    });

            AppController.getInstance(context).addToRequestQueue(getIssueDetailsReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPublishersList() {
        try {
            String getPublishersListReqURL = String.format("%s/Publishers?dtticks=%s", Constants.MAIN_URL, getDTTicks());

            JsonObjectRequest getPublishersListReq = new JsonObjectRequest(Request.Method.GET, getPublishersListReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray publishers = response.getJSONArray("Publishers");

                                Type listType = new TypeToken<ArrayList<Publisher>>() {
                                }.getType();

                                ArrayList<Publisher> allPublisherArrayList = new ArrayList<>();
                                // Adding Select Publisher Object at index 0.
                                Publisher selectPublisher = new Publisher("0", "Select Publisher");
                                allPublisherArrayList.add(selectPublisher);

                                ArrayList<Publisher> publisherArrayList = new Gson().fromJson(publishers.toString(), listType);
                                allPublisherArrayList.addAll(publisherArrayList);

                                ((SearchActivity) activity).updatePublishersList(allPublisherArrayList, true);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((SearchActivity) activity).onVolleyErrorResponse(error);
                        }
                    }
            );

            AppController.getInstance(context).addToRequestQueue(getPublishersListReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getSearchPredictionsForKeyword(String keyword) {
        final ArrayList<String> predictionsArrayList = new ArrayList<>();

        try {
            if (networkConnectionDetector.isNetworkConnected()) {
                String getSearchPredictionsReqURL = String.format("%s/Search?dtticks=%s&&prediction=%s", Constants.MAIN_URL, getDTTicks(), URLEncoder.encode(keyword, Constants.CHARSET_NAME));

                JsonObjectRequest getSearchPredictionsReq = new JsonObjectRequest(Request.Method.GET, getSearchPredictionsReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONArray predictions = response.getJSONArray("Predictions");

                                    Type listType = new TypeToken<ArrayList<String>>() {
                                    }.getType();

                                    ArrayList<String> predictionsList = new Gson().fromJson(predictions.toString(), listType);
                                    predictionsArrayList.addAll(predictionsList);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((SearchActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                );

                AppController.getInstance(context).addToRequestQueue(getSearchPredictionsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return predictionsArrayList;
    }

    public void getIssuesListForSearchPage(SearchCriteria searchCriteria, int pageNo, int pageSize) {
        try {
            String getIssuesListForSearchPageReqURL = String.format("%s/Catalog?dtticks=%s&PublisherId=%s&CategoryId=All&ProductName=%s&ProductId=0&IssueYear=%d&IssueMonth=%d&PageIndex=%d&pageSize=%d",
                    Constants.MAIN_URL, getDTTicks(), searchCriteria.getSelectedPublisherId(), URLEncoder.encode(searchCriteria.getSelectedProductName(), Constants.CHARSET_NAME), searchCriteria.getSelectedYear(), searchCriteria.getSelectedMonth(), pageNo, pageSize);

            JsonObjectRequest getIssuesListForSearchPageReq = new JsonObjectRequest(Request.Method.GET, getIssuesListForSearchPageReqURL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Type catalogIssuesType = new TypeToken<CatalogIssues>() {
                            }.getType();

                            CatalogIssues catalogIssues = new Gson().fromJson(response.toString(), catalogIssuesType);
                            ((SearchActivity) activity).updateIssuesListForSearchPage(catalogIssues);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ((SearchActivity) activity).onVolleyErrorResponse(error);
                        }
                    });

            AppController.getInstance(context).addToRequestQueue(getIssuesListForSearchPageReq, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
