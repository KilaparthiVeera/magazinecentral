package com.mirabel.magazinecentral.customviews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by venkat on 20/06/16.
 *
 * @author venkat
 */
public class MCTextView extends AppCompatTextView {

    public MCTextView(Context context) {
        super(context);

        CustomFontUtils.applyCustomFont(this, context, null);
    }

    public MCTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontUtils.applyCustomFont(this, context, attrs);
    }

    public MCTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFontUtils.applyCustomFont(this, context, attrs);
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/AvantGardeBook.ttf");
        setTypeface(tf);
        setTextColor(Color.WHITE);
    }

    public void initWithColor(int color) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/AvantGardeBook.ttf");
        setTypeface(tf);
        setTextColor(color);

    }
}
