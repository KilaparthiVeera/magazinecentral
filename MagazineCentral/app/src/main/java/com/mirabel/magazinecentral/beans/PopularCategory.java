package com.mirabel.magazinecentral.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by venkat on 6/12/17.
 */

public class PopularCategory implements Serializable {
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("Issues")
    @Expose
    private List<Content> issues = null;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Content> getIssues() {
        return issues;
    }

    public void setIssues(List<Content> issues) {
        this.issues = issues;
    }
}
