package com.mirabel.magazinecentral.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Html;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MainActivity;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.asynctasks.DownloadTask;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.database.DBHelper;
import com.mirabel.magazinecentral.interfaces.ConnectivityListener;
import com.mirabel.magazinecentral.interfaces.DownloadServiceInterface;
import com.mirabel.magazinecentral.receivers.ConnectivityStatusReceiver;
import com.mirabel.magazinecentral.receivers.DownloadProgressReceiver;
import com.mirabel.magazinecentral.util.MCSharedPreferences;

import java.io.File;
import java.util.ArrayList;

public class DownloadService extends Service implements DownloadServiceInterface, ConnectivityListener {
    private MCSharedPreferences sharedPreferences;
    private DBHelper mDbHelper;
    GlobalContent globalContentInstance;
    private ArrayList<Content> pendingDownloads = null;
    private ArrayList<DownloadTask> runningDownloads = null;
    private boolean isDownloaderRunning = false;
    private String issueFolderPath;
    private NotificationCompat.Builder notificationBuilder = null;
    private NotificationCompat.InboxStyle notificationInboxStyle = null;

    /**
     * The system calls this method when the service is first created using onStartCommand() or onBind(). This call is required to perform one-time set-up.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        try {
            ConnectivityStatusReceiver.listener = this;

            Intent intent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));

            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.app_icon)
                    .setContentTitle(getResources().getString(R.string.downloaded_successfully))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            notificationInboxStyle = new NotificationCompat.InboxStyle();

            globalContentInstance = GlobalContent.getInstance();
            sharedPreferences = new MCSharedPreferences(MyApplication.getAppContext());
            mDbHelper = new DBHelper(MyApplication.getAppContext());

            this.issueFolderPath = GlobalContent.issueFolderPath;
            if (issueFolderPath.isEmpty()) {
                issueFolderPath = sharedPreferences.getString(Constants.ISSUES_FOLDER_PATH);
                GlobalContent.issueFolderPath = issueFolderPath;
            }

            File issueFolder = new File(issueFolderPath);
            if (!issueFolder.exists()) {
                issueFolder.mkdir();
            }

            if (pendingDownloads == null) {
                pendingDownloads = new ArrayList<>();
                runningDownloads = new ArrayList<>();
                pendingDownloads.addAll(mDbHelper.getPendingDownloads());

                globalContentInstance.getDownloadingContents().clear();
                globalContentInstance.getRunningContents().clear();

                for (Content c : pendingDownloads) {
                    globalContentInstance.getDownloadingContents().add(c);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The system calls this method when another component wants to bind with the service by calling bindService().
     *
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    /**
     * The system calls this method when another component, such as an activity, requests that the service be started, by calling startService().
     * If you implement this method, it is your responsibility to stop the service when its work is done, by calling stopSelf() or stopService() methods.
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        try {
            Bundle bundle = null;

            if (intent != null) {
                bundle = intent.getExtras();
            }

            if (bundle != null) {
                Content content = (Content) bundle.getSerializable(Constants.BUNDLE_CONTENT);

                String action = String.format("%s--%s--%s", content.getPublisherName(), content.getProductName(), content.getName());
                String label = String.format("/%s/%s/%s/android/%d/page%d", content.getPublisherId(), content.getProductName(), content.getName(), Constants.APP_TYPE, 0);
                ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", globalContentInstance.primaryEmail);
                ((MyApplication) getApplication()).trackEvent("Android-Issue Downloaded", action, label);

                content.setContentState(Constants.ContentState.ContentStateDownloading);

                mDbHelper.insertContent(content);
                if (!pendingDownloads.contains(content))
                    pendingDownloads.add(content);

                if (!globalContentInstance.getDownloadingContents().contains(content))
                    globalContentInstance.getDownloadingContents().add(content);

                globalContentInstance.updatePendingDownloadContents();

                // When a new Issue downloading starts or added to queue, we are refreshing all views
                Intent refreshViewIntent = new Intent(Constants.INTENT_ACTION_REFRESH_VIEW);
                refreshViewIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
                LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(refreshViewIntent);
            }

            updatePendingDownloads();

        } catch (Exception e) {
            e.printStackTrace();
        }

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    /**
     * The system calls this method when the service is no longer used and is being destroyed.
     * Your service should implement this to clean up any resources such as threads, registered listeners, receivers, etc.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).unregisterReceiver(broadcastReceiver);
    }

    public void updatePendingDownloads() {
        if (!isDownloaderRunning) {
            if (pendingDownloads.size() > 0) {
                for (Content c : pendingDownloads) {
                    if (c.getContentState() != Constants.ContentState.ContentStatePaused) {
                        isDownloaderRunning = true;
                        c.setContentState(Constants.ContentState.ContentStateDownloading);
                        DownloadTask downloadTask = new DownloadTask(sharedPreferences, this, c);
                        runningDownloads.add(downloadTask);
                        globalContentInstance.getRunningContents().add(c);// Adding Issue to Running Contents List
                        GlobalContent.isDownloadRunning = true;
                        downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[]) null);
                        break;
                    }
                }
            } else {
                // As there is no any pending downloads so we are stopping download service.
                stopSelf();
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);
                Content content = (Content) intent.getSerializableExtra(Constants.BUNDLE_CONTENT);

                if (intent.getAction().equalsIgnoreCase(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                    pauseDownload(content);
                } else if (intent.getAction().equalsIgnoreCase(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                    resumeDownload(content);
                } else if (intent.getAction().equalsIgnoreCase(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                    removeRunningDownload(content);
                    cancelDownload(content);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void pauseDownload(Content content) {
        pendingDownloads.remove(content);
        pendingDownloads.add(content);
        content.setContentState(Constants.ContentState.ContentStatePaused);
        isDownloaderRunning = false;
        GlobalContent.isDownloadRunning = false;
        removeRunningDownload(content);
        globalContentInstance.updateDownloadedContents();
    }

    public void resumeDownload(Content content) {
        pendingDownloads.remove(content);
        pendingDownloads.add(0, content);
        content.setContentState(Constants.ContentState.ContentStateDownloading);
        isDownloaderRunning = true;
        GlobalContent.isDownloadRunning = true;

        // In order to rearrange Issues in download manager i.e. showing currently downloading Issue at first position.
        globalContentInstance.getDownloadingContents().clear();
        globalContentInstance.getDownloadingContents().addAll(pendingDownloads);

        DownloadTask task = new DownloadTask(sharedPreferences, this, content);
        runningDownloads.add(task);
        globalContentInstance.getRunningContents().add(content);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[]) null);

        globalContentInstance.updateDownloadedContents();

        Intent refreshViewIntent = new Intent(Constants.INTENT_ACTION_REFRESH_VIEW);
        refreshViewIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(refreshViewIntent);
    }

    public void removeRunningDownload(Content content) {
        for (DownloadTask runningDownload : runningDownloads) {
            if (runningDownload.getContent().getId().equalsIgnoreCase(content.getId())) {
                runningDownload.cancel(true);
                runningDownloads.remove(runningDownload);
                globalContentInstance.getRunningContents().remove(runningDownload.getContent());
                break;
            }
        }
    }

    public void cancelDownload(Content content) {
        try {
            content.setContentState(Constants.ContentState.ContentStateNone);
            pendingDownloads.remove(content);
            globalContentInstance.getDownloadingContents().remove(content);
            globalContentInstance.getRunningContents().remove(content);
            mDbHelper.deleteContent(content.getId());
            globalContentInstance.updateDownloadedContents();

            if (runningDownloads.size() == 0) {
                isDownloaderRunning = false;
                GlobalContent.isDownloadRunning = false;

                // In order to start downloading of pending Issue which is in queue, we are changing it's state
                if (pendingDownloads.size() > 0) {
                    if (pendingDownloads.get(0).getContentState() == Constants.ContentState.ContentStatePaused)
                        pendingDownloads.get(0).setContentState(Constants.ContentState.ContentStateDownloading);
                }

                updatePendingDownloads();
            }

            File issueDirectory = new File(GlobalContent.issueFolderPath + content.getId());
            this.deleteFile(issueDirectory);

            File cat_file_370 = new File(getFilesDir() + "/Catalog/" + content.getId() + "_370.jpeg");
            this.deleteFile(cat_file_370);

            File cat_file_200 = new File(getFilesDir() + "/Catalog/" + content.getId() + "_200.jpeg");
            this.deleteFile(cat_file_200);

            Intent refreshViewIntent = new Intent(Constants.INTENT_ACTION_REFRESH_VIEW);
            refreshViewIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(refreshViewIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (pendingDownloads.size() == 0) {
            stopSelf();
        }
    }

    public void deleteFile(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteFile(child);
        }
        fileOrDirectory.delete();
    }

    @Override
    public void downloadFinished(Content content) {
        if (content.getContentState() == Constants.ContentState.ContentStateDownloaded) {
            pendingDownloads.remove(content);
            removeRunningDownload(content);

            globalContentInstance.getDownloadingContents().remove(content);
            globalContentInstance.getRunningContents().remove(content);
            mDbHelper.updateContentDownloadStatus(content.getId(), Constants.ContentState.ContentStateDownloaded);
            globalContentInstance.updateDownloadedContents();

            isDownloaderRunning = false;
            GlobalContent.isDownloadRunning = false;

            if (runningDownloads.size() == 0) {
                // In order to start downloading of pending Issue which is in queue, we are changing it's state
                if (pendingDownloads.size() > 0) {
                    if (pendingDownloads.get(0).getContentState() == Constants.ContentState.ContentStatePaused)
                        pendingDownloads.get(0).setContentState(Constants.ContentState.ContentStateDownloading);
                }

                updatePendingDownloads();
            }

            Intent downloadFinishIntent = new Intent(Constants.INTENT_ACTION_DOWNLOAD_FINISH);
            downloadFinishIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
            LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(downloadFinishIntent);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                notificationInboxStyle.addLine(Html.fromHtml(String.format("<b>%s</b> --  \n<i>%s</i>", content.getProductName(), content.getName()), Html.FROM_HTML_MODE_LEGACY));
            } else {
                notificationInboxStyle.addLine(Html.fromHtml(String.format("<b>%s</b> --  \n<i>%s</i>", content.getProductName(), content.getName())));
            }

            notificationInboxStyle.setSummaryText(getResources().getString(R.string.app_name));
            notificationBuilder.setStyle(notificationInboxStyle).setWhen(System.currentTimeMillis());
            NotificationManager notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notifyManager.notify(0, notificationBuilder.build());
        }

        if (pendingDownloads.size() == 0) {
            stopSelf();
        }
    }

    @Override
    public void networkStateChanged(boolean isConnected) {
        if (isConnected) {
            if (!isDownloaderRunning) {
                if (pendingDownloads.size() > 0) {
                    try {
                        Content content = pendingDownloads.get(0);
                        DownloadTask downloadTask = new DownloadTask(sharedPreferences, this, content);
                        downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[]) null);
                        runningDownloads.add(downloadTask);
                        globalContentInstance.getRunningContents().add(content);
                        isDownloaderRunning = true;
                        GlobalContent.isDownloadRunning = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            if (isDownloaderRunning) {
                isDownloaderRunning = false;
                GlobalContent.isDownloadRunning = false;
            }
        }
    }

    /*DownloadProgressReceiver pauseOrResumeDownloadReceiver = new DownloadProgressReceiver() {
        public void onReceive(Context context, Intent intent) {
            Content content = (Content) intent.getSerializableExtra(Constants.BUNDLE_CONTENT);

            if (intent.getAction().equalsIgnoreCase(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                pauseDownload(content);
            } else {
                resumeDownload(content);
            }

            Intent i = new Intent(Constants.INTENT_ACTION_DOWNLOAD_FINISH);
            i.putExtra(content.getId(), Constants.BUNDLE_CONTENT_ID);
            MyApplication.getAppContext().sendBroadcast(i);
        }
    };

    DownloadProgressReceiver cancelDownloadReceiver = new DownloadProgressReceiver() {
        public void onReceive(Context context, Intent intent) {
            Content content = (Content) intent.getSerializableExtra(Constants.BUNDLE_CONTENT);
            int tag = intent.getIntExtra("tag", -1);
            removeRunningDownload(content);
            cancelDownload(content, tag);

            Intent i = new Intent(Constants.INTENT_ACTION_DOWNLOAD_FINISH);
            i.putExtra(content.getId(), Constants.BUNDLE_CONTENT_ID);
            MyApplication.getAppContext().sendBroadcast(i);
        }
    };*/
}
