package com.mirabel.magazinecentral.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.adapters.LibraryIssuesGridViewAdapter;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.database.DBHelper;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;

import java.io.File;

public class LibraryActivity extends AppCompatActivity {
    public static final String TAG = LibraryActivity.class.getSimpleName();

    private Context activityContext;
    private GridView library_issues_grid_view;
    private MCTextView no_issues_found_message;

    private LibraryIssuesGridViewAdapter libraryIssuesGridViewAdapter;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        try {
            activityContext = LibraryActivity.this;

            library_issues_grid_view = (GridView) findViewById(R.id.library_issues_grid_view);
            no_issues_found_message = (MCTextView) findViewById(R.id.no_issues_found_message_in_library);

            dbHelper = new DBHelper(activityContext);

            libraryIssuesGridViewAdapter = new LibraryIssuesGridViewAdapter(activityContext, this);
            library_issues_grid_view.setAdapter(libraryIssuesGridViewAdapter);

            checkForLibraryIssues();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_ARCHIVE_ISSUE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        onBackPressed();
    }

    public void showDownloadManager(View view) {
        Intent openDownloadManagerIntent = new Intent(activityContext, DownloadManagerActivity.class);
        startActivity(openDownloadManagerIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void showLibrarySearch(View view) {
        Intent showLibrarySearchIntent = new Intent(activityContext, LibrarySearchActivity.class);
        startActivity(showLibrarySearchIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void checkForLibraryIssues() {
        if (GlobalContent.getInstance().getLibraryIssuesList().size() > 0) {
            library_issues_grid_view.setVisibility(View.VISIBLE);
            no_issues_found_message.setVisibility(View.GONE);
        } else {
            no_issues_found_message.setVisibility(View.VISIBLE);
            library_issues_grid_view.setVisibility(View.GONE);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (intent.getAction().equals(Constants.INTENT_ACTION_ARCHIVE_ISSUE)) {
                    archiveIssue(contentId);
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_REFRESH_VIEW)) {
                    checkForLibraryIssues();
                    libraryIssuesGridViewAdapter.notifyDataSetChanged();
                } else {
                    if (library_issues_grid_view != null) {
                        for (int i = 0; i < library_issues_grid_view.getChildCount(); i++) {
                            View magazineView = library_issues_grid_view.getChildAt(i);
                            LibraryIssuesGridViewAdapter.LibraryIssueViewViewHolder viewHolder = (LibraryIssuesGridViewAdapter.LibraryIssueViewViewHolder) magazineView.getTag();

                            if (viewHolder.getContentId().equalsIgnoreCase(contentId)) {
                                ImageView downloadIcon = viewHolder.download_status_icon;
                                ImageView delete_magazine_icon = viewHolder.delete_magazine_icon;
                                ProgressBar currentProgressBar = viewHolder.download_progress_bar;

                                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                                    int progress = (int) ((downloaded / total) * 100);

                                    // we are showing currently downloading Issue progressbar only
                                    if (contentId == GlobalContent.currentDownloadingIssue.getId()) {
                                        currentProgressBar.setVisibility(View.VISIBLE);
                                        currentProgressBar.setProgress(progress);
                                        downloadIcon.setImageResource(R.drawable.icon_downloading);
                                        delete_magazine_icon.setVisibility(View.INVISIBLE);
                                    }
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_preview);
                                    delete_magazine_icon.setVisibility(View.VISIBLE);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    currentProgressBar.setProgress(0);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                    delete_magazine_icon.setVisibility(View.INVISIBLE);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                    delete_magazine_icon.setVisibility(View.INVISIBLE);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                                    libraryIssuesGridViewAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void archiveIssue(final String contentId) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activityContext, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle("Are you sure you want to archive this magazine ?");
            alertDialogBuilder.setMessage("This magazine will be removed from your device. You may download it again at anytime for free.");
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("Archive", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MCProgressDialog.showProgressDialog(activityContext, "Deleting Issue, Please wait..");

                    dbHelper.deleteContent(contentId);

                    File issueDirectory = new File(GlobalContent.issueFolderPath + contentId);
                    deleteFile(issueDirectory);

                    File cat_file_370 = new File(getFilesDir() + "/Catalog/" + contentId + "_370.jpeg");
                    deleteFile(cat_file_370);

                    File cat_file_200 = new File(getFilesDir() + "/Catalog/" + contentId + "_200.jpeg");
                    deleteFile(cat_file_200);

                    Toast.makeText(activityContext, "Issue deleted successfully..!", Toast.LENGTH_LONG).show();

                    GlobalContent.getInstance().updateDownloadedContents();

                    checkForLibraryIssues();

                    libraryIssuesGridViewAdapter.notifyDataSetChanged();

                    MCProgressDialog.hideProgressDialog();
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(activityContext, R.color.gray));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteFile(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteFile(child);
        }
        fileOrDirectory.delete();
    }
}
