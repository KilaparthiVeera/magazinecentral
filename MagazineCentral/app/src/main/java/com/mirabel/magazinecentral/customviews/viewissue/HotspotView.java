package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.beans.viewissue.Hotspot;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.interfaces.ViewIssueEventListener;

public class HotspotView extends View implements View.OnTouchListener {
    Hotspot _data;
    Color _borderColor;
    Color _bgColor;
    Paint p = new Paint();
    Rect rect = null;
    GestureDetector mGesture = null;
    public String redirectURL = Constants.HOTSPOT_REDIRECT_URL;
    String userID = "0D8938F3-5E12-4D25-95B1-E67186402121";
    String contentID = null;
    Bitmap buyNow;
    Rect srcBmp, desBmp;

    public HotspotView(Context context, Hotspot hostpot, String contentID) {
        super(context);
        this._data = hostpot;
        this.contentID = contentID;
        rect = new Rect(0, 0, (int) _data.getWidth(), (int) _data.getHeight());
        setOnTouchListener(this);
        mGesture = new GestureDetector(context, new GestureListener());
        if (_data.type == 3) {
            buyNow = BitmapFactory.decodeResource(getResources(), R.drawable.buynow);
            srcBmp = new Rect(0, 0, buyNow.getWidth(), buyNow.getHeight());
            desBmp = new Rect(0, 0, (int) _data.width, (int) _data.height);
        }
    }

    public HotspotView(Context context, AttributeSet attrs, int i) {
        super(context, attrs, i);
        // TODO Auto-generated constructor stub
    }

    public HotspotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }


    @Override
    protected void onDraw(Canvas canvas) {
        p.setStrokeWidth(_data.getBorderWidth());
        p.setColor(_data.getBgColor());

        if (_data.type == 3 && buyNow != null) {
            canvas.drawBitmap(buyNow, srcBmp, desBmp, p);
            return;
        } else if (_data.bordered) {
            p.setStyle(Style.STROKE);
        }

        p.setAlpha(150);
        canvas.drawRect(rect, p);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension((int) _data.getWidth(), (int) _data.getHeight());
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mGesture.onTouchEvent(event);
        PageView.handleEvent = false;
        LandscapePageView.handleEvent = false;
        if (event.getAction() == MotionEvent.ACTION_CANCEL) {
            PageView.handleEvent = true;
            LandscapePageView.handleEvent = true;
        }
        return true;
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (_data.type == 4) {

                ((ViewIssueEventListener) getContext()).mailTo(_data);

            } else if (_data.command.contains("http://")) {
                String command = _data.command.replace("http://", "");
                String url = String.format(redirectURL, command, userID, contentID);
                ((ViewIssueEventListener) getContext()).hotspotTapped(url);
            } else if (_data.command.contains("goto://")) {
                String pageName = _data.command.replace("goto://", "");
                ((ViewIssueEventListener) getContext()).jumpToPage(pageName);
            }
//	    	else if (_data.command.contains("mailto://")) {
//	    		String mailTo = _data.command.replace("mailto://", "");
//	    		((ViewIssueEventListener) getContext()).mailTo(mailTo);
//			}
            return super.onSingleTapConfirmed(e);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        return super.dispatchTouchEvent(event);
    }
}

