package com.mirabel.magazinecentral.customviews;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.interfaces.DialogSelectionListener;

/**
 * Created by venkat on 09/29/17.
 */

public class MCDialogWithCustomView extends Dialog {
    private Context context;
    private DialogSelectionListener listener = null;
    private Constants.SelectionType selectionType;
    private String title;
    private String okButtonTitle;
    private View contentView;

    private MCTextView custom_dialog_title, custom_dialog_cancel_text_view, custom_dialog_ok_text_view;

    public MCDialogWithCustomView(Context context, DialogSelectionListener listener, Constants.SelectionType selectionType, String title, String okButtonTitle, View contentView) {
        super(context, R.style.DialogWithListViewStyle);

        this.context = context;
        this.listener = listener;
        this.selectionType = selectionType;
        this.title = title;
        this.okButtonTitle = okButtonTitle;
        this.contentView = contentView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(contentView);

        try {
            setCancelable(false);

            custom_dialog_title = (MCTextView) contentView.findViewById(R.id.custom_dialog_title);
            custom_dialog_cancel_text_view = (MCTextView) contentView.findViewById(R.id.custom_dialog_cancel_text_view);
            custom_dialog_ok_text_view = (MCTextView) contentView.findViewById(R.id.custom_dialog_ok_text_view);

            custom_dialog_title.setText(title);

            custom_dialog_cancel_text_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();

                    // in order to close pop up on orientation change
                    if (selectionType == Constants.SelectionType.PINTEREST_BOARD) {
                        if (listener != null) {
                            listener.dialogSelectionCallback(Constants.SelectionType.PINTEREST_BOARD);
                        }
                    }
                }
            });

            if (!okButtonTitle.isEmpty()) {
                custom_dialog_ok_text_view.setText(okButtonTitle);
                custom_dialog_ok_text_view.setVisibility(View.VISIBLE);
            } else {
                custom_dialog_ok_text_view.setVisibility(View.GONE);
            }

            custom_dialog_ok_text_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    setOkButtonClicked();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOkButtonClicked() {
        if (listener != null) {
            listener.dialogSelectionCallback(selectionType);
        }
    }
}
