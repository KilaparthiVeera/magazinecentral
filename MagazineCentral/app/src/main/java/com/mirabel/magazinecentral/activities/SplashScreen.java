package com.mirabel.magazinecentral.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.util.MCSharedPreferences;

import java.util.Calendar;

/**
 * @author venkat
 */

public class SplashScreen extends Activity {
    private TextView copyright_text;

    private Runnable mRunnable;
    private Handler mHandler = new Handler();
    private MCSharedPreferences sharedPreferences;

    public static String copyright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        try {
            sharedPreferences = new MCSharedPreferences(SplashScreen.this);

            copyright_text = (TextView) findViewById(R.id.copyright_text);

            int current_year = Calendar.getInstance().get(Calendar.YEAR);
            copyright = String.format("© 2002-%d Mirabel Technologies, Inc. All Rights Reserved.", current_year);

            copyright_text.setText(copyright);

            mRunnable = new Runnable() {
                @Override
                public void run() {
                    mHandler.removeCallbacks(mRunnable);

                    sharedPreferences.putString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD, "");
                    sharedPreferences.putBoolean(Constants.SP_IS_DOWNLOAD_SERVICE_STARTED, false);

                    Intent mainActivity = new Intent(SplashScreen.this, MainActivity.class);
                    //mainActivity.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(mainActivity);
                    finish();
                }
            };

            mHandler.postDelayed(mRunnable, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
