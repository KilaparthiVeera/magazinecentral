package com.mirabel.magazinecentral.interfaces;

import android.view.View;

public interface ViewPagerInterface {
    void enable();// allow view pager to scroll

    void disable();// disallow view pager to scroll

    void onSingleTapConfirmed();

    void thumbTapped(int tappedIndex);

    void updateZoomValue(float zoomValue);

    void enablePinch(View view);
}
