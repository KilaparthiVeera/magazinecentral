package com.mirabel.magazinecentral.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.SplashScreen;
import com.mirabel.magazinecentral.activities.viewissue.WebViewActivity;
import com.mirabel.magazinecentral.customviews.MCTextView;

public class AboutUsFragment extends Fragment {
    private LinearLayout currentLayout;
    private MCTextView terms_of_service, acceptable_use_policy, privacy_policy, about_us_copyright_text_view;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        currentLayout = (LinearLayout) inflater.inflate(R.layout.fragment_about_us, container, false);

        return currentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        terms_of_service = (MCTextView) currentLayout.findViewById(R.id.terms_of_service);
        acceptable_use_policy = (MCTextView) currentLayout.findViewById(R.id.acceptable_use_policy);
        privacy_policy = (MCTextView) currentLayout.findViewById(R.id.privacy_policy);

        about_us_copyright_text_view = (MCTextView) currentLayout.findViewById(R.id.about_us_copyright_text_view);
        about_us_copyright_text_view.setText(SplashScreen.copyright);

        terms_of_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadURLInWebView(getString(R.string.terms_of_service_url));
            }
        });

        acceptable_use_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadURLInWebView(getString(R.string.acceptable_use_policy_url));
            }
        });

        privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadURLInWebView(getString(R.string.privacy_statement_url));
            }
        });
    }

    public void loadURLInWebView(String url) {
        Intent intent = new Intent(getContext(), WebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
    }
}
