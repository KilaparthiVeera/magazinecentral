package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Hotspot;
import com.mirabel.magazinecentral.beans.viewissue.Interactivity;
import com.mirabel.magazinecentral.beans.viewissue.InteractivityItem;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

import java.io.File;

public class PageView extends ZoomView implements ZoomView.ZoomViewListener {
    ImageView imageView;
    public Page page = null;
    Content content = null;
    ViewPagerInterface viewPagerInterface = null;
    public RelativeLayout mainLayout = null;
    double xScale, yScale, scale;
    int scaledWidth, scaledHeight;
    int originalWidth = -1;
    int originalHeight = -1;
    GestureDetector mGesture = null;
    public static boolean handleEvent = true;
    boolean isShownInteractivites = false;
    private Handler handler = new Handler();
    public InteractivityView iv = null;
    public AudioInteractivityView aiv = null;
    public boolean isZoomed = false;

    public PageView(Context context, Page page, Content content, ViewPagerInterface viewPagerInterface) {
        super(context);
        mGesture = new GestureDetector(context, new GestureListener());
        this.page = new Page(page);
        this.content = content;
        this.viewPagerInterface = viewPagerInterface;
        mainLayout = new RelativeLayout(context);
        mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        mainLayout.setGravity(Gravity.CENTER);
        requestDisallowInterceptTouchEvent(true);
        addView(mainLayout);
        this.setListner(this);

        try {
            displayImage();
            displayHotspots();
            new Runnable() {

                @Override
                public void run() {
                    displayInteractivities();
                }
            }.run();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayImage() {
        imageView = new ImageView(this.getContext());
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bmp = null;

        try {
            bmp = BitmapFactory.decodeFile(GlobalContent.issueFolderPath + content.getId() + "/" + page.imageName + "." + page.imageType, options);
        } catch (Exception e) {
            e.printStackTrace();
        }

        originalWidth = bmp.getWidth();
        originalHeight = bmp.getHeight();
        xScale = (double) GlobalContent.screenWidth / originalWidth;
        yScale = (double) GlobalContent.screenHeight / originalHeight;
        if (xScale < yScale) {
            scale = xScale;
        } else {
            scale = yScale;
        }
        scaledWidth = (int) (originalWidth * scale);
        scaledHeight = (int) (originalHeight * scale);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(scaledWidth, scaledHeight);
        layoutParams.gravity = Gravity.CENTER;
        imageView.setLayoutParams(layoutParams);
        imageView.setImageBitmap(bmp);
        mainLayout.addView(imageView);
    }

    public void resize() {

    }

    public void displayHotspots() {
        HotspotView hsv = null;
        float scaleX, scaleY;
        scaleX = (float) scaledWidth / GlobalContent.width;
        scaleY = (float) scaledHeight / GlobalContent.height;

        for (Hotspot hs : page.getHotspots()) {
            hs = new Hotspot(hs);
            if (!hs.scaled) {
                hs.setX((int) (hs.getX() * scaleX));
                hs.setY((int) (hs.getY() * scaleY));
                hs.setWidth((int) (hs.getWidth() * scaleX));
                hs.setHeight((int) (hs.getHeight() * scaleY));
                hs.scaled = true;
            }

            hsv = new HotspotView(this.getContext(), hs, content.getId());
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) hs.getWidth(), (int) hs.getHeight());
            params.setMargins((int) hs.getX(), (int) hs.getY(), 0, 0);
            hsv.setLayoutParams(params);
            mainLayout.addView(hsv);
        }
    }

    public void displayInteractivities() {
        float scaleX, scaleY;
        scaleX = (float) scaledWidth / GlobalContent.width;
        scaleY = (float) scaledHeight / GlobalContent.height;

        if (page.getActivities() == null)
            return;

        for (Interactivity interactivity : page.getActivities()) {

            interactivity = new Interactivity(interactivity);
            isShownInteractivites = true;

            if (!interactivity.scaled) {
                interactivity.x = ((int) (interactivity.x * scaleX));
                interactivity.y = ((int) (interactivity.y * scaleY));
                interactivity.width = ((int) (interactivity.width * scaleX));
                interactivity.height = ((int) (interactivity.height * scaleY));
                interactivity.scaled = true;
            }

            if (interactivity.type == 4 && interactivity.items.size() > 0) {
                InteractivityItem item = interactivity.items.get(0);
                if (new File(item.source).exists()) {
                    aiv = new AudioInteractivityView(getContext(), interactivity, content, page.imageName);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(interactivity.width, interactivity.height);
                    layoutParams.setMargins(interactivity.x, interactivity.y, 0, 0);
                    aiv.setLayoutParams(layoutParams);
                    mainLayout.addView(aiv);
                }
            } else {
                iv = new InteractivityView(getContext(), interactivity, content, page.imageName);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(interactivity.width, interactivity.height);
                params.setMargins(interactivity.x, interactivity.y, 0, 0);
                iv.setLayoutParams(params);
                mainLayout.addView(iv);
            }
        }
    }

    public void hideInteractivities() {
        if (zoom > 1.0 && isShownInteractivites) {
            for (int i = 0; i < mainLayout.getChildCount(); i++) {
                if (mainLayout.getChildAt(i).getClass().equals(InteractivityView.class)) {
                    InteractivityView iv = (InteractivityView) mainLayout.getChildAt(i);
                    if (iv.videoView != null && iv.videoView.isPlaying()) {
                        iv.videoView.pause();
                    }
                    iv.setVisibility(View.GONE);
                }
            }

            isShownInteractivites = false;
            isZoomed = true;

        } else if (zoom == 1 && isShownInteractivites == false) {
            //displayInteractivities();

            for (int i = 0; i < mainLayout.getChildCount(); i++) {
                if (mainLayout.getChildAt(i).getClass().equals(InteractivityView.class)) {
                    InteractivityView iv = (InteractivityView) mainLayout.getChildAt(i);
                    iv.setVisibility(View.VISIBLE);
                }
            }

            isShownInteractivites = true;
            isZoomed = false;
        }

        AudioInteractivityView aiv = new AudioInteractivityView(getContext(), null);
        aiv.setIconForPlayAudioButton();
    }

    @Override
    public void onZoomStarted(float zoom, float zoomx, float zoomy) {
        hideInteractivities();
    }

    @Override
    public void onZooming(float zoom, float zoomx, float zoomy) {
        //viewPagerInterface.disable();
        hideInteractivities();
    }

    @Override
    public void onZoomEnded() {
        hideInteractivities();
        //viewPagerInterface.disable();
    }

    // We are not using this method and ViewIssueActivity.class.onSingleTapConfirmed() is calling from PageView.class#239
    @Override
    public void onReceivedSingleTap() {
        showHotspotsBackground();

        viewPagerInterface.onSingleTapConfirmed();
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            //Log.d("GestureListener", "Single Tap from Gesture Listener PageView");
            // ZoomView.this.listener.onReceivedSingleTap();

            showHotspotsBackground();

            if (handleEvent) {
                viewPagerInterface.onSingleTapConfirmed();
            } else {
                handleEvent = true;
            }
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return super.onDown(e);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_CANCEL) {
            Log.d("PageView", "Cancel");
        }
        mGesture.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    public void hotspotTouched() {

    }

    public void triggerVideo() {
        if (!isZoomed) {

            InteractivityView iv = null;

            for (int i = 0; i < mainLayout.getChildCount(); i++) {

                if (mainLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(InteractivityView.class.getName())) {

                    iv = (InteractivityView) mainLayout.getChildAt(i);
                    try {
                        if (iv.interactivity.items.get(0).vSource.length() == 0 && iv.interactivity.type != 5) {
                            iv.playVideo();
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }

                    break;
                }
            }
        }
    }

    public void hideHotspotsBackground() {
        if (mainLayout != null) {
            for (int i = 0; i < mainLayout.getChildCount(); i++) {
                if (mainLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(HotspotView.class.getName())) {
                    HotspotView hotspotView = (HotspotView) mainLayout.getChildAt(i);
                    hotspotView.setVisibility(View.GONE);
                }
            }
        }
    }

    public void showHotspotsBackground() {
        if (mainLayout != null) {
            for (int i = 0; i < mainLayout.getChildCount(); i++) {
                if (mainLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(HotspotView.class.getName())) {
                    HotspotView hotspotView = (HotspotView) mainLayout.getChildAt(i);
                    hotspotView.setVisibility(View.VISIBLE);
                }
            }
        }

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                hideHotspotsBackground();
            }
        }, 3000);
    }
}
