package com.mirabel.magazinecentral.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Category;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.HomePageCatalog;
import com.mirabel.magazinecentral.beans.PopularCategory;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.database.DBHelper;
import com.mirabel.magazinecentral.fragments.FragmentDrawer;
import com.mirabel.magazinecentral.fragments.HomePageFragment;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;
import com.mirabel.magazinecentral.interfaces.ConnectivityListener;
import com.mirabel.magazinecentral.interfaces.FragmentDrawerListener;
import com.mirabel.magazinecentral.models.CatalogDownloaderModel;
import com.mirabel.magazinecentral.receivers.ConnectivityStatusReceiver;
import com.mirabel.magazinecentral.services.DownloadService;
import com.mirabel.magazinecentral.util.MCSharedPreferences;
import com.mirabel.magazinecentral.util.NetworkConnectionDetector;
import com.mirabel.magazinecentral.util.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements FragmentDrawerListener, ConnectivityListener, AlertDialogSelectionListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    private Context activityContext;
    public static Context applicationContext;
    private MCSharedPreferences sharedPreferences;
    private LayoutInflater mLayoutInflater;
    private Toolbar mToolbar;
    private FragmentDrawer fragmentDrawer;
    private HomePageFragment homePageFragment;
    private FragmentActivity homePageFragmentActivity;
    private MCTextView home_page_header_title, popular_category1_issues_title, popular_category2_issues_title, popular_category3_issues_title, popular_category4_issues_title, popular_category5_issues_title;
    private LinearLayout recent_issues_gallery, popular_issues_gallery, popular_category1_issues_gallery, popular_category2_issues_gallery, popular_category3_issues_gallery, popular_category4_issues_gallery, popular_category5_issues_gallery;

    private Utility utility;
    private GlobalContent globalContent;
    private CatalogDownloaderModel catalogDownloaderModel;
    private ArrayList<Category> categoriesList;
    private HomePageCatalog homePageCatalog;

    public boolean isFragmentUIReferencesInitialized = false, isUIDataRefreshed = false;
    private boolean exitApp = false;
    private long lastRefreshedTime;

    private DBHelper dbHelper;
    private ArrayList<ProgressBar> currentlyDownloadingProgressBars = new ArrayList<>();
    private ArrayList<ImageView> currentlyDownloadingStatusIcons = new ArrayList<>();
    private boolean isCurrentlyDownloadingMagazineViewRefsInstantiated = false;
    private boolean isDownloadServiceStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            applicationContext = getApplicationContext();
            activityContext = MainActivity.this;
            sharedPreferences = new MCSharedPreferences(activityContext);
            mLayoutInflater = LayoutInflater.from(this);

            if (!new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
                displayToast(getResources().getString(R.string.error_no_internet));
            }

            mToolbar = (Toolbar) findViewById(R.id.main_activity_toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(R.string.app_name);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false); // if true toolbar title will display

            fragmentDrawer = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
            fragmentDrawer.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.home_page_drawer_layout), mToolbar);
            fragmentDrawer.setDrawerListener(this);

            homePageFragment = new HomePageFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.home_page_frame_container, homePageFragment);
            fragmentTransaction.addToBackStack("HomePage");
            fragmentTransaction.commit();

            utility = Utility.getInstance();
            globalContent = GlobalContent.getInstance();
            catalogDownloaderModel = new CatalogDownloaderModel(activityContext, this, Constants.RequestFrom.HOME_PAGE);

            isFragmentUIReferencesInitialized = false;
            isUIDataRefreshed = false;

            MyApplication.context = this;
            GlobalContent.setContext(MyApplication.getAppContext());

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            GlobalContent.screenWidth = size.x;
            GlobalContent.screenHeight = size.y;

            //getDeviceSizeInInches();

            String issuesFolderPath = getExternalFilesDir(null) + "/" + getResources().getString(R.string.app_name) + "/";
            if (issuesFolderPath.length() > 0) {
                GlobalContent.issueFolderPath = issuesFolderPath;
                sharedPreferences.putString(Constants.ISSUES_FOLDER_PATH, issuesFolderPath);
            }

            // Creating Catalog Files Folder if it's not exists
            File CatalogFilesDirectory = new File(GlobalContent.context.getFilesDir() + "/Catalog");
            if (!CatalogFilesDirectory.exists()) {
                CatalogFilesDirectory.mkdir();
            }

            // Assigning Connectivity Status Receiver Listener
            ConnectivityStatusReceiver.listener = this;

            dbHelper = new DBHelper(activityContext);

            if (utility.verifyGetAccountsPermissions(this)) {
                getAndUpdatePrimaryEmailAddress();
            }

            //Intent downloadService = new Intent(this, DownloadService.class);
            //startService(downloadService);

            globalContent.updateLibrary();
            globalContent.updateDownloadedContents();

            ArrayList<Content> pendingDownloads = dbHelper.getPendingDownloads();
            for (Content content : pendingDownloads) {
                globalContent.getDownloadingContents().add(content);
            }

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAndUpdatePrimaryEmailAddress() {
        // Getting primary s_email_icon address configured in device settings
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(applicationContext).getAccounts();
        //Account[] accounts = AccountManager.get(applicationContext).getAccountsByType("com.google");

        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                String email = account.name;
                globalContent.primaryEmail = email;
                sharedPreferences.putString(Constants.SP_PRIMARY_EMAIL, email);
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(downloadManagerBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshData();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }

        if (MCProgressDialog.isProgressDialogShown) {
            MCProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        if (exitApp) {
            super.onBackPressed();

            finish();
        } else {
            displayToast("Press back button once again to exit app.");
            exitApp = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exitApp = false;
                }
            }, 3 * 1000); // 3 sec
        }
    }

    public void displayToast(String message) {
        Utility.displayToast(this, message, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page_toolbar, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_library:
                openLibrary();
                return true;
            case R.id.action_search:
                showSearchPage();
                return true;
            case R.id.action_settings:
                openSettings();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onVolleyErrorResponse(VolleyError error) {
        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), getResources().getString(R.string.error_failure));
    }

    public void viewMoreCategoriesButtonClicked(View view) {
        if (fragmentDrawer != null)
            fragmentDrawer.openDrawer();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        showIssuesForSelectedCategory(categoriesList.get(position).getCategoryName());
    }

    public void showMagazinesForSelectedCategory(View view) {
        int tag = Integer.parseInt(view.getTag().toString());

        String categoryName;

        if (tag == 1 || tag == 2) {
            categoryName = "all";
            showIssuesForSelectedCategory(categoryName);
        } else {
            // checking weather home page catalog is downloaded or not.
            if (homePageCatalog != null) {
                // Popular category 1 tag is 3 so we are subtracting 3 in order to access popular category 1 name from PopularCategories in response.
                categoryName = homePageCatalog.getPopularCategories().get(tag - 3).getCategoryName();

                showIssuesForSelectedCategory(categoryName);
            }
        }
    }

    public void showIssuesForSelectedCategory(String categoryName) {
        Intent showCategoryIssuesIntent = new Intent(activityContext, CategoryIssuesActivity.class);
        showCategoryIssuesIntent.putExtra(Constants.BUNDLE_SELECTED_CATEGORY, categoryName);
        startActivity(showCategoryIssuesIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void refreshData() {
        if (catalogDownloaderModel == null) {
            utility = Utility.getInstance();
            globalContent = GlobalContent.getInstance();
            catalogDownloaderModel = new CatalogDownloaderModel(activityContext, this, Constants.RequestFrom.HOME_PAGE);
        }

        String savedTimeStamp = sharedPreferences.getString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD);
        isDownloadServiceStarted = sharedPreferences.getBoolean(Constants.SP_IS_DOWNLOAD_SERVICE_STARTED);

        if (savedTimeStamp != "") {
            lastRefreshedTime = Long.parseLong(savedTimeStamp);
            long currentTimeStamp = System.currentTimeMillis();
            long timeDifferenceInSec = (currentTimeStamp - lastRefreshedTime) / 1000;

            // If time difference is more than 120 sec i.e. 2 min, we will send a request to load data from server o/w. we will display Issues by fetching from GlobalContent.
            if (timeDifferenceInSec > 120) {
                loadDataFromService();
            } else {
                /*initializeHomePageFragmentUIReferences();
                updateMagazinesCountInHeader(globalContent.getMagazinesCount(), false);
                //updateHomePageCatalog(globalContent.getHomePageCatalog(), false);
                updateCategories(globalContent.getCategoriesList(), false);*/

                if (globalContent.getHomePageCatalog() != null) {
                    updateHomePageCatalog(globalContent.getHomePageCatalog(), false);
                } else {
                    loadDataFromService();
                }
            }
        } else {
            loadDataFromService();
        }
    }

    public void loadDataFromService() {
        if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
            if (!MCProgressDialog.isProgressDialogShown)
                MCProgressDialog.showProgressDialog(activityContext, Constants.LOADING_MESSAGE);

            if (MCAlertDialog.isAlertDialogShown) {
                MCAlertDialog.dismiss();
            }

            catalogDownloaderModel.getMagazinesCountForHomePage();
            catalogDownloaderModel.getHomePageCatalog();

            ArrayList<Category> cachedCategoriesList = globalContent.getCategoriesList();
            if (cachedCategoriesList.size() > 0)
                updateCategories(cachedCategoriesList, false);
            else
                catalogDownloaderModel.getCategoriesList();

            lastRefreshedTime = System.currentTimeMillis();
            sharedPreferences.putString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD, "" + lastRefreshedTime);
        } else {
            displayToast(getResources().getString(R.string.error_no_internet));
        }
    }

    public void initializeHomePageFragmentUIReferences() {
        try {
            homePageFragmentActivity = homePageFragment.getActivity();

            if (homePageFragmentActivity != null) {
                home_page_header_title = (MCTextView) homePageFragmentActivity.findViewById(R.id.home_page_header_title);

                recent_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.recent_issues_gallery);
                popular_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.popular_issues_gallery);

                popular_category1_issues_title = (MCTextView) homePageFragmentActivity.findViewById(R.id.popular_category1_issues_title);
                popular_category2_issues_title = (MCTextView) homePageFragmentActivity.findViewById(R.id.popular_category2_issues_title);
                popular_category3_issues_title = (MCTextView) homePageFragmentActivity.findViewById(R.id.popular_category3_issues_title);
                popular_category4_issues_title = (MCTextView) homePageFragmentActivity.findViewById(R.id.popular_category4_issues_title);
                popular_category5_issues_title = (MCTextView) homePageFragmentActivity.findViewById(R.id.popular_category5_issues_title);

                popular_category1_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.popular_category1_issues_gallery);
                popular_category2_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.popular_category2_issues_gallery);
                popular_category3_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.popular_category3_issues_gallery);
                popular_category4_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.popular_category4_issues_gallery);
                popular_category5_issues_gallery = (LinearLayout) homePageFragmentActivity.findViewById(R.id.popular_category5_issues_gallery);

                isFragmentUIReferencesInitialized = true;
            } else {
                isFragmentUIReferencesInitialized = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMagazinesCountInHeader(String count, boolean needToCache) {
        if (isFragmentUIReferencesInitialized) {
            home_page_header_title.setText(String.format("Read over %s magazines online!", count));

            if (needToCache)
                globalContent.setMagazinesCount(count);

        } else {
            // initializing Fragment UI References
            initializeHomePageFragmentUIReferences();

            updateMagazinesCountInHeader(count, needToCache);
        }
    }

    public void updateCategories(ArrayList<Category> categoriesArrayList, boolean needToCache) {
        for (Category cat : categoriesArrayList) {
            String name = cat.getCategoryName();

            // formatting category icon name in order to access it directory from drawable by it's name
            String iconName = "cat_" + name.toLowerCase().replace(" & ", "_").replace(" ", "_").replace("/", "_");
            int iconResourceIdentifier = utility.getDrawableByResourcesName(activityContext, iconName);
            cat.setCategoryIcon(iconResourceIdentifier);
        }

        categoriesList = categoriesArrayList;
        fragmentDrawer.setCategoriesArrayList(categoriesArrayList);

        if (needToCache)
            globalContent.setCategoriesList(categoriesList);

    }

    public void updateHomePageCatalog(HomePageCatalog catalog, boolean needToCache) {
        if (isFragmentUIReferencesInitialized) {
            if (catalog != null) {
                this.homePageCatalog = catalog;

                updateRecentIssues(homePageCatalog.getRecentIssues());
                updatePopularIssues(homePageCatalog.getPopularIssues());

                // Updating Popular Categories
                List<PopularCategory> popularCategories = homePageCatalog.getPopularCategories();
                if (popularCategories != null && popularCategories.size() > 0) {

                    for (int i = 0; i < popularCategories.size(); i++) {
                        PopularCategory category = popularCategories.get(i);
                        updatePopularCategoryIssues(i, category.getCategoryName(), category.getIssues());
                    }
                }

                // In order to reload data without fetching from service when you came back to dashboard with in 2min, we are caching that data in GlobalContent.
                if (needToCache)
                    globalContent.setHomePageCatalog(catalog);

                resetCurrentlyDownloadingMagazineViewRefs();

                // In order to update all magazine views which are having same content that are in queue.
                for (Content content : globalContent.getDownloadingContents())
                    checkAllIssuesAndUpdateStatusIcons(content.getId());

                if (!isDownloadServiceStarted) {
                    Intent downloadService = new Intent(this, DownloadService.class);
                    startService(downloadService);
                    sharedPreferences.putBoolean(Constants.SP_IS_DOWNLOAD_SERVICE_STARTED, true);
                }
            }
        } else {
            // initializing Fragment UI References if already not initialized
            initializeHomePageFragmentUIReferences();

            updateHomePageCatalog(catalog, needToCache);
        }

        MCProgressDialog.hideProgressDialog();
    }

    public View getMagazineView(final Content content) {
        View magazineView = null;

        try {
            // Inflating magazine view from layout.
            magazineView = mLayoutInflater.inflate(R.layout.magazine_issue, recent_issues_gallery, false);
            magazineView.setTag(content.getId());

            ImageView cover_page = (ImageView) magazineView.findViewById(R.id.cover_page);
            MCTextView product_name = (MCTextView) magazineView.findViewById(R.id.product_name);
            MCTextView issue_name = (MCTextView) magazineView.findViewById(R.id.issue_name);
            final ImageView download_status_icon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
            final ProgressBar download_progress_bar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

            product_name.setText(content.getProductName());
            issue_name.setText(content.getName());

            //Updating Issue Cover Page using Glide library
            String coverPageImageUrl = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());

            RequestOptions glideReqOptions = new RequestOptions();
            glideReqOptions.placeholder(R.drawable.cover_page);
            //glideReqOptions.format(DecodeFormat.PREFER_ARGB_8888);
            Glide.with(this).load(coverPageImageUrl).apply(glideReqOptions).into(cover_page);

            //Updating Issue Cover Page using Picasso library
            //Picasso.with(this).load(coverPageImageUrl).placeholder(R.drawable.cover_page).into(cover_page);

            final Constants.ContentState currentStateOfMagazine = globalContent.getCurrentStateOfMagazine(content);

            if (currentStateOfMagazine == Constants.ContentState.ContentStateNone) {
                download_status_icon.setImageResource(R.drawable.icon_download);
                download_progress_bar.setVisibility(View.INVISIBLE);
            } else if (currentStateOfMagazine == Constants.ContentState.ContentStateDownloaded) {
                download_status_icon.setImageResource(R.drawable.icon_preview);
                download_progress_bar.setVisibility(View.INVISIBLE);
            } else {
                download_status_icon.setImageResource(R.drawable.icon_downloading);
                download_progress_bar.setVisibility(View.VISIBLE);
                download_progress_bar.setProgress(0);
            }

            cover_page.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
                        openIssueDetailPage(content);
                    } else {
                        // Read Magazine
                        readMagazine(content);
                    }
                }
            });

            download_status_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
                        downloadIssue(content);
                    } else {
                        // Open Issue Detail Page
                        openIssueDetailPage(content);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return magazineView;
    }

    public void updateRecentIssues(List<Content> recentIssues) {
        try {
            // removing previously added Issues in order to update with latest data.
            if (recent_issues_gallery.getChildCount() > 0)
                recent_issues_gallery.removeAllViews();

            for (Content content : recentIssues) {
                View view = getMagazineView(content);

                if (view != null)
                    recent_issues_gallery.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePopularIssues(List<Content> popularIssues) {
        try {
            if (popular_issues_gallery.getChildCount() > 0)
                popular_issues_gallery.removeAllViews();

            for (Content content : popularIssues) {
                View view = getMagazineView(content);

                if (view != null)
                    popular_issues_gallery.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePopularCategoryIssues(int index, String categoryTitle, List<Content> issues) {
        try {
            MCTextView categoryTitleTextView = null;
            LinearLayout categoryIssuesLayout = null;

            switch (index) {
                case 0:
                    categoryTitleTextView = popular_category1_issues_title;
                    categoryIssuesLayout = popular_category1_issues_gallery;
                    break;
                case 1:
                    categoryTitleTextView = popular_category2_issues_title;
                    categoryIssuesLayout = popular_category2_issues_gallery;
                    break;
                case 2:
                    categoryTitleTextView = popular_category3_issues_title;
                    categoryIssuesLayout = popular_category3_issues_gallery;
                    break;
                case 3:
                    categoryTitleTextView = popular_category4_issues_title;
                    categoryIssuesLayout = popular_category4_issues_gallery;
                    break;
                case 4:
                    categoryTitleTextView = popular_category5_issues_title;
                    categoryIssuesLayout = popular_category5_issues_gallery;
                    break;
            }

            if (categoryTitleTextView != null) {
                categoryTitleTextView.setText(categoryTitle);
            }

            if (categoryIssuesLayout != null) {
                // removing previously added Issues in order to update with latest data.
                if (categoryIssuesLayout.getChildCount() > 0)
                    categoryIssuesLayout.removeAllViews();

                for (Content content : issues) {
                    View view = getMagazineView(content);

                    if (view != null)
                        categoryIssuesLayout.addView(view);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void networkStateChanged(boolean isConnected) {
        // we are hiding alert dialog if it's already shown
        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }

        // when device connected to network we are refreshing home page data.
        if (isConnected)
            refreshData();
    }

    public void showSearchPage() {
        Intent openIssuesIntent = new Intent(activityContext, SearchActivity.class);
        startActivity(openIssuesIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void openLibrary() {
        Intent openLibraryIntent = new Intent(activityContext, LibraryActivity.class);
        startActivity(openLibraryIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void openSettings() {
        Intent openLibraryIntent = new Intent(activityContext, SettingsActivity.class);
        startActivity(openLibraryIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void openIssueDetailPage(Content content) {
        Intent openIssuesIntent = new Intent(activityContext, IssueDetailsActivity.class);
        openIssuesIntent.putExtra(Constants.BUNDLE_SELECTED_ISSUE_CONTENT, content);
        openIssuesIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(openIssuesIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void downloadIssue(Content content) {
        if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateNone) {
            if (utility.verifyStoragePermissions(this)) {
                startIssueDownload(content);
            } else {
                GlobalContent.currentDownloadingIssue = content;
            }
        }
    }

    public void startIssueDownload(Content content) {
        content.setDownloadType(Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD);
        content.setContentState(Constants.ContentState.ContentStateDownloading);
        Intent downloadServiceIntent = new Intent(activityContext, DownloadService.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
        downloadServiceIntent.putExtras(bundle);
        startService(downloadServiceIntent);

        // In order to update all magazine views under multiple categories which are having same content in home page.
        checkAllIssuesAndUpdateStatusIcons(content.getId());
    }

    public void readMagazine(Content content) {
        File file = new File(GlobalContent.issueFolderPath + content.getId() + "/issue.xml");

        if (file.exists()) {
            Intent viewIssueActivity = new Intent(MainActivity.this, ViewIssueActivity.class);
            viewIssueActivity.putExtra(Constants.BUNDLE_CONTENT, content);
            startActivity(viewIssueActivity);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateDownloading) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.download_progress), getResources().getString(R.string.download_wait_message));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startIssueDownload(GlobalContent.currentDownloadingIssue);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    MCAlertDialog.listener = this;
                    MCAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.need_storage_permission_title), getResources().getString(R.string.need_storage_permission));
                } else {
                    //Previously Permission Request was cancelled with 'Dont Ask Again'.
                    displayToast(getResources().getString(R.string.storage_permission_not_given));
                }
            }
        } else if (requestCode == Constants.REQUEST_GET_ACCOUNTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getAndUpdatePrimaryEmailAddress();
            }
        }
    }

    @Override
    public void alertDialogCallback() {
        ActivityCompat.requestPermissions(MainActivity.this, Constants.STORAGE_PERMISSIONS, Constants.REQUEST_EXTERNAL_STORAGE);
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver downloadManagerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (!isCurrentlyDownloadingMagazineViewRefsInstantiated)
                    instantiateCurrentDownloadingMagazineViewRef(contentId);

                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                    int progress = (int) ((downloaded / total) * 100);

                    for (ProgressBar progressBar : currentlyDownloadingProgressBars) {
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.setProgress(progress);
                    }

                    for (ImageView download_status_icon : currentlyDownloadingStatusIcons) {
                        download_status_icon.setImageResource(R.drawable.icon_downloading);
                    }
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                    for (ProgressBar progressBar : currentlyDownloadingProgressBars) {
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    for (ImageView download_status_icon : currentlyDownloadingStatusIcons) {
                        download_status_icon.setImageResource(R.drawable.icon_preview);
                    }

                    resetCurrentlyDownloadingMagazineViewRefs();
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                    for (ProgressBar progressBar : currentlyDownloadingProgressBars) {
                        progressBar.setVisibility(View.INVISIBLE);
                        progressBar.setProgress(0);
                    }

                    for (ImageView download_status_icon : currentlyDownloadingStatusIcons) {
                        download_status_icon.setImageResource(R.drawable.icon_downloading);
                    }
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                    for (ProgressBar progressBar : currentlyDownloadingProgressBars) {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    for (ImageView download_status_icon : currentlyDownloadingStatusIcons) {
                        download_status_icon.setImageResource(R.drawable.icon_downloading);
                    }
                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                    for (ProgressBar progressBar : currentlyDownloadingProgressBars) {
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    for (ImageView download_status_icon : currentlyDownloadingStatusIcons) {
                        download_status_icon.setImageResource(R.drawable.icon_download);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void instantiateCurrentDownloadingMagazineViewRef(String contentId) {
        if (recent_issues_gallery != null) {
            for (int i = 0; i < recent_issues_gallery.getChildCount(); i++) {
                View magazineView = recent_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        if (popular_issues_gallery != null) {
            for (int i = 0; i < popular_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        if (popular_category1_issues_gallery != null) {
            for (int i = 0; i < popular_category1_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category1_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        if (popular_category2_issues_gallery != null) {
            for (int i = 0; i < popular_category2_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category2_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        if (popular_category3_issues_gallery != null) {
            for (int i = 0; i < popular_category3_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category3_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        if (popular_category4_issues_gallery != null) {
            for (int i = 0; i < popular_category4_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category4_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        if (popular_category5_issues_gallery != null) {
            for (int i = 0; i < popular_category5_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category5_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);

                    currentlyDownloadingProgressBars.add(progressBar);
                    currentlyDownloadingStatusIcons.add(downloadStatusIcon);
                }
            }
        }

        isCurrentlyDownloadingMagazineViewRefsInstantiated = true;
    }

    public void resetCurrentlyDownloadingMagazineViewRefs() {
        currentlyDownloadingProgressBars.clear();
        currentlyDownloadingStatusIcons.clear();
        isCurrentlyDownloadingMagazineViewRefsInstantiated = false;
    }

    public void checkAllIssuesAndUpdateStatusIcons(String contentId) {
        if (recent_issues_gallery != null) {
            for (int i = 0; i < recent_issues_gallery.getChildCount(); i++) {
                View magazineView = recent_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }

        if (popular_issues_gallery != null) {
            for (int i = 0; i < popular_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }

        if (popular_category1_issues_gallery != null) {
            for (int i = 0; i < popular_category1_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category1_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }

        if (popular_category2_issues_gallery != null) {
            for (int i = 0; i < popular_category2_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category2_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }

        if (popular_category3_issues_gallery != null) {
            for (int i = 0; i < popular_category3_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category3_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }

        if (popular_category4_issues_gallery != null) {
            for (int i = 0; i < popular_category4_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category4_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }

        if (popular_category5_issues_gallery != null) {
            for (int i = 0; i < popular_category5_issues_gallery.getChildCount(); i++) {
                View magazineView = popular_category5_issues_gallery.getChildAt(i);

                if (magazineView.getTag().toString().equalsIgnoreCase(contentId)) {
                    ImageView downloadStatusIcon = (ImageView) magazineView.findViewById(R.id.download_status_icon);
                    ProgressBar progressBar = (ProgressBar) magazineView.findViewById(R.id.download_progress_bar);
                    updateStatusIcon(downloadStatusIcon, progressBar);
                }
            }
        }
    }

    public void updateStatusIcon(ImageView downloadIcon, ProgressBar progressBar) {
        try {
            downloadIcon.setImageResource(R.drawable.icon_downloading);
            progressBar.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double getDeviceSizeInInches() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        double wi = (double) width / (double) dm.xdpi;
        double hi = (double) height / (double) dm.ydpi;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);
        Log.d(MainActivity.class.getSimpleName(), "******* Device Size ******** " + width + " x " + height + " && DPI : " + dm.xdpi + " x " + dm.ydpi + " && Density Dpi : " + dm.densityDpi + " && Inches : " + screenInches);
        return screenInches;
    }
}
