package com.mirabel.magazinecentral.activities.viewissue;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.content.FileProvider;

import android.text.PrecomputedText;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.Sharer.Result;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.github.scribejava.apis.PinterestApi;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.adapters.CustomDialogListAdapter;
import com.mirabel.magazinecentral.asynctasks.SocialClass;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCDialogWithCustomView;
import com.mirabel.magazinecentral.customviews.MCEditText;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.DialogSelectionListener;
import com.mirabel.magazinecentral.interfaces.SocialInterface;
import com.mirabel.magazinecentral.util.Utility;
import com.pinterest.android.pdk.PDKBoard;
import com.pinterest.android.pdk.PDKCallback;
import com.pinterest.android.pdk.PDKClient;
import com.pinterest.android.pdk.PDKException;
import com.pinterest.android.pdk.PDKResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.piruin.quickaction.ActionItem;
import me.piruin.quickaction.QuickAction;


import static com.mirabel.magazinecentral.activities.MyApplication.getAppContext;

public class PostMessageActivity extends Activity implements SocialInterface, DialogSelectionListener {
    Context applicationContext, activityContext;
    ImageView postImage = null;
    MCTextView postLink = null;
    MCEditText postMessage = null;
    ContentValues contentValues = null;
    SocialClass social = null;
    Constants.SocialType socialType = null;
    Constants.ShareType shareType = null;
    ImageButton shareButton = null;
    ProgressBar progressBar = null;
    Button postButton = null;
    PDKClient pdkClient;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_message);


        //working here for pinterest issue
      /*  if ("android.intent.action.VIEW".equals(getIntent().getAction())) {
            Uri uri = getIntent().getData();
            if (uri != null && uri.toString().contains("pdk" + getResources().getString(R.string.pinterest_app_id) + "://")) {
                System.out.println("=========uri.toString() = " + uri.toString());
            }
        }*/


        try {
            applicationContext = getApplicationContext();
            activityContext = PostMessageActivity.this;

            shareButton = findViewById(R.id.shareDropDown);
            progressBar = findViewById(R.id.progressBar);
            postButton = findViewById(R.id.postButton);
            postImage = findViewById(R.id.postImage);
            postLink = findViewById(R.id.link);
            postMessage = findViewById(R.id.message);

            contentValues = getIntent().getParcelableExtra("contentValues");

            if (contentValues == null && savedInstanceState != null) {
                contentValues = savedInstanceState.getParcelable("contentValues");
            }

            socialType = Constants.SocialType.values()[contentValues.getAsInteger("socialType")];
            shareType = Constants.ShareType.values()[contentValues.getAsInteger("shareType")];
            postMessage.setText(contentValues.getAsString("postMessage"));

            if (shareType == Constants.ShareType.link) {
                postLink.setText(contentValues.getAsString("name"));
            }

            postImage.setImageBitmap(BitmapFactory.decodeFile(contentValues.getAsString("imagePath")));

            if (socialType != null) {
                setShareImage(socialType);
            }

            //Instantiating Pinterest Client
            pdkClient = PDKClient.configureInstance(this, getResources().getString(R.string.pinterest_app_id));
            pdkClient.onConnect(this);
            PDKClient.setDebugMode(true);

            WindowManager wm = (WindowManager) MyApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            getWindow().setGravity(Gravity.CENTER);

            if (Utility.isTabletDevice(PostMessageActivity.this)) {
                if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                    getWindow().setLayout((int) (width * 0.7), FrameLayout.LayoutParams.WRAP_CONTENT);
                } else {
                    getWindow().setLayout((int) (width * 0.45), FrameLayout.LayoutParams.WRAP_CONTENT);
                }
            } else {
                getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            }

            setFinishOnTouchOutside(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("contentValues", contentValues);

        super.onSaveInstanceState(outState);
    }

    public void shareDropDownAction(View v) {
        ActionItem item0 = new ActionItem(Constants.SocialType.facebook.ordinal(), "", R.drawable.s_facebook_icon);
        ActionItem item1 = new ActionItem(Constants.SocialType.twitter.ordinal(), "", R.drawable.s_twitter_icon);
        ActionItem item2 = new ActionItem(Constants.SocialType.tumblr.ordinal(), "", R.drawable.s_tumblr_icon);
        ActionItem item3 = new ActionItem(Constants.SocialType.linkedin.ordinal(), "", R.drawable.s_linkedin_icon);
        ActionItem item4 = new ActionItem(Constants.SocialType.pinterest.ordinal(), "", R.drawable.s_pinterest_icon);

        QuickAction shareDropDown = new QuickAction(this);
        shareDropDown.addActionItem(item0);
        shareDropDown.addActionItem(item1);
        shareDropDown.addActionItem(item2);
        shareDropDown.addActionItem(item3);
        shareDropDown.addActionItem(item4);

        shareDropDown.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(ActionItem item) {

                socialType = Constants.SocialType.values()[item.getActionId()];
                ViewIssueActivity.socialType = socialType;
                setShareImage(socialType);
            }
        });

        shareDropDown.show(shareButton);
    }

    public void setShareImage(Constants.SocialType type) {
        switch (type) {
            case facebook:
                shareButton.setImageResource(R.drawable.s_facebook_icon);
                break;

            case twitter:
                shareButton.setImageResource(R.drawable.s_twitter_icon);
                break;

            case pinterest:
                shareButton.setImageResource(R.drawable.s_pinterest_icon);
                break;
            case tumblr:
                shareButton.setImageResource(R.drawable.s_tumblr_icon);
                break;

            case linkedin:
                shareButton.setImageResource(R.drawable.s_linkedin_icon);
                break;

            default:
                shareButton.setImageResource(R.drawable.s_facebook_icon);
                break;
        }
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.animate();

    }

    public void hideProgressBar() {
        progressBar.clearAnimation();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(postMessage.getWindowToken(), 0);
    }

    public void close(View v) {
        setResult(RESULT_FIRST_USER);
        finish();
    }

    @Override
    public void updateStatus(boolean status) {
        hideProgressBar();
        if (status) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }

        finish();
    }

    public void post(View v) {
        //to clear all the browser cache  (due to an issue in tumblr)
        Utility.ClearCookies(getAppContext());
        hideKeyboard();
        showProgressBar();

        contentValues.put("postMessage", postMessage.getText().toString());

        if (socialType == Constants.SocialType.facebook) {
            this.postToFacebook(contentValues);
        } else if (socialType == Constants.SocialType.pinterest) {
            hideProgressBar();
            String fileUrl = contentValues.getAsString("imagePath");
            String info= contentValues.getAsString("postMessage") + "\n For Digital Edition, Click " + contentValues.getAsString("shortUrl");
            File file =new File(fileUrl);
            Uri apkURI = FileProvider.getUriForFile(
                    PostMessageActivity.this,
                    getApplicationContext()
                            .getPackageName() + ".provider", file);

            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("image/*");
           // emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Magazine Central");
            emailIntent.putExtra(Intent.EXTRA_TEXT,info);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            emailIntent.putExtra(Intent.EXTRA_STREAM, apkURI);
            startActivityForResult(Intent.createChooser(emailIntent, "Share with..."), 400);

           // Toast.makeText(applicationContext, "Pinterest Sdk has been Deprecated", Toast.LENGTH_SHORT).show();
            //this.postToPinterest(contentValues);
        }

        /* uncomment this to use linkedin sdk*/
        /*else if (socialType == Constants.SocialType.linkedin) {
            this.postToLinkedIn();
        }*/
        else {
            social = new SocialClass(socialType, contentValues, shareType, this, activityContext);
            social.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (PrecomputedText.Params) null);
        }
    }

    @Override
    public void login(Intent intent) {
        startActivityForResult(intent, 200);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( socialType == Constants.SocialType.pinterest) {
            hideProgressBar();
          //  PDKClient.getInstance().onOauthResponse(requestCode, resultCode, data);
        } else {
            if ((requestCode == 200 && resultCode == RESULT_OK) || socialType == Constants.SocialType.facebook || socialType == Constants.SocialType.linkedin || socialType == Constants.SocialType.twitter) {
              
                switch (socialType) {
                    case facebook: {
                        super.onActivityResult(requestCode, resultCode, data);
                        callbackManager.onActivityResult(requestCode, resultCode, data);
                    }
                    break;

                    case twitter: {
                        social.postToTwitter();
                        hideProgressBar();
                    }
                    break;

                    case tumblr: {
                        social.postToTumblr();
                    }
                    break;

                    case linkedin: {
                        //comment by bharadwaja
                       /* social.postToLinkedin();
                        hideProgressBar();*/


                        // new implementation
                        social.shareAPostInLinkedin();
                        hideProgressBar();

                        /* uncomment this to use linkedin sdk*/



                       /* LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
                        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
                        deepLinkHelper.onActivityResult(this, requestCode, resultCode, data);*/

                    }
                    break;

                    default:
                        break;
                }

            } else {
                hideProgressBar();
            }
        }
    }

    public void postToFacebook(final ContentValues contentValues) {
        try {
            callbackManager = CallbackManager.Factory.create();
            shareDialog = new ShareDialog(this);

            String clippedImageSharingChoice = "Photo"; // Link or Photo

            if (shareType == Constants.ShareType.image) {

                File imageFile = new File(contentValues.getAsString("imagePath"));

                if (clippedImageSharingChoice.equals("Photo")) {

                    //facebook app should be installed to share images, videos, etc...So if facebook app is not installed then the below condition fails.
                    if (ShareDialog.canShow(SharePhotoContent.class)) {

                        if (imageFile.exists()) {
                            try {
                                Bitmap image = BitmapFactory.decodeFile(imageFile.getAbsolutePath());

                                SharePhoto photo = new SharePhoto.Builder()
                                        .setBitmap(image)
                                        .setCaption(contentValues.getAsString("postMessage") + "\n For Digital Edition, Click " + contentValues.getAsString("shortUrl")).build();

                                SharePhotoContent content = new SharePhotoContent.Builder()
                                        .addPhoto(photo).build();

                                shareDialog.show(content);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        hideProgressBar();


                        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), "Install Facebook Application");
                    }
                } else {

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        try {
                            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                    // .setContentDescription(contentValues.getAsString("postMessage") + "\n For Digital Edition, Click " + contentValues.getAsString("shortUrl"))
                                    .setContentUrl(Uri.fromFile(imageFile))
                                    .setQuote(contentValues.getAsString("postMessage") + "\n For Digital Edition, Click " + contentValues.getAsString("shortUrl"))
                                    .build();

                            shareDialog.show(linkContent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        hideProgressBar();
                        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), "Install Facebook Application");
                    }
                }

            } else {

                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    try {
                        //final String description = (contentValues.getAsString("description").length() != 0) ? contentValues.getAsString("description") : contentValues.getAsString("postMessage");

                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(contentValues.getAsString("longUrl"))).build();
                        //.setContentTitle(contentValues.getAsString("name"))
                        //.setContentDescription(description)
                        //.setImageUrl(Uri.parse(contentValues.getAsString("picture")))

                        shareDialog.show(linkContent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), "Install Facebook Application");
                }
            }

            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

                @Override
                public void onSuccess(Result result) {
                    updateStatus(true);


                }

                @Override
                public void onCancel() {
                    updateStatus(false);

                }

                @Override
                public void onError(FacebookException error) {
                    updateStatus(false);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /********************* Pinterest Sharing Functionality Starts Here **********************/

    public void postToPinterest(final ContentValues contentValues) {
        /*SharedPreferences sharedPreferences = getSharedPreferences(PinterestApi.class.getName(), 0);
        if (sharedPreferences.getBoolean("isLogged", false)) {
            createNewPinInPinterest("488148115799841012");
        } else {
            logInIntoPinterest();
        }*/

        logInIntoPinterest();
    }

    public void logInIntoPinterest() {
        try {
            showProgressBar();

            SharedPreferences sharedPreferences = getSharedPreferences(PinterestApi.class.getName(), 0);
            final SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();

            List<String> scopes = new ArrayList<>();
            scopes.add(PDKClient.PDKCLIENT_PERMISSION_READ_PUBLIC);
            scopes.add(PDKClient.PDKCLIENT_PERMISSION_WRITE_PUBLIC);

            PDKClient.getInstance().login(this, scopes, new PDKCallback() {
                @Override
                public void onSuccess(PDKResponse response) {

                    sharedPreferencesEditor.putBoolean("isLogged", true);
                    sharedPreferencesEditor.apply();

                    fetchAndDisplayPinterestBoards();
                }

                @Override
                public void onFailure(PDKException exception) {
                    sharedPreferencesEditor.putBoolean("isLogged", false);
                    sharedPreferencesEditor.commit();
                    Log.e(getClass().getName(), exception.getDetailMessage());

                    updateStatus(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchAndDisplayPinterestBoards() {

        String BOARD_FIELDS = "id,name,description,creator,image,counts,created_at";

        PDKClient.getInstance().getMyBoards(BOARD_FIELDS, new PDKCallback() {
            @Override
            public void onSuccess(PDKResponse response) {


                if (response.getBoardList().size() > 0) {
                    final List<PDKBoard> allBoards = response.getBoardList();
                    List<String> allBoardsNames = new ArrayList<>();

                    for (PDKBoard board : allBoards) {
                        allBoardsNames.add(board.getName());
                    }

                    View contentView = getLayoutInflater().inflate(R.layout.custom_dialog_with_list_view, null);
                    ListView customListView = contentView.findViewById(R.id.custom_list_view);
                    final CustomDialogListAdapter customListAdapter = new CustomDialogListAdapter(activityContext, R.layout.custom_dialog_list_item, allBoardsNames, Constants.SelectionType.PINTEREST_BOARD);
                    customListView.setAdapter(customListAdapter);

                    final MCDialogWithCustomView dialogWithListView = new MCDialogWithCustomView(activityContext, PostMessageActivity.this, Constants.SelectionType.PINTEREST_BOARD, "Choose Board", "Ok", contentView);

                    customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            dialogWithListView.dismiss();

                            createNewPinInPinterest(allBoards.get(position).getUid());
                        }
                    });

                    dialogWithListView.show();

                } else {

                    hideProgressBar();
                    MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), "No Pinterest Board(s) Found.");
                }
            }

            @Override
            public void onFailure(PDKException exception) {

                Log.e(getClass().getName(), exception.getDetailMessage());
            }
        });
    }

    public void createNewPinInPinterest(String boardId) {
        try {
            String url = contentValues.getAsString("pinItUrl");

            /*if (shareType == Constants.ShareType.image) {
                File imageFile = new File(contentValues.getAsString("imagePath"));
                //byte[] imageData = readLocalFile(imageFile);
                //url = Base64.encodeToString(imageData, Base64.NO_WRAP);
                url = imageFile.toString();
            }*/

            PDKClient.getInstance().createPin(contentValues.getAsString("postMessage"), boardId, contentValues.getAsString("picture"), url, new PDKCallback() {
                @Override
                public void onSuccess(PDKResponse response) {
                    Log.d(getClass().getName(), response.getData().toString());
                    updateStatus(true);
                }

                @Override
                public void onFailure(PDKException exception) {
                    Log.e(getClass().getName(), exception.getDetailMessage());
                    updateStatus(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dialogSelectionCallback(Constants.SelectionType selectionType) {
        // You can write any code to execute after selecting any row & click on dialog Ok button.
    }

    /*public byte[] readLocalFile(File file) throws IOException, FileNotFoundException {
        ByteArrayOutputStream ous = null;
        InputStream ios = null;

        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        } finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }

        return ous.toByteArray();
    }*/

    /********************* Pinterest Sharing Functionality Ends Here **********************/


    /*****linkedin functionality starts******/

    // Build the list of member permissions our LinkedIn session requires
    /*private static Scope buildScope() {
        //Check Scopes in Application Settings before passing here else you won't able to read that data
        //scope("r_basicprofile w_share")
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
    }*/
/*

    public void postToLinkedIn() {
        //if user ia already loggedin and authorisation takens are stored in shared prefs
        //First check if user is already authenticated or not and session is valid or not
        hideProgressBar();
        if (!LISessionManager.getInstance(this).getSession().isValid()) {
            //if not valid then start authentication
            LISessionManager.getInstance(getApplicationContext()).init(PostMessageActivity.this, buildScope()
                    , new AuthListener() {
                        @Override
                        public void onAuthSuccess() {
                            // Authentication was successful. You can now do other calls with the SDK.
                            onShareClickLinkLinkedin(contentValues);
                        }

                        @Override
                        public void onAuthError(LIAuthError error) {
                            // Handle authentication errors

                        }
                    }, true);//if TRUE then it will show dialog.

        } else {
            //if user is already authenticated fetch basic profile data for user
            onShareClickLinkLinkedin(contentValues);

        }

    }
*/

/*
    public void onShareClickLinkLinkedin(final ContentValues contentValues) {

        //LINK : https://developer.linkedin.com/docs/share-on-linkedin
        String url = "https://api.linkedin.com/v1/people/~/shares";
        JSONObject jsonMap = new JSONObject();
        try {
            jsonMap.put("comment", contentValues.getAsString("postMessage"));
            JSONObject contentObject = new JSONObject();
            contentObject.put("title", contentValues.get("name"));
            contentObject.put("submitted-url", contentValues.getAsString("longUrl"));
            contentObject.put("submitted-image-url", contentValues.getAsString("picture"));
            jsonMap.put("content", contentObject);
            JSONObject visibilityObject = new JSONObject();
            visibilityObject.put("code", "anyone");
            jsonMap.put("visibility", visibilityObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.postRequest(this, url, jsonMap, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                updateStatus(true);

            }

            @Override
            public void onApiError(LIApiError liApiError) {
                updateStatus(false);

            }
        });


    }*/


}