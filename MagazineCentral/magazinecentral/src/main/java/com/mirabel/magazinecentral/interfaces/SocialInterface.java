package com.mirabel.magazinecentral.interfaces;

import android.content.Intent;

public interface SocialInterface {
    void login(Intent intent);

    void updateStatus(boolean status);
}
