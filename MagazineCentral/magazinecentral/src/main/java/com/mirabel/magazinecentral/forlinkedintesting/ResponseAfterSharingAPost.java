package com.mirabel.magazinecentral.forlinkedintesting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseAfterSharingAPost {

    @SerializedName("contentEntities")
    @Expose
    private List<ContentEntity> contentEntities = null;
    @SerializedName("shareMediaCategory")
    @Expose
    private String shareMediaCategory;
    @SerializedName("title")
    @Expose
    private String title;

    public List<ContentEntity> getContentEntities() {
        return contentEntities;
    }

    public void setContentEntities(List<ContentEntity> contentEntities) {
        this.contentEntities = contentEntities;
    }

    public String getShareMediaCategory() {
        return shareMediaCategory;
    }

    public void setShareMediaCategory(String shareMediaCategory) {
        this.shareMediaCategory = shareMediaCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public class ContentEntity {

        @SerializedName("entityLocation")
        @Expose
        private String entityLocation;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("thumbnails")
        @Expose
        private List<Thumbnail> thumbnails = null;

        public String getEntityLocation() {
            return entityLocation;
        }

        public void setEntityLocation(String entityLocation) {
            this.entityLocation = entityLocation;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<Thumbnail> getThumbnails() {
            return thumbnails;
        }

        public void setThumbnails(List<Thumbnail> thumbnails) {
            this.thumbnails = thumbnails;
        }

    }

    public class Created {

        @SerializedName("actor")
        @Expose
        private String actor;
        @SerializedName("time")
        @Expose
        private Integer time;

        public String getActor() {
            return actor;
        }

        public void setActor(String actor) {
            this.actor = actor;
        }

        public Integer getTime() {
            return time;
        }

        public void setTime(Integer time) {
            this.time = time;
        }

    }

    public class Distribution {

        @SerializedName("linkedInDistributionTarget")
        @Expose
        private LinkedInDistributionTarget linkedInDistributionTarget;

        public LinkedInDistributionTarget getLinkedInDistributionTarget() {
            return linkedInDistributionTarget;
        }

        public void setLinkedInDistributionTarget(LinkedInDistributionTarget linkedInDistributionTarget) {
            this.linkedInDistributionTarget = linkedInDistributionTarget;
        }

    }


    public class Example {

        @SerializedName("owner")
        @Expose
        private String owner;
        @SerializedName("activity")
        @Expose
        private String activity;
        @SerializedName("edited")
        @Expose
        private Boolean edited;
        @SerializedName("created")
        @Expose
        private Created created;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("serviceProvider")
        @Expose
        private String serviceProvider;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("lastModified")
        @Expose
        private LastModified lastModified;
        @SerializedName("text")
        @Expose
        private Text text;
        @SerializedName("distribution")
        @Expose
        private Distribution distribution;
        @SerializedName("content")
        @Expose
        private ResponseAfterSharingAPost content;

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public Boolean getEdited() {
            return edited;
        }

        public void setEdited(Boolean edited) {
            this.edited = edited;
        }

        public Created getCreated() {
            return created;
        }

        public void setCreated(Created created) {
            this.created = created;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getServiceProvider() {
            return serviceProvider;
        }

        public void setServiceProvider(String serviceProvider) {
            this.serviceProvider = serviceProvider;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public LastModified getLastModified() {
            return lastModified;
        }

        public void setLastModified(LastModified lastModified) {
            this.lastModified = lastModified;
        }

        public Text getText() {
            return text;
        }

        public void setText(Text text) {
            this.text = text;
        }

        public Distribution getDistribution() {
            return distribution;
        }

        public void setDistribution(Distribution distribution) {
            this.distribution = distribution;
        }

        public ResponseAfterSharingAPost getContent() {
            return content;
        }

        public void setContent(ResponseAfterSharingAPost content) {
            this.content = content;
        }

    }

    public class ImageSpecificContent {


    }

    public class LastModified {

        @SerializedName("actor")
        @Expose
        private String actor;
        @SerializedName("time")
        @Expose
        private Integer time;

        public String getActor() {
            return actor;
        }

        public void setActor(String actor) {
            this.actor = actor;
        }

        public Integer getTime() {
            return time;
        }

        public void setTime(Integer time) {
            this.time = time;
        }

    }

    public class LinkedInDistributionTarget {

        @SerializedName("visibleToGuest")
        @Expose
        private Boolean visibleToGuest;

        public Boolean getVisibleToGuest() {
            return visibleToGuest;
        }

        public void setVisibleToGuest(Boolean visibleToGuest) {
            this.visibleToGuest = visibleToGuest;
        }

    }

    public class Text {

        @SerializedName("text")
        @Expose
        private String text;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }

    public class Thumbnail {

        @SerializedName("imageSpecificContent")
        @Expose
        private ImageSpecificContent imageSpecificContent;
        @SerializedName("resolvedUrl")
        @Expose
        private String resolvedUrl;

        public ImageSpecificContent getImageSpecificContent() {
            return imageSpecificContent;
        }

        public void setImageSpecificContent(ImageSpecificContent imageSpecificContent) {
            this.imageSpecificContent = imageSpecificContent;
        }

        public String getResolvedUrl() {
            return resolvedUrl;
        }

        public void setResolvedUrl(String resolvedUrl) {
            this.resolvedUrl = resolvedUrl;
        }

    }
}
