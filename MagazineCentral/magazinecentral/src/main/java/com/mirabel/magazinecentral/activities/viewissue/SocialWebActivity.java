package com.mirabel.magazinecentral.activities.viewissue;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.scribejava.apis.LinkedInApi;
import com.github.scribejava.apis.TumblrApi;
import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.gson.Gson;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.forlinkedintesting.AccessTokenResponseBean;
import com.mirabel.magazinecentral.forlinkedintesting.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SocialWebActivity extends Activity {
    WebView webView = null;
    OAuth10aService service = null;
    OAuth1RequestToken requestToken = null;
    public String apiKey = null;
    public String apiSecret = null;
    public String sharedPreferencesKey = null;
    Constants.SocialType socialType = null;
    ProgressBar progressBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_web);

        try {
            WindowManager wm = (WindowManager) MyApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            getWindow().setGravity(Gravity.CENTER);

            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            Intent intent = getIntent();
            apiKey = intent.getStringExtra("apiKey");
            apiSecret = intent.getStringExtra("apiSecret");
            Constants.SocialType type = Constants.SocialType.values()[intent.getIntExtra("socialType", 0)];
            String authorizationUrl = null;

            switch (type) {
                case twitter: {
                    service = new ServiceBuilder(getResources().getString(R.string.twitter_api_key))
                            .apiSecret(getResources().getString(R.string.twitter_api_secret))
                            // .callback("twitter://open")
                            .callback("http://twitter/open")
                            .build(TwitterApi.instance());

                    requestToken = service.getRequestToken();
                    authorizationUrl = service.getAuthorizationUrl(requestToken);

                    sharedPreferencesKey = TwitterApi.class.getName();
                    socialType = Constants.SocialType.twitter;
                }
                break;

                case tumblr: {
                    service = new ServiceBuilder(getResources().getString(R.string.tumblr_api_key))
                            .apiSecret(getResources().getString(R.string.tumblr_api_secret))
                            .callback("tumblr:open")
                            .build(TumblrApi.instance());

                    requestToken = service.getRequestToken();
                    authorizationUrl = service.getAuthorizationUrl(requestToken);

                    sharedPreferencesKey = TumblrApi.class.getName();
                    socialType = Constants.SocialType.tumblr;
                }
                break;
                /* case linkedin: {
                    try {
                        service = new ServiceBuilder(getResources().getString(R.string.linkedin_api_key))
                                .scope("r_basicprofile w_share")
                                .apiSecret(getResources().getString(R.string.linkedin_api_secret))
                                .callback("http://www.magazinecentral/redirecturi/")
                                .build(LinkedInApi.instance());

                        requestToken = service.getRequestToken();
                        authorizationUrl = service.getAuthorizationUrl(requestToken);

                        sharedPreferencesKey = LinkedInApi.class.getName();
                        socialType = Constants.SocialType.linkedin;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;*/

                case linkedin: {
                    try {
                        sharedPreferencesKey = LinkedInApi.class.getName();
                        socialType = Constants.SocialType.linkedin;
                        // authorizationUrl = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77lo3j87zrp5mb&redirect_uri=http://www.magazinecentral/redirecturi/&scope=w_member_social r_emailaddress w_share r_liteprofile";
                        authorizationUrl = String.format("https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=%s&redirect_uri=http://www.magazinecentral/redirecturi/&scope=w_member_social r_emailaddress r_liteprofile", getResources().getString(R.string.linkedin_api_key));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

                default:
                    break;
            }

            SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);

            if (prefs.getBoolean("isLogged", false)) {
                // if isLogged is true we don't come to this activity & we are posting image directly to the selected social site in Social class itself.
            } else {
                progressBar = (ProgressBar) findViewById(R.id.progressBar);
                progressBar.animate();
                webView = (WebView) findViewById(R.id.webView);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setUseWideViewPort(true);
                webView.setWebViewClient(new CustomWebClient());
                webView.loadUrl(authorizationUrl);
                webView.getSettings().setSupportZoom(true);
                webView.getSettings().setBuiltInZoomControls(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CustomWebClient extends WebViewClient {

        @TargetApi(android.os.Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            return shouldInterceptRequest(view, request.getUrl().toString());
        }

        @SuppressWarnings("deprecation")
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            return super.shouldInterceptRequest(view, url);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();

            return shouldOverrideUrlLoading(view, url);
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url) {
            if (!progressBar.isShown()) {
                progressBar.animate();
                progressBar.setVisibility(View.VISIBLE);
            }

            //check for our custom callback protocol otherwise use default behavior
            if (socialType == Constants.SocialType.tumblr && url.startsWith("tumblr")) {
                try {
                    Uri uri = Uri.parse(url);
                    String oauth_token = uri.getQueryParameter("oauth_token");
                    String oauth_verifier = uri.getQueryParameter("oauth_verifier");

                    final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, oauth_verifier);

                    SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putString("ACCESS_TOKEN", accessToken.getToken());
                    prefsEditor.putString("ACCESS_SECRET", accessToken.getTokenSecret());
                    OAuthRequest userInfoReq = new OAuthRequest(Verb.GET, "http://api.tumblr.com/v2/user/info");
                    service.signRequest(accessToken, userInfoReq);
                    final Response response = service.execute(userInfoReq);

                    JSONObject jObj = new JSONObject(response.getBody());
                    String blogName = jObj.getJSONObject("response").getJSONObject("user").getString("name");
                    prefsEditor.putString("blogName", blogName + ".tumblr.com");
                    prefsEditor.putBoolean("isLogged", true);
                    prefsEditor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!url.equals("tumblr:open#_=_")) {
                    setResult(RESULT_OK);
                }
                finish();

            } else if (socialType == Constants.SocialType.twitter && url.startsWith("http://twitter")) {
                try {
                    Uri uri = Uri.parse(url);
                    String oauth_token = uri.getQueryParameter("oauth_token");
                    String oauth_verifier = uri.getQueryParameter("oauth_verifier");

                    final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, oauth_verifier);
                    // Now let's go and ask for a protected resource! Below request will get logged in user profile details.
                    /*final OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.twitter.com/1.1/account/verify_credentials.json");
                    service.signRequest(accessToken, request);
                    final Response response = service.execute(request);*/

                    //host twitter detected from callback twitter://open
                    // (uri.getHost().equals("open"))
                    if (uri.toString().contains("twitter/open")) {
                        SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                        SharedPreferences.Editor prefsEditor = prefs.edit();
                        prefsEditor.putString("ACCESS_TOKEN", accessToken.getToken());
                        prefsEditor.putString("ACCESS_SECRET", accessToken.getTokenSecret());
                        prefsEditor.putBoolean("isLogged", true);
                        prefsEditor.apply();
                        setResult(RESULT_OK);
                        finish();
                        return true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;

            } /*else if (socialType == Constants.SocialType.linkedin && url.contains("magazinecentral") && url.contains("oauth_verifier")) {
                try {
                    Uri uri = Uri.parse(url);
                    String oauth_token = uri.getQueryParameter("oauth_token");
                    String oauth_verifier = uri.getQueryParameter("oauth_verifier");

                    final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, oauth_verifier);

                    SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                    SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putString("ACCESS_TOKEN", accessToken.getToken());
                    prefsEditor.putString("ACCESS_SECRET", accessToken.getTokenSecret());
                    prefsEditor.putBoolean("isLogged", true);
                    prefsEditor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                setResult(RESULT_OK);
                finish();

            }
            return super.shouldOverrideUrlLoading(webview, url);*/ else if (url.contains("http://www.magazinecentral/redirecturi/")) {


                if (url.contains("code")) {
                    String code = url.split("code=")[1];
                    String redirectUrl = "http://www.magazinecentral/redirecturi/";
                    String str = String.format("https://www.linkedin.com/oauth/v2/accessToken?grant_type=%s&code=%s&redirect_uri=%s&client_id=%s&client_secret=%s", "authorization_code", code, redirectUrl, getResources().getString(R.string.linkedin_api_key), getResources().getString(R.string.linkedin_api_secret));
                    fetchJsonResultFromLinkedinServer(str);
                    return true;
                } else {
                    // setResult(RESULT_OK);
                    finish();
                }


            }

            return super.shouldOverrideUrlLoading(webview, url);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url.contains("verifier")) {
                Log.d(SocialWebActivity.class.getSimpleName(), url);
            }

            //close the webviewactivity , if the user clicks cancel or not interested to share.
            switch (url) {
                case "http://www.magazinecentral/redirecturi/?error=user_cancelled_login&error_description=The+user+cancelled+LinkedIn+login":
                    finish();
                    break;


            }

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (progressBar.isShown()) {
                progressBar.clearAnimation();
                progressBar.setVisibility(View.INVISIBLE);
            }

            if (url.contains("verifier")) {
                Log.d(SocialWebActivity.class.getSimpleName(), url);
            }

            super.onPageFinished(view, url);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }
    }

    public void onDone(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }


    public void fetchJsonResultFromLinkedinServer(String url) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, null, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        AccessTokenResponseBean people = gson.fromJson(response.toString(), AccessTokenResponseBean.class);
                        getUrnIdOfOwnerLinkedin(people.getAccessToken());
                    }
                }, new com.android.volley.Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        ;
        // Access the RequestQueue through your singleton class.
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);


    }

    public void getUrnIdOfOwnerLinkedin(final String AuthToken) {
        String url = "https://api.linkedin.com/v2/me";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        UserData userdata = gson.fromJson(response.toString(), UserData.class);
                        //save the reqd data in shared prefs and go back.
                        SharedPreferences prefs = getSharedPreferences(sharedPreferencesKey, MODE_PRIVATE);
                        SharedPreferences.Editor prefsEditor = prefs.edit();
                        prefsEditor.putString("ACCESS_TOKEN", AuthToken);
                        prefsEditor.putString("URN_ID", userdata.getId());
                        prefsEditor.putBoolean("isLogged", true);
                        prefsEditor.commit();
                        setResult(RESULT_OK);
                        finish();


                    }
                }, new com.android.volley.Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + AuthToken);
                return params;
            }
        };
        ;
        // Access the RequestQueue through your singleton class.
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);

    }
}