package com.mirabel.magazinecentral.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by venkat on 6/30/17.
 */

public class CatalogIssues implements Serializable {
    @SerializedName("PageCount")
    @Expose
    private int pageCount;
    @SerializedName("TotalRecord")
    @Expose
    private int totalNoOfRecords;
    @SerializedName("lstContent")
    @Expose
    private List<Content> catalogIssues = null;

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getTotalNoOfRecords() {
        return totalNoOfRecords;
    }

    public void setTotalNoOfRecords(int totalNoOfRecords) {
        this.totalNoOfRecords = totalNoOfRecords;
    }

    public List<Content> getCatalogIssues() {
        return catalogIssues;
    }

    public void setCatalogIssues(List<Content> catalogIssues) {
        this.catalogIssues = catalogIssues;
    }
}
