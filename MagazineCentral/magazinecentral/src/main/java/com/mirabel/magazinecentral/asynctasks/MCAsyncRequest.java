package com.mirabel.magazinecentral.asynctasks;

import android.content.Context;
import android.os.AsyncTask;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.interfaces.OnAsyncRequestCompleteListener;
import com.mirabel.magazinecentral.util.NetworkManager;

import org.json.JSONObject;

/**
 * Created by venkat on 20/07/17.
 */
public abstract class MCAsyncRequest extends AsyncTask<Void, Void, String> implements OnAsyncRequestCompleteListener {
    private Context context;
    private String requestURL = null;
    private JSONObject parameters = null;
    private MethodType methodType = MethodType.GET;
    private Constants.RequestCode requestCode = null;

    public enum MethodType {
        GET,
        POST
    }

    public abstract void onResponseReceived(String response);

    public void onResponseReceived(String response, Constants.RequestCode requestCode) {
        // Empty body and we can override this method with your implementation if you want.
    }

    // Constructors
    public MCAsyncRequest(Context context) {
        this.context = context;
    }

    public MCAsyncRequest(Context context, String url, MethodType method) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
    }

    public MCAsyncRequest(Context context, String url, MethodType method, JSONObject params) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.parameters = params;
    }

    public MCAsyncRequest(Context context, String url, MethodType method, Constants.RequestCode reqCode) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.requestCode = reqCode;
    }

    public MCAsyncRequest(Context context, String url, MethodType method, JSONObject params, Constants.RequestCode reqCode) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.parameters = params;
        this.requestCode = reqCode;
    }

    @Override
    protected void onPreExecute() {
        /*if (new NetworkConnectionDetector(context).isNetworkConnected()) {
            MCProgressDialog.showProgressDialog(context, context.getResources().getString(R.string.loading_message));
        }*/
    }

    @Override
    protected String doInBackground(Void... params) {
        String response = null;
        try {
            if (methodType == MethodType.POST) {
                response = new NetworkManager().makeHttpPostConnection(requestURL, parameters);
            } else {
                response = new NetworkManager().makeHttpGetConnection(requestURL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String responseString) {
        // Sending response back to listener
        if (responseString.equals("failure") || responseString.equals("error")) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.error_failure));
        } else if (responseString.equals("timeout")) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.error_request_timeout));
        } else {
            if (requestCode == null)
                onResponseReceived(responseString);
            else
                onResponseReceived(responseString, requestCode);
        }
    }

    @Override
    protected void onCancelled(String response) {
        MCProgressDialog.hideProgressDialog();

        // Sending response back to listener
        if (response.equals("failure") || response.equals("error")) {
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.error_failure));
        } else if (response.equals("timeout")) {
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.error_title), context.getResources().getString(R.string.error_request_timeout));
        } else {
            if (requestCode == null)
                onResponseReceived(response);
            else
                onResponseReceived(response, requestCode);
        }
    }
}
