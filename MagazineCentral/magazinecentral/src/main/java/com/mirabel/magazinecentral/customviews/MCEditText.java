package com.mirabel.magazinecentral.customviews;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by venkat on 20/06/16.
 */
public class MCEditText extends AppCompatEditText {
    public MCEditText(Context context) {
        super(context);

        CustomFontUtils.applyCustomFont(this, context, null);
    }

    public MCEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        CustomFontUtils.applyCustomFont(this, context, attrs);
    }

    public MCEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        CustomFontUtils.applyCustomFont(this, context, attrs);
    }
}
