package com.mirabel.magazinecentral.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by venkat on 6/12/17.
 */

public class HomePageCatalog implements Serializable {
    @SerializedName("PopularCategories")
    @Expose
    private List<PopularCategory> popularCategories = null;
    @SerializedName("PopularIssues")
    @Expose
    private List<Content> popularIssues = null;
    @SerializedName("RecentIssues")
    @Expose
    private List<Content> recentIssues = null;

    public List<PopularCategory> getPopularCategories() {
        return popularCategories;
    }

    public void setPopularCategories(List<PopularCategory> popularCategories) {
        this.popularCategories = popularCategories;
    }

    public List<Content> getPopularIssues() {
        return popularIssues;
    }

    public void setPopularIssues(List<Content> popularIssues) {
        this.popularIssues = popularIssues;
    }

    public List<Content> getRecentIssues() {
        return recentIssues;
    }

    public void setRecentIssues(List<Content> recentIssues) {
        this.recentIssues = recentIssues;
    }
}
