package com.mirabel.magazinecentral.interfaces;

import com.mirabel.magazinecentral.beans.Content;

/**
 * Created by venkat on 7/31/17.
 */

public interface DownloadServiceInterface {
    void downloadFinished(Content content);
}
