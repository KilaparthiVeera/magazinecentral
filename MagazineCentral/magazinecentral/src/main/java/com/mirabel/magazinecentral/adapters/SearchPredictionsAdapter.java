package com.mirabel.magazinecentral.adapters;

import android.content.Context;
import android.os.AsyncTask;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.util.EncryptDecryptStringWithDES;
import com.mirabel.magazinecentral.util.NetworkConnectionDetector;
import com.mirabel.magazinecentral.util.NetworkManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by venkat on 7/20/17.
 */

public class SearchPredictionsAdapter extends BaseAdapter implements Filterable {
    private static final int MAX_RESULTS = 10;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> searchPredictionsArrayList = new ArrayList<>();

    public SearchPredictionsAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return searchPredictionsArrayList.size();
    }

    @Override
    public String getItem(int position) {
        return searchPredictionsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        try {
            SearchPredictionsViewHolder searchPredictionsViewHolder;

            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, viewGroup, false);

                searchPredictionsViewHolder = new SearchPredictionsViewHolder();
                searchPredictionsViewHolder.textView = (TextView) convertView.findViewById(android.R.id.text1);
                searchPredictionsViewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.black));

                convertView.setTag(searchPredictionsViewHolder);

            } else {
                searchPredictionsViewHolder = (SearchPredictionsViewHolder) convertView.getTag();
            }

            searchPredictionsViewHolder.textView.setText(getItem(position));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private class SearchPredictionsViewHolder {
        TextView textView;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults filterResults = new FilterResults();

                if (charSequence != null) {
                    try {
                        if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                            ArrayList<String> predictionsList = new ArrayList<>();

                            String response = new GetSearchPredictionsAsyncTask().execute(charSequence.toString()).get();

                            if (response != null) {
                                JSONObject responseObj = new JSONObject(response);

                                if (responseObj.has("Predictions")) {
                                    JSONArray predictions = responseObj.getJSONArray("Predictions");
                                    for (int i = 0; i < predictions.length(); i++) {
                                        predictionsList.add(predictions.getString(i));
                                    }
                                }

                                // Assign the data to the FilterResults
                                filterResults.values = predictionsList;
                                filterResults.count = predictionsList.size();
                            }
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                if (results != null && results.count > 0) {
                    searchPredictionsArrayList = (ArrayList<String>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    private class GetSearchPredictionsAsyncTask extends AsyncTask<String, Void, String> {
        String responseString = null;

        @Override
        protected String doInBackground(String... strings) {
            try {
                String keyword = strings[0];
                String dtTicks = new EncryptDecryptStringWithDES().getEncryptedString(String.valueOf(System.currentTimeMillis()));
                String getSearchPredictionsReqURL = String.format("%s/Search?dtticks=%s&&prediction=%s", Constants.MAIN_URL, dtTicks, URLEncoder.encode(keyword, Constants.CHARSET_NAME));

                responseString = new NetworkManager().makeHttpGetConnection(getSearchPredictionsReqURL);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String response) {
            try {
                if (response != null) {
                    JSONObject responseObj = new JSONObject(response);

                    if (responseObj.has("Predictions")) {
                        JSONArray predictions = responseObj.getJSONArray("Predictions");
                        ArrayList<String> predictionsList = new ArrayList<>();
                        for (int i = 0; i < predictions.length(); i++) {
                            predictionsList.add(predictions.getString(i));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
