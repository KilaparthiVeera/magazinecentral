package com.mirabel.magazinecentral.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mirabel.magazinecentral.beans.viewissue.Bookmark;
import com.mirabel.magazinecentral.constants.Constants;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by venkat on 6/12/17.
 */

public class Content implements Serializable {
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("ContentID")
    @Expose
    private String id;
    @SerializedName("CntSize")
    @Expose
    private Long contentSize;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("IssuedOn")
    @Expose
    private String issuedOn;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ProductID")
    @Expose
    private Integer productId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("PublishedOn")
    @Expose
    private String publishedOn;
    @SerializedName("PublisherID")
    @Expose
    private String publisherId;
    @SerializedName("PublisherName")
    @Expose
    private String publisherName;
    @SerializedName("WebUrl")
    @Expose
    private String webUrl;
    @SerializedName("pinitUrl")
    @Expose
    private String pinItUrl = Constants.WEB_URL;
    @Expose
    private float price = 0.00f;
    @Expose
    private Constants.ContentState contentState = Constants.ContentState.ContentStateNone;
    @Expose
    private Constants.DOWNLOAD_TYPE downloadType = Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD;
    @Expose
    private float downloadProgress;
    @Expose
    private int startPageIndex = -1;
    @Expose
    private ArrayList<Bookmark> bookmarks = new ArrayList<>();

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getContentSize() {
        return contentSize;
    }

    public void setContentSize(Long contentSize) {
        this.contentSize = contentSize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssuedOn() {
        return issuedOn;
    }

    public void setIssuedOn(String issuedOn) {
        this.issuedOn = issuedOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(String publishedOn) {
        this.publishedOn = publishedOn;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getPinItUrl() {
        return pinItUrl;
    }

    public void setPinItUrl(String pinItUrl) {
        this.pinItUrl = pinItUrl;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Constants.ContentState getContentState() {
        return contentState;
    }

    public void setContentState(Constants.ContentState contentState) {
        this.contentState = contentState;
    }

    public Constants.DOWNLOAD_TYPE getDownloadType() {
        return downloadType;
    }

    public void setDownloadType(Constants.DOWNLOAD_TYPE downloadType) {
        this.downloadType = downloadType;
    }

    public float getDownloadProgress() {
        return downloadProgress;
    }

    public void setDownloadProgress(float downloadProgress) {
        this.downloadProgress = downloadProgress;
    }

    public int getStartPageIndex() {
        return startPageIndex;
    }

    public void setStartPageIndex(int startPageIndex) {
        this.startPageIndex = startPageIndex;
    }

    public ArrayList<Bookmark> getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(ArrayList<Bookmark> bookmarks) {
        this.bookmarks = bookmarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Content content = (Content) o;

        return id.equalsIgnoreCase(content.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Content{" +
                "category='" + category + '\'' +
                ", id='" + id + '\'' +
                ", contentSize=" + contentSize +
                ", description='" + description + '\'' +
                ", issuedOn='" + issuedOn + '\'' +
                ", name='" + name + '\'' +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", publishedOn='" + publishedOn + '\'' +
                ", publisherId='" + publisherId + '\'' +
                ", publisherName='" + publisherName + '\'' +
                ", webUrl='" + webUrl + '\'' +
                ", pinItUrl='" + pinItUrl + '\'' +
                ", price=" + price +
                ", contentState=" + contentState +
                ", downloadType=" + downloadType +
                ", downloadProgress=" + downloadProgress +
                ", startPageIndex=" + startPageIndex +
                ", bookmarks=" + bookmarks +
                '}';
    }
}
