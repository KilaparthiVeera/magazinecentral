package com.mirabel.magazinecentral.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.beans.NavDrawerItem;
import com.mirabel.magazinecentral.customviews.MCTextView;

import java.util.ArrayList;

/**
 * Created by venkat on 27/06/16.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private LayoutInflater layoutInflater;

    public NavigationDrawerAdapter(Context context, ArrayList<NavDrawerItem> items) {
        this.context = context;
        this.navDrawerItems = items;
        this.layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public void delete(int position) {
        navDrawerItems.remove(position);
        notifyItemRemoved(position);
    }

    public void setNavDrawerItems(ArrayList<NavDrawerItem> navDrawerItems) {
        this.navDrawerItems = navDrawerItems;

        // reloading navigationDrawerAdapter items
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.nav_drawer_list_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem item = navDrawerItems.get(position);
        holder.icon.setImageResource(item.getIcon());
        holder.title.setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return navDrawerItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        MCTextView title;

        public MyViewHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(R.id.nav_drawer_icon);
            title = (MCTextView) itemView.findViewById(R.id.nav_drawer_title);
        }
    }
}
