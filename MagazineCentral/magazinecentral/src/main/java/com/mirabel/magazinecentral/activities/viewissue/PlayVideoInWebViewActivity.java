package com.mirabel.magazinecentral.activities.viewissue;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mirabel.magazinecentral.R;

public class PlayVideoInWebViewActivity extends Activity {
    WebView webView = null;
    ProgressBar progressBar = null;
    private String videoUrl;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video_in_webview);

        try {
            webView = (WebView) findViewById(R.id.videoWebView);
            progressBar = (ProgressBar) findViewById(R.id.videoWebViewProgressBar);

            videoUrl = getIntent().getStringExtra("videoUrl");

            if (webView != null) {
                webView.clearCache(true);

                if (Build.VERSION.SDK_INT < 18) {
                    webView.clearView();
                }

                webView.clearHistory();
            }

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_NORMAL);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setPluginState(PluginState.ON);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            //webView.setLayerType(View.LAYER_TYPE_HARDWARE, null); // Don't add this, this is giving blinking effect on Kindle Fire HD.

            webView.getSettings().setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
            //webView.getSettings().setUserAgentString("Android Mozilla/5.0 AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
            webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

            webView.setWebChromeClient(new WebChromeClient());

            webView.setWebViewClient(new PlayVideoInWebViewClient());

            webView.loadUrl(videoUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class PlayVideoInWebViewClient extends WebViewClient {

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            showProgressBar();
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            showProgressBar();
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgressBar();
            super.onPageStarted(view, url, favicon);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            hideProgressBar();
            Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            hideProgressBar();
            Toast.makeText(getApplicationContext(), error.getDescription(), Toast.LENGTH_SHORT).show();
            super.onReceivedError(view, request, error);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            hideProgressBar();
            super.onPageFinished(view, url);
        }
    }

    public void showProgressBar() {
        isLoading = true;

        if (progressBar != null) {
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.animate();
            }
        }
    }

    public void hideProgressBar() {
        isLoading = false;

        if (progressBar != null) {
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.clearAnimation();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        clear();
        finish();
    }

    @Override
    public void onBackPressed() {
        clear();

        super.onBackPressed();
    }

    public void onComplete(View v) {
        clear();

        finish();
    }

    public void clear() {
        if (webView != null) {
            if (isLoading)
                webView.stopLoading();
            //webView.clearCache(true);
            //webView.clearView();
            //webView.freeMemory();
            webView.destroy();
        }
    }

    @Override
    public void finish() {
        super.finish();
    }
}
