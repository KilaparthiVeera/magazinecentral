package com.mirabel.magazinecentral.beans.viewissue;

import java.util.ArrayList;

public class Interactivity {
    public int x;
    public int y;
    public int width;
    public int height;
    public int type;
    public ArrayList<InteractivityItem> items;
    public boolean scaled;
    public String title;
    public String description;

    public Interactivity() {

    }

    public Interactivity(Interactivity iv) {
        this.x = iv.x;
        this.y = iv.y;
        this.width = iv.width;
        this.height = iv.height;
        this.type = iv.type;
        if (iv.items != null) {
            this.items = iv.items;
        }
        this.scaled = iv.scaled;
        this.title = iv.title;
        this.description = iv.description;
    }
}