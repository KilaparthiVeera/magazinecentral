package com.mirabel.magazinecentral.fragments;

import android.app.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.facebook.login.LoginManager;
import com.github.scribejava.apis.LinkedInApi;
import com.github.scribejava.apis.PinterestApi;
import com.github.scribejava.apis.TumblrApi;
import com.github.scribejava.apis.TwitterApi;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCButton;
import com.mirabel.magazinecentral.util.Utility;


import static com.mirabel.magazinecentral.activities.MyApplication.getAppContext;

public class OptionsFragment extends Fragment {
    private RelativeLayout currentLayout;
    private MCButton clearCacheButton;

    public OptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        currentLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_options, container, false);

        return currentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        clearCacheButton = (MCButton) currentLayout.findViewById(R.id.clearCacheButton);

        clearCacheButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] prefsNames = {TwitterApi.class.getName(), TumblrApi.class.getName(), LinkedInApi.class.getName(), PinterestApi.class.getName()};
                for (String pref : prefsNames) {
                    SharedPreferences.Editor ed = getActivity().getSharedPreferences(pref, Activity.MODE_PRIVATE).edit();
                    ed.putBoolean("isLogged", false);
                    ed.apply();


                }

                Utility.ClearCookies(getAppContext());
                //facebook logout
                LoginManager.getInstance().logOut();
                //linkedin clear session on logout
                // LISessionManager.getInstance(getActivity()).clearSession();
                //pinterest logout
                // PDKClient.getInstance().logout();


                MCAlertDialog.showAlertDialog(getContext(), "", "Cache cleared successfully.");
            }
        });
    }


   /* //to clear the browser cookies for showing the login screen for social sharing
    public static void ClearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }*/
}
