package com.mirabel.magazinecentral.asynctasks;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.scribejava.apis.LinkedInApi;
import com.github.scribejava.apis.TumblrApi;
import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.google.gson.Gson;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.activities.viewissue.SocialWebActivity;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.forlinkedintesting.ResponseAfterSharingAPost;
import com.mirabel.magazinecentral.interfaces.SocialInterface;
import com.tumblr.jumblr.JumblrClient;
import com.tumblr.jumblr.types.PhotoPost;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SocialClass extends AsyncTask {
    SharedPreferences sharedPreferences = null;
    Constants.SocialType socialType = null;
    Constants.ShareType shareType; // 0 = LinkShare & 1 = ImageShare
    SocialInterface socialInterface = null;
    public ContentValues contentValues = null;
    boolean status = false;
    OAuth10aService service = null;
    OAuth1AccessToken accessToken = null;
    JumblrClient tumblrClient;
    Context context;

    public SocialClass(Constants.SocialType socialType, ContentValues contentValues, Constants.ShareType shareType, SocialInterface socialInterface, Context context) {
        this.socialType = socialType;
        this.contentValues = contentValues;
        this.shareType = shareType;
        this.socialInterface = socialInterface;
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object... params) {
        switch (socialType) {
            case twitter: {
                service = new ServiceBuilder(context.getResources().getString(R.string.twitter_api_key))
                        .apiSecret(context.getResources().getString(R.string.twitter_api_secret))
                        .callback("")
                        .build(TwitterApi.instance());

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TwitterApi.class.getName(), 0);

                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToTwitter();
                } else {
                    login(context.getResources().getString(R.string.twitter_api_key), context.getResources().getString(R.string.twitter_api_secret));
                }
            }
            break;

            case tumblr: {
               /* service = new ServiceBuilder(Constants.TUMBLR_API_KEY)
                        .apiSecret(Constants.TUMBLR_API_SECRET)
                        .callback("")
                        .build(TumblrApi.instance());

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TumblrApi.class.getName(), 0);
                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToTumblr();
                } else {
                    login(Constants.TUMBLR_API_KEY, Constants.TUMBLR_API_SECRET);
                }*/

                tumblrClient = new JumblrClient(
                        context.getResources().getString(R.string.tumblr_api_key),
                        context.getResources().getString(R.string.tumblr_api_secret)
                );

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TumblrApi.class.getName(), 0);
                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToTumblr();
                } else {
                    login(context.getResources().getString(R.string.tumblr_api_key), context.getResources().getString(R.string.tumblr_api_secret));
                }


            }
            break;

            case linkedin: {

                //  code commented by bharadwaja for developing new linkedin integration

               /* service = new ServiceBuilder(context.getResources().getString(R.string.linkedin_api_key))
                        .scope("r_basicprofile w_share")
                        .apiSecret(context.getResources().getString(R.string.linkedin_api_secret))
                        .callback("http://www.magazinecentral/redirecturi/")
                        .build(LinkedInApi.instance());

                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(LinkedInApi.class.getName(), 0);
                if (sharedPreferences.getBoolean("isLogged", false)) {
                    postToLinkedin();
                } else {
                    login(context.getResources().getString(R.string.linkedin_api_key), context.getResources().getString(R.string.linkedin_api_secret));
                }*/


                //new implementation
                sharedPreferences = MyApplication.getAppContext().getSharedPreferences(LinkedInApi.class.getName(), 0);
                if (sharedPreferences.getBoolean("isLogged", false)) {
                    shareAPostInLinkedin();
                } else {
                    login(context.getResources().getString(R.string.linkedin_api_key), context.getResources().getString(R.string.linkedin_api_secret));

                }

            }
            break;

            default:
                break;
        }

        return status;
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        socialInterface.updateStatus((Boolean) values[0]);
    }


    public void login(String apiKey, String apiSecret) {

        Intent i = new Intent(MyApplication.getAppContext(), SocialWebActivity.class);
        i.putExtra("socialType", socialType.ordinal());
        i.putExtra("apiKey", apiKey);
        i.putExtra("apiSecret", apiSecret);
        socialInterface.login(i);
    }

    public void postToTwitter() {
        sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TwitterApi.class.getName(), MODE_PRIVATE);

        if (sharedPreferences.getBoolean("isLogged", false)) {
            try {

                service = new ServiceBuilder(context.getResources().getString(R.string.twitter_api_key))
                        .apiSecret(context.getResources().getString(R.string.twitter_api_secret))
                        .callback("")
                        .build(TwitterApi.instance());

                accessToken = new OAuth1AccessToken(sharedPreferences.getString("ACCESS_TOKEN", ""), sharedPreferences.getString("ACCESS_SECRET", ""));
                OAuthRequest request = new OAuthRequest(Verb.POST, "https://api.twitter.com/1.1/statuses/update_with_media.json");
                //service.signRequest(accessToken, request);
                //final Response response = service.execute(request); // if you want to authorize every uncomment this line.

                try {
                    MultipartEntity entity = new MultipartEntity();

                    // Twitter allows its message length up to 160 characters only.
                    if (shareType == Constants.ShareType.image) {
                        String twitterMessage = "For Digital Edition,\nClick " + contentValues.getAsString("longUrl");
                        entity.addPart("status", new StringBody(twitterMessage));
                        entity.addPart("media", new FileBody(new File(contentValues.getAsString("imagePath").replace("_t", ""))));  // THIS IS THE PHOTO TO UPLOAD
                    } else {
                        entity.addPart("status", new StringBody(contentValues.getAsString("postMessage")));
                        entity.addPart("media", new FileBody(new File(contentValues.getAsString("imagePath").replace("_t", ""))));  // THIS IS THE PHOTO TO UPLOAD
                    }

                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    entity.writeTo(out);
                    request.setPayload(out.toByteArray());
                    request.addHeader(entity.getContentType().getName(), entity.getContentType().getValue());

                    service.signRequest(accessToken, request);
                    Response response = service.execute(request);

                    JSONObject responseJSONObject = new JSONObject(response.getBody());

                    if (responseJSONObject.has("text")) {
                        status = true;
                        publishProgress(status);
                    } else {
                        status = false;
                        publishProgress(status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }


    public void postToTumblr() {
        tumblrClient = new JumblrClient(
                context.getResources().getString(R.string.tumblr_api_key),
                context.getResources().getString(R.string.tumblr_api_secret)
        );
        sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TumblrApi.class.getName(), 0);
        tumblrClient.setToken(
                sharedPreferences.getString("ACCESS_TOKEN", ""),
                sharedPreferences.getString("ACCESS_SECRET", "")
        );


        if (shareType == Constants.ShareType.link) {
            PhotoPost post = null;
            try {
                post = tumblrClient.newPost(sharedPreferences.getString("blogName", ""), PhotoPost.class);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }

            post.setSource(contentValues.getAsString("picture"));
            post.setTags(new ArrayList<String>(Arrays.asList("Magazine Central", "Mirabel")));
            post.setCaption(contentValues.getAsString("postMessage"));
            post.setLinkUrl(contentValues.getAsString("shortUrl"));
            try {
                //Initiates upload of image file
                post.save();
            } catch (IOException e) {
                e.printStackTrace();
            }

            status = true;
            publishProgress(status);


        } else {
            PhotoPost post = null;
            try {
                post = tumblrClient.newPost(sharedPreferences.getString("blogName", ""), PhotoPost.class);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
            post.setTags(new ArrayList<String>(Arrays.asList("Magazine Central", "Mirabel")));
            post.setCaption(contentValues.getAsString("postMessage"));
            post.setLinkUrl(contentValues.getAsString("shortUrl"));
            File file = new File(contentValues.getAsString("imagePath"));
            post.setData(file);
            try {
                //Initiates upload of image file
                post.save();
            } catch (IOException e) {
                e.printStackTrace();
            }

            status = true;
            publishProgress(status);

        }

    }

/*
    public void postToTumblr() {
        try {
            service = new ServiceBuilder(Constants.TUMBLR_API_KEY)
                    .apiSecret(Constants.TUMBLR_API_SECRET)
                    .callback("")
                    .build(TumblrApi.instance());

            sharedPreferences = MyApplication.getAppContext().getSharedPreferences(TumblrApi.class.getName(), 0);
            String url = String.format("http://api.tumblr.com/v2/blog/%s/post", sharedPreferences.getString("blogName", ""));
            accessToken = new OAuth1AccessToken(sharedPreferences.getString("ACCESS_TOKEN", ""), sharedPreferences.getString("ACCESS_SECRET", ""));
            OAuthRequest postRequest = new OAuthRequest(Verb.POST, url);

            if (shareType == Constants.ShareType.link) {
                postRequest.addBodyParameter("link", contentValues.getAsString("shortUrl"));
                postRequest.addBodyParameter("type", "photo");
                postRequest.addBodyParameter("tags", "Magazine Central,Mirabel");
                postRequest.addBodyParameter("source", contentValues.getAsString("picture"));
                postRequest.addBodyParameter("caption", contentValues.getAsString("postMessage"));

                service.signRequest(accessToken, postRequest);
                Response response = service.execute(postRequest);

                try {
                    JSONObject responseJSONObject = new JSONObject(response.getBody());

                    if (responseJSONObject.has("response")) {
                        status = true;
                        publishProgress(status);
                    } else {
                        status = false;
                        publishProgress(status);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                postRequest.addBodyParameter("type", "photo");
                postRequest.addBodyParameter("link", contentValues.getAsString("shortUrl"));
                postRequest.addBodyParameter("caption", contentValues.getAsString("postMessage"));

                try {
                    MultipartEntity entity = new MultipartEntity();
                    entity.addPart("link", new StringBody(contentValues.getAsString("shortUrl")));
                    entity.addPart("type", new StringBody("photo"));
                    entity.addPart("caption", new StringBody(contentValues.getAsString("postMessage")));
                    ContentBody cb = new FileBody(new File(contentValues.getAsString("imagePath")), "image/jpeg");
                    entity.addPart("data", cb);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    entity.writeTo(out);
                    postRequest.setPayload(out.toByteArray());
                    postRequest.addHeader(entity.getContentType().getName(), entity.getContentType().getValue());

                    service.signRequest(accessToken, postRequest);
                    Response response = service.execute(postRequest);

                    JSONObject responseJSONObject = new JSONObject(response.getBody());

                    if (responseJSONObject.has("response")) {
                        status = true;
                        publishProgress(status);
                    } else {
                        status = false;
                        publishProgress(status);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    //old code commented by bharadwaja reddy
    /*public void postToLinkedin() {
        try {
            service = new ServiceBuilder(context.getResources().getString(R.string.linkedin_api_key))
                    .scope("r_basicprofile w_share")
                    .apiSecret(context.getResources().getString(R.string.linkedin_api_secret))
                    .callback("")
                    .build(LinkedInApi.instance());

            sharedPreferences = MyApplication.getAppContext().getSharedPreferences(LinkedInApi.class.getName(), 0);

            accessToken = new OAuth1AccessToken(sharedPreferences.getString("ACCESS_TOKEN", ""), sharedPreferences.getString("ACCESS_SECRET", ""));
            OAuthRequest postRequest = new OAuthRequest(Verb.POST, "http://api.linkedin.com/v1/people/~/shares");

            //set the headers to the server knows what we are sending
            postRequest.addHeader("Content-Type", "application/json");
            postRequest.addHeader("x-li-format", "json");

            try {
                JSONObject jsonMap = new JSONObject();
                jsonMap.put("comment", contentValues.getAsString("postMessage"));

                JSONObject contentObject = new JSONObject();
                contentObject.put("title", contentValues.get("name"));
                contentObject.put("submitted-url", contentValues.getAsString("longUrl"));
                contentObject.put("submitted-image-url", contentValues.getAsString("picture"));
                jsonMap.put("content", contentObject);

                JSONObject visibilityObject = new JSONObject();
                visibilityObject.put("code", "anyone");
                jsonMap.put("visibility", visibilityObject);

                postRequest.setPayload(jsonMap.toString());

                service.signRequest(accessToken, postRequest);
                Response response = service.execute(postRequest);

                JSONObject responseJSONObject = new JSONObject(response.getBody());

                if (responseJSONObject.has("updateKey")) {
                    status = true;
                    publishProgress(true);
                } else {
                    status = false;
                    publishProgress(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public void shareAPostInLinkedin() {

        //first verify weather the request is coming with auth token or not in sharedprefs
        if (!sharedPreferences.getBoolean("isLogged", false)) {
           /* status = false;
            publishProgress(status);*/
            return;
        }

        //  as per the documentation, the accessToken will expire at some point of time.
        // So , when ever we gat an error related to access token  , then we have to generate a new token.
        // the possible request codes will be 500 internal server error and 401 unauthorised.

        JSONObject postbody = null;
        try {
            postbody = new JSONObject();
            JSONObject resolvedUrl = new JSONObject();
            resolvedUrl.put("resolvedUrl", contentValues.getAsString("picture"));

            JSONArray thumbnails = new JSONArray();
            thumbnails.put(resolvedUrl);

            JSONObject entityLocation_and_thumbnails_object = new JSONObject();
            entityLocation_and_thumbnails_object.put("entityLocation", contentValues.getAsString("longUrl"));
            entityLocation_and_thumbnails_object.put("thumbnails", thumbnails);
            JSONArray contentEntities = new JSONArray();
            contentEntities.put(entityLocation_and_thumbnails_object);

            JSONObject contentEntities_and_title_object = new JSONObject();
            contentEntities_and_title_object.put("contentEntities", contentEntities);
            contentEntities_and_title_object.put("title", contentValues.get("name"));

            postbody.put("content", contentEntities_and_title_object);


            JSONObject distributionObject = new JSONObject();
            JSONObject emptyObj = new JSONObject();
            distributionObject.put("linkedInDistributionTarget", emptyObj);
            postbody.put("distribution", distributionObject);


            JSONObject textobject = new JSONObject();
            textobject.put("text", contentValues.getAsString("postMessage"));
            postbody.put("text", textobject);
            postbody.put("subject", "Sharing Magazine Page");
            postbody.put("owner", "urn:li:person:" + sharedPreferences.getString("URN_ID", ""));

        } catch (JSONException e) {
            e.printStackTrace();
        }

         // String shareurl = "http://api.linkedin.com/v1/people/~/shares";

        String shareurl = "https://api.linkedin.com/v2/shares";
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, shareurl, postbody, new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        ResponseAfterSharingAPost responseData = gson.fromJson(response.toString(), ResponseAfterSharingAPost.class);

                        status = true;
                        publishProgress(status);
                    }
                }, new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        status = false;
                        publishProgress(status);

                        error.printStackTrace();

                        int statusCode = error.networkResponse.statusCode;
                        switch (statusCode) {
                            //generate new access tokens
                            case 401:
                                // clear shared prefs of linkedin
                                SharedPreferences linkedinsettings1 = context.getSharedPreferences(LinkedInApi.class.getName(), MODE_PRIVATE);
                                linkedinsettings1.edit().clear().apply();
                                login(context.getResources().getString(R.string.linkedin_api_key), context.getResources().getString(R.string.linkedin_api_secret));
                                break;
                            case 500:
                                // clear shared prefs of linkedin
                                SharedPreferences linkedinsettings2 = context.getSharedPreferences(LinkedInApi.class.getName(), MODE_PRIVATE);
                                linkedinsettings2.edit().clear().apply();
                                login(context.getResources().getString(R.string.linkedin_api_key), context.getResources().getString(R.string.linkedin_api_secret));
                                break;
                        }
                    }
                })


        {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", "Bearer " + sharedPreferences.getString("ACCESS_TOKEN", ""));
                return params;
            }
        };
        // Access the RequestQueue through your singleton class.
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjectRequest);

    }
}