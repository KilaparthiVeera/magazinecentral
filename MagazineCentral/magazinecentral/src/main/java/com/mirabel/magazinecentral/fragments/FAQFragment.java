package com.mirabel.magazinecentral.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.R;

import static com.mirabel.magazinecentral.activities.MyApplication.getAppContext;

public class FAQFragment extends Fragment {
    private RelativeLayout currentLayout;
    private WebView webView = null;

    public FAQFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        currentLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_faq, container, false);
        return currentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        webView = (WebView) currentLayout.findViewById(R.id.faqWebView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        if (getAppContext().getResources().getBoolean(R.bool.is_branded_app)) {
            webView.loadUrl(getResources().getString(R.string.faq_url_branded_app));
        } else {
            webView.loadUrl(getResources().getString(R.string.faq_url_mc_app));
        }
        webView.getSettings().setSupportZoom(true);
    }
}
