package com.mirabel.magazinecentral.activities;

import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.fragments.AboutUsFragment;
import com.mirabel.magazinecentral.fragments.FAQFragment;
import com.mirabel.magazinecentral.fragments.InstructionsFragment;
import com.mirabel.magazinecentral.fragments.OptionsFragment;

public class SettingsActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    public static double deviceSizeInInches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        try {
            deviceSizeInInches = getDeviceSizeInInches();

            bottomNavigationView = (BottomNavigationView) findViewById(R.id.settings_bottom_navigation_view);
            if (getResources().getBoolean(R.bool.is_branded_app)) {
                bottomNavigationView.getMenu().removeItem(R.id.action_item4);

            }
            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    if (getResources().getBoolean(R.bool.is_branded_app)) {
                        bottomNavigationView.getMenu().removeItem(R.id.action_item4);
                        if (item.getItemId() == R.id.action_item1) {
                            selectedFragment = new InstructionsFragment();
                        } else if (item.getItemId() == R.id.action_item2) {
                            selectedFragment = new FAQFragment();
                        } else if (item.getItemId() == R.id.action_item3) {
                            selectedFragment = new OptionsFragment();
                        }
                    } else {
                        if (item.getItemId() == R.id.action_item1) {
                            selectedFragment = new InstructionsFragment();
                        } else if (item.getItemId() == R.id.action_item2) {
                            selectedFragment = new FAQFragment();
                        } else if (item.getItemId() == R.id.action_item3) {
                            selectedFragment = new OptionsFragment();
                        } else if (item.getItemId() == R.id.action_item4) {
                            selectedFragment = new AboutUsFragment();
                        }

                    }
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.settings_frame_layout, selectedFragment);
                    transaction.commit();
                    return true;
                }
            });

            //Manually displaying the first fragment - one time only
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.settings_frame_layout, new InstructionsFragment());
            transaction.commit();

            //Used to select an item programmatically
            //bottomNavigationView.getMenu().getItem(0).setChecked(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        onBackPressed();
    }

    public double getDeviceSizeInInches() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        double wi = (double) width / (double) dm.xdpi;
        double hi = (double) height / (double) dm.ydpi;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);
        return screenInches;
    }
}
