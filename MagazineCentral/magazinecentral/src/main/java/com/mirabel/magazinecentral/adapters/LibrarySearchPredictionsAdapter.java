package com.mirabel.magazinecentral.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.LibrarySearchActivity;
import com.mirabel.magazinecentral.beans.GlobalContent;

import java.util.ArrayList;

/**
 * Created by venkat on 7/20/17.
 */

public class LibrarySearchPredictionsAdapter extends BaseAdapter implements Filterable {
    private static final int MAX_RESULTS = 10;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> filteredList;
    private ArrayList<String> productNames;

    public LibrarySearchPredictionsAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        filteredList = new ArrayList<>();
        productNames = GlobalContent.getInstance().getProductNames();
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public String getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        try {
            SearchPredictionsViewHolder searchPredictionsViewHolder;

            if (convertView == null) {
                convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, viewGroup, false);

                searchPredictionsViewHolder = new SearchPredictionsViewHolder();
                searchPredictionsViewHolder.textView = (TextView) convertView.findViewById(android.R.id.text1);
                searchPredictionsViewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.black));

                convertView.setTag(searchPredictionsViewHolder);

            } else {
                searchPredictionsViewHolder = (SearchPredictionsViewHolder) convertView.getTag();
            }

            searchPredictionsViewHolder.textView.setText(getItem(position));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private class SearchPredictionsViewHolder {
        TextView textView;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults filterResults = new FilterResults();

                if (charSequence != null) {

                    LibrarySearchActivity.searchKeyword = charSequence.toString(); // in order to update in SearchCriteria object.

                    try {
                        filteredList.clear();

                        for (String productName : productNames) {
                            if (productName.toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                filteredList.add(productName);
                            }
                        }

                        // Assign the data to the FilterResults
                        filterResults.values = filteredList;
                        filterResults.count = filteredList.size();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                if (results != null && results.count > 0) {
                    filteredList = (ArrayList<String>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }
}
