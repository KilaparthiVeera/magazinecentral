package com.mirabel.magazinecentral.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.SettingsActivity;
import com.mirabel.magazinecentral.adapters.InstructionsAdapter;

import static com.mirabel.magazinecentral.activities.MyApplication.getAppContext;

public class InstructionsFragment extends Fragment implements ViewPager.OnPageChangeListener {
    RelativeLayout currentLayout = null;
    ViewPager instructionsViewPager = null;
    private LinearLayout pager_indicator;
    InstructionsAdapter instructionsAdapter = null;

    private int dotsCount;
    private ImageView[] dotsImageViews;
    int[] resources;

    public InstructionsFragment() {
        if (getAppContext().getResources().getBoolean(R.bool.is_branded_app)) {
            if (SettingsActivity.deviceSizeInInches > 7.0) {
                resources = new int[]{R.drawable.instructions_ba, R.drawable.instructions_02, R.drawable.instructions_03_tablet, R.drawable.instructions_04};
            } else {
                resources = new int[]{R.drawable.instructions_ba, R.drawable.instructions_02, R.drawable.instructions_03, R.drawable.instructions_04};
            }
        } else {
            if (SettingsActivity.deviceSizeInInches > 7.0) {
                resources = new int[]{R.drawable.instructions_01, R.drawable.instructions_02, R.drawable.instructions_03_tablet, R.drawable.instructions_04};
            } else {
                resources = new int[]{R.drawable.instructions_01, R.drawable.instructions_02, R.drawable.instructions_03, R.drawable.instructions_04};
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        currentLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_instructions, container, false);

        return currentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        instructionsAdapter = new InstructionsAdapter(getContext(), resources);

        instructionsViewPager = (ViewPager) currentLayout.findViewById(R.id.instructionPager);
        instructionsViewPager.setAdapter(instructionsAdapter);
        instructionsViewPager.setOffscreenPageLimit(1);
        instructionsViewPager.setCurrentItem(0);
        instructionsViewPager.addOnPageChangeListener(this);

        pager_indicator = (LinearLayout) currentLayout.findViewById(R.id.viewPagerCountDots);

        dotsCount = instructionsAdapter.getCount();
        dotsImageViews = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dotsImageViews[i] = new ImageView(getContext());
            dotsImageViews[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nonselected_view_pager_item_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dotsImageViews[i], params);
        }

        dotsImageViews[0].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.selected_view_pager_item_dot));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dotsImageViews[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nonselected_view_pager_item_dot));
        }

        dotsImageViews[position].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.selected_view_pager_item_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
