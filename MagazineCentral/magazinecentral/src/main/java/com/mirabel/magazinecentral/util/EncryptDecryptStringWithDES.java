package com.mirabel.magazinecentral.util;

import android.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * @author venkat
 */

public class EncryptDecryptStringWithDES {

    public String KEY = "MgC@2o!7";

    public int KEY_LENGTH = 8;

    public String CHARSET_NAME = "UTF-8";

    public String ALGORITHM_NAME = "DES/CBC/PKCS5Padding";

    SecretKey secretKey = null;
    IvParameterSpec iv = null;

    public EncryptDecryptStringWithDES() {
        try {
            String shorterKey = trimString(KEY);

            // Encode the key into bytes using UTF-8
            byte[] encodedKey = shorterKey.getBytes(CHARSET_NAME);

            // Getting hash key for encoded key
            byte[] hashKey = getHashKey(encodedKey, KEY_LENGTH);

            KeySpec keySpec = new DESKeySpec(hashKey);

            secretKey = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);

            iv = new IvParameterSpec(hashKey);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getEncryptedString(String inputString) {
        String encString = null;

        try {
            // Encode the input string into bytes using UTF-8
            byte[] inputBytes = inputString.getBytes(CHARSET_NAME);

            String encryptedString = encrypt(inputBytes, secretKey, iv);

            encString = ((encryptedString.replace('=', ',')).replace('/', '_')).replace('+', '-');

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encString;
    }

    public String getDecryptedString(String encryptedString) {
        String decString = null;

        try {
            decString = decrypt(encryptedString, secretKey, iv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return decString;
    }

    public String encrypt(byte[] inputBytes, SecretKey secretKey, IvParameterSpec iv) throws Exception {
        String encString = null;
        try {
            // Instantiating & initializing encrypt mode cipher
            Cipher ecipher = Cipher.getInstance(ALGORITHM_NAME);
            ecipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
            byte[] encodedBytes = ecipher.doFinal(inputBytes);

            // Encode bytes to base64 to get a string
            //encString = new Base64().encodeToString(encodedBytes); // Uncomment this for other applications like Java Desktop
            encString = Base64.encodeToString(encodedBytes, Base64.NO_WRAP); //In Android we need to use android.util.Base64 instead of Apache Commons-Codec

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return encString;
    }

    public String decrypt(String encryptedString, SecretKey secretKey, IvParameterSpec iv) throws Exception {
        String decString = null;
        try {
            // Decode base64 to get bytes
            //byte[] decodedBytes = new Base64().decodeBase64(encryptedString); // Uncomment this for other applications like Java Desktop

            byte[] decodedBytes = Base64.decode(encryptedString, Base64.NO_WRAP | Base64.URL_SAFE); //In Android we need to use android.util.Base64 instead of Apache Commons-Codec

            // Instantiating & initializing decrypt mode cipher
            Cipher dcipher = Cipher.getInstance(ALGORITHM_NAME);
            dcipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
            byte[] utf8 = dcipher.doFinal(decodedBytes);

            // Decode using UTF-8
            decString = new String(utf8, CHARSET_NAME);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        return decString;
    }

    // Trim the shared secret key to 8 chars
    public String trimString(String value) {
        if (value.length() >= KEY_LENGTH)
            return value.substring(0, KEY_LENGTH);

        return value;
    }

    public byte[] getHashKey(byte[] encKey, int length) {
        byte[] truncatedHash = new byte[length];
        try {
            byte[] sha = DigestUtils.sha1(encKey);

            byte[] hashKey = new byte[sha.length];

            for (int i = 0; i < sha.length; i++) {
                if (sha[i] < 0) {
                    sha[i] = (byte) (sha[i] & 0xFF);
                }

                hashKey[i] = sha[i];
            }

            System.arraycopy(hashKey, 0, truncatedHash, 0, length);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return truncatedHash;
    }
}
