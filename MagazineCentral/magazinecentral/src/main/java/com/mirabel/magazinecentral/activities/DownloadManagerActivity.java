package com.mirabel.magazinecentral.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.adapters.DownloadManagerIssuesGridViewAdapter;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.database.DBHelper;

public class DownloadManagerActivity extends AppCompatActivity {
    private Context activityContext;
    private GridView downloading_issues_grid_view;
    private MCTextView no_downloads_are_in_progress_message;

    private DownloadManagerIssuesGridViewAdapter downloadManagerIssuesGridViewAdapter;
    private DBHelper dbHelper;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_manager);

        try {
            activityContext = DownloadManagerActivity.this;

            downloading_issues_grid_view = (GridView) findViewById(R.id.downloading_issues_grid_view);
            no_downloads_are_in_progress_message = (MCTextView) findViewById(R.id.no_downloads_are_in_progress_message);

            dbHelper = new DBHelper(activityContext);
            handler = new Handler();

            downloadManagerIssuesGridViewAdapter = new DownloadManagerIssuesGridViewAdapter(activityContext, this);
            downloading_issues_grid_view.setAdapter(downloadManagerIssuesGridViewAdapter);

            checkForDownloadingIssues();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD_CONFIRMATION));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(broadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        Intent showHomePageIntent = new Intent(activityContext, MainActivity.class);
        showHomePageIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(showHomePageIntent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void checkForDownloadingIssues() {
        if (GlobalContent.getInstance().getDownloadingContents().size() > 0) {
            downloading_issues_grid_view.setVisibility(View.VISIBLE);
            no_downloads_are_in_progress_message.setVisibility(View.GONE);
        } else {
            no_downloads_are_in_progress_message.setVisibility(View.VISIBLE);
            downloading_issues_grid_view.setVisibility(View.GONE);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (intent.getAction().equals(Constants.INTENT_ACTION_REFRESH_VIEW)) {
                    checkForDownloadingIssues();
                    downloadManagerIssuesGridViewAdapter.notifyDataSetChanged();
                } else {
                    if (downloading_issues_grid_view != null) {
                        for (int i = 0; i < downloading_issues_grid_view.getChildCount(); i++) {
                            View magazineView = downloading_issues_grid_view.getChildAt(i);
                            DownloadManagerIssuesGridViewAdapter.DownloadManagerIssueViewViewHolder viewHolder = (DownloadManagerIssuesGridViewAdapter.DownloadManagerIssueViewViewHolder) magazineView.getTag();

                            if (viewHolder.getContentId().equalsIgnoreCase(contentId)) {
                                ProgressBar currentProgressBar = viewHolder.download_progress_bar;
                                ImageView downloadIcon = viewHolder.download_status_icon;
                                ImageView pause_or_resume_download_icon = viewHolder.pause_or_resume_download_icon;

                                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                                    int progress = (int) ((downloaded / total) * 100);

                                    // we are showing currently downloading Issue progressbar only
                                    if (contentId == GlobalContent.currentDownloadingIssue.getId()) {
                                        currentProgressBar.setVisibility(View.VISIBLE);
                                        currentProgressBar.setProgress(progress);
                                        downloadIcon.setImageResource(R.drawable.icon_downloading);
                                    }
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                                    checkForDownloadingIssues();
                                    downloadManagerIssuesGridViewAdapter.notifyDataSetChanged();
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                                    pause_or_resume_download_icon.setImageResource(R.drawable.icon_continue_download);
                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    //currentProgressBar.setProgress(0);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                                    pause_or_resume_download_icon.setImageResource(R.drawable.icon_pause_download);
                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD_CONFIRMATION)) {
                                    Content content = (Content) intent.getSerializableExtra(Constants.BUNDLE_CONTENT);
                                    cancelIssueDownload(content);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                                    checkForDownloadingIssues();
                                    downloadManagerIssuesGridViewAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void cancelIssueDownload(final Content content) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activityContext, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle("Are you sure you want to cancel downloading this magazine?");
            alertDialogBuilder.setMessage("This magazine will be removed from the download queue");
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MCProgressDialog.showProgressDialog(activityContext, "Cancelling Issue Downloading, Please wait..");

                    content.setContentState(Constants.ContentState.ContentStateNone);
                    Intent cancelMagazineDownloadIntent = new Intent(Constants.INTENT_ACTION_CANCEL_DOWNLOAD);
                    cancelMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
                    cancelMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT, content);
                    LocalBroadcastManager.getInstance(activityContext).sendBroadcast(cancelMagazineDownloadIntent);

                    MCProgressDialog.hideProgressDialog();
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(activityContext, R.color.gray));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
