package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Interactivity;
import com.mirabel.magazinecentral.beans.viewissue.InteractivityItem;
import com.mirabel.magazinecentral.constants.Constants;

import static com.mirabel.magazinecentral.activities.MyApplication.getAppContext;

public class InteractivityView extends RelativeLayout {
    public VideoView videoView = null;
    public Interactivity interactivity = null;
    ImageButton playButton = null;
    ImageButton closeButton = null;
    boolean isTracked = false;
    Content content = null;
    String pageName = null;
    RelativeLayout layout;
    boolean isClosed = false;
    public boolean isPaused;
    public int stopPosition = 0;

    public InteractivityView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public InteractivityView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InteractivityView(Context context, Interactivity interactivity, Content con, String pn) {
        super(context);
        this.interactivity = interactivity;
        this.content = con;
        this.pageName = pn;
        final InteractivityItem item = interactivity.items.get(0);
        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (RelativeLayout) inflater.inflate(R.layout.interactive_view, null);
        layout.setBackgroundColor(Color.TRANSPARENT);
        playButton = (ImageButton) inflater.inflate(R.layout.mcbutton, null);
        playButton.setImageDrawable(ContextCompat.getDrawable(MyApplication.getAppContext(), R.drawable.blue_play));
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playVideo();
            }
        });

        addView(playButton);

        if (item.vSource != null && item.vSource.length() > 0 || interactivity.type == 5) {
            playButton.setScaleType(ScaleType.FIT_XY);
            playButton.setAdjustViewBounds(true);
            RelativeLayout.LayoutParams playButtonParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            playButtonParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            playButton.setLayoutParams(playButtonParams);
            addView(layout);
        } else {
            RelativeLayout.LayoutParams playButtonParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            playButtonParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            playButton.setLayoutParams(playButtonParams);

            layout.setAlpha(0);

            try {
                videoView = new VideoView(context) {
                    @Override
                    public void pause() {
                        super.pause();
                        stopPosition = videoView.getCurrentPosition();
                        isPaused = true;
                        layout.setBackgroundColor(Color.TRANSPARENT);
                        videoView.setVisibility(View.INVISIBLE);
                        videoView.stopPlayback();
                        playButton.setVisibility(View.VISIBLE);
                        closeButton.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void start() {
                        super.start();

                        if (!isTracked) {
                            String label;
                            if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                                label = String.format("/%s/%s/%s/android/%d/p/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), pageName);
                            } else {
                                label = String.format("/%s/%s/%s/android/%d/l/page%s", content.getPublisherId(), content.getProductName(), content.getName(),getAppContext().getResources().getInteger(R.integer.app_type), pageName);
                            }

                            String title = null;
                            if (item.text.length() > 0) {
                                title = item.text;
                            } else {
                                title = String.format("Video on Page : %s", pageName);
                            }

                            MyApplication.setCustomVar(1, "primaryEmail", GlobalContent.getInstance().primaryEmail);
                            MyApplication.trackEvent("Android-Multimedia", title, label);
                            isTracked = true;
                        }
                    }

                    @Override
                    public void resume() {
                        super.resume();
                        isPaused = false;
                        videoView.seekTo(stopPosition);
                        videoView.start();
                        layout.setAlpha(1.0f);
                    }
                };

            } catch (Exception e) {
                // TODO: handle exception
            }

            videoView.setVisibility(View.INVISIBLE);
            videoView.setTag(pageName);
            layout.setAlpha(0.7f);
            videoView.setVideoPath(item.source);
            RelativeLayout.LayoutParams videoParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            videoParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            videoParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            videoParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            videoParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            videoParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            videoView.setLayoutParams(videoParams);
            videoView.setMediaController(new MediaController(context));
            videoView.setEnabled(false);
            layout.addView(videoView);
            addView(layout);

            closeButton = (ImageButton) inflater.inflate(R.layout.mcbutton, null);
            closeButton.setImageDrawable(ContextCompat.getDrawable(MyApplication.getAppContext(), R.drawable.close));
            RelativeLayout.LayoutParams closeParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            closeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT | RelativeLayout.ALIGN_PARENT_TOP);
            closeButton.setLayoutParams(closeParams);
            closeButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    isClosed = true;
                    InteractivityView.this.removeAllViews();

                }
            });

            addView(closeButton);
        }
    }

    public void playVideo() {
        InteractivityItem item = null;
        if (interactivity.items.size() > 0) {
            item = interactivity.items.get(0);
        }

        if (item.vSource != null) {
            if (item.vSource.equalsIgnoreCase("E")) {

                Intent playVideo = new Intent("PlayExternalVideo");
                playVideo.putExtra("source", item.source);
                getContext().sendBroadcast(playVideo);
            } else if (item.vSource.equalsIgnoreCase("S")) {

                String videoUrl = item.source;

                if (videoUrl.contains("youtube")) { // To play video from you tube
                    //videoUrl = "https://www."+videoUrl.substring(index, videoUrl.length());
                    Intent playVideo = new Intent("PlayYoutubeVideo");
                    playVideo.putExtra("source", item.source);
                    getContext().sendBroadcast(playVideo);
                } else { // To play video from other streaming sites
                    Intent playVideo = new Intent("PlayVideoInWebView");
                    playVideo.putExtra("source", item.source);
                    getContext().sendBroadcast(playVideo);
                }
            } else {
                playLocalVideo(item);
            }
        } else {
            playLocalVideo(item);
        }
    }

    public void playLocalVideo(InteractivityItem item) {
        if (interactivity.type == 5) {
            Intent playVideo = new Intent("PlayModalVideo");
            playVideo.putExtra("source", item.source);
            getContext().sendBroadcast(playVideo);
        } else if (!isClosed) {
            if (isPaused) {
                videoView.resume();
            } else {
                videoView.start();
            }

            layout.setAlpha(1.0f);
            layout.setBackgroundColor(Color.TRANSPARENT);
            videoView.setVisibility(View.VISIBLE);
            playButton.setVisibility(View.INVISIBLE);
            closeButton.setVisibility(View.VISIBLE);
            videoView.setEnabled(true);
        }
    }
}