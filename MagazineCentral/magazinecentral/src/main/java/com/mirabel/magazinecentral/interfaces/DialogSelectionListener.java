package com.mirabel.magazinecentral.interfaces;

import com.mirabel.magazinecentral.constants.Constants;

/**
 * Created by venkat on 25/10/16.
 */
public interface DialogSelectionListener {
    void dialogSelectionCallback(Constants.SelectionType selectionType);
}
