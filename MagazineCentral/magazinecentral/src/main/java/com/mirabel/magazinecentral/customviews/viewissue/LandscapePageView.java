package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Hotspot;
import com.mirabel.magazinecentral.beans.viewissue.Interactivity;
import com.mirabel.magazinecentral.beans.viewissue.InteractivityItem;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

import java.io.File;

public class LandscapePageView extends ZoomView implements ZoomView.ZoomViewListener {
    public Page leftPage, rightPage;
    Content content = null;
    ViewPagerInterface viewPagerInterface = null;
    GestureDetector mGesture = null;
    public static boolean handleEvent = true;
    LinearLayout mainLayout = null;
    RelativeLayout superLayout = null;
    private boolean isShownInteractivites;
    ImageView leftImageView, rightImageView;
    public RelativeLayout leftLayout, rightLayout;
    LoadImage loadImage;
    ImageView placeHolder;
    private Handler handler = new Handler();
    public InteractivityView liv = null, riv = null;
    public boolean isZoomed = false;

    public LandscapePageView(Context context) {
        super(context);
    }

    public LandscapePageView(Context context, Page leftPage, Page rightPage, Content content, ViewPagerInterface viewPagerInterface) {
        super(context);

        mGesture = new GestureDetector(context, new GestureListener());
        if (leftPage == null) {
            this.leftPage = leftPage;
        } else {
            this.leftPage = new Page(leftPage);
        }

        if (rightPage == null) {
            this.rightPage = rightPage;
        } else {
            this.rightPage = new Page(rightPage);
        }

        this.content = content;
        this.viewPagerInterface = viewPagerInterface;
        superLayout = new RelativeLayout(context);
        superLayout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mainLayout = new LinearLayout(context);
        mainLayout.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        par.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainLayout.setLayoutParams(par);
        requestDisallowInterceptTouchEvent(true);
        superLayout.addView(mainLayout);
        addView(superLayout);
        this.setListner(this);
        leftImageView = new ImageView(context);
        rightImageView = new ImageView(context);

        placeHolder = new ImageView(context);
        placeHolder.setImageResource(R.drawable.action_bar_holo_dark_style);
        RelativeLayout.LayoutParams ivParams = new RelativeLayout.LayoutParams(100, 100);
        ivParams.addRule(CENTER_IN_PARENT);
        placeHolder.setLayoutParams(ivParams);

        superLayout.addView(placeHolder);
    }

    public void dismissAllViews() {
        if (leftLayout != null) {
            leftLayout.removeAllViews();

        }
        if (rightLayout != null) {
            rightLayout.removeAllViews();
        }
        if (mainLayout != null) {
            mainLayout.removeAllViews();
        }
    }

    public void displayImage() {
        loadImage = new LoadImage(true);
        loadImage.isSmall = false;
        loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    public void displaySmallImage() {
        loadImage = new LoadImage(true);
        loadImage.isSmall = true;
        loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    public void displayLeftImage() {

        LoadImage loadImage = new LoadImage(true);
        if (leftPage == null) {
            loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, GlobalContent.issueFolderPath + content.getId() + "/" + leftPage.imageName + "." + leftPage.imageType);
        }
    }

    public void displayRightImage() {
        LoadImage loadImage = new LoadImage(false);
        if (rightPage == null) {
            loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        } else {
            loadImage.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, GlobalContent.issueFolderPath + content.getId() + "/" + rightPage.imageName + "." + rightPage.imageType);
        }
    }

    @Override
    public void onZoomStarted(float zoom, float zoomx, float zoomy) {
        hideInteractivities();
    }

    @Override
    public void onZooming(float zoom, float zoomx, float zoomy) {
        //viewPagerInterface.disable();
        hideInteractivities();
    }

    @Override
    public void onZoomEnded() {
        hideInteractivities();
        //viewPagerInterface.disable();
    }

    @Override
    public void onReceivedSingleTap() {
        showHotspotsBackground();

        viewPagerInterface.onSingleTapConfirmed();
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            showHotspotsBackground();

            if (handleEvent) {
                viewPagerInterface.onSingleTapConfirmed();
            } else {
                handleEvent = true;
            }

            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return super.onDown(e);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_CANCEL) {
            Log.d("LandscapePageView", "Cancel");
        }

        mGesture.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }


    public void hideInteractivities() {
        if (zoom > 1.0 && isShownInteractivites) {

            if (leftLayout != null) {
                for (int i = 0; i < leftLayout.getChildCount(); i++) {
                    if (leftLayout.getChildAt(i).getClass().equals(InteractivityView.class)) {
                        InteractivityView iv = (InteractivityView) leftLayout.getChildAt(i);
                        if (iv.videoView != null && iv.videoView.isPlaying()) {
                            iv.videoView.pause();
                        }
                        iv.setVisibility(View.GONE);
                    }
                }
            }

            if (rightLayout != null) {
                for (int i = 0; i < rightLayout.getChildCount(); i++) {
                    if (rightLayout.getChildAt(i).getClass().equals(InteractivityView.class)) {
                        InteractivityView iv = (InteractivityView) rightLayout.getChildAt(i);
                        if (iv.videoView != null && iv.videoView.isPlaying()) {
                            iv.videoView.pause();
                        }
                        iv.setVisibility(View.GONE);
                    }
                }
            }

            isShownInteractivites = false;
            isZoomed = true;

        } else if (zoom == 1 && isShownInteractivites == false) {
            /*if (loadImage != null){
                //loadImage = new LoadImage(true);

				if (leftPage != null) {
					loadImage.displayLeftInteractivities();
				}
				if (rightPage != null) {
					loadImage.displayRightInteractivities();
				}
			}*/

            if (leftLayout != null) {
                for (int i = 0; i < leftLayout.getChildCount(); i++) {
                    if (leftLayout.getChildAt(i).getClass().equals(InteractivityView.class)) {
                        InteractivityView iv = (InteractivityView) leftLayout.getChildAt(i);
                        iv.setVisibility(View.VISIBLE);
                    }
                }
            }

            if (rightLayout != null) {
                for (int i = 0; i < rightLayout.getChildCount(); i++) {
                    if (rightLayout.getChildAt(i).getClass().equals(InteractivityView.class)) {
                        InteractivityView iv = (InteractivityView) rightLayout.getChildAt(i);
                        iv.setVisibility(View.VISIBLE);
                    }
                }
            }

            isShownInteractivites = true;
            isZoomed = false;
        }

        AudioInteractivityView aiv = new AudioInteractivityView(getContext(), null);
        aiv.setIconForPlayAudioButton();
    }

    public class LoadImage extends AsyncTask<String, Void, String> {
        double xScale, yScale, scale;
        int scaledWidth, scaledHeight;
        int originalWidth = -1;
        int originalHeight = -1;
        String leftPath = "", rightPath = "";
        BitmapFactory.Options bitmapOptions;
        Bitmap leftBitmap = null, rightBitmap = null;
        boolean isSmall;

        public LoadImage(boolean isLeft) {
            leftLayout = new RelativeLayout(LandscapePageView.this.getContext());
            rightLayout = new RelativeLayout(LandscapePageView.this.getContext());
        }

        @Override
        protected String doInBackground(String... params) {
            if (leftPage != null) {
                if (isSmall) {
                    leftPath = GlobalContent.issueFolderPath + content.getId() + "/" + leftPage.imageName + "_t." + leftPage.imageType;
                } else {
                    leftPath = GlobalContent.issueFolderPath + content.getId() + "/" + leftPage.imageName + "." + leftPage.imageType;
                }

                leftBitmap = BitmapFactory.decodeFile(leftPath);
            }
            if (rightPage != null) {

                if (isSmall) {
                    rightPath = GlobalContent.issueFolderPath + content.getId() + "/" + rightPage.imageName + "_t." + rightPage.imageType;
                } else {
                    rightPath = GlobalContent.issueFolderPath + content.getId() + "/" + rightPage.imageName + "." + rightPage.imageType;
                }

                rightBitmap = BitmapFactory.decodeFile(rightPath);
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            displayLeftImage(leftBitmap);
            displayRightImage(rightBitmap);
            superLayout.removeView(placeHolder);
        }

        public void displayLeftImage(Bitmap bmp) {
            try {
                if (bmp == null) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(GlobalContent.screenWidth / 2, GlobalContent.screenHeight);
                    leftImageView.setLayoutParams(layoutParams);
                    leftLayout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    leftLayout.addView(leftImageView);
                    mainLayout.addView(leftLayout);
                } else {
                    originalWidth = bmp.getWidth();
                    originalHeight = bmp.getHeight();
                    bitmapOptions = new BitmapFactory.Options();
                    bitmapOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(leftPath, bitmapOptions);
                    bitmapOptions.inSampleSize = calculateInSampleSize(bitmapOptions, GlobalContent.screenWidth / 2, GlobalContent.screenHeight);
                    bitmapOptions.inJustDecodeBounds = false;

                    xScale = (double) (GlobalContent.screenWidth / 2) / originalWidth;
                    yScale = (double) GlobalContent.screenHeight / originalHeight;
                    if (xScale < yScale) {
                        scale = xScale;
                    } else {
                        scale = yScale;
                    }
                    scaledWidth = Math.min(GlobalContent.screenWidth / 2, (int) (originalWidth * scale));
                    scaledHeight = (int) (originalHeight * scale);

                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(scaledWidth, scaledHeight);
                    leftImageView.setLayoutParams(layoutParams);
                    leftLayout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    leftImageView.setImageBitmap(BitmapFactory.decodeFile(leftPath, bitmapOptions));
                    leftImageView.setScaleType(ScaleType.FIT_XY);
                    leftLayout.addView(leftImageView);
                    mainLayout.addView(leftLayout);
                    displayLeftHotspots();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void displayLeftHotspots() {
            HotspotView hsv = null;
            float scaleX, scaleY;
            scaleX = (float) scaledWidth / GlobalContent.width;
            scaleY = (float) scaledHeight / GlobalContent.height;

            for (Hotspot hs : leftPage.getHotspots()) {
                hs = new Hotspot(hs);
                if (!hs.scaled) {
                    hs.setX((int) (hs.getX() * scaleX));
                    hs.setY((int) (hs.getY() * scaleY));
                    hs.setWidth((int) (hs.getWidth() * scaleX));
                    hs.setHeight((int) (hs.getHeight() * scaleY));
                    hs.scaled = true;
                }

                hsv = new HotspotView(LandscapePageView.this.getContext(), hs, content.getId());
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) hs.getWidth(), (int) hs.getHeight());
                params.setMargins((int) hs.getX(), (int) hs.getY(), 0, 0);
                hsv.setLayoutParams(params);
                leftLayout.addView(hsv);
            }

            displayLeftInteractivities();
        }

        public void displayLeftInteractivities() {
            float scaleX, scaleY;
            scaleX = (float) scaledWidth / GlobalContent.width;
            scaleY = (float) scaledHeight / GlobalContent.height;

            if (leftPage.getActivities() == null)
                return;

            for (Interactivity interactivity : leftPage.getActivities()) {
                interactivity = new Interactivity(interactivity);
                isShownInteractivites = true;

                if (!interactivity.scaled) {
                    interactivity.x = ((int) (interactivity.x * scaleX));
                    interactivity.y = ((int) (interactivity.y * scaleY));
                    interactivity.width = ((int) (interactivity.width * scaleX));
                    interactivity.height = ((int) (interactivity.height * scaleY));
                    interactivity.scaled = true;
                }

                if (interactivity.type == 4 && interactivity.items.size() > 0) {
                    InteractivityItem item = interactivity.items.get(0);
                    if (new File(item.source).exists()) {
                        AudioInteractivityView aiv = new AudioInteractivityView(getContext(), interactivity, content, leftPage.imageName);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(interactivity.width, interactivity.height);
                        layoutParams.setMargins(interactivity.x, interactivity.y, 0, 0);
                        aiv.setLayoutParams(layoutParams);
                        leftLayout.addView(aiv);
                    }
                } else {
                    liv = new InteractivityView(getContext(), interactivity, content, leftPage.imageName);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(interactivity.width, interactivity.height);
                    params.setMargins(interactivity.x, interactivity.y, 0, 0);
                    liv.setLayoutParams(params);
                    leftLayout.addView(liv);
                }
            }
        }

        public void displayRightImage(Bitmap bmp) {
            try {
                if (bmp == null) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(GlobalContent.screenWidth / 2, GlobalContent.screenHeight);
                    rightLayout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    rightImageView.setLayoutParams(layoutParams);
                    rightLayout.addView(rightImageView);
                    mainLayout.addView(rightLayout);
                } else {
                    bitmapOptions = new BitmapFactory.Options();
                    bitmapOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(rightPath, bitmapOptions);
                    bitmapOptions.inSampleSize = calculateInSampleSize(bitmapOptions, GlobalContent.screenWidth / 2, GlobalContent.screenHeight);
                    bitmapOptions.inJustDecodeBounds = false;
                    originalWidth = bmp.getWidth();
                    originalHeight = bmp.getHeight();
                    xScale = (double) (GlobalContent.screenWidth / 2) / originalWidth;
                    yScale = (double) GlobalContent.screenHeight / originalHeight;

                    if (xScale < yScale) {
                        scale = xScale;
                    } else {
                        scale = yScale;
                    }

                    scaledWidth = Math.min(GlobalContent.screenWidth / 2, (int) (originalWidth * scale));
                    scaledHeight = (int) (originalHeight * scale);

                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(scaledWidth, scaledHeight);
                    rightLayout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                    rightImageView.setLayoutParams(layoutParams);
                    rightImageView.setScaleType(ScaleType.FIT_XY);
                    bmp = BitmapFactory.decodeFile(rightPath, bitmapOptions);
                    rightImageView.setImageBitmap(bmp);
                    rightLayout.addView(rightImageView);
                    mainLayout.addView(rightLayout);
                    displayRightHotspots();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void displayRightHotspots() {
            HotspotView hsv = null;
            float scaleX, scaleY;
            scaleX = (float) scaledWidth / GlobalContent.width;
            scaleY = (float) scaledHeight / GlobalContent.height;

            for (Hotspot hs : rightPage.getHotspots()) {
                hs = new Hotspot(hs);

                if (!hs.scaled) {
                    hs.setX((int) (hs.getX() * scaleX));
                    hs.setY((int) (hs.getY() * scaleY));
                    hs.setWidth((int) (hs.getWidth() * scaleX));
                    hs.setHeight((int) (hs.getHeight() * scaleY));
                    hs.scaled = true;
                }

                hsv = new HotspotView(LandscapePageView.this.getContext(), hs, content.getId());
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) hs.getWidth(), (int) hs.getHeight());
                params.setMargins((int) hs.getX(), (int) hs.getY(), 0, 0);
                hsv.setLayoutParams(params);
                rightLayout.addView(hsv);

            }

            displayRightInteractivities();
        }

        public void displayRightInteractivities() {
            float scaleX, scaleY;
            scaleX = (float) scaledWidth / GlobalContent.width;
            scaleY = (float) scaledHeight / GlobalContent.height;

            if (rightPage.getActivities() == null)
                return;

            for (Interactivity interactivity : rightPage.getActivities()) {
                interactivity = new Interactivity(interactivity);
                isShownInteractivites = true;

                if (!interactivity.scaled) {
                    interactivity.x = ((int) (interactivity.x * scaleX));
                    interactivity.y = ((int) (interactivity.y * scaleY));
                    interactivity.width = ((int) (interactivity.width * scaleX));
                    interactivity.height = ((int) (interactivity.height * scaleY));
                    interactivity.scaled = true;
                }

                if (interactivity.type == 4 && interactivity.items.size() != 0) {
                    InteractivityItem item = interactivity.items.get(0);
                    if (new File(item.source).exists()) {
                        AudioInteractivityView aiv = new AudioInteractivityView(getContext(), interactivity, content, rightPage.imageName);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(interactivity.width, interactivity.height);
                        layoutParams.setMargins(interactivity.x, interactivity.y, 0, 0);
                        aiv.setLayoutParams(layoutParams);
                        rightLayout.addView(aiv);
                    }
                } else {
                    riv = new InteractivityView(getContext(), interactivity, content, rightPage.imageName);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(interactivity.width, interactivity.height);
                    params.setMargins(interactivity.x, interactivity.y, 0, 0);
                    riv.setLayoutParams(params);
                    rightLayout.addView(riv);
                }
            }
        }

        public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                // Calculate ratios of height and width to requested height and width
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);

                // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
                // with both dimensions larger than or equal to the requested height and width.
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

                // This offers some additional logic in case the image has a strange
                // aspect ratio. For example, a panorama may have a much larger
                // width than height. In these cases the total pixels might still
                // end up being too large to fit comfortably in memory, so we should
                // be more aggressive with sample down the image (=larger inSampleSize).

                final float totalPixels = width * height;

                // Anything more than 2x the requested pixels we'll sample down further
                final float totalReqPixelsCap = reqWidth * reqHeight * 2;

                while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                    inSampleSize++;
                }
            }

            return inSampleSize;
        }
    }

    public void triggerVideo() {
        if (!isZoomed) {

            boolean hasTriggeredVideo = false;

            for (int i = 0; i < leftLayout.getChildCount(); i++) {

                if (leftLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(InteractivityView.class.getName())) {

                    InteractivityView iv = (InteractivityView) leftLayout.getChildAt(i);

                    try {
                        if (iv.interactivity.items.get(0).vSource.length() == 0 && iv.interactivity.type != 5) {
                            iv.playVideo();
                            hasTriggeredVideo = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                }
            }

            if (!hasTriggeredVideo) {

                for (int i = 0; i < rightLayout.getChildCount(); i++) {

                    if (rightLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(InteractivityView.class.getName())) {

                        InteractivityView iv = (InteractivityView) rightLayout.getChildAt(i);

                        try {
                            if (iv.interactivity.items.get(0).vSource.length() == 0 && iv.interactivity.type != 5) {
                                iv.playVideo();
                                hasTriggeredVideo = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;
                    }
                }
            }
        }
    }

    public void hideHotspotsBackground() {
        if (leftLayout != null) {
            for (int i = 0; i < leftLayout.getChildCount(); i++) {
                if (leftLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(HotspotView.class.getName())) {
                    HotspotView hotspotView = (HotspotView) leftLayout.getChildAt(i);
                    hotspotView.setVisibility(View.GONE);
                }
            }
        }

        if (rightLayout != null) {
            for (int i = 0; i < rightLayout.getChildCount(); i++) {
                if (rightLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(HotspotView.class.getName())) {
                    HotspotView hotspotView = (HotspotView) rightLayout.getChildAt(i);
                    hotspotView.setVisibility(View.GONE);
                }
            }
        }
    }

    public void showHotspotsBackground() {
        if (leftLayout != null) {
            for (int i = 0; i < leftLayout.getChildCount(); i++) {
                if (leftLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(HotspotView.class.getName())) {
                    HotspotView hotspotView = (HotspotView) leftLayout.getChildAt(i);
                    hotspotView.setVisibility(View.VISIBLE);
                }
            }
        }

        if (rightLayout != null) {
            for (int i = 0; i < rightLayout.getChildCount(); i++) {
                if (rightLayout.getChildAt(i).getClass().getName().equalsIgnoreCase(HotspotView.class.getName())) {
                    HotspotView hotspotView = (HotspotView) rightLayout.getChildAt(i);
                    hotspotView.setVisibility(View.VISIBLE);
                }
            }
        }

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                hideHotspotsBackground();
            }
        }, 3000);
    }
}
