package com.mirabel.magazinecentral.adapters.viewissue;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.fragments.viewissue.LandscapeViewIssueFragment;
import com.mirabel.magazinecentral.fragments.viewissue.ViewIssueFragment;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

import java.util.ArrayList;
import java.util.List;

public class ViewIssueAdapter extends FragmentStatePagerAdapter {
    Context context;
    List<Page> pages;
    Content content = null;
    ViewPagerInterface viewPagerInterface = null;
    public Constants.OrientationType orientation = Constants.OrientationType.portrait;

    public ViewIssueAdapter(FragmentManager fragmentManager, Context context, List<Page> pages, Content content) {
        super(fragmentManager);

        this.pages = new ArrayList<>(pages);
        this.context = context;
        this.content = content;
        this.viewPagerInterface = (ViewPagerInterface) context;
    }

    @Override
    public Fragment getItem(int arg0) {
        if (orientation == Constants.OrientationType.portrait) {
            Page p = new Page(this.pages.get(arg0));
            Fragment f = ViewIssueFragment.newInstance(context, this.content, p, viewPagerInterface);
            return f;
        } else {
            if (pages.size() % 2 == 0) { // if total no. of pages are even
                if (arg0 == 0) {
                    Page rp = new Page(this.pages.get(arg0));
                    Fragment f = LandscapeViewIssueFragment.newInstance(context, this.content, null, rp, viewPagerInterface);
                    return f;
                } else if (arg0 == pages.size() / 2) {
                    Page lp = new Page(pages.get(arg0 * 2 - 1));
                    Fragment f = LandscapeViewIssueFragment.newInstance(context, this.content, lp, null, viewPagerInterface);
                    return f;
                } else {
                    Page lp = new Page(pages.get(arg0 * 2 - 1));
                    Page rp = new Page(pages.get(arg0 * 2));
                    Fragment f = LandscapeViewIssueFragment.newInstance(context, this.content, lp, rp, viewPagerInterface);
                    return f;
                }
            } else { // if total no. of pages are odd
                if (arg0 == 0) {
                    Page rp = new Page(this.pages.get(arg0));
                    Fragment f = LandscapeViewIssueFragment.newInstance(context, this.content, null, rp, viewPagerInterface);
                    return f;
                } else {
                    Page lp = new Page(pages.get(arg0 * 2 - 1));
                    Page rp = new Page(pages.get(arg0 * 2));
                    Fragment f = LandscapeViewIssueFragment.newInstance(context, this.content, lp, rp, viewPagerInterface);
                    return f;
                }
            }
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (orientation == Constants.OrientationType.portrait) {
            return pages.size();
        } else {
            return (pages.size() / 2) + 1;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        super.destroyItem(container, position, object);
    }


    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub
        return POSITION_NONE;
    }
}