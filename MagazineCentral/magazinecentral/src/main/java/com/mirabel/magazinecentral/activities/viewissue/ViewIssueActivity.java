package com.mirabel.magazinecentral.activities.viewissue;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.FileProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MainActivity;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.adapters.viewissue.TOCRecyclerViewAdapter;
import com.mirabel.magazinecentral.adapters.viewissue.ThumbsRecyclerViewAdapter;
import com.mirabel.magazinecentral.adapters.viewissue.ViewIssueAdapter;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Bookmark;
import com.mirabel.magazinecentral.beans.viewissue.Hotspot;
import com.mirabel.magazinecentral.beans.viewissue.IndexItem;
import com.mirabel.magazinecentral.beans.viewissue.Interactivity;
import com.mirabel.magazinecentral.beans.viewissue.InteractivityItem;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCEditText;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.customviews.viewissue.AudioInteractivityView;
import com.mirabel.magazinecentral.customviews.viewissue.BookmarkView;
import com.mirabel.magazinecentral.customviews.viewissue.ClipShare;
import com.mirabel.magazinecentral.customviews.viewissue.CustomViewPager;
import com.mirabel.magazinecentral.database.DBHelper;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;
import com.mirabel.magazinecentral.interfaces.ClipShareListener;
import com.mirabel.magazinecentral.interfaces.ViewIssueEventListener;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;
import com.mirabel.magazinecentral.util.NetworkManager;
import com.mirabel.magazinecentral.util.Utility;

import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import me.piruin.quickaction.ActionItem;
import me.piruin.quickaction.QuickAction;

import static com.mirabel.magazinecentral.activities.MyApplication.getAppContext;
import static com.mirabel.magazinecentral.constants.Constants.SocialType.pinterest;

public class ViewIssueActivity extends FragmentActivity implements ViewPagerInterface, ViewIssueEventListener, ClipShareListener, AlertDialogSelectionListener {
    public static final String TAG = ViewIssueActivity.class.getSimpleName();

    private Context applicationContext, activityContext;
    FrameLayout topToolBar;
    MCTextView issueName;
    LinearLayout bookmarkBar = null;
    ProgressBar progressBar = null;

    private GlobalContent globalContent = null;
    private Utility utility = null;
    private DBHelper dbHelper;
    private NetworkManager networkManager;

    CustomViewPager viewIssuePager;
    RecyclerView thumbsRecyclerView;
    LinearLayoutManager thumbsLayoutManager;
    ViewIssueAdapter viewIssueAdapter;
    ViewIssueEventListener listener;
    ThumbsRecyclerViewAdapter thumbsRecyclerViewAdapter = null;
    TOCRecyclerViewAdapter tocRecyclerViewAdapter = null;
    QuickAction shareDropDown = null, moreDropDown = null;
    ClipShare clipView = null;
    Content content = null;
    ArrayList<Page> pages = new ArrayList<>();
    ArrayList<Page> _multimediaPages = new ArrayList<>();
    ArrayList<String> _multimediaPageIndexes = new ArrayList<>();
    ArrayList<IndexItem> contents = null;
    ArrayList<Integer> tocPageIndexes;
    ArrayList<String> fileNames = null, pageNames = null;
    public static String issueFolderPath;
    public static MediaPlayer mediaPlayer = null;
    public static Constants.SocialType socialType = null;
    Constants.ShareType shareType = null;
    String imageType = null;
    String orientationString;
    boolean hasContentIndex = false;
    boolean isPagesSelected = false;
    boolean isAnimating = false;
    public static int selectedPageIndex = 0;
    boolean hasStartedClip = false;
    Constants.OrientationType orientation = null;
    int pageType = 1;// 1 = right, 0= neutral, -1 =left;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_issue);

        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                content = (Content) bundle.getSerializable(Constants.BUNDLE_CONTENT);
            }

            if (content == null) {
                onBackPressed();
                return;
            }

            applicationContext = getApplicationContext();
            activityContext = ViewIssueActivity.this;
            handler = new Handler();

            utility = Utility.getInstance();
            globalContent = GlobalContent.getInstance();
            dbHelper = new DBHelper(activityContext);
            networkManager = new NetworkManager();

            topToolBar = (FrameLayout) findViewById(R.id.topToolBar);
            issueName = (MCTextView) findViewById(R.id.issueName);
            bookmarkBar = (LinearLayout) findViewById(R.id.bookmarksBar);
            viewIssuePager = (CustomViewPager) findViewById(R.id.viewIssuePager);
            thumbsRecyclerView = (RecyclerView) findViewById(R.id.thumbs_recycler_view);
            progressBar = (ProgressBar) findViewById(R.id.progressBar);

            issueName.setText(content.getName());

            thumbsLayoutManager = new LinearLayoutManager(this);
            thumbsLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            thumbsRecyclerView.setLayoutManager(thumbsLayoutManager);

            thumbsRecyclerView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                orientation = Constants.OrientationType.landscape;
                orientationString = "l";
                GlobalContent.orientation = Constants.OrientationType.landscape;
            } else {
                orientation = Constants.OrientationType.portrait;
                orientationString = "p";
                GlobalContent.orientation = Constants.OrientationType.portrait;
            }

            issueFolderPath = GlobalContent.issueFolderPath + content.getId();

            // updating Issue current state
            content.setContentState(globalContent.getCurrentStateOfMagazine(content));

            if (content.getPinItUrl().isEmpty())
                content.setPinItUrl(Constants.WEB_URL);

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            GlobalContent.screenWidth = size.x;
            GlobalContent.screenHeight = size.y;

            listener = this;
            parseIssueXML();

            if (content.getContentState() == Constants.ContentState.ContentStateDownloading) {
                // Registering Local Broadcast Manager
                LocalBroadcastManager.getInstance(activityContext).registerReceiver(issueDownloadingBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
                LocalBroadcastManager.getInstance(activityContext).registerReceiver(issueDownloadingBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            }

            registerReceiver(playVideoBroadcastReceiver, new IntentFilter("PlayExternalVideo"));
            registerReceiver(playVideoBroadcastReceiver, new IntentFilter("PlayYoutubeVideo"));
            registerReceiver(playVideoBroadcastReceiver, new IntentFilter("PlayModalVideo"));
            registerReceiver(playVideoBroadcastReceiver, new IntentFilter("PlayVideoInWebView"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(issueDownloadingBroadcastReceiver);

        unregisterReceiver(playVideoBroadcastReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
                AudioInteractivityView.isPlaying = false;
            }

            if (MCProgressDialog.isProgressDialogShown)
                MCProgressDialog.hideProgressDialog();

            if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
                MCAlertDialog.alertDialog.dismiss();
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (hasStartedClip) {
            dismissClipShare();
        }

        updateCurrentPageInLocalDB();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            AudioInteractivityView.isPlaying = false;
        }

        updateCurrentPageInLocalDB();

        Intent showHomePageIntent = new Intent(activityContext, MainActivity.class);
        showHomePageIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(showHomePageIntent);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void updateCurrentPageInLocalDB() {
        int currentPage = getCurrentPage();
        dbHelper.updateStartPageIndex(content.getId(), currentPage);
    }

    public void gotoCoverPage(View v) {
        updatePageView(0, false);
    }

    public void onVolleyErrorResponse(VolleyError error) {
        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), getResources().getString(R.string.error_failure));
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver issueDownloadingBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            try {
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                    if (viewIssuePager == null) {
                        return;
                    }

                    if (!contentId.equalsIgnoreCase(content.getId())) {
                        return;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!intent.hasExtra("fileName")) {
                                return;
                            }

                            String fileName = intent.getStringExtra("fileName");

                            if (fileName.contains("_t")) {

                                ViewIssueActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (thumbsRecyclerView.isShown()) {
                                            if (hasContentIndex && !isPagesSelected && tocRecyclerViewAdapter != null) {
                                                tocRecyclerViewAdapter.notifyDataSetChanged();
                                            } else {
                                                if (thumbsRecyclerViewAdapter != null) {
                                                    thumbsRecyclerViewAdapter.notifyDataSetChanged();
                                                } else {

                                                }
                                            }
                                        }
                                    }
                                });

                            } else {

                                int currentItem = getCurrentPage();

                                if (currentItem > 1) {
                                    for (int i = currentItem - 1; i < currentItem + 2; i++) {
                                        Page p = pages.get(i);
                                        if (fileName.equalsIgnoreCase(p.imageName + "." + p.imageType)) {
                                            viewIssueAdapter.notifyDataSetChanged();
                                            break;
                                        }
                                    }
                                } else {
                                    for (int i = 0; i <= 1; i++) {
                                        Page p = pages.get(i);
                                        if (fileName.equalsIgnoreCase(p.imageName + "." + p.imageType)) {
                                            viewIssueAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            }
                        }
                    });

                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                    if (content.getId().equalsIgnoreCase(contentId)) {
                        viewIssueAdapter.notifyDataSetChanged();
                        content.setContentState(Constants.ContentState.ContentStateDownloaded);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    BroadcastReceiver playVideoBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String url = intent.getExtras().getString("source");

            String pageName = pages.get(getCurrentPage()).imageName;
            String label;
            String title = null;

            if (GlobalContent.orientation == Constants.OrientationType.portrait) {
                label = String.format("/%s/%s/%s/android/%d/p/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), pageName);
            } else {
                label = String.format("/%s/%s/%s/android/%d/l/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), pageName);
            }

            ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", GlobalContent.getInstance().primaryEmail);

            if (intent.getAction().equalsIgnoreCase("PlayExternalVideo")) {
                Intent playVideo = new Intent(ViewIssueActivity.this, VideoPlayerActivity.class);
                playVideo.putExtra("videoUrl", url);
                ViewIssueActivity.this.startActivity(playVideo);
                title = String.format(url);
                ((MyApplication) getApplication()).trackEvent("Android-Multimedia", title, label);
            } else if (intent.getAction().equalsIgnoreCase("PlayYoutubeVideo")) {
                String manufacturer = android.os.Build.MANUFACTURER;
                if (manufacturer.equalsIgnoreCase("Amazon")) {
                    Intent playVideoInWebViewIntent = new Intent(ViewIssueActivity.this, PlayVideoInWebViewActivity.class);
                    /*String videoUrl = url;
                    if(!videoUrl.startsWith("http"))
						videoUrl = "http:" + videoUrl; */

                    int index = url.indexOf("youtube");
                    String videoUrl = "https://www." + url.substring(index, url.length()) + "?autoplay=1&rel=0&showinfo=0"; // To play automatically when web view shown

                    playVideoInWebViewIntent.putExtra("videoUrl", videoUrl);
                    ViewIssueActivity.this.startActivity(playVideoInWebViewIntent);
                    title = String.format(url);
                    ((MyApplication) getApplication()).trackEvent("Android-Multimedia", title, label);
                } else {
                    Intent playVideo = new Intent(ViewIssueActivity.this, YoutubeActivity.class);
                    playVideo.putExtra("videoUrl", url);
                    ViewIssueActivity.this.startActivity(playVideo);
                    title = String.format(url);
                    ((MyApplication) getApplication()).trackEvent("Android-Multimedia", title, label);
                }

            } else if (intent.getAction().equalsIgnoreCase("PlayVideoInWebView")) {
                Intent playVideoInWebViewIntent = new Intent(ViewIssueActivity.this, PlayVideoInWebViewActivity.class);
                String videoUrl = url;
                if (!videoUrl.startsWith("http"))
                    videoUrl = "http:" + videoUrl;
                playVideoInWebViewIntent.putExtra("videoUrl", videoUrl);
                ViewIssueActivity.this.startActivity(playVideoInWebViewIntent);
                title = String.format(url);
                ((MyApplication) getApplication()).trackEvent("Android-Multimedia", title, label);
            } else {
                title = String.format("Video on Page : %s", pageName);
                ((MyApplication) getApplication()).trackEvent("Android-Multimedia", title, label);
                Intent playModalVideo = new Intent(ViewIssueActivity.this, VideoPlayerActivity.class);
                playModalVideo.putExtra("videoUrl", url);
                startActivity(playModalVideo);
            }
        }
    };

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public void parseIssueXML() {
        try {
            File f = new File(issueFolderPath + "/issue.xml");
            FileInputStream fis = new FileInputStream(f);

            ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
            byte[] b1 = new byte[1024 * 8];
            int read1;
            while ((read1 = fis.read(b1)) != -1) {
                bos1.write(b1, 0, read1);
            }

            byte[] encBytes1 = bos1.toByteArray();
            byte[] decBytes1 = decrypt(Constants.ENCRYPTION_KEY.getBytes(), encBytes1);
            bos1.flush();
            bos1.close();
            fis.close();

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser parser = spf.newSAXParser();
            XMLReader xr = parser.getXMLReader();
            IssueXMLParserHandler handler = new IssueXMLParserHandler();
            xr.setContentHandler(handler);
            xr.parse(new InputSource(new ByteArrayInputStream(decBytes1)));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class IssueXMLParserHandler extends DefaultHandler {
        ArrayList<String> pageNames = new ArrayList<>();
        ArrayList<String> fileNames = new ArrayList<>();
        ArrayList<Hotspot> _hotSpots = null;
        ArrayList<Page> _pages = new ArrayList<>();
        Page _currentPage = null;
        String currentParsingElement = null;
        ArrayList<IndexItem> contents = new ArrayList<>();
        Interactivity interactivity = null;
        InteractivityItem interactivityItem = null;
        ArrayList<InteractivityItem> items = null;
        ArrayList<Page> _multimediaPages = new ArrayList<>();
        ArrayList<String> _currentIssueMultimediaPageIndexes = new ArrayList<>();

        int index = 0;
        private ArrayList<Interactivity> activities;

        @Override
        public void startDocument() throws SAXException {
            super.startDocument();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);

            try {
                if (localName.equalsIgnoreCase("issue")) {
                    GlobalContent.width = Integer.parseInt(attributes.getValue("width"));
                    GlobalContent.height = Integer.parseInt(attributes.getValue("height"));
                } else if (localName.equalsIgnoreCase("page")) {
                    _currentPage = new Page();
                    _hotSpots = new ArrayList<>();
                    String imageName = attributes.getValue("name");
                    String imageType = attributes.getValue("type");
                    String text = attributes.getValue("text");
                    _currentPage.setImageName(imageName);
                    _currentPage.setImageType(imageType);
                    _currentPage.setText(text);
                    fileNames.add(imageName + "_t." + imageType);
                    pageNames.add(text.isEmpty() ? imageName : text);
                } else if (localName.equalsIgnoreCase("hotspot")) {
                    Hotspot _hotSpot = new Hotspot();
                    _hotSpot.x = Float.parseFloat(attributes.getValue("x"));
                    _hotSpot.y = Float.parseFloat(attributes.getValue("y"));
                    _hotSpot.width = Float.parseFloat(attributes.getValue("width"));
                    _hotSpot.height = Float.parseFloat(attributes.getValue("height"));
                    _hotSpot.type = Integer.parseInt(attributes.getValue("type"));
                    _hotSpot.command = attributes.getValue("command");
                    _hotSpot.toolTip = attributes.getValue("tooltip");
                    _hotSpot.borderWidth = Float.parseFloat(attributes.getValue("borderWidth"));
                    _hotSpot.borderColor = Color.parseColor(attributes.getValue("borderColor").replaceFirst("0x", "#"));
                    _hotSpot.bgColor = Color.parseColor(attributes.getValue("backgroundColor").replaceFirst("0x", "#"));

                    if (attributes.getValue("border") != null)
                        _hotSpot.bordered = Boolean.parseBoolean(attributes.getValue("border"));
                    else
                        _hotSpot.bordered = true;

                    if (_hotSpot.borderWidth == 0)
                        _hotSpot.bordered = false;

                    _hotSpots.add(_hotSpot);
                } else if (localName.equalsIgnoreCase("hotspots")) {
                    _hotSpots = new ArrayList<>();
                } else if (localName.equals("activities")) {
                    items = new ArrayList<>();
                    activities = new ArrayList<>();
                } else if (localName.equals("activity")) {
                    currentParsingElement = localName;
                    interactivity = new Interactivity();
                    interactivity.x = (int) Float.parseFloat(attributes.getValue("x"));
                    interactivity.x = (int) Float.parseFloat(attributes.getValue("x"));
                    interactivity.y = (int) Float.parseFloat(attributes.getValue("y"));
                    interactivity.width = (int) Float.parseFloat(attributes.getValue("width"));
                    interactivity.height = (int) Float.parseFloat(attributes.getValue("height"));
                    interactivity.title = (attributes.getValue("title") != null) ? attributes.getValue("title") : "";
                    interactivity.type = Integer.parseInt(attributes.getValue("type"));
                    interactivity.description = attributes.getValue("description");
                    interactivity.scaled = false;
                } else if (localName.equalsIgnoreCase("contents")) {
                    currentParsingElement = localName;
                } else if (localName.equals("item") && currentParsingElement.equals("activity")) {
                    interactivityItem = new InteractivityItem();
                    interactivityItem.text = attributes.getValue("text");
                    interactivityItem.vSource = attributes.getValue("vSource");

                    if (interactivityItem.vSource == null || interactivityItem.vSource.isEmpty()) {
                        interactivityItem.source = String.format("%s/%s", issueFolderPath, attributes.getValue("source"));
                    } else {
                        try {
                            interactivityItem.source = URLDecoder.decode(attributes.getValue("source"), "utf-8");
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                    }
                } else if (localName.equals("item") && currentParsingElement.equals("contents")) {
                    if (tocPageIndexes == null)
                        tocPageIndexes = new ArrayList<>();

                    IndexItem indexItem = new IndexItem();
                    indexItem.imageName = attributes.getValue("pageName");
                    indexItem.pageNumber = Integer.parseInt(attributes.getValue("pagenumber"));

                    byte[] bytes = Base64.decode(attributes.getValue("text"), Base64.DEFAULT);

                    try {
                        indexItem.text = new String(bytes, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    tocPageIndexes.add(indexItem.pageNumber);
                    contents.add(indexItem);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            super.characters(ch, start, length);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            try {
                if (localName.equals("page")) {
                    _currentPage.setHotspots(_hotSpots);
                    _currentPage.index = index;
                    _pages.add(_currentPage);
                    _currentPage = null;
                    index++;
                } else if (localName.equals("item") && currentParsingElement.equals("activity")) {
                    if (interactivityItem.vSource == null || interactivityItem.vSource.isEmpty()) {
                        //if (new File(interactivityItem.source).exists()) // Commented this line bcoz in order to add to list even though it is not existing on device & later it will download.
                        items.add(interactivityItem);
                    } else {
                        items.add(interactivityItem);
                    }
                } else if (localName.equals("activity")) {
                    if (interactivity.items == null) {
                        interactivity.items = new ArrayList<>();
                    }

                    interactivity.items.addAll(items);
                    items.clear();
                    activities.add(interactivity);
                    _currentPage.index = index;

                    if (interactivity.items.size() > 0) { // We are adding only activities which are having multimedia.
                        _multimediaPages.add(_currentPage);

                        if (interactivity.type != 4 && !_currentIssueMultimediaPageIndexes.contains(_currentPage.imageName)) {
                            _currentIssueMultimediaPageIndexes.add(_currentPage.imageName);
                        }
                    }
                } else if (localName.equals("activities")) {
                    _currentPage.setActivities(activities);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            super.endElement(uri, localName, qName);
        }

        @Override
        public void endDocument() throws SAXException {
            super.endDocument();

            ViewIssueActivity.this.onFinishedParsing(_pages, fileNames, pageNames, contents, _multimediaPages, _currentIssueMultimediaPageIndexes);
        }
    }

    public void onFinishedParsing(ArrayList<Page> pages, ArrayList<String> fileNames, ArrayList<String> pageNames, ArrayList<IndexItem> contents, ArrayList<Page> _multimediaPages, ArrayList<String> _currentIssueMultimediaPageIndexes) {
        try {
            this.contents = contents;
            this.pages.addAll(pages);
            this.imageType = pages.get(0).imageType;
            this._multimediaPages = _multimediaPages;
            this._multimediaPageIndexes = _currentIssueMultimediaPageIndexes;
            this.fileNames = fileNames;
            this.pageNames = pageNames;
            content.setBookmarks(dbHelper.getAllBookmarks(content.getId()));

            if (contents != null && contents.size() > 0) {
                hasContentIndex = true;
            }

            if (mediaPlayer != null) {
                mediaPlayer = null;
            }

            viewIssueAdapter = new ViewIssueAdapter(getSupportFragmentManager(), this, this.pages, content);
            viewIssueAdapter.orientation = orientation;
            viewIssuePager.setAdapter(viewIssueAdapter);
            viewIssuePager.requestDisallowInterceptTouchEvent(true);

            if (content.getStartPageIndex() < 0) {
                int startPageIndex = dbHelper.getStatePageIndex(content.getId());

                if (startPageIndex < 0) {
                    startPageIndex = 0;
                    content.setStartPageIndex(0);
                }

                updatePageView(startPageIndex, false);
                selectedPageIndex = startPageIndex;
            } else {
                selectedPageIndex = Math.min(content.getStartPageIndex(), this.pages.size() - 1);
                updatePageView(selectedPageIndex, false);
            }

            if (hasContentIndex) {
                tocRecyclerViewAdapter = new TOCRecyclerViewAdapter(this, content.getId(), contents, _multimediaPageIndexes);
                thumbsRecyclerView.setAdapter(tocRecyclerViewAdapter);
            } else {
                thumbsRecyclerViewAdapter = new ThumbsRecyclerViewAdapter(this, content.getId(), fileNames, pageNames, imageType, orientation, _multimediaPageIndexes);
                thumbsRecyclerView.setAdapter(thumbsRecyclerViewAdapter);

                if (orientation == Constants.OrientationType.portrait)
                    updateThumbView(selectedPageIndex);
                else
                    updateThumbView(selectedPageIndex / 2);
            }

            viewIssuePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(final int position) {

                    new Runnable() {

                        @Override
                        public void run() {
                            selectedPageIndex = getCurrentPage();

                            String primaryEmail = GlobalContent.getInstance().primaryEmail;
                            ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", primaryEmail);

                            if (orientation == Constants.OrientationType.portrait) {
                                Page p = ViewIssueActivity.this.pages.get(position);
                                ((MyApplication) getApplication()).trackPageView(String.format("/%s/%s/%s/android/%d/%s/page%s$%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, p.imageName, p.text.isEmpty() ? ("page" + p.imageName) : p.text));
                            } else {
                                if (selectedPageIndex == 0) {
                                    Page p = ViewIssueActivity.this.pages.get(position * 2);
                                    ((MyApplication) getApplication()).trackPageView(String.format("/%s/%s/%s/android/%d/%s/page%s$%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, p.imageName, p.text.isEmpty() ? ("page" + p.imageName) : p.text));
                                } else if (selectedPageIndex == ViewIssueActivity.this.pages.size() - 1) {
                                    Page p = ViewIssueActivity.this.pages.get(position * 2 - 1);
                                    ((MyApplication) getApplication()).trackPageView(String.format("/%s/%s/%s/android/%d/%s/page%s$%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, p.imageName, p.text.isEmpty() ? ("page" + p.imageName) : p.text));
                                } else {
                                    Page lp = ViewIssueActivity.this.pages.get(position * 2);
                                    Page rp = ViewIssueActivity.this.pages.get(position * 2 + 1);
                                    ((MyApplication) getApplication()).trackPageView(String.format("/%s/%s/%s/android/%d/%s/page%s$%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, lp.imageName, lp.text.isEmpty() ? ("page" + lp.imageName) : lp.text));

                                    ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", primaryEmail);
                                    ((MyApplication) getApplication()).trackPageView(String.format("/%s/%s/%s/android/%d/%s/page%s$%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, rp.imageName, rp.text.isEmpty() ? ("page" + rp.imageName) : rp.text));
                                }
                            }
                        }

                    }.run();
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    new Runnable() {
                        @Override
                        public void run() {
                            if (bookmarkBar.isShown()) {
                                ((HorizontalScrollView) bookmarkBar.getParent()).setVisibility(View.INVISIBLE);
                                bookmarkBar.setVisibility(View.INVISIBLE);
                                hideBookmarks();
                            }
                            if (topToolBar.isShown()) {
                                topToolBar.setVisibility(View.INVISIBLE);
                            }
                            if (thumbsRecyclerView.isShown()) {
                                thumbsRecyclerView.setVisibility(View.INVISIBLE);
                            }
                        }
                    }.run();
                }

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updatePageView(final int pageIndex, boolean isOrientationChanged) {
        try {
            viewIssueAdapter.notifyDataSetChanged();

            viewIssuePager.updateZoomValue(1.0f);
            ViewIssueActivity.selectedPageIndex = pageIndex;

            if (orientation == Constants.OrientationType.portrait) {
                setViewIssuePagerCurrentItem(pageIndex, false);
            } else {
                if (pageIndex / 2 == pages.size() - 1) {
                    setViewIssuePagerCurrentItem(pageIndex / 2 + 1, false);
                } else if (pageIndex == 0) {
                    setViewIssuePagerCurrentItem(0, false);
                } else if (pageIndex % 2 == 0) {
                    setViewIssuePagerCurrentItem(pageIndex / 2, false);
                } else {
                    setViewIssuePagerCurrentItem((pageIndex / 2) + 1, false);
                }
            }

            if (!isOrientationChanged && thumbsRecyclerView.isShown()) {
                if (hasContentIndex && !isPagesSelected && tocRecyclerViewAdapter != null) {
                    thumbsRecyclerView.setAdapter(tocRecyclerViewAdapter);
                    tocRecyclerViewAdapter.refreshTOCs();

                    int currentTOCPNIndex;
                    if (orientation == Constants.OrientationType.portrait)
                        currentTOCPNIndex = tocPageIndexes.indexOf(new Integer(pageIndex + 1));
                    else
                        currentTOCPNIndex = tocPageIndexes.indexOf(new Integer(pageIndex));

                    if (currentTOCPNIndex >= 0)
                        updateThumbView(currentTOCPNIndex);

                } else if (thumbsRecyclerViewAdapter != null) {
                    thumbsRecyclerViewAdapter.notifyDataSetChanged();

                    if (orientation == Constants.OrientationType.portrait)
                        updateThumbView(selectedPageIndex);
                    else
                        updateThumbView(selectedPageIndex / 2);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setViewIssuePagerCurrentItem(final int pageIndex, final boolean flag) {
        try {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    viewIssuePager.setCurrentItem(pageIndex, flag);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateThumbView(final int thumbIndex) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                thumbsRecyclerView.smoothScrollToPosition(thumbIndex);
            }
        }, 200);
    }

    public int getCurrentPage() {
        try {
            if (orientation == Constants.OrientationType.portrait) {
                return viewIssuePager.getCurrentItem();
            } else {
                if (viewIssuePager.getCurrentItem() != pages.size() / 2) {
                    return viewIssuePager.getCurrentItem() * 2;
                } else {
                    return pages.size() - 1;
                }
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public void hideBookmarks() {
        bookmarkBar.removeAllViews();
    }

    public void showOrHideBookmarks(View v) {
        if (!bookmarkBar.isShown()) {
            showBookmarks();
        }
    }

    public void showBookmarks() {
        try {
            BookmarkView bv = null;
            for (Bookmark bookmark : content.getBookmarks()) {
                bv = new BookmarkView(this, bookmark, 0);
                bv.deleteBookmark.setOnClickListener(deleteBookmarkListener);
                bv.bookmarkImage.setOnClickListener(bookmarkSelectListener);
                bookmarkBar.addView(bv);
            }

            FrameLayout addBookmarkLayout = new FrameLayout(this);
            int width = getResources().getInteger(R.integer.bookmark_width);
            int height = getResources().getInteger(R.integer.bookmark_height);
            final float scale = getResources().getDisplayMetrics().density;
            ImageView imgView = new ImageView(this);
            FrameLayout.LayoutParams imageParams = new FrameLayout.LayoutParams((int) (width * scale + 0.5f), (int) (height * scale + 0.5f));
            imageParams.setMargins(getResources().getInteger(R.integer.b_left_margin), getResources().getInteger(R.integer.b_top_margin), 0, 0);
            imgView.setLayoutParams(imageParams);
            imgView.setImageResource(R.drawable.add_bookmark);
            imgView.setScaleType(ImageView.ScaleType.FIT_XY);
            MCTextView tv = new MCTextView(this);
            tv.setGravity(Gravity.CENTER);
            tv.setText("Add \nBookmark");
            tv.setTextColor(Color.WHITE);
            tv.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
            tv.setLines(2);
            FrameLayout.LayoutParams tvLayout = (FrameLayout.LayoutParams) tv.getLayoutParams();
            tvLayout.gravity = Gravity.CENTER;
            tvLayout.setMargins(getResources().getInteger(R.integer.b_left_margin), 20, 20, 0);
            tv.setLayoutParams(tvLayout);
            addBookmarkLayout.addView(imgView);
            addBookmarkLayout.addView(tv);
            addBookmarkLayout.setOnClickListener(addBookmarListener);
            bookmarkBar.addView(addBookmarkLayout);
            bookmarkBar.setVisibility(View.VISIBLE);
            ((HorizontalScrollView) bookmarkBar.getParent()).setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener addBookmarListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            int currentPageIndex = getCurrentPage();
            boolean hasBookmark = false;

            for (Bookmark b : content.getBookmarks()) {
                if (b.pageIndex == currentPageIndex) {
                    hasBookmark = true;
                    break;
                }
            }

            if (!hasBookmark) {
                Bookmark b = new Bookmark();
                b.contentId = content.getId();
                b.pageIndex = getCurrentPage();
                Page page = pages.get(currentPageIndex);
                b.pageName = page.imageName + "_t." + page.imageType;
                content.getBookmarks().add(b);

                dbHelper.addBookmark(b);

                BookmarkView bv = new BookmarkView(ViewIssueActivity.this, b, 2);
                bv.deleteBookmark.setOnClickListener(deleteBookmarkListener);
                bv.bookmarkImage.setOnClickListener(bookmarkSelectListener);
                bookmarkBar.addView(bv, 0);

                String eventLabel = String.format("/%s/%s/%s/android/%d/%s/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, page.imageName);
                String eventAction = String.format("%s--%s--%s--Page No : %s ", content.getPublisherName(), content.getProductName(), content.getName(), page.imageName);
                ((MyApplication) getApplication()).trackEvent("Android-Bookmark", eventAction, eventLabel);
            }
        }
    };

    View.OnClickListener deleteBookmarkListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            BookmarkView bv = (BookmarkView) v.getParent().getParent();
            dbHelper.deleteBookmark(bv.bookmark);
            content.getBookmarks().remove(bv.bookmark);
            bookmarkBar.removeView(bv);
        }
    };

    View.OnClickListener bookmarkSelectListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (bookmarkBar.isShown()) {
                ((HorizontalScrollView) bookmarkBar.getParent()).setVisibility(View.INVISIBLE);
                bookmarkBar.setVisibility(View.INVISIBLE);
                hideBookmarks();
            }

            if (topToolBar.isShown()) {
                topToolBar.setVisibility(View.INVISIBLE);
            }

            if (thumbsRecyclerView.isShown()) {
                thumbsRecyclerView.setVisibility(View.INVISIBLE);
            }

            BookmarkView bv = (BookmarkView) v.getParent().getParent();
            updatePageView(bv.bookmark.pageIndex, false);
        }
    };

    Animation.AnimationListener animationListener = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
            isAnimating = true;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            isAnimating = false;

            if (shareDropDown != null) {
                shareDropDown.dismiss();
            }

            if (moreDropDown != null) {
                moreDropDown.dismiss();
            }
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (hasStartedClip) {
            dismissClipShare();
            hasStartedClip = true;
        }

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        GlobalContent.screenWidth = size.x;
        GlobalContent.screenHeight = size.y;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            viewIssueAdapter.orientation = Constants.OrientationType.landscape;
            orientation = Constants.OrientationType.landscape;
            orientationString = "l";
            GlobalContent.orientation = Constants.OrientationType.landscape;
        } else {
            viewIssueAdapter.orientation = Constants.OrientationType.portrait;
            orientation = Constants.OrientationType.portrait;
            orientationString = "p";
            GlobalContent.orientation = Constants.OrientationType.portrait;
        }

        updatePageView(selectedPageIndex, true);

        if (thumbsRecyclerViewAdapter != null) {
            thumbsRecyclerViewAdapter.setOrientation(orientation);
        }

        if (thumbsRecyclerView.isShown()) {
            if (hasContentIndex && !isPagesSelected && tocRecyclerViewAdapter != null) {
                thumbsRecyclerView.setAdapter(tocRecyclerViewAdapter);
                tocRecyclerViewAdapter.refreshTOCs();

                int currentTOCPNIndex;
                if (orientation == Constants.OrientationType.portrait)
                    currentTOCPNIndex = tocPageIndexes.indexOf(new Integer(selectedPageIndex + 1));
                else
                    currentTOCPNIndex = tocPageIndexes.indexOf(new Integer(selectedPageIndex + 1));

                if (currentTOCPNIndex >= 0)
                    updateThumbView(currentTOCPNIndex);

            } else if (thumbsRecyclerViewAdapter != null) {
                thumbsRecyclerViewAdapter.notifyDataSetChanged();

                if (orientation == Constants.OrientationType.portrait)
                    updateThumbView(selectedPageIndex);
                else
                    updateThumbView(selectedPageIndex / 2);
            }
        }

        if (hasStartedClip) {
            hasStartedClip = false;
            enablePinch(null);
        }
    }

    @Override
    public void enable() {
        viewIssuePager.enable();
    }

    @Override
    public void disable() {
        viewIssuePager.disable();
    }

    @Override
    public void onSingleTapConfirmed() {
        try {
            if (isAnimating) {
                return;
            }

            TranslateAnimation thumbsAnimation = null;
            TranslateAnimation topToolBarAnimation = null;

            if (thumbsRecyclerView.isShown()) {
                thumbsAnimation = new TranslateAnimation(0, 0, 0, thumbsRecyclerView.getHeight());
                thumbsAnimation.setDuration(200);
                thumbsRecyclerView.startAnimation(thumbsAnimation);
                thumbsAnimation.setAnimationListener(animationListener);
                thumbsRecyclerView.setVisibility(View.INVISIBLE);
            } else {
                // This condition is, if pages is selected, then we will be showing the thumbs bar.
                if (isPagesSelected) {
                    isPagesSelected = false;
                }

                if (hasContentIndex) {
                    int currentTOCPNIndex;
                    if (orientation == Constants.OrientationType.portrait)
                        currentTOCPNIndex = tocPageIndexes.indexOf(new Integer(ViewIssueActivity.selectedPageIndex + 1));
                    else
                        currentTOCPNIndex = tocPageIndexes.indexOf(new Integer(ViewIssueActivity.selectedPageIndex));

                    thumbsRecyclerView.setAdapter(tocRecyclerViewAdapter);
                    tocRecyclerViewAdapter.refreshTOCs();
                    if (currentTOCPNIndex >= 0)
                        updateThumbView(currentTOCPNIndex);
                } else {
                    thumbsRecyclerViewAdapter.notifyDataSetChanged();

                    if (orientation == Constants.OrientationType.portrait)
                        updateThumbView(selectedPageIndex);
                    else
                        updateThumbView(selectedPageIndex / 2);
                }

                thumbsRecyclerView.setVisibility(View.VISIBLE);
                thumbsAnimation = new TranslateAnimation(0, 0, thumbsRecyclerView.getHeight(), 0);
                thumbsAnimation.setDuration(250);
                thumbsRecyclerView.startAnimation(thumbsAnimation);
            }

            if (topToolBar.isShown()) {
                topToolBarAnimation = new TranslateAnimation(0, 0, 0, -topToolBar.getHeight());
                topToolBarAnimation.setDuration(200);
                topToolBarAnimation.setAnimationListener(animationListener);
                topToolBar.startAnimation(topToolBarAnimation);
                topToolBar.setVisibility(View.INVISIBLE);
            } else {
                if (content.getContentState() == Constants.ContentState.ContentStateDownloading) {
                    if (hasContentIndex && !isPagesSelected && tocRecyclerViewAdapter != null) {
                        tocRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        thumbsRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }

                topToolBar.setVisibility(View.VISIBLE);
                topToolBarAnimation = new TranslateAnimation(0, 0, -topToolBar.getHeight(), 0);
                topToolBarAnimation.setDuration(200);
                topToolBar.startAnimation(topToolBarAnimation);
            }

            if (bookmarkBar.isShown()) {
                ((HorizontalScrollView) bookmarkBar.getParent()).setVisibility(View.INVISIBLE);
                bookmarkBar.setVisibility(View.INVISIBLE);
                hideBookmarks();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void thumbTapped(final int tappedIndex) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                updatePageView(tappedIndex, false);
            }
        });
    }

    @Override
    public void updateZoomValue(float zoomValue) {
        viewIssuePager.updateZoomValue(zoomValue);
    }

    @Override
    public void enablePinch(View view) {
        if (!hasStartedClip) {
            if (topToolBar.isShown()) {
                onSingleTapConfirmed();
            }

            hasStartedClip = true;
            final RelativeLayout layout = (RelativeLayout) findViewById(R.id.contentView);
            clipView = new ClipShare(this);
            clipView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            clipView.requestDisallowInterceptTouchEvent(true);
            layout.addView(clipView);
        }
    }

    @Override
    public void hotspotTapped(String url) {
        Page p = pages.get(getCurrentPage());
        String eventLabel = String.format("/%s/%s/%s/android/%d/%s/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, p.imageName);
        Intent i = new Intent(this, WebViewActivity.class);
        i.putExtra("url", url);
        i.putExtra("eventLabel", eventLabel);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
    }

    @Override
    public void jumpToPageIndex(int pageIndex) {
        updatePageView(pageIndex, false);
    }

    @Override
    public void jumpToPage(String pageName) {
        Page p = new Page();
        p.imageName = pageName;

        int pageIndex = pages.indexOf(p);
        pageIndex = -1;

        for (int i = 0; i < pages.size(); i++) {
            Page p1 = pages.get(i);
            if (p1.imageName.equals(pageName)) {
                pageIndex = i;
                break;
            }
        }

        updatePageView(pageIndex, false);
    }

    @Override
    public void mailTo(final Hotspot _data) {
        final Page p = pages.get(getCurrentPage());

        AsyncTask asyncTask = new AsyncTask() {

            String mailTo = "";
            //String responseString = "mailto:youthconnect%40worknetpinellas.org?subject=";

            @Override
            protected Object doInBackground(Object... params) {
                try {
                    String command = _data.command.replace("http://", "");
                    String url = String.format(Constants. K_MAIL_HOTSPOT_URL, command, "0D8938F3-5E12-4D25-95B1-E67186402121", content.getId(), p.imageName);
                    String response = networkManager.makeHttpGetConnection(url);

                    if (!response.equals("failure") && !response.equals("timeout")) {
                        String urlDecoded = URLDecoder.decode(response, "UTF-8");
                        mailTo = urlDecoded.replace("mailto:", "").replace("?subject=", "");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object result) {
                String[] emailIDs = mailTo.split(",|;");

                Page p = pages.get(getCurrentPage());
                String eventLabel = String.format("/%s/%s/%s/android/%d/%s/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, p.imageName);
                ((MyApplication) getApplication()).trackEvent("Android-MailTo", mailTo, eventLabel);

                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, emailIDs);
                startActivity(emailIntent);

                super.onPostExecute(result);
            }
        };

        asyncTask.execute();
    }


    @Override
    public void share(Constants.ShareType shareType) {

    }

    @Override
    public void clipImage(Constants.SocialType socialType) {
        try {
            View view = getWindow().getDecorView();
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache();

            Bitmap b1 = view.getDrawingCache();
            int _x1 = (int) clipView.changeView.getX() + 15;
            int _y1 = (int) clipView.changeView.getY() + 15;
            int _x2 = clipView.changeView.getWidth() - 30;
            int _y2 = clipView.changeView.getHeight() - 30;
            Bitmap b = Bitmap.createBitmap(b1, _x1, _y1, _x2, _y2);

            try {
                FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/clip_image.jpg"));
                b.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
                view.destroyDrawingCache();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (orientation == Constants.OrientationType.landscape) {
                if (_x1 < (GlobalContent.screenWidth / 2)) {
                    if ((_x1 + _x2) > (GlobalContent.screenWidth / 2)) {
                        pageType = 0;
                    } else {
                        pageType = -1;
                    }
                } else {
                    pageType = 1;
                }
            }

            shareType = Constants.ShareType.image;
            this.socialType = socialType;

            switch (socialType) {
                case email:
                    promptName();
                    break;
                default:
                    showPostMessageView();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissClipShare() {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.contentView);
        layout.removeView(clipView);
        hasStartedClip = false;
        clipView = null;
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.animate();

    }

    public void hideProgressBar() {
        progressBar.clearAnimation();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void showThumbnails(View view) {
        if (hasContentIndex && !isPagesSelected) {
            isPagesSelected = true;

            thumbsRecyclerViewAdapter = new ThumbsRecyclerViewAdapter(this, content.getId(), fileNames, pageNames, imageType, orientation, _multimediaPageIndexes);
            thumbsRecyclerView.setAdapter(thumbsRecyclerViewAdapter);
            thumbsRecyclerViewAdapter.notifyDataSetChanged();

            if (orientation == Constants.OrientationType.portrait)
                updateThumbView(selectedPageIndex);
            else
                updateThumbView(selectedPageIndex / 2);
        }
    }

    public void showMultimedia(View v) {
        Intent multimediaIntent = new Intent(ViewIssueActivity.this, MultimediaListActivity.class);
        multimediaIntent.putExtra("contentID", content.getId());
        multimediaIntent.putExtra("content", content);
        MultimediaListActivity._multimediaPages.clear();
        MultimediaListActivity._multimediaPages.addAll(_multimediaPages);
        startActivityForResult(multimediaIntent, 500);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void more(View v) {
        if (moreDropDown != null)
            moreDropDown.dismiss();

        Integer tag = Integer.parseInt(v.getTag().toString());

        ActionItem item0 = new ActionItem(0, "Clip & Share", R.drawable.icon_topbar_crop_active);
        ActionItem item1 = new ActionItem(1, "Multimedia", R.drawable.icon_topbar_videos_active);
        ActionItem item2 = new ActionItem(2, "Bookmarks", R.drawable.icon_topbar_bookmark_active);
        ActionItem item3 = new ActionItem(3, "Pages", R.drawable.icon_topbar_thumbview_active);
        ActionItem item4 = new ActionItem(4, "Cover", R.drawable.icon_topbar_coverpage_active);

        moreDropDown = new QuickAction(this);
        moreDropDown.setEnabledDivider(true);

        if (tag == 1) {
            moreDropDown.addActionItem(item0);
            moreDropDown.addActionItem(item1);
        }

        moreDropDown.addActionItem(item2);
        moreDropDown.addActionItem(item3);
        moreDropDown.addActionItem(item4);

        moreDropDown.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(ActionItem item) {
                switch (item.getActionId()) {
                    case 0:
                        enablePinch(null);
                        break;
                    case 1:
                        showMultimedia(null);
                        break;
                    case 2:
                        showOrHideBookmarks(null);
                        break;
                    case 3:
                        showThumbnails(null);
                        break;
                    case 4:
                        gotoCoverPage(null);
                        break;
                    default:
                        break;
                }
            }
        });

        moreDropDown.show(v);
    }

    public void share(View v) {
        if (shareDropDown != null)
            shareDropDown.dismiss();

        ActionItem item0 = new ActionItem(Constants.SocialType.email.ordinal(), "Email", R.drawable.s_email_icon);
        ActionItem item1 = new ActionItem(Constants.SocialType.facebook.ordinal(), "Facebook", R.drawable.s_facebook_icon);
        ActionItem item2 = new ActionItem(Constants.SocialType.twitter.ordinal(), "Twitter", R.drawable.s_twitter_icon);
        ActionItem item3 = new ActionItem(Constants.SocialType.tumblr.ordinal(), "Tumblr", R.drawable.s_tumblr_icon);
        ActionItem item4 = new ActionItem(Constants.SocialType.linkedin.ordinal(), "Linkedin", R.drawable.s_linkedin_icon);
        ActionItem item5 = new ActionItem(pinterest.ordinal(), "More", R.drawable.s_pinterest_icon);

        shareDropDown = new QuickAction(this);
        shareDropDown.setEnabledDivider(true);

        shareDropDown.addActionItem(item0);
        shareDropDown.addActionItem(item1);
        shareDropDown.addActionItem(item2);
        shareDropDown.addActionItem(item3);
        shareDropDown.addActionItem(item4);
        shareDropDown.addActionItem(item5);

        shareDropDown.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(ActionItem item) {
                socialType = Constants.SocialType.values()[item.getActionId()];
                shareType = Constants.ShareType.link;

                if (socialType == Constants.SocialType.email) {
                    promptName();
                } else {
                    showPostMessageView();
                }
            }
        });

        shareDropDown.show(v);
    }

    @Override
    public void alertDialogCallback() {
        promptName();
    }

    public void promptName() {
        final MCEditText textView = new MCEditText(activityContext);
        textView.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textView.setTextAppearance(ViewIssueActivity.this, android.R.style.TextAppearance_DeviceDefault_Medium);
        textView.setRawInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        android.widget.LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.WRAP_CONTENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        textView.setTextColor(Color.BLACK);
        textView.setHint("Please enter your name");
        textView.setHintTextColor(Color.LTGRAY);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/AvantGardeBook.ttf"));
        textView.setGravity(Gravity.CENTER);

        new AlertDialog.Builder(activityContext)
                .setTitle(" ")
                .setView(textView)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String enteredName = textView.getText().toString().trim();
                        if (enteredName.isEmpty()) {
                            MCAlertDialog.listener = ViewIssueActivity.this;
                            MCAlertDialog.showAlertDialogWithCallback(activityContext, "Invalid Input", "Please enter your name.");
                        } else
                            emailShare(enteredName);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    public void emailShare(String name) {
        Page p = pages.get(getCurrentPage());

        String digitalEditionURL = "";
        if (orientation == Constants.OrientationType.landscape) {
            digitalEditionURL = String.format(Constants.DIGITAL_EDITION_URL, content.getId(), p.imageName, "d");
        } else {
            digitalEditionURL = String.format(Constants.DIGITAL_EDITION_URL, content.getId(), p.imageName, "s");
        }

        //String appStoreURL = getResources().getString(R.string.k_app_store_url);
        String appStoreURL = String.format(Constants.FIND_TARGET_APP_STORE_URL, getResources().getString(R.string.apple), getResources().getString(R.string.google), getResources().getString(R.string.amazon));
        String schemaName = getResources().getString(R.string.urlschema);
        String appName = getResources().getString(R.string.app_name);
        String launchAppURL = "";

        try {
            launchAppURL = String.format(Constants.LAUNCH_APP_URL, content.getId(), content.getPublisherId(), getCurrentPage(), URLEncoder.encode(appStoreURL, "UTF-8"), schemaName, p.imageName);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String contentName = null, subject = null;

        if (shareType == Constants.ShareType.link) {
            contentName = String.format("%s - %s", content.getProductName(), content.getName());
            subject = String.format("%s is forwarding a page from %s", name, content.getProductName());
        } else {
            contentName = String.format("Image clipped from %s - %s", content.getProductName(), content.getName());
            subject = String.format("%s is forwarding a clipped image from %s", name, content.getProductName());
        }

        String htmlFormattedBody = String.format(Constants.HTML_BODY_PORTRAIT, contentName, digitalEditionURL, appName, launchAppURL, appName, appStoreURL);

       // htmlFormattedBody=htmlFormattedBody.replaceAll("\"", "");

        String fileUrl = null;

        if (shareType == Constants.ShareType.link) {
            fileUrl = String.format(GlobalContent.issueFolderPath + "%s/%s.%s", content.getId(), p.imageName, p.imageType);
        } else {
            fileUrl = Environment.getExternalStorageDirectory() + "/clip_image.jpg";
        }

        File file =new File(fileUrl);
        Uri apkURI = FileProvider.getUriForFile(
                ViewIssueActivity.this,
                getApplicationContext()
                        .getPackageName() + ".provider", file);

        Log.e("veera",htmlFormattedBody);
        Log.e("veera",Utility.fromHtml(htmlFormattedBody).toString());

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, Utility.fromHtml(htmlFormattedBody));
        emailIntent.putExtra(Intent.EXTRA_HTML_TEXT, Utility.fromHtml(htmlFormattedBody));
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        emailIntent.putExtra(Intent.EXTRA_STREAM, apkURI);
        trackSocialShareEvent();
        startActivityForResult(Intent.createChooser(emailIntent, "Send email..."), 400);
    }

    public void showPostMessageView() {
        showProgressBar();
        final Page page = pages.get(getCurrentPage());
        final ContentValues contentValues = new ContentValues();

        if (shareType == Constants.ShareType.link) {
            contentValues.put("imagePath", String.format(GlobalContent.issueFolderPath + "%s/%s_t.%s", content.getId(), page.imageName, page.imageType));
        } else {
            contentValues.put("imagePath", String.format(Environment.getExternalStorageDirectory() + "/clip_image.jpg"));
        }

        String imageSrc = String.format("http://%s/Content/%s/%s/%s.%s", content.getWebUrl(), content.getPublisherId(), content.getId(), page.imageName, page.imageType);
        contentValues.put("picture", imageSrc);
        contentValues.put("description", content.getDescription());
        contentValues.put("name", String.format("%s, %s, Page %s", content.getProductName(), content.getName(), page.imageName));
        contentValues.put("shareType", shareType.ordinal());
        contentValues.put("socialType", socialType.ordinal());

        if (orientation == Constants.OrientationType.landscape) {
            contentValues.put("longUrl", String.format(Constants.DIGITAL_EDITION_URL, content.getId(), page.imageName, "d"));
            contentValues.put("pinItUrl", String.format("%s?id=%s&pn=%s&pv=%s", content.getPinItUrl(), content.getId(), page.imageName, "d"));
        } else {
            contentValues.put("longUrl", String.format(Constants.DIGITAL_EDITION_URL, content.getId(), page.imageName, "s"));
            contentValues.put("pinItUrl", String.format("%s?id=%s&pn=%s&pv=%s", content.getPinItUrl(), content.getId(), page.imageName, "s"));
        }

        AsyncTask ast = new AsyncTask() {

            @Override
            protected ContentValues doInBackground(Object... params) {
                ContentValues cv = (ContentValues) params[0];

                try {
                    JSONObject jsonReqObj = new JSONObject();

                    if (orientation == Constants.OrientationType.landscape)
                        jsonReqObj.put("longUrl", String.format(Constants.DIGITAL_EDITION_URL, content.getId(), page.imageName, "d"));
                    else
                        jsonReqObj.put("longUrl", String.format(Constants.DIGITAL_EDITION_URL, content.getId(), page.imageName, "s"));

                    String googleShortURL = String.format(Constants.GOOGLE_URL_SHORTENER_URL, Constants.GOOGLE_API_KEY);
                    String googleURLShortenerResponse = networkManager.makeHttpPostConnection(googleShortURL, jsonReqObj);

                    JSONObject obj = new JSONObject(googleURLShortenerResponse);
                    String shortUrl = obj.getString("id"); // = "https://goo.gl/RppkDn";
                    cv.put("shortUrl", shortUrl);

                    if (shareType == Constants.ShareType.link)
                        cv.put("postMessage", "For Digital Edition,\nClick " + shortUrl);
                    else
                        cv.put("postMessage", "Clipped from " + content.getProductName() + " - " + content.getName());

                    return cv;

                } catch (Exception e) {
                    e.printStackTrace();

                    if (orientation == Constants.OrientationType.landscape)
                        cv.put("shortUrl", String.format(Constants.DIGITAL_EDITION_URL, content.getId(), page.imageName, "d"));
                    else
                        cv.put("shortUrl", String.format(Constants.DIGITAL_EDITION_URL, content.getId(), page.imageName, "s"));

                    if (shareType == Constants.ShareType.link)
                        cv.put("postMessage", "For Digital Edition,\nClick " + cv.getAsString("longUrl"));
                    else
                        cv.put("postMessage", "Clipped from " + content.getProductName() + " - " + content.getName());

                    return cv;
                }
            }

            @Override
            protected void onPostExecute(Object result) {
                ContentValues cv = (ContentValues) result;
                Intent intent = new Intent(ViewIssueActivity.this, PostMessageActivity.class);
                intent.putExtra("contentValues", cv);
                startActivityForResult(intent, 300);
                hideProgressBar();
            }
        };

        ast.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, contentValues);
    }

    public void trackSocialShareEvent() {
        String event = "";

        switch (socialType) {
            case email:
                event = "Email";
                break;
            case facebook:
                event = "Facebook";
                break;
            case linkedin:
                event = "Linkedin";
                break;
            case tumblr:
                event = "Tumblr";
                break;
            case twitter:
                event = "Twitter";
                break;
            case pinterest:
                event = "More";
                break;
            default:
                break;
        }

        if (shareType == Constants.ShareType.image) {
            event = "Clip-" + event;
        }

        event = "Android-" + event;

        Page p = null;
        int currentPage = getCurrentPage();
        p = pages.get(currentPage);

        if (orientation == Constants.OrientationType.landscape) {
            if (currentPage != pages.size() - 1 && currentPage != 0) {
                if (pageType == -1) {
                    p = pages.get(currentPage - 1);
                }
            }
        }

        String eventAction = String.format("%s--%s--%s--Page No : %s", content.getPublisherId(), content.getProductName(), content.getName(), p.imageName);
        String eventLabel = String.format("/%s/%s/%s/android/%d/%s/page%s", content.getPublisherId(), content.getProductName(), content.getName(), getAppContext().getResources().getInteger(R.integer.app_type), orientationString, p.imageName);
        ((MyApplication) getApplication()).setCustomVar(1, "primaryEmail", GlobalContent.getInstance().primaryEmail);
        ((MyApplication) getApplication()).trackEvent(event, eventAction, eventLabel);
        pageType = 1;
    }

    public void updateStatus(boolean status) {
        if (status) {
            MCAlertDialog.showAlertDialog(activityContext, "Post Status", "Message Posted Successfully.");
            trackSocialShareEvent();
        } else {
            MCAlertDialog.showAlertDialog(activityContext, "Post Status", "Failed to post message.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("hima", "" + resultCode);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            Page p = pages.get(getCurrentPage());
            String postMessage = data.getStringExtra("postMessage");
            ContentValues contentValue = new ContentValues();
            contentValue.put("postMessage", postMessage);
            String imageSrc = String.format("http://%s/Content/%s/%s/%s.%s", content.getWebUrl(), content.getPublisherId(), content.getId(), p.imageName, p.imageType);
            contentValue.put("picture", imageSrc);
        } else if (requestCode == 300) {
            if (socialType != pinterest) {
                if (resultCode == RESULT_OK) {
                    updateStatus(true);
                } else if (resultCode == RESULT_CANCELED) {
                    updateStatus(false);
                }
            }

        } else if (requestCode == 400) {

        } else if (requestCode == 500 && resultCode == 500) { // This is for Multimedia selection thing!!
            if (!data.getBooleanExtra("isDone", true)) {
                updatePageView(data.getIntExtra("index", getCurrentPage()), false);
            }
        }
    }
}
