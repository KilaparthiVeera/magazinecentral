package com.mirabel.magazinecentral.beans.viewissue;

public class Hotspot {
    public float x;
    public float y;
    public float width;
    public float height;
    public int type;
    public String command;
    public String toolTip;
    public boolean scaled;
    public boolean bordered;
    public int borderColor;
    public int bgColor;
    public float borderWidth;
    public String pageName;

    public Hotspot() {

    }

    public Hotspot(Hotspot hs) {
        this.x = hs.x;
        this.y = hs.y;
        this.width = hs.width;
        this.height = hs.height;
        this.type = hs.type;
        this.command = hs.command;
        this.toolTip = hs.toolTip;
        this.scaled = hs.scaled;
        this.bordered = hs.bordered;
        this.bgColor = hs.bgColor;
        this.borderWidth = hs.borderWidth;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public boolean isScaled() {
        return scaled;
    }

    public void setScaled(boolean scaled) {
        this.scaled = scaled;
    }

    public boolean isBordered() {
        return bordered;
    }

    public void setBordered(boolean bordered) {
        this.bordered = bordered;
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }
}