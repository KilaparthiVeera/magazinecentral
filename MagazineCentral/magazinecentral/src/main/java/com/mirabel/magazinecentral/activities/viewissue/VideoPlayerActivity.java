package com.mirabel.magazinecentral.activities.viewissue;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.mirabel.magazinecentral.R;

public class VideoPlayerActivity extends Activity {

    VideoView vv = null;
    int stopPosition;

    public VideoPlayerActivity() {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        try {
            vv = (VideoView) findViewById(R.id.videoPlayer);
            vv.setMediaController(new MediaController(this));

            String filePath = getIntent().getStringExtra("videoUrl");

            if (filePath.contains("http")) {
                vv.setVideoURI(Uri.parse(filePath));
            } else {
                vv.setVideoPath(filePath);
            }

            vv.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void done(View v) {
        finish();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (vv != null && vv.isPlaying()) {
            stopPosition = vv.getCurrentPosition();
            vv.pause();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (vv != null) {
            vv.seekTo(stopPosition);
            vv.start();
        }
    }
}
