package com.mirabel.magazinecentral.customviews.viewissue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Bookmark;

public class BookmarkView extends FrameLayout {
    FrameLayout thisLayout = null;
    public Bookmark bookmark;
    public ImageButton deleteBookmark = null;
    public ImageView bookmarkImage = null;

    public BookmarkView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        thisLayout = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.bookmark_layout, null);
    }

    public BookmarkView(Context context, AttributeSet attrs) {
        super(context, attrs);
        thisLayout = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.bookmark_layout, null);
    }

    public BookmarkView(Context context, Bookmark bookmark, int tag) {
        super(context);
        int width = getResources().getInteger(R.integer.bookmark_width);
        int height = getResources().getInteger(R.integer.bookmark_height);
        final float scale = getResources().getDisplayMetrics().density;
        thisLayout = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.bookmark_layout, null);
        this.bookmark = bookmark;
        bookmarkImage = (ImageView) thisLayout.findViewById(R.id.bookmarkImage);
        Bitmap bmp = BitmapFactory.decodeFile(GlobalContent.issueFolderPath + bookmark.getContentId() + "/" + bookmark.getPageName());
        bookmarkImage.setImageBitmap(bmp);
        bookmarkImage.setTag(tag);
        deleteBookmark = (ImageButton) thisLayout.findViewById(R.id.deleteBookmark);
        deleteBookmark.setTag(tag);
        FrameLayout.LayoutParams imageViewParams = (LayoutParams) bookmarkImage.getLayoutParams();
        int leftMargin = getResources().getInteger(R.integer.b_left_margin);
        int topMargin = getResources().getInteger(R.integer.b_top_margin);
        imageViewParams.setMargins(leftMargin, topMargin, 0, 0);
        imageViewParams.width = (int) (width * scale + 0.5f);
        imageViewParams.height = (int) (height * scale + 0.5f);
        bookmarkImage.setLayoutParams(imageViewParams);
        addView(thisLayout);
    }
}