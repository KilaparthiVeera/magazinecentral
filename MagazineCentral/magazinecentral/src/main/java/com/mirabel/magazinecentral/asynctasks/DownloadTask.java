package com.mirabel.magazinecentral.asynctasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.MyApplication;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.interfaces.ConnectivityListener;
import com.mirabel.magazinecentral.interfaces.DownloadServiceInterface;
import com.mirabel.magazinecentral.util.MCSharedPreferences;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by venkat on 7/31/17.
 */

public class DownloadTask extends AsyncTask implements ConnectivityListener {
    public static final String TAG = DownloadTask.class.getSimpleName();

    private Context mContext;
    private MCSharedPreferences sharedPreferences;
    private DownloadServiceInterface downloadServiceInterface;
    private Content content = null;
    private Constants.DOWNLOAD_TYPE downloadType = Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD;
    public Constants.DOWNLOAD_STATUS downloadStatus = null;
    private String issueFolderPath = null;
    final int BUFFER_SIZE = 1024;
    private long downloaded = 0;

    public Content getContent() {
        return content;
    }

    public DownloadTask(MCSharedPreferences mSharedPreferences, DownloadServiceInterface downloadServiceInterface, Content _content) {
        this.mContext = MyApplication.getAppContext();
        this.sharedPreferences = mSharedPreferences;
        this.downloadServiceInterface = downloadServiceInterface;
        this.content = _content;
        this.downloadType = content.getDownloadType();

        this.issueFolderPath = GlobalContent.issueFolderPath;
        if (issueFolderPath.isEmpty()) {
            issueFolderPath = sharedPreferences.getString(Constants.ISSUES_FOLDER_PATH);
            GlobalContent.issueFolderPath = issueFolderPath;
        }
        this.issueFolderPath = new String(GlobalContent.issueFolderPath + _content.getId());

        GlobalContent.currentDownloadingIssue = content;

        // creating new folder for latest Issue if it's not exists.
        File file = new File(issueFolderPath);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // To show magazine thumbnails in Library we are downloading thumbs.
        new DownloadMagazineThumbs().executeOnExecutor(THREAD_POOL_EXECUTOR, "");
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        initiateReadAsYouDownload();
        return content.getId();
    }

    @Override
    protected void onProgressUpdate(Object... values) {

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onCancelled(Object object) {
        super.onCancelled(object);
    }

    @Override
    public void networkStateChanged(boolean isConnected) {
        try {
            if (isConnected) {
                this.executeOnExecutor(THREAD_POOL_EXECUTOR, (Object[]) null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initiateReadAsYouDownload() {
        content.setContentState(Constants.ContentState.ContentStateDownloading);
        String xmlPath = issueFolderPath.concat("/issue.xml");
        File issueXmlFile = new File(xmlPath);

        if (issueXmlFile.exists()) {
            parseIssueXml(xmlPath);
        } else {
            boolean status = downloadIssueXmlFile();

            if (status) {
                parseIssueXml(xmlPath);
            }
        }
    }

    public boolean downloadIssueXmlFile() {
        try {

          //  String url="www.mirabelsmagazinecentral.com/";
            String issueXMLPathURL = String.format(Constants.ASSET_SERVICE, content.getWebUrl(), content.getPublisherId(), content.getId(), "issue.xml");

            Log.e("veera","XML : "+issueXMLPathURL);

            URL urlObj = new URL(issueXMLPathURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObj.openConnection();
            httpURLConnection.setRequestMethod("GET");

            int responseCode = httpURLConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                ByteArrayOutputStream bos = new ByteArrayOutputStream(inputStream.available());
                byte[] buffer = new byte[1024 * 8];
                int read;

                while ((read = inputStream.read(buffer)) != -1) {
                    bos.write(buffer, 0, read);
                }

                try {
                    byte[] array = Constants.ENCRYPTION_KEY.getBytes();
                    byte[] encryptedBytes = encrypt(array, bos.toByteArray());
                    File issueXmlFile = new File(issueFolderPath + "/issue.xml");
                    BufferedOutputStream bfos = new BufferedOutputStream(new FileOutputStream(issueXmlFile));
                    bfos.write(encryptedBytes);
                    bfos.flush();
                    bfos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (inputStream != null) {
                    inputStream.close();
                }

                return true;

            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public void parseIssueXml(String xmlPath) {
        File issueXmlFile = new File(xmlPath);
        byte[] decryptedBytes = null;

        try {
            FileInputStream fis = new FileInputStream(issueXmlFile);
            byte[] encryptedBytes = new byte[fis.available()];
            fis.read(encryptedBytes);
            fis.close();
            decryptedBytes = decrypt(Constants.ENCRYPTION_KEY.getBytes(), encryptedBytes);
        } catch (FileNotFoundException fne) {
            fne.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            issueXmlFile.delete();
        }

        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser parser;

        try {
            parser = spf.newSAXParser();
            XMLReader xr = parser.getXMLReader();
            IssueXMLParserHandler handler = new IssueXMLParserHandler();
            xr.setContentHandler(handler);
            xr.parse(new InputSource(new ByteArrayInputStream(decryptedBytes)));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
            issueXmlFile.delete();
            initiateReadAsYouDownload();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class IssueXMLParserHandler extends DefaultHandler {
        ArrayList<String> downloadableFileNames = new ArrayList<>(); // contains pages & thumbnails which we need to download
        ArrayList<String> resourceFileNames = new ArrayList<>(); // contains video or audio file names which we need to download
        ArrayList<String> tocNames = new ArrayList<>();
        String currentElementName = "";

        @Override
        public void startDocument() throws SAXException {
            super.startDocument();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);

            if (localName.equalsIgnoreCase("page")) {
                String imageName = attributes.getValue("name");
                String imageType = attributes.getValue("type");
                downloadableFileNames.add(imageName + "_t." + imageType);
                downloadableFileNames.add(imageName + "." + imageType);
            } else if (localName.equalsIgnoreCase("item") && currentElementName.equalsIgnoreCase("contents")) {
                tocNames.add(attributes.getValue("pageName"));
            } else if (localName.equalsIgnoreCase("item")) {
                if (attributes.getIndex("source") != -1 && (attributes.getValue("vSource") == null || attributes.getValue("vSource").isEmpty())) {
                    File f = new File(String.format(issueFolderPath + "/%s", attributes.getValue("source")));
                    if (!f.exists()) {
                        downloadableFileNames.add(attributes.getValue("source"));
                        resourceFileNames.add(attributes.getValue("source"));
                    }
                }
            } else if (localName.equals("contents")) {
                currentElementName = localName;
            } else {
                currentElementName = "";
            }
        }

        @Override
        public void endDocument() throws SAXException {
            super.endDocument();

            ArrayList<String> remainingFileNames = new ArrayList<String>();
            for (String fileName : downloadableFileNames) {
                File f = new File(issueFolderPath + "/" + fileName);
                if (!f.exists()) {
                    remainingFileNames.add(fileName);
                }
            }

            int totalNoOfFiles = downloadableFileNames.size();
            int remainingNoOfFiles = remainingFileNames.size();
            downloadableFileNames.clear();

            DownloadTask.this.readAsYouDownload(remainingFileNames, resourceFileNames, totalNoOfFiles, remainingNoOfFiles, tocNames);
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            super.error(e);
            e.printStackTrace();
        }
    }

    public void readAsYouDownload(ArrayList<String> remainingFileNames, ArrayList<String> resourceFileNames, int totalNoOfFiles, int remainingNoOfFiles, ArrayList<String> tocNames) {
        int noOfFilesDownloaded = 0;

        try {
            String issueZipAssetPath = "";
            if (totalNoOfFiles == remainingNoOfFiles) {
                issueZipAssetPath = String.format(Constants.ZIP_ASSET_SERVICE, "0D8938F3-5E12-4D25-95B1-E67186402121", content.getPublisherId(), content.getId(), mContext.getResources().getInteger(R.integer.app_type), downloadType.ordinal(), 0);
            } else {
                issueZipAssetPath = String.format(Constants.ZIP_ASSET_SERVICE, "0D8938F3-5E12-4D25-95B1-E67186402121", content.getPublisherId(), content.getId(),mContext.getResources().getInteger(R.integer.app_type), downloadType.ordinal(), 1);
            }

            URL zipAssetURL = new URL(issueZipAssetPath);
            HttpURLConnection zipAssetURLConnection = (HttpURLConnection) zipAssetURL.openConnection();
            zipAssetURLConnection.setRequestMethod("HEAD");
            zipAssetURLConnection.connect();
            int zipAssetSize = zipAssetURLConnection.getContentLength();
            float unitSize = (float) (zipAssetSize / (1024 * 1024)) / totalNoOfFiles;
            zipAssetURLConnection.disconnect();

            Intent progressBroadcast = new Intent(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE);
            progressBroadcast.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
            progressBroadcast.putExtra(Constants.BUNDLE_TOTAL, (float) zipAssetSize / (1024 * 1024));
            // Broadcasting Zip Asset Size to Receiver Activity
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(progressBroadcast);

            //Collections.reverse(remainingFileNames);

            for (String fileName : remainingFileNames) {
                if (isCancelled()) {
                    break;
                }

                // if file is video or audio
                if (resourceFileNames.contains(fileName)) {
                    HttpURLConnection resourceHttpURLConnection = null;
                    URL resourceURLObj = null;
                    BufferedInputStream bis = null;
                    FileOutputStream fos = null;
                    BufferedOutputStream bos = null;
                    File resourceTempFile = null;
                    long downloaded = 0;
                    String resourceDestinationPath = null, resourceTempPath = null;

                    try {
                        resourceDestinationPath = new String(issueFolderPath + "/" + fileName);
                        resourceTempPath = new String(Environment.getExternalStorageDirectory() + "/" + fileName);

                        resourceTempFile = new File(resourceTempPath);
                        if (resourceTempFile.exists()) {
                            downloaded = resourceTempFile.length();
                        }

                        String resourceURL = String.format(Constants.ASSET_SERVICE, content.getWebUrl(), content.getPublisherId(), content.getId(), fileName);
                        resourceURLObj = new URL(resourceURL);
                        resourceHttpURLConnection = (HttpURLConnection) resourceURLObj.openConnection();
                        resourceHttpURLConnection.setRequestProperty("Range", "bytes=" + downloaded + "-");
                        int connectionTimeout = Constants.CONNECTION_TIMEOUT; // set the connection timeout to 30 seconds
                        int readTimeout = Constants.CONNECTION_TIMEOUT; // set the read timeout to 30 seconds
                        resourceHttpURLConnection.setConnectTimeout(connectionTimeout);
                        resourceHttpURLConnection.setReadTimeout(readTimeout);
                        bis = new BufferedInputStream(resourceHttpURLConnection.getInputStream());
                        fos = (downloaded == 0) ? new FileOutputStream(resourceTempFile) : new FileOutputStream(resourceTempFile, true);
                        bos = new BufferedOutputStream(fos, BUFFER_SIZE);
                        byte[] data = new byte[BUFFER_SIZE];
                        int x = 0;
                        while ((x = bis.read(data, 0, BUFFER_SIZE)) >= 0) {
                            bos.write(data, 0, x);
                            downloaded += x;
                        }

                        resourceTempFile.renameTo(new File(resourceDestinationPath));

                    } catch (SocketTimeoutException e) {
                        Log.e(TAG, "SocketTimeoutException Occured : " + e.toString());
                        e.printStackTrace();
                    } catch (IOException io) {
                        io.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (bos != null) {
                            bos.flush();
                            bos.close();
                        }

                        if (fos != null) {
                            fos.flush();
                            fos.close();
                        }

                        if (bis != null)
                            bis.close();

                        if (resourceTempFile != null && resourceTempFile.exists())
                            resourceTempFile.delete();

                    }
                } else {
                    String imageURL = String.format(Constants.ASSET_SERVICE, content.getWebUrl(), content.getPublisherId(), content.getId(), fileName);
                    String targetPath = issueFolderPath + "/" + fileName;
                    downloadFile(imageURL, targetPath);

                    if (!fileName.contains("_t")) {
                        String[] aplitss = fileName.split("\\.");
                        String tocName = aplitss[0];

                        if (tocNames.contains(tocName)) {
                            String tocURL = String.format(Constants.ASSET_SERVICE, content.getWebUrl(), content.getPublisherId(), content.getId(), tocName + "_toc." + aplitss[1]);
                            targetPath = issueFolderPath + "/" + tocName + "_toc." + aplitss[1];
                            downloadFile(tocURL, targetPath);
                        }
                    }
                }

                noOfFilesDownloaded++;
                progressBroadcast.putExtra(Constants.BUNDLE_DOWNLOADED, (float) (totalNoOfFiles - remainingNoOfFiles + noOfFilesDownloaded) * unitSize);
                progressBroadcast.putExtra(Constants.BUNDLE_FILE_NAME, fileName);

                // Broadcasting Downloading Progress to Receiver Activity
                if (GlobalContent.isDownloadRunning)
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(progressBroadcast);
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (remainingNoOfFiles == noOfFilesDownloaded) {
                downloadStatus = Constants.DOWNLOAD_STATUS.FINISHED;
            }
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if (downloadStatus == Constants.DOWNLOAD_STATUS.FINISHED) {
            content.setContentState(Constants.ContentState.ContentStateDownloaded);
        }

        downloadServiceInterface.downloadFinished(content);
    }

    public class DownloadMagazineThumbs extends AsyncTask {

        @Override
        protected Object doInBackground(Object... params) {
            downloadThumbImages();
            return null;
        }

        public void downloadThumbImages() {
            if (GlobalContent.context == null) {
                return;
            }

            try {
                String f_200_target_path = GlobalContent.context.getFilesDir() + "/Catalog/" + content.getId() + "_200.jpeg";
                File f_200 = new File(f_200_target_path);
                if (!f_200.exists()) {
                    String thumb_image_200_url = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());
                    downloadFile(thumb_image_200_url, f_200_target_path);
                }

                String f_370_target_path = GlobalContent.context.getFilesDir() + "/Catalog/" + content.getId() + "_370.jpeg";
                File f_370 = new File(f_370_target_path);
                if (!f_370.exists()) {
                    String thumb_image_370_url = String.format(Constants.IMG_370, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());
                    downloadFile(thumb_image_370_url, f_370_target_path);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Object obj) {
            super.onPostExecute(obj);
        }
    }

    public void downloadFile(String imageURL, String targetPath) {
        try {
            HttpURLConnection imageHttpURLConnection = null;
            InputStream inputStream = null;
            ByteArrayOutputStream bos = null;
            FileOutputStream fOut = null;

            try {
                URL imageURLObj = new URL(imageURL);
                imageHttpURLConnection = (HttpURLConnection) imageURLObj.openConnection();
                imageHttpURLConnection.setRequestMethod("GET");

                int responseCode = imageHttpURLConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    inputStream = new BufferedInputStream(imageHttpURLConnection.getInputStream());
                    bos = new ByteArrayOutputStream(inputStream.available());
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int read;

                    while ((read = inputStream.read(buffer)) != -1) {
                        bos.write(buffer, 0, read);
                    }

                    /*while (true) {
                        read = inputStream.read(buffer);

                        if (read <= 0) {
                            break;
                        }

                        bos.write(buffer, 0, read);
                    }*/

                    byte[] byteArray = bos.toByteArray();
                    File imageFile = new File(targetPath);

                    if (!imageFile.exists()) {
                        imageFile.createNewFile();
                        fOut = new FileOutputStream(imageFile);
                        fOut.write(byteArray);
                    }
                }
            } catch (IOException io) {
                io.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (fOut != null) {
                    fOut.flush();
                    fOut.close();
                }

                if (bos != null) {
                    bos.flush();
                    bos.close();
                }

                if (inputStream != null)
                    inputStream.close();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
