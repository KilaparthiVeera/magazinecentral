package com.mirabel.magazinecentral.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.DownloadManagerActivity;
import com.mirabel.magazinecentral.activities.IssueDetailsActivity;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;

import java.io.File;

/**
 * Created by venkat on 6/30/17.
 */

public class DownloadManagerIssuesGridViewAdapter extends BaseAdapter {
    private Context context;
    private DownloadManagerActivity activity;
    private LayoutInflater layoutInflater;
    private GlobalContent globalContent;

    public DownloadManagerIssuesGridViewAdapter(Context context, DownloadManagerActivity activity) {
        this.context = context;
        this.activity = activity;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.globalContent = GlobalContent.getInstance();
    }

    static public class DownloadManagerIssueViewViewHolder {
        public ImageView cover_page, download_status_icon, cancel_download_icon, pause_or_resume_download_icon;
        public MCTextView product_name, issue_name;
        public ProgressBar download_progress_bar;
        public String contentId;

        public String getContentId() {
            return contentId;
        }

        public void setContentId(String contentId) {
            this.contentId = contentId;
        }
    }

    @Override
    public int getCount() {
        return globalContent.getDownloadingContents().size();
    }

    @Override
    public Content getItem(int position) {
        return globalContent.getDownloadingContents().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final DownloadManagerIssueViewViewHolder downloadManagerIssueViewViewHolder;

        try {
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.magazine_issue_for_download_manager, viewGroup, false);

                downloadManagerIssueViewViewHolder = new DownloadManagerIssueViewViewHolder();
                downloadManagerIssueViewViewHolder.cover_page = (ImageView) convertView.findViewById(R.id.cover_page);
                downloadManagerIssueViewViewHolder.product_name = (MCTextView) convertView.findViewById(R.id.product_name);
                downloadManagerIssueViewViewHolder.issue_name = (MCTextView) convertView.findViewById(R.id.issue_name);
                downloadManagerIssueViewViewHolder.download_status_icon = (ImageView) convertView.findViewById(R.id.download_status_icon);
                downloadManagerIssueViewViewHolder.cancel_download_icon = (ImageView) convertView.findViewById(R.id.cancel_download_icon);
                downloadManagerIssueViewViewHolder.pause_or_resume_download_icon = (ImageView) convertView.findViewById(R.id.pause_or_resume_download_icon);
                downloadManagerIssueViewViewHolder.download_progress_bar = (ProgressBar) convertView.findViewById(R.id.download_progress_bar);

                if (context.getResources().getBoolean(R.bool.is_branded_app)) {
                    downloadManagerIssueViewViewHolder.product_name.setVisibility(View.GONE);
                } else {
                    downloadManagerIssueViewViewHolder.product_name.setVisibility(View.VISIBLE);
                }

                convertView.setTag(downloadManagerIssueViewViewHolder);
            } else {
                downloadManagerIssueViewViewHolder = (DownloadManagerIssueViewViewHolder) convertView.getTag();
            }

            final Content content = getItem(position);

            downloadManagerIssueViewViewHolder.setContentId(content.getId());
            downloadManagerIssueViewViewHolder.product_name.setText(content.getProductName());
            downloadManagerIssueViewViewHolder.issue_name.setText(content.getName());

            File file_370 = new File(GlobalContent.context.getFilesDir() + "/Catalog/" + content.getId() + "_370.jpeg");
            if (file_370.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file_370.getAbsolutePath());
                downloadManagerIssueViewViewHolder.cover_page.setImageBitmap(bitmap);
            } else {
                //Updating Issue Cover Page using Glide library
                String coverPageImageUrl = String.format(Constants.IMG_200, Constants.K_DOWNLOAD_URL, content.getPublisherId(), content.getId());
                RequestOptions glideReqOptions = new RequestOptions();
                glideReqOptions.placeholder(R.drawable.cover_page);
                Glide.with(activity).load(coverPageImageUrl).apply(glideReqOptions).into(downloadManagerIssueViewViewHolder.cover_page);
            }

            downloadManagerIssueViewViewHolder.download_progress_bar.setVisibility(View.VISIBLE);
            downloadManagerIssueViewViewHolder.download_progress_bar.setProgress(0);

            if (globalContent.isIssueCurrentlyDownloading(content.getId())) {
                downloadManagerIssueViewViewHolder.pause_or_resume_download_icon.setImageResource(R.drawable.icon_pause_download);
            } else {
                downloadManagerIssueViewViewHolder.pause_or_resume_download_icon.setImageResource(R.drawable.icon_continue_download);
            }

            downloadManagerIssueViewViewHolder.cover_page.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    readMagazine(content);
                }
            });

            downloadManagerIssueViewViewHolder.download_status_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openIssueDetailPage(content);
                }
            });

            downloadManagerIssueViewViewHolder.pause_or_resume_download_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pauseOrResumeMagazineDownload(content);
                }
            });

            downloadManagerIssueViewViewHolder.cancel_download_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelMagazineDownload(content);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void readMagazine(Content content) {
        File file = new File(GlobalContent.issueFolderPath + content.getId() + "/issue.xml");

        if (file.exists()) {
            Intent viewIssueActivity = new Intent(context, ViewIssueActivity.class);
            viewIssueActivity.putExtra(Constants.BUNDLE_CONTENT, content);
            context.startActivity(viewIssueActivity);
            activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (globalContent.getCurrentStateOfMagazine(content) == Constants.ContentState.ContentStateDownloading) {
            MCProgressDialog.hideProgressDialog();
            MCAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.download_progress), context.getResources().getString(R.string.download_wait_message));
        }
    }

    public void openIssueDetailPage(Content content) {
        Intent openIssueDetailIntent = new Intent(context, IssueDetailsActivity.class);
        openIssueDetailIntent.putExtra(Constants.BUNDLE_SELECTED_ISSUE_CONTENT, content);
        openIssueDetailIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(openIssueDetailIntent);
        activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void pauseOrResumeMagazineDownload(final Content content) {
        if (GlobalContent.currentDownloadingIssue != content) {
            //Pause downloading of currently downloading Issue.
            GlobalContent.currentDownloadingIssue.setContentState(Constants.ContentState.ContentStatePaused);
            Intent pauseMagazineDownloadIntent = new Intent(Constants.INTENT_ACTION_PAUSE_DOWNLOAD);
            pauseMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT_ID, GlobalContent.currentDownloadingIssue.getId());
            pauseMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT, GlobalContent.currentDownloadingIssue);
            LocalBroadcastManager.getInstance(context).sendBroadcast(pauseMagazineDownloadIntent);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Starting new Issue download
                    Intent resumeMagazineDownloadIntent = new Intent(Constants.INTENT_ACTION_RESUME_DOWNLOAD);
                    resumeMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
                    resumeMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT, content);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(resumeMagazineDownloadIntent);
                }
            }, 200);

        } else {
            if (content.getContentState() == Constants.ContentState.ContentStateDownloading) {
                content.setContentState(Constants.ContentState.ContentStatePaused);
                Intent pauseMagazineDownloadIntent = new Intent(Constants.INTENT_ACTION_PAUSE_DOWNLOAD);
                pauseMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
                pauseMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT, content);
                LocalBroadcastManager.getInstance(context).sendBroadcast(pauseMagazineDownloadIntent);
            } else {
                content.setContentState(Constants.ContentState.ContentStateDownloading);

                Intent resumeMagazineDownloadIntent = new Intent(Constants.INTENT_ACTION_RESUME_DOWNLOAD);
                resumeMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
                resumeMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT, content);
                LocalBroadcastManager.getInstance(context).sendBroadcast(resumeMagazineDownloadIntent);
            }
        }
    }

    public void cancelMagazineDownload(Content content) {
        content.setContentState(Constants.ContentState.ContentStateNone);
        Intent cancelMagazineDownloadIntent = new Intent(Constants.INTENT_ACTION_CANCEL_DOWNLOAD_CONFIRMATION);
        cancelMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT_ID, content.getId());
        cancelMagazineDownloadIntent.putExtra(Constants.BUNDLE_CONTENT, content);
        LocalBroadcastManager.getInstance(context).sendBroadcast(cancelMagazineDownloadIntent);
    }
}
