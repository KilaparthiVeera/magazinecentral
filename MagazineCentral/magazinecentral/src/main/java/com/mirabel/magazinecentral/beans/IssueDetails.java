package com.mirabel.magazinecentral.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by venkat on 7/10/17.
 */

public class IssueDetails implements Serializable {
    @SerializedName("IssueInfo")
    @Expose
    private Content issueInfo;
    @SerializedName("RecentIssues")
    @Expose
    private List<Content> recentIssues = null;
    @SerializedName("SimilarIssues")
    @Expose
    private List<Content> similarIssues = null;

    public Content getIssueInfo() {
        return issueInfo;
    }

    public void setIssueInfo(Content issueInfo) {
        this.issueInfo = issueInfo;
    }

    public List<Content> getRecentIssues() {
        return recentIssues;
    }

    public void setRecentIssues(List<Content> recentIssues) {
        this.recentIssues = recentIssues;
    }

    public List<Content> getSimilarIssues() {
        return similarIssues;
    }

    public void setSimilarIssues(List<Content> similarIssues) {
        this.similarIssues = similarIssues;
    }
}
