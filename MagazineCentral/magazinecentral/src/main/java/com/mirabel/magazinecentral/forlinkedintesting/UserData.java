package com.mirabel.magazinecentral.forlinkedintesting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("lastName")
    @Expose
    private LastName lastName;
    @SerializedName("firstName")
    @Expose
    private FirstName firstName;
    @SerializedName("id")
    @Expose
    private String id;

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public FirstName getFirstName() {
        return firstName;
    }

    public void setFirstName(FirstName firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public class FirstName {

        @SerializedName("localized")
        @Expose
        private Localized_ localized;
        @SerializedName("preferredLocale")
        @Expose
        private PreferredLocale_ preferredLocale;

        public Localized_ getLocalized() {
            return localized;
        }

        public void setLocalized(Localized_ localized) {
            this.localized = localized;
        }

        public PreferredLocale_ getPreferredLocale() {
            return preferredLocale;
        }

        public void setPreferredLocale(PreferredLocale_ preferredLocale) {
            this.preferredLocale = preferredLocale;
        }

    }


    public class LastName {

        @SerializedName("localized")
        @Expose
        private Localized localized;
        @SerializedName("preferredLocale")
        @Expose
        private PreferredLocale preferredLocale;

        public Localized getLocalized() {
            return localized;
        }

        public void setLocalized(Localized localized) {
            this.localized = localized;
        }

        public PreferredLocale getPreferredLocale() {
            return preferredLocale;
        }

        public void setPreferredLocale(PreferredLocale preferredLocale) {
            this.preferredLocale = preferredLocale;
        }

    }

    public class Localized {

        @SerializedName("en_US")
        @Expose
        private String enUS;

        public String getEnUS() {
            return enUS;
        }

        public void setEnUS(String enUS) {
            this.enUS = enUS;
        }

    }

    public class Localized_ {

        @SerializedName("en_US")
        @Expose
        private String enUS;

        public String getEnUS() {
            return enUS;
        }

        public void setEnUS(String enUS) {
            this.enUS = enUS;
        }

    }

    public class PreferredLocale {

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("language")
        @Expose
        private String language;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

    }


    public class PreferredLocale_ {

        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("language")
        @Expose
        private String language;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

    }
}




