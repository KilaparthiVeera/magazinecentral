package com.mirabel.magazinecentral.constants;

import android.Manifest;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;

/**
 * Created by venkat on 07/06/17.
 */

public class Constants {
    public static final String LOADING_MESSAGE = "Loading Please wait..";
    public static final String CHARSET_NAME = "UTF-8";

    public static final int DEVICE_MIN_WIDTH = 1100; // i.e. pixels, based on this, we will consider device as small or large
    public static final int VOLLEY_SOCKET_TIMEOUT = 30000; // 120 seconds.
    public static final int CONNECTION_TIMEOUT = 30000;
    public static final int HOME_SCREEN_PAGE_SIZE = 10;
    public static final int PAGE_SIZE = 20;
    public static final int PAGE_SIZE_FOR_TABLETS = 25;

    // Shared Preference Constants
    public static final String SHARED_PREFERENCE = "MagazineCentral";
    public static final String SP_LAST_REFRESHED_TIME_IN_DASHBOARD = "LastRefreshedTimeInDashboard";
    public static final String SP_IS_DOWNLOAD_SERVICE_STARTED = "IsDownloadServiceStarted";
    public static final String ISSUES_FOLDER_PATH = "issueFolderPath";
    public static final String SP_PRIMARY_EMAIL = "primaryEmail";

    // Bundle Constants
    public static final String BUNDLE_SELECTED_CATEGORY = "categoryName";
    public static final String BUNDLE_SELECTED_ISSUE_CONTENT = "selectedIssueContent";
    public static final String BUNDLE_CONTENT = "content";
    public static final String BUNDLE_CONTENT_ID = "contentId";
    public static final String BUNDLE_ISSUE_NAME = "issueName";
    public static final String BUNDLE_TOTAL = "total";
    public static final String BUNDLE_DOWNLOADED = "downloaded";
    public static final String BUNDLE_FILE_NAME = "fileName";

    // Local Broadcast Manager Intent Actions Constants
    public static final String INTENT_ACTION_OPEN_ISSUE = "com.mirabel.OpenIssue";
    public static final String INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE = "com.mirabel.DownloadProgressReceive";
    public static final String INTENT_ACTION_PAUSE_DOWNLOAD = "com.mirabel.PauseDownload";
    public static final String INTENT_ACTION_RESUME_DOWNLOAD = "com.mirabel.ResumeDownload";
    public static final String INTENT_ACTION_CANCEL_DOWNLOAD = "com.mirabel.CancelDownload";
    public static final String INTENT_ACTION_DOWNLOAD_FINISH = "com.mirabel.DownloadFinish";
    public static final String INTENT_ACTION_ARCHIVE_ISSUE = "com.mirabel.ArchiveIssue";
    public static final String INTENT_ACTION_ISSUE_DELETED = "com.mirabel.IssueDeleted";
    public static final String INTENT_ACTION_SUBSCRIBE_ISSUE = "com.mirabel.SubscribeIssue";
    public static final String INTENT_ACTION_PURCHASE_ISSUE = "com.mirabel.PurchaseIssue";
    public static final String INTENT_ACTION_CANCEL_DOWNLOAD_CONFIRMATION = "com.mirabel.CancelDownloadConfirmation";
    public static final String INTENT_ACTION_REFRESH_VIEW = "com.mirabel.RefreshView";

    // Activity Request Codes
    public static final int REQUEST_EXTERNAL_STORAGE = 1;
    public static final int REQUEST_GET_ACCOUNTS = 2;

    // Storage Permissions
    public static String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    public static String[] STORAGE_PERMISSIONS_33 = {
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_AUDIO,
            Manifest.permission.READ_MEDIA_VIDEO
    };

    // Get accounts Permissions
    public static String[] GET_ACCOUNTS_PERMISSIONS = {Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_PHONE_STATE};


    public enum DOWNLOAD_TYPE {
        ZIP_DOWNLOAD,
        READ_AS_YOU_DOWNLOAD
    }

    public enum DOWNLOAD_STATUS {
        INITIATED,
        DOWNLOADING,
        PAUSED,
        FINISHED,
        CANCELLED
    }

    public enum ContentState {
        ContentStateNone,
        ContentStateDownloaded,
        ContentStateDownloading,
        ContentStatePaused
    }

    public enum RequestFrom {
        HOME_PAGE,
        CATEGORY,
        SEARCH_PAGE,
        LIBRARY_SEARCH_PAGE
    }

    public enum RequestCode {
        DEFAULT
    }

    public enum SocialType {
        email,
        facebook,
        twitter,
        tumblr,
        linkedin,
        pinterest
    }

    public enum ShareType {
        link,
        image
    }

    public enum OrientationType {
        portrait,
        landscape
    }

    public enum SelectionType {
        PINTEREST_BOARD
    }

    public static final String HTML_BODY_PORTRAIT = "<html><body><p><b><i>%s</i></b></p><p>To view this page in the online Digital Edition <a href=\"%s\" style='color: -webkit-link; text-decoration: underline;'><b><i>click here</i></b></a></p><p>To view this page on your device, perform one of the following:</p><dl ><dd><p>- &nbsp &nbsp	If you have the %s App on your device <a href=\"%s\"><b><i> click here.</i></b></a></p></li><dd> <p>- &nbsp &nbsp	If you do not have the %s App on your device <a href=\"%s\"><b><i> click here.</i></b></a></p></li></dl></body></html>";
  //  public static final String HTML_BODY_PORTRAIT = "<html><body><p><b><i>%s</i></b></p><p>To view this page in the online Digital Edition :%s</p><p>To view this page on your device, perform one of the following:</p><dl ><dd><p>- &nbsp &nbsp	If you have the %s App on your device : %s </p></li><dd> <p>- &nbsp &nbsp	If you do not have the %s App on your device : %s </p></li></dl></body></html>";

    // click Links  :
  //  public static final String HTML_BODY_PORTRAIT = "<html><body><p><b><i>%s</i></b></p><p>To view this page in the online Digital Edition :%s</p><p>To view this page on your device, perform one of the following:</p><dl ><dd><p>- &nbsp &nbsp	If you have the %s App on your device <a href=\"%s\"><b><i> click here.</i></b></a></p></li><dd> <p>- &nbsp &nbsp	If you do not have the %s App on your device <a href=\"%s\"><b><i> click here.</i></b></a></p></li></dl></body></html>";


    public static final String ENCRYPTION_KEY = "strstrstrstrstrs";

    /* All Application Service URL's Listed Below */

    /**
     * This is the main url & we are using this url to make service calls.
     */
    public static final String MAIN_URL = "http://www.mirabelsmagazinecentral.com/BrandedApp/DigitalService.svc";

    public static final String K_DOWNLOAD_URL = "http://www.mirabelsmagazinecentral.com/Distributionv3/Handlers/Downloadv3.ashx";

    /**
     * This url is used to download _200 image for cover page icon.</br><p>This will take publisherId & contentId as parameters </p></br>
     * <p>
     * e.g., <p><b>String.format(imgSrc_200,publisherId,contentId)</b></p>
     */
    public static final String IMG_200 = "%s?pid=%s&tid=%s_200&tp=i";

    /**
     * This url is used to download _370 image for cover page icon.</br> <p>This will take publisherId & contentId as parameters </p></br>
     * <p>
     * e.g., <p><b>String.format(imgSrc_370,publisherId,contentId)</b></p>
     */
    public static final String IMG_370 = "%s?pid=%s&tid=%s_370&tp=i";
    public static final String ASSET_SERVICE = "http://%s/Content/%s/%s/%s";

    /**
     * This is the url to download zip asset for a magazine </br> <p> This will take the following parameters to download the zip file</br>
     * uid = User ID = 0D8938F3-5E12-4D25-95B1-E67186402121 </br> </br>
     * pid = publisherID</br> </br>
     * tid=Content ID</br> </br>
     * type = 10/11/12
     * <ul>
     * <li>10 = Magazine Central</li>
     * <li>11 = Branded App </li>
     * <li>12 = Paid App </li>
     * </ul>
     * dtype = downloadType = 0/1/2
     * <p>
     * <ul>
     * <li>0 = ZIPPED DOWNLOAD</li>
     * <li>1 = READ AS YOU DOWNLOAD</li>
     * <li>2 = PREVIEW DOWNLOAD</li>
     * </ul>
     * isResumed = 0/1 = To check if download is resuming or starting
     * <p>
     * 0 = Not Resuming = Fresh download
     * 1 = Resuming
     */
    public static final String ZIP_ASSET_SERVICE = "http://www.mirabelsmagazinecentral.com/Distributionv3/Handlers/Downloadv3.ashx?uid=%s&pid=%s&tid=%s&tp=z&type=%d&dtype=%d&isResumed=%d";

    /**
     * This is the url for the digital edition. </br>This will take contentId, pageName & pv i.e., page view("s"= single,"d"=double) </br></br>
     * <p>
     * e.g.,<p><b> String url =String.format(digitalEditionUrl,contentId,pageName,"s") </b></p> for single page view
     */
    public static final String DIGITAL_EDITION_URL = "http://www.mirabelsmagazinecentral.com/DigitalEdition/index.html?id=%s&pn=%s&pv=%s";

    public static final String WEB_URL = "http://www.mirabelsmagazinecentral.com/DigitalEdition/index.html";

    /**
     * This is the Url that is used to launch the application </br> This url will take the following parameters </br>
     * <ul>
     * <p>
     * <li>contentId</li>
     * <li>publisherID</li>
     * <li>pageIndex</li>
     * <li>App Store URL</li>
     * <li>Schema Name</li>
     * <li>Page Name</li>
     * </ul>
     * <p>
     */
    public static final String LAUNCH_APP_URL = "http://brandedapp.mirabelsdigitalstudio.com/BrandedApp/androidapp/launchV3_android.php?contentId=%s&publisherID=%s&pageIndex=%s&kAppStoreURL=%s&schemaName=%s&pageName=%s";

    /**
     * This is the Url that is used to find device type based on browser, depend on that result we will open app in corresponding app store to install in device.
     */
    public static final String FIND_TARGET_APP_STORE_URL = "http://brandedapp.mirabelsdigitalstudio.com/BrandedApp/app.php?ap=%s&go=%s&am=%s";

    /**
     * <ul>
     * <li>suid = command</li>
     * <li>user id = 0D8938F3-5E12-4D25-95B1-E67186402121</li>
     * <li>tid = contentId</li>
     * </ul>
     */
    public static final String HOTSPOT_REDIRECT_URL = "http://www.mirabelsmagazinecentral.com/Distribution/Handlers/Redirect.ashx?suid=%s&uid=%s&tid=%s";

    public static final String K_MAIL_HOTSPOT_URL = "http://mirabelsmagazinecentral.com/Distributionv3/handlers/IARedirect.ashx?suid=%s&uid=%s&tid=%s&pg=%s";

    /**
     * <b>USER_REGISTER_SERVICE</b> is used to register the user </br> This is POST Service with the request in the form of JSON
     * </br>
     * <b><i>Parameters </i></b>
     * <ul>
     * <li>s_email_icon - String </li>
     * <li>password - String</li>
     * </ul>
     * <p>
     * <b>E.g., REQUEST</b> </br>
     * {
     * "s_email_icon" : "EMAIL_ID",
     * "password" : "PASSWORD"
     * }
     * <p>
     * </br>
     * <b>RESPONSE</b> </br>
     * Response would be status code i.e., </br>
     * 200 -Successfully Registered</br>
     * 100 - Failure i.e., s_email_icon ID already exists
     */
    public static final String USER_REGISTER_SERVICE = "http://mirabelsmagazinecentral.com/MTAndroidService/AndroidService.svc/SetLoginUser";

    /**
     * <b>USER_DEVICE_ENROLLMENT</b> is used to enroll the user device </br> This is POST Service with the request in the form of JSON
     * <p>
     * deviceid | bundleId | device version | data from server(3) | first name | last name | s_email_icon | zipcode
     */
    public static final String USER_DEVICE_ENROLLMENT = "http://www.mirabelsmagazinecentral.com/Distributionv3/handlers/EnrollDevices.ashx?id=%s";

    /**
     * <b>USER_LOGIN_SERVICE</b> is used to validate the user </br> This is POST Service with the request in the form of JSON
     * </br>
     * <b><i>Parameters </i></b>
     * <ul>
     * <li>s_email_icon - String </li>
     * <li>password - String</li>
     * </ul>
     * <p>
     * <b>E.g., REQUEST</b> </br>
     * {
     * "s_email_icon" : "EMAIL_ID",
     * "password" : "PASSWORD"
     * }
     * </br>
     * <b>RESPONSE</b> </br>
     * Response would be status code i.e., </br>
     * 300 - Success </br>
     * 200 - Wrong EmailID/password combination</br>
     * 100 - Email ID/user doesn't exists
     */
    public static final String USER_LOGIN_SERVICE = "http://mirabelsmagazinecentral.com/MTAndroidService/AndroidService.svc/GetLoginStatus";

    public static final String INSERT_RECEIPTS_SERVICE = "http://mirabelsmagazinecentral.com/MTAndroidService/AndroidService.svc/SaveReceipt";

    public static final String FETCH_RECEIPTS_SERVICE = "http://mirabelsmagazinecentral.com/MTAndroidService/AndroidService.svc/GetReceiptList";

    /**
     * This is the service, which is used to get the PinIt URL Based On The Content Id. This URL is used while we share into Pinterest site in order to represent, corresponding PIN to particular client
     **/
    public static final String GET_PIN_IT_URL_BY_CONTENT_ID = "http://www.mirabelsmagazinecentral.com/MTAndroidService/AndroidService.svc/GetPinitUrlByContentId";

    public static final String GOOGLE_URL_SHORTENER_URL = "https://www.googleapis.com/urlshortener/v1/url?key=%s";

    public static final String GOOGLE_API_KEY = "AIzaSyDAl1g9QetZRWpP5AFsu6BXqpmFUK5KfhI";


    //for goldcoast
    // public static final String PINTEREST_APP_ID = "5000421149197227329";
    // public static final String LINKED_IN_API_KEY = "78srddjbs8kmqy";
    //  public static final String LINKED_IN_API_SECRET = "6AbN3yJXm7ZSn7AQ";
    // public static final String TWITTER_API_KEY = "Srz6THj6LRNti1cGLeIDTe53F";
    // public static final String TWITTER_API_SECRET = "CpDOHeYzl4rVuKOLeg1VNBxrQwCmoVO57aWJbdhhBr295YxgWL";
    // public static final String TUMBLR_API_KEY = "UCAFiwaaybqVlpv63rRbz5ARz6BtUBI1cqfUQFSDAwnilvAKUJ";
    //  public static final String TUMBLR_API_SECRET = "KbAvPfAGiMjbKajugOgfvKlCwy4LsXXNunUsSz8sEnuslk8Gxb";

    /**
     * MAGAZINE CENTRAL = 10
     * BRANDED APP = 11
     * PAID APP = 12
     */
   // public static int APP_TYPE = 11;

}
