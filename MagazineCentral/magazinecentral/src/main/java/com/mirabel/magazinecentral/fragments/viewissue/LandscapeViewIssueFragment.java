package com.mirabel.magazinecentral.fragments.viewissue;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.viewissue.Page;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.viewissue.AudioInteractivityView;
import com.mirabel.magazinecentral.customviews.viewissue.LandscapePageView;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

public class LandscapeViewIssueFragment extends Fragment {
    public String issueFolderPath = "";
    Content content;
    Context context;
    public Page leftPage, rightPage;
    ViewPagerInterface viewPagerInterface;
    LandscapePageView pageView;
    int screenWidth, screenHeight;
    private Handler handler = new Handler();

    public static Fragment newInstance(Context context, Content content, Page leftPage, Page rightPage, ViewPagerInterface viewPagerInterface) {
        LandscapeViewIssueFragment sf = new LandscapeViewIssueFragment();
        sf.context = context;
        sf.content = content;

        if (leftPage == null) {
            sf.leftPage = leftPage;
        } else {
            sf.leftPage = new Page(leftPage);
        }

        if (rightPage == null) {
            sf.rightPage = rightPage;
        } else {
            sf.rightPage = new Page(rightPage);
        }

        sf.viewPagerInterface = viewPagerInterface;
        sf.screenHeight = GlobalContent.screenWidth;
        sf.screenWidth = GlobalContent.screenHeight;
        sf.setUserVisibleHint(true);
        return sf;
    }

    Runnable triggerVideo = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            pageView.triggerVideo();
        }
    };

    Runnable hideHotspotsBackground = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            pageView.hideHotspotsBackground();
        }
    };

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (GlobalContent.orientation == Constants.OrientationType.portrait) {
            return;
        }

        if (menuVisible) { // -- visible page

            if ((leftPage != null && leftPage.activities != null && leftPage.activities.size() > 0) || (rightPage != null && rightPage.activities != null && rightPage.activities.size() > 0)) {
                handler.postDelayed(triggerVideo, 3000);
            }

            if ((leftPage != null && leftPage.hotspots.size() > 0) || (rightPage != null && rightPage.hotspots.size() > 0)) {
                handler.postDelayed(hideHotspotsBackground, 3000);
            }

        } else { // -- invisible page

            //To stop audio playing when we swipe
            if (ViewIssueActivity.mediaPlayer != null && ViewIssueActivity.mediaPlayer.isPlaying()) {
                ViewIssueActivity.mediaPlayer.stop();
                ViewIssueActivity.mediaPlayer.release();
                ViewIssueActivity.mediaPlayer = null;
                AudioInteractivityView.isPlaying = false;
            }

            //To stop video playing when we swipe
            if (pageView != null) {

                if (pageView.liv != null) {
                    if (pageView.liv.videoView != null && pageView.liv.videoView.isPlaying()) {
                        pageView.liv.videoView.pause();
                        //InteractivityView.stopPosition = pageView.liv.videoView.getCurrentPosition();
                        //InteractivityView.isPaused = true;
                    }
                }

                if (pageView.riv != null) {
                    if (pageView.riv.videoView != null && pageView.riv.videoView.isPlaying()) {
                        pageView.riv.videoView.pause();
                        //InteractivityView.stopPosition = pageView.riv.videoView.getCurrentPosition();
                        //InteractivityView.isPaused = true;
                    }
                }
            }

            if (leftPage != null || rightPage != null) {
                handler.removeCallbacks(triggerVideo);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        pageView = new LandscapePageView(context, leftPage, rightPage, content, viewPagerInterface);

        try {
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            pageView.setLayoutParams(params);
            pageView.requestDisallowInterceptTouchEvent(true);
            pageView.displayImage();
            return pageView;
        } catch (Exception e) {
            e.printStackTrace();
            return pageView;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        pageView.hideInteractivities();

        //To stop video playing when we swipe
        if (pageView != null) {

            if (pageView.liv != null) {
                if (pageView.liv.videoView != null && pageView.liv.videoView.isPlaying()) {
                    pageView.liv.videoView.pause();
                    //InteractivityView.stopPosition = pageView.liv.videoView.getCurrentPosition();
                    //InteractivityView.isPaused = true;
                }
            }

            if (pageView.riv != null) {
                if (pageView.riv.videoView != null && pageView.riv.videoView.isPlaying()) {
                    pageView.riv.videoView.pause();
                    //InteractivityView.stopPosition = pageView.riv.videoView.getCurrentPosition();
                    //InteractivityView.isPaused = true;
                }
            }
        }

        if (leftPage != null || rightPage != null) {
            handler.removeCallbacks(triggerVideo);
        }
    }
}
