package com.mirabel.magazinecentral.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.adapters.CategoryIssuesGridViewAdapter;
import com.mirabel.magazinecentral.adapters.SearchPredictionsAdapter;
import com.mirabel.magazinecentral.adapters.SpinnerAdapter;
import com.mirabel.magazinecentral.beans.CatalogIssues;
import com.mirabel.magazinecentral.beans.Content;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.beans.Publisher;
import com.mirabel.magazinecentral.beans.SearchCriteria;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.controller.AppController;
import com.mirabel.magazinecentral.customviews.DelayAutoCompleteTextView;
import com.mirabel.magazinecentral.customviews.MCAlertDialog;
import com.mirabel.magazinecentral.customviews.MCProgressDialog;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;
import com.mirabel.magazinecentral.models.CatalogDownloaderModel;
import com.mirabel.magazinecentral.services.DownloadService;
import com.mirabel.magazinecentral.util.NetworkConnectionDetector;
import com.mirabel.magazinecentral.util.Utility;

import java.util.ArrayList;
import java.util.Arrays;

public class SearchActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AlertDialogSelectionListener {
    public static final String TAG = SearchActivity.class.getSimpleName();

    private Context applicationContext, activityContext;
    private DelayAutoCompleteTextView search_auto_complete_text_view;
    private Spinner publishers_spinner, years_spinner, months_spinner;
    private MCTextView search_results_count;
    private GridView search_result_issues_grid_view;

    private CatalogDownloaderModel catalogDownloaderModel;
    private GlobalContent globalContent;
    private Utility utilityInstance;
    private ArrayList<Publisher> publisherList = new ArrayList<>();
    private ArrayList<String> publishersNamesList, yearsList, monthsList;
    private SearchPredictionsAdapter searchPredictionsAdapter;
    private SpinnerAdapter publishersAdapter, yearsAdapter, monthsAdapter;
    private SearchCriteria searchCriteria;
    private int currentPageNumber, totalNoOfPages, totalNoOfRecords, pageSize;
    private boolean isLoading = false, isInitialRequest = false, isAllRecordsLoaded = false, isAutoCompleteTextViewReset = false, isSpinnersReset = false, canWeSendRequest = false, isDeviceOrientationChanged = false;
    private CategoryIssuesGridViewAdapter gridViewAdapter;
    public static String searchKeyword;

    public static final int SHOW_SELECT_INDEX = 0;
    public static final int SHOW_ALL_INDEX = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // In order to hide keyboard when this screen appears because of AutoCompleteTextView.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        try {
            applicationContext = getApplicationContext();
            activityContext = SearchActivity.this;

            search_auto_complete_text_view = (DelayAutoCompleteTextView) findViewById(R.id.search_auto_complete_text_view);
            publishers_spinner = (Spinner) findViewById(R.id.publishers_spinner);
            years_spinner = (Spinner) findViewById(R.id.years_spinner);
            months_spinner = (Spinner) findViewById(R.id.months_spinner);
            search_results_count = (MCTextView) findViewById(R.id.search_results_count);
            search_result_issues_grid_view = (GridView) findViewById(R.id.search_result_issues_grid_view);

            catalogDownloaderModel = new CatalogDownloaderModel(activityContext, this);
            globalContent = GlobalContent.getInstance();
            utilityInstance = Utility.getInstance();
            searchCriteria = new SearchCriteria();

            yearsList = utilityInstance.getFilterYears();
            monthsList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.months_list)));
            publisherList = globalContent.getPublisherList();
            publishersNamesList = globalContent.getPublishersNamesList();

            if (publisherList.size() <= 0) {
                if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
                    if (!MCProgressDialog.isProgressDialogShown)
                        MCProgressDialog.showProgressDialog(activityContext, Constants.LOADING_MESSAGE);

                    catalogDownloaderModel.getPublishersList();
                } else {
                    NetworkConnectionDetector.displayNoNetworkError(activityContext);
                }
            }

            // Adding Select Publisher Object to Publishers List in order to show initially i.e. before getting Publishers List from Service.
            Publisher selectPublisher = new Publisher("0", "Select Publisher");
            publisherList.add(selectPublisher);
            globalContent.setPublisherList(publisherList);

            searchPredictionsAdapter = new SearchPredictionsAdapter(activityContext);
            search_auto_complete_text_view.setThreshold(1); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            search_auto_complete_text_view.setAdapter(searchPredictionsAdapter);
            search_auto_complete_text_view.setLoadingIndicator((android.widget.ProgressBar) findViewById(R.id.search_pb_loading_indicator));

            search_auto_complete_text_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    String selectedProductName = adapterView.getItemAtPosition(position).toString();
                    search_auto_complete_text_view.setAdapter(null); // to stop filtering after selecting row from drop down
                    search_auto_complete_text_view.setText(selectedProductName);
                    search_auto_complete_text_view.setAdapter(searchPredictionsAdapter);

                    searchCriteria.setSearchKeyword(SearchActivity.searchKeyword);
                    searchCriteria.setSelectedProductName(selectedProductName);

                    if (!isSpinnersReset)
                        resetSpinners(SHOW_SELECT_INDEX);

                    hideKeyboard();
                    resetValues();
                    getIssuesForSelectedValues(currentPageNumber);
                }
            });

            publishersAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, publishersNamesList);
            publishersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            publishersAdapter.setObjects(globalContent.getPublishersNamesList()); // showing Select Publisher initially in select publisher spinner.
            publishers_spinner.setAdapter(publishersAdapter);
            publishers_spinner.setSelection(SHOW_SELECT_INDEX, false); // here animate is false in order to prevent execution of onItemSelected() on initial loading.
            //setSpinnerSelectionWithoutCallingListener(publishers_spinner, 0);
            publishers_spinner.setOnItemSelectedListener(this);

            yearsAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, yearsList);
            yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            years_spinner.setAdapter(yearsAdapter);
            years_spinner.setSelection(SHOW_SELECT_INDEX, false);
            years_spinner.setOnItemSelectedListener(this);

            /*monthsAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, monthsList);
            monthsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            months_spinner.setAdapter(monthsAdapter);*/
            months_spinner.setSelection(SHOW_SELECT_INDEX, false);
            months_spinner.setOnItemSelectedListener(this);

            isLoading = true; // in order to restrict initial loading due to grid view scroll listener.

            if (Utility.isLargeScreen(activityContext) || Utility.isTabletDevice(activityContext))
                pageSize = Constants.PAGE_SIZE_FOR_TABLETS;
            else
                pageSize = Constants.PAGE_SIZE;

            // Binding adapter to grid view first time.
            gridViewAdapter = new CategoryIssuesGridViewAdapter(activityContext, this, Constants.RequestFrom.SEARCH_PAGE);
            search_result_issues_grid_view.setAdapter(gridViewAdapter);
            search_result_issues_grid_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int visibleThreshold = 2;

                    if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                        currentPageNumber++;
                        getIssuesForSelectedValues(currentPageNumber);
                    }
                }
            });

            if (savedInstanceState != null) {
                searchCriteria = (SearchCriteria) savedInstanceState.get("SearchCriteria");
                isDeviceOrientationChanged = true;

                if (searchCriteria != null) {
                    if (searchCriteria.isSearchPerformed()) {
                        ArrayList<Content> previouslyLoadedIssues = globalContent.getSearchResultIssuesList();
                        currentPageNumber = savedInstanceState.getInt("currentPageNumber");
                        totalNoOfPages = savedInstanceState.getInt("totalNoOfPages");
                        totalNoOfRecords = savedInstanceState.getInt("totalNoOfRecords");
                        isAllRecordsLoaded = savedInstanceState.getBoolean("isAllRecordsLoaded");

                        if (!searchCriteria.getSelectedProductName().isEmpty()) {
                            search_auto_complete_text_view.setAdapter(null); // to stop filtering after populating auto complete text view with previously selected value.
                            search_auto_complete_text_view.post(new Runnable() {
                                @Override
                                public void run() {
                                    search_auto_complete_text_view.setText(searchCriteria.getSelectedProductName());
                                    search_auto_complete_text_view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            search_auto_complete_text_view.setAdapter(searchPredictionsAdapter);
                                        }
                                    });
                                }
                            });

                            if (!isSpinnersReset)
                                resetSpinners(SHOW_SELECT_INDEX);

                            hideKeyboard();

                            if (previouslyLoadedIssues.isEmpty() && !isAllRecordsLoaded) {
                                resetValues();
                                getIssuesForSelectedValues(currentPageNumber);
                            } else {
                                updatePreviouslyLoadedIssuesList();
                            }
                        } else {
                            int selectedPublisherIndex = publishersNamesList.indexOf(searchCriteria.getSelectedPublisherName());
                            int selectedYearIndex = yearsList.indexOf(searchCriteria.getYear());
                            int selectedMonthIndex = monthsList.indexOf(searchCriteria.getMonthName());

                            setSpinnerSelectionWithoutCallingListener(publishers_spinner, selectedPublisherIndex);
                            setSpinnerSelectionWithoutCallingListener(years_spinner, selectedYearIndex);
                            setSpinnerSelectionWithoutCallingListener(months_spinner, selectedMonthIndex);

                            // Checking weather user have selected any publisher, year or month before device orientation change.
                            if (selectedPublisherIndex == 0 && selectedYearIndex == 0 && selectedMonthIndex == 0)
                                canWeSendRequest = false;
                            else
                                canWeSendRequest = true;

                            if (canWeSendRequest) {
                                if (previouslyLoadedIssues.isEmpty() && !isAllRecordsLoaded) {
                                    canWeSendRequest = false;
                                    resetValues();
                                    getIssuesForSelectedValues(currentPageNumber);
                                } else {
                                    updatePreviouslyLoadedIssuesList();
                                }
                            }
                        }
                    } else {
                        isDeviceOrientationChanged = false;
                    }
                }
            } else {
                resetValues();
            }

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_DOWNLOAD_FINISH));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_PAUSE_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_RESUME_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_CANCEL_DOWNLOAD));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(downloadManagerBroadcastReceiver, new IntentFilter(Constants.INTENT_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(downloadManagerBroadcastReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        try {
            outState.putSerializable("SearchCriteria", searchCriteria);
            outState.putInt("currentPageNumber", currentPageNumber);
            outState.putInt("totalNoOfPages", totalNoOfPages);
            outState.putInt("totalNoOfRecords", totalNoOfRecords);
            outState.putBoolean("isAllRecordsLoaded", isAllRecordsLoaded);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        if (MCAlertDialog.isAlertDialogShown && MCAlertDialog.alertDialog != null) {
            MCAlertDialog.alertDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void goToHomePage(View view) {
        onBackPressed();
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(search_auto_complete_text_view.getWindowToken(), 0);
    }

    public void onVolleyErrorResponse(VolleyError error) {
        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();

        MCAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.error_title), getResources().getString(R.string.error_failure));
    }

    // if you want to show previous search results we can use this method.
    public void updatePreviouslySelectedValues() {

    }

    public void updatePublishersList(ArrayList<Publisher> publisherArrayList, boolean needToCache) {
        this.publisherList = publisherArrayList;

        if (needToCache)
            globalContent.setPublisherList(this.publisherList);

        publishersNamesList = globalContent.getPublishersNamesList();
        publishersAdapter.setObjects(publishersNamesList);
        publishersAdapter.notifyDataSetChanged();

        if (MCProgressDialog.isProgressDialogShown)
            MCProgressDialog.hideProgressDialog();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        try {
            int spinnersTag = Integer.parseInt(adapterView.getTag().toString());

            if (!isDeviceOrientationChanged) {

                if (!isAutoCompleteTextViewReset)
                    resetAutoCompleteTextView();

                if (spinnersTag == 2) {
                    if (position > 0) {
                        Publisher selectedPublisher = publisherList.get(position);

                        searchCriteria.setSelectedPublisherId(selectedPublisher.getPublisherId());
                        searchCriteria.setSelectedPublisherName(selectedPublisher.getPublisherName());
                        searchCriteria.setYear("All");
                        searchCriteria.setSelectedYear(-1);
                        searchCriteria.setMonthName("All");
                        searchCriteria.setSelectedMonth(-1);

                        setSpinnerSelectionWithoutCallingListener(years_spinner, SHOW_ALL_INDEX);
                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_ALL_INDEX);

                        canWeSendRequest = true;
                    } else {
                        searchCriteria.setSelectedPublisherId("-1");
                        searchCriteria.setSelectedPublisherName("Select Publisher");

                        setSpinnerSelectionWithoutCallingListener(years_spinner, SHOW_SELECT_INDEX);
                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_SELECT_INDEX);

                        resetValues();
                    }
                } else if (spinnersTag == 3) {
                    if (position > 0) {
                        String selectedYear = yearsList.get(position);
                        searchCriteria.setYear(selectedYear);

                        if (position == 1) {
                            searchCriteria.setSelectedYear(-1);
                        } else {
                            searchCriteria.setSelectedYear(Integer.parseInt(selectedYear));
                        }

                        searchCriteria.setMonthName("All");
                        searchCriteria.setSelectedMonth(-1);

                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_ALL_INDEX);

                        canWeSendRequest = true;
                    } else {
                        searchCriteria.setYear("Select Year");
                        searchCriteria.setSelectedYear(-1);

                        setSpinnerSelectionWithoutCallingListener(months_spinner, SHOW_SELECT_INDEX);

                        if (searchCriteria.getSelectedPublisherName().equals("Select Publisher") && searchCriteria.getMonthName().equals("Select Month"))
                            resetValues();
                        else
                            canWeSendRequest = true;
                    }
                } else if (spinnersTag == 4) {
                    if (position > 0) {
                        if (!isDeviceOrientationChanged) {
                            String selectedMonth = monthsList.get(position);
                            searchCriteria.setMonthName(selectedMonth);

                            if (position == 1) {
                                searchCriteria.setSelectedMonth(-1);
                            } else {
                                searchCriteria.setSelectedMonth(position - 1); // Months names index starting from 2 so we are subtracting 1 from selected index
                            }
                        }

                        canWeSendRequest = true;
                    } else {
                        searchCriteria.setMonthName("Select Month");
                        searchCriteria.setSelectedMonth(-1);

                        if (searchCriteria.getSelectedPublisherName().equals("Select Publisher") && searchCriteria.getYear().equals("Select Year"))
                            resetValues();
                        else
                            canWeSendRequest = true;
                    }
                }

                if (canWeSendRequest) {
                    resetValues();
                    canWeSendRequest = false;
                    getIssuesForSelectedValues(currentPageNumber);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);
        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(selection);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(listener);
                    }
                });
            }
        });
    }

    public void resetSpinners(int selectedIndex) {
        isAutoCompleteTextViewReset = false;
        isSpinnersReset = true;

        setSpinnerSelectionWithoutCallingListener(publishers_spinner, selectedIndex);
        setSpinnerSelectionWithoutCallingListener(years_spinner, selectedIndex);
        setSpinnerSelectionWithoutCallingListener(months_spinner, selectedIndex);

        if (selectedIndex == 0) {
            searchCriteria.setSelectedPublisherName("Select Publisher");
            searchCriteria.setYear("Select Year");
            searchCriteria.setMonthName("Select Month");
        } else {
            searchCriteria.setSelectedPublisherName("All");
            searchCriteria.setYear("All");
            searchCriteria.setMonthName("All");
        }

        searchCriteria.setSelectedPublisherId("-1");
        searchCriteria.setSelectedYear(-1);
        searchCriteria.setSelectedMonth(-1);
    }

    public void resetAutoCompleteTextView() {
        isAutoCompleteTextViewReset = true;
        isSpinnersReset = false;

        search_auto_complete_text_view.setText("");

        searchCriteria.setSearchKeyword("");
        searchCriteria.setSelectedProductName("");
    }

    public void resetValues() {
        currentPageNumber = 1;
        totalNoOfPages = -1;
        totalNoOfRecords = 0;
        isLoading = true;// assigned to true in order to restrict initial loading due to grid view scroll listener.
        isInitialRequest = true;
        isAllRecordsLoaded = false;
        //canWeSendRequest = false;

        // clearing previous search results saved in global content
        globalContent.setSearchResultIssuesList(null);
        gridViewAdapter.notifyDataSetChanged();

        search_results_count.setVisibility(View.INVISIBLE);
    }

    public void getIssuesForSelectedValues(int pageNumber) {
        if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
            currentPageNumber = pageNumber;

            if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
                if (!MCProgressDialog.isProgressDialogShown)
                    MCProgressDialog.showProgressDialog(activityContext, Constants.LOADING_MESSAGE);

                catalogDownloaderModel.getIssuesListForSearchPage(searchCriteria, pageNumber, pageSize);
                isLoading = true;
                isDeviceOrientationChanged = false;
            }
        } else {
            NetworkConnectionDetector.displayNoNetworkError(activityContext);
        }
    }

    public void updateIssuesListForSearchPage(CatalogIssues catalogIssues) {
        try {
            // if no magazines found we are hiding grid view
            if (currentPageNumber == 1 && catalogIssues.getCatalogIssues().size() <= 0) {
                search_results_count.setVisibility(View.VISIBLE);
                search_results_count.setText("0 magazine(s) found.");
            } else {
                globalContent.setSearchResultIssuesList((ArrayList<Content>) catalogIssues.getCatalogIssues());
                gridViewAdapter.notifyDataSetChanged();

                if (currentPageNumber == 1) {
                    totalNoOfPages = catalogIssues.getPageCount();
                    totalNoOfRecords = catalogIssues.getTotalNoOfRecords();

                    search_results_count.setVisibility(View.VISIBLE);
                    search_results_count.setText(String.format("%,d magazine(s) found.", totalNoOfRecords));
                }

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
            }

            isLoading = false;
            isInitialRequest = false;
            searchCriteria.setSearchPerformed(true);
            hideKeyboard();

        } catch (Exception e) {
            e.printStackTrace();
        }

        MCProgressDialog.hideProgressDialog();
    }

    public void clearValues(View view) {
        resetAutoCompleteTextView();
        resetSpinners(SHOW_SELECT_INDEX);
        resetValues();
        isDeviceOrientationChanged = false;

        searchCriteria = new SearchCriteria();
    }

    public void updatePreviouslyLoadedIssuesList() {
        try {
            search_results_count.setText(String.format("%,d magazine(s) found.", totalNoOfRecords));
            search_results_count.setVisibility(View.VISIBLE);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;

            isLoading = false;
            isInitialRequest = false;
            searchCriteria.setSearchPerformed(true);
            isDeviceOrientationChanged = false;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayToast(String message) {
        Utility.displayToast(this, message, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startIssueDownload(GlobalContent.currentDownloadingIssue);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    MCAlertDialog.listener = this;
                    MCAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.need_storage_permission_title), getResources().getString(R.string.need_storage_permission));
                } else {
                    displayToast(getResources().getString(R.string.storage_permission_not_given));
                }
            }
        }
    }

    @Override
    public void alertDialogCallback() {
        ActivityCompat.requestPermissions(SearchActivity.this, Constants.STORAGE_PERMISSIONS, Constants.REQUEST_EXTERNAL_STORAGE);
    }

    public void startIssueDownload(Content content) {
        if (content != null) {
            content.setDownloadType(Constants.DOWNLOAD_TYPE.READ_AS_YOU_DOWNLOAD);
            content.setContentState(Constants.ContentState.ContentStateDownloading);
            Intent downloadServiceIntent = new Intent(activityContext, DownloadService.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_CONTENT, content);
            downloadServiceIntent.putExtras(bundle);
            startService(downloadServiceIntent);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver downloadManagerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                String contentId = intent.getStringExtra(Constants.BUNDLE_CONTENT_ID);

                if (intent.getAction().equals(Constants.INTENT_ACTION_REFRESH_VIEW)) {
                    gridViewAdapter.notifyDataSetChanged();
                } else {
                    if (search_result_issues_grid_view != null) {
                        for (int i = 0; i < search_result_issues_grid_view.getChildCount(); i++) {
                            View magazineView = search_result_issues_grid_view.getChildAt(i);
                            CategoryIssuesGridViewAdapter.MagazineIssueViewViewHolder viewHolder = (CategoryIssuesGridViewAdapter.MagazineIssueViewViewHolder) magazineView.getTag();

                            if (viewHolder.getContentId().equalsIgnoreCase(contentId)) {
                                ImageView downloadIcon = viewHolder.download_status_icon;
                                ProgressBar currentProgressBar = viewHolder.download_progress_bar;

                                if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_PROGRESS_RECEIVE)) {
                                    float total = intent.getFloatExtra(Constants.BUNDLE_TOTAL, 1.0f);
                                    float downloaded = intent.getFloatExtra(Constants.BUNDLE_DOWNLOADED, 0.0f);
                                    int progress = (int) ((downloaded / total) * 100);

                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    currentProgressBar.setProgress(progress);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_DOWNLOAD_FINISH)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_preview);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_PAUSE_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    currentProgressBar.setProgress(0);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_RESUME_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.VISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_downloading);
                                } else if (intent.getAction().equals(Constants.INTENT_ACTION_CANCEL_DOWNLOAD)) {
                                    currentProgressBar.setVisibility(View.INVISIBLE);
                                    downloadIcon.setImageResource(R.drawable.icon_download);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
