package com.mirabel.magazinecentral.adapters.viewissue;

import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.activities.viewissue.ViewIssueActivity;
import com.mirabel.magazinecentral.beans.GlobalContent;
import com.mirabel.magazinecentral.constants.Constants;
import com.mirabel.magazinecentral.customviews.MCTextView;
import com.mirabel.magazinecentral.interfaces.ViewPagerInterface;

import java.util.ArrayList;

public class ThumbsRecyclerViewAdapter extends RecyclerView.Adapter<ThumbsRecyclerViewAdapter.ThumbsViewHolder> {
    private ViewPagerInterface viewPagerListener;
    private String contentID = null;
    private ArrayList<String> thumbFileNames, pageNames;
    private ArrayList<String> multimediaPageIndexes;
    private String imageType = null;
    private String filePath = null;
    private View selectedThumb = null;
    public Constants.OrientationType orientation;
    private int totalSize = 0, thumbsPageCount;

    public ThumbsRecyclerViewAdapter(ViewPagerInterface listener, String contentID, ArrayList<String> thumbFileNames, ArrayList<String> pageNames, String imageType, Constants.OrientationType orientation, ArrayList<String> multimediaPageIndexes) {
        this.viewPagerListener = listener;
        this.contentID = contentID;
        this.thumbFileNames = thumbFileNames;
        this.pageNames = pageNames;
        this.imageType = imageType;
        this.multimediaPageIndexes = multimediaPageIndexes;
        this.filePath = GlobalContent.issueFolderPath + contentID;
        this.totalSize = thumbFileNames.size();

        setOrientation(orientation);
    }

    public void setOrientation(Constants.OrientationType orientation) {
        this.orientation = orientation;

        if (orientation == Constants.OrientationType.portrait) {
            thumbsPageCount = totalSize;
        } else {
            thumbsPageCount = (totalSize / 2) + 1;
        }
    }

    @Override
    public ThumbsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.landscape_base_thumb_unit, parent, false);

        ThumbsViewHolder viewHolder = new ThumbsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ThumbsViewHolder holder, int position) {
        try {
            if (orientation == Constants.OrientationType.portrait) {
                holder.leftThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position)));
                holder.rightThumbLayout.setVisibility(View.GONE);

                holder.pageName.setText(pageNames.get(position));

                if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position).replace("_t." + imageType, ""))) {
                    holder.leftPageVideoIcon.setVisibility(View.VISIBLE);
                } else {
                    holder.leftPageVideoIcon.setVisibility(View.GONE);
                }

                if (ViewIssueActivity.selectedPageIndex == position) {
                    holder.thumbLayout.setBackgroundResource(R.drawable.border_selected);
                    selectedThumb = holder.view;
                } else {
                    holder.thumbLayout.setBackgroundResource(R.drawable.border);
                }

                holder.view.setTag(position);

            } else if (orientation == Constants.OrientationType.landscape) {
                holder.rightThumbLayout.setVisibility(View.VISIBLE);

                if (totalSize % 2 == 0) { // if total no. of pages are even
                    if (position == 0) {
                        holder.leftThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position)));
                        holder.rightThumbLayout.setVisibility(View.GONE);

                        holder.pageName.setText(pageNames.get(position));

                        //To show video icon on thumbnail's when their page is having any multimedia
                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position).replace("_t." + imageType, ""))) {
                            holder.leftPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.leftPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (ViewIssueActivity.selectedPageIndex == position) {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border_selected);
                            selectedThumb = holder.view;
                        } else {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border);
                        }

                        holder.view.setTag(position);

                    } else if (position == totalSize / 2) {
                        holder.leftThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position * 2 - 1)));
                        holder.rightThumbLayout.setVisibility(View.GONE);

                        holder.pageName.setText(pageNames.get(position * 2 - 1));

                        //To show video icon on thumbnail's when their page is having any multimedia
                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position * 2 - 1).replace("_t." + imageType, ""))) {
                            holder.leftPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.leftPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (ViewIssueActivity.selectedPageIndex == (position * 2 - 1)) {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border_selected);
                            selectedThumb = holder.view;
                        } else {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border);
                        }

                        holder.view.setTag(position * 2 - 1);

                    } else {
                        holder.rightThumbLayout.setVisibility(View.VISIBLE);

                        holder.leftThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position * 2 - 1)));
                        holder.rightThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position * 2)));

                        String text = String.format("%s - %s", pageNames.get(position * 2 - 1), pageNames.get(position * 2));
                        holder.pageName.setText(text);

                        //To show video icon on thumbnail's when their page is having any multimedia
                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position * 2 - 1).replace("_t." + imageType, ""))) {
                            holder.leftPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.leftPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position * 2).replace("_t." + imageType, ""))) {
                            holder.rightPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.rightPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (ViewIssueActivity.selectedPageIndex == (position * 2 - 1) || ViewIssueActivity.selectedPageIndex == (position * 2)) {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border_selected);
                            selectedThumb = holder.view;
                        } else {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border);
                        }

                        holder.view.setTag(position * 2 - 1);
                    }
                } else { // if total no. of pages are odd
                    if (position == 0) {
                        holder.leftThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position)));
                        holder.rightThumbLayout.setVisibility(View.GONE);

                        holder.pageName.setText(pageNames.get(position));

                        //To show video icon on thumbnail's when their page is having any multimedia
                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position).replace("_t." + imageType, ""))) {
                            holder.leftPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.leftPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (ViewIssueActivity.selectedPageIndex == position) {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border_selected);
                            selectedThumb = holder.view;
                        } else {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border);
                        }

                        holder.view.setTag(position);

                    } else {
                        holder.rightThumbLayout.setVisibility(View.VISIBLE);

                        holder.leftThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position * 2 - 1)));
                        holder.rightThumbImage.setImageBitmap(BitmapFactory.decodeFile(filePath + "/" + thumbFileNames.get(position * 2)));

                        String text = String.format("%s - %s", pageNames.get(position * 2 - 1), pageNames.get(position * 2));
                        holder.pageName.setText(text);

                        //To show video icon on thumbnail's when their page is having any multimedia
                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position * 2 - 1).replace("_t." + imageType, ""))) {
                            holder.leftPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.leftPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (!multimediaPageIndexes.isEmpty() && multimediaPageIndexes.contains(thumbFileNames.get(position * 2).replace("_t." + imageType, ""))) {
                            holder.rightPageVideoIcon.setVisibility(View.VISIBLE);
                        } else {
                            holder.rightPageVideoIcon.setVisibility(View.GONE);
                        }

                        if (ViewIssueActivity.selectedPageIndex == (position * 2 - 1) || ViewIssueActivity.selectedPageIndex == (position * 2)) {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border_selected);
                            selectedThumb = holder.view;
                        } else {
                            holder.thumbLayout.setBackgroundResource(R.drawable.border);
                        }

                        holder.view.setTag(position * 2 - 1);
                    }
                }
            }

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedThumb != null) {
                        ((LinearLayout) selectedThumb).getChildAt(0).setBackgroundResource(R.drawable.border);
                    }

                    ((LinearLayout) v).getChildAt(0).setBackgroundResource(R.drawable.border_selected);
                    selectedThumb = v;

                    if (viewPagerListener != null)
                        viewPagerListener.thumbTapped(Integer.parseInt(selectedThumb.getTag().toString()));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return thumbsPageCount;
    }

    public class ThumbsViewHolder extends RecyclerView.ViewHolder {
        View view;
        RelativeLayout thumbLayout, leftThumbLayout, rightThumbLayout;
        ImageView leftThumbImage, leftPageVideoIcon, rightThumbImage, rightPageVideoIcon;
        MCTextView pageName;

        public ThumbsViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.thumbLayout = (RelativeLayout) itemView.findViewById(R.id.thumbLayout);

            this.leftThumbLayout = (RelativeLayout) itemView.findViewById(R.id.leftThumbLayout);
            this.leftThumbImage = (ImageView) itemView.findViewById(R.id.leftThumbImage);
            this.leftPageVideoIcon = (ImageView) itemView.findViewById(R.id.leftPageVideoIcon);

            this.rightThumbLayout = (RelativeLayout) itemView.findViewById(R.id.rightThumbLayout);
            this.rightThumbImage = (ImageView) itemView.findViewById(R.id.rightThumbImage);
            this.rightPageVideoIcon = (ImageView) itemView.findViewById(R.id.rightPageVideoIcon);

            this.pageName = (MCTextView) itemView.findViewById(R.id.pageName);
        }
    }
}
