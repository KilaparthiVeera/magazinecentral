package com.mirabel.magazinecentral.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import android.widget.TextView;

import com.mirabel.magazinecentral.R;
import com.mirabel.magazinecentral.interfaces.AlertDialogSelectionListener;

/**
 * Created by venkat on 22/06/16.
 */
public class MCAlertDialog {

    public static AlertDialog alertDialog = null;
    public static boolean isAlertDialogShown = false;
    public static AlertDialogSelectionListener listener = null;

    public void setListener(AlertDialogSelectionListener listener) {
        this.listener = listener;
    }

    /**
     * @param title
     * @param message
     */
    public static void showAlertDialog(Context context, String title, String message) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(context.getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            isAlertDialogShown = true;

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public static void showAlertDialogWithCancelButton(Context context, String title, String message) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            isAlertDialogShown = true;

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(context, R.color.gray));

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public static void showAlertDialogWithCallback(Context context, String title, String message) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    okCallback();
                }
            });

            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(context.getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            isAlertDialogShown = true;

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(context, R.color.gray));

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public static void okCallback() {
        if (listener != null) {
            listener.alertDialogCallback();
        }
    }

    public static void dismiss() {
        if (alertDialog != null)
            alertDialog.dismiss();
    }
}
