/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.linkedin.android.mobilesdk;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.linkedin.android.mobilesdk";
  public static final String BUILD_TYPE = "release";
}
